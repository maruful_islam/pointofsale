<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Order Management',
            'description' => 'manage order, order status, customer, sync offline data',
            'slug' => 'order',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'User Management',
            'description' => 'manage staff, permission, Staffs, daily access report',
            'slug' => 'user-management',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'System Configuration',
            'description' => 'manage category, Item, Staffs, daily access report',
            'slug' => 'system-configuration',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Reports',
            'description' => 'Checkout order reports, end of day reports',
            'slug' => 'reports',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
    }
}
