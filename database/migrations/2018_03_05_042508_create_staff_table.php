<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id');
            $table->integer('designation_id');
            $table->integer('country_id');
            $table->string('employee_id');
            $table->string('name',40);
            $table->string('email',40);
            $table->string('phone',20);
            $table->string('f_name',40)->nullable();
            $table->string('m_name',40)->nullable();
            $table->string('job_type',15);
            $table->string('city',40)->nullable();
            $table->text('address')->nullable();
            $table->string('blood_group', 10)->nullable();
            $table->string('gender', 10)->nullable();
            $table->string('d_o_b', 10)->nullable();
            $table->string('password', 100);
            $table->string('join_date', 40)->nullable();
            $table->text('image')->nullable();
            $table->text('id_card')->nullable();
            $table->text('cv')->nullable();
            $table->string('status',15)->default('inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
