<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_type_id')->nullable();
            $table->string('persone_number')->nullable();
            $table->string('table_id')->nullable();
            $table->string('method_id')->nullable();
            $table->string('sub_total',15)->nullable();
            $table->string('total',15)->nullable();
            $table->string('paid',15)->nullable();
            $table->string('discount',15)->nullable();
            $table->string('tax',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
