<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRMItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_m_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rmc_id')->unsigned();
            $table->foreign('rmc_id')->references('id')->on('r_m_categories');

            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('measurement')->nullable();
            $table->string('warning_level')->nullable();
            $table->string('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_m_items');
    }
}
