<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 17-Sep-18
 * Time: 5:22 PM
 */

namespace App\Repositories;


use App\PurchaseRequisition;
use App\Supplier;

class InventoryRepository
{
    public function requisitions()
    {
        return PurchaseRequisition::all()->sortByDesc('created_at')->pluck('requisition_id','id');
    }

    public function suppliers()
    {
        return Supplier::all()->pluck('name','id');
    }
}