<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condiment extends Model
{
    protected $fillable = ['name','note','local_name','status'];

    /**
     * A condiment is belongs to an item
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * A condiment is belongs to a category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class,'cat_id');
    }
}
