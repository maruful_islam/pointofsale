<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public function getDepartmentName($id)
    {
        return Department::where('id',$id)->value('name');
    }

    public function getDesignationTitle($id)
    {
        return Designation::where('id',$id)->value('title');
    }

    public function getCountryName($id)
    {
        return Country::where('id',$id)->value('name');
    }
}
