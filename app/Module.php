<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //

    public function getPermissionInfo($user_id, $module_id)
    {
        $result = Permission::where(['user_id' => $user_id, 'module_id' => $module_id])->value('status');

        return $result;
    }
}
