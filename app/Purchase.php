<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['total_cost','user_id'];

    public function purchased_item()
    {
        return $this->hasMany(PurchasedItem::class, 'purchase_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getRMCName($id)
    {
        return RMCategory::where('id',$id)->value('name');
    }

    public function getRMIName($id)
    {
        return RMItem::where('id',$id)->value('name');
    }

    public function getSupplierName($id)
    {
        $name = Supplier::where('id',$id)->value('name');

        if($name)
        {
            return $name;
        }
        else
        {
            return 'N/A';
        }
    }
}
