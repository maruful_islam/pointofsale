<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RMItem extends Model
{
    //
    public function category()
    {
        return $this->belongsTo(RMCategory::class, 'rmc_id', 'id');
    }

    public function item_stock()
    {
        return $this->hasOne(Stock::class, 'rmi_id', 'id');
    }

    public function purchased_item()
    {
        return $this->hasMany(PurchasedItem::class,'rmi_id', 'id');
    }

    public function removed_item()
    {
        return $this->hasMany(Consumption::class,'rmi_id', 'id');
    }

    public function requisitions()
    {
        return $this->belongsToMany(PurchaseRequisition::class,'item_requisition','item_id','requisition_id')->withPivot('qty');
    }
}
