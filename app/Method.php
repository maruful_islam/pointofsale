<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = 'payment_methods';

    public function order()
    {
        return $this->belongsToMany(Order::class);
    }
}
