<?php

use App\Setting;

function systemInfo($key){
    $setting = Setting::all()->where('key',$key)->pluck('value');
    if($setting->count() == 0){
        return '';
    }else{
        return $setting->first();
    }
}