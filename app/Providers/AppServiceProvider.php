<?php

namespace App\Providers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

//        $start = microtime(true);

        $system_name = Cache::remember('system_name', 3600, function () {
            return DB::table('settings')->where('key', 'system_name')->value('value');
        });
        $system_email = Cache::remember('system_email', 3600, function () {
            return DB::table('settings')->where('key', 'system_email')->value('value');
        });
        $address = Cache::remember('system_address', 3600, function () {
            return DB::table('settings')->where('key', 'address')->value('value');
        });
        $phone = Cache::remember('system_phone', 3600, function () {
            return DB::table('settings')->where('key', 'phone')->value('value');
        });
        $logo = Cache::remember('logo', 3600, function () {
            return DB::table('settings')->where('key', 'logo')->value('value');
        });
        $currency = Cache::remember('currency', 3600, function () {
            return DB::table('settings')->where('key', 'currency')->value('value');
        });
        $timezone = Cache::remember('timezone', 3600, function () {
            return DB::table('settings')->where('key', 'timezone')->value('value');
        });

//        $duration = (microtime(true) - $start) * 1000;

//        Log::info('Cache Speed: ' . $duration.' ms.');

        Config::set('app.name', $system_name);
        Config::set('app.email', $system_email);
        Config::set('app.address', $address);
        Config::set('app.phone', $phone);
        Config::set('app.logo', $logo);
        Config::set('app.currency', $currency);
        Config::set('app.timezone', $timezone);
        date_default_timezone_set($timezone);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
