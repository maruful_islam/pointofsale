<?php

namespace App\Http\Middleware;

use App\Attendance;
use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AttendanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = auth()->user()->id;
        $date = date('d.m.Y');

        $attendance_check = Attendance::where(['emp_id'=>$id, 'date'=>$date])->first();

        if ($attendance_check == null)
        {
            Session::flash('message', 'Welcome to system, Please Check In!');

            return redirect('/res/attendance-and-break-monitor');
        }

//        $out_time_check = DB::table('attendances')->where(['emp_id'=> $id, 'date' => $date])->value('leave_time');
//
//        if ($out_time_check != null)
//        {
//            Session::flash('message', 'You have checked out from this system, please logout!');
//
//            return redirect('/res/attendance-and-break-monitor');
//        }
//
//        $start_break_check = DB::table('break_logs')->where(['emp_id'=>$id,'date'=>$date])->orderBy('id','desc')->limit(1)->first();
//
//        if (count($start_break_check) > 0)
//        {
//            if ($start_break_check->start_break != NULL && $start_break_check->end_break == NULL)
//            {
//                Session::flash('message', 'You are on break, to access system please end your break!');
//
//                return redirect('/res/attendance-and-break-monitor');
//            }
//        }
        

        return $next($request);
    }
}
