<?php

namespace App\Http\Middleware;

use App\Module;
use App\Permission;
use Closure;

class OrderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = auth()->user()->id;

        $module_id = $order_id = Module::where('slug','order')->value('id');;
        $permission = Permission::where(['user_id'=>$id, 'module_id' => $module_id])->value('status');

        if ($permission == 0)
        {
            $notification = array(
                'message' => 'You Don\'t Have Access to this page',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }

        return $next($request);
    }
}
