<?php

namespace App\Http\Controllers;

use App\Order;
use App\ProductOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('get_products');
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::today()->addHours(6);
        //$last_week = date('Y-m-d',strtotime('-7 Days'));
        $last_week = Carbon::today()->subDays(7)->startOfDay()->addHours(6);
        //$last_month = date('Y-m-d',strtotime('-30 Days'));
        $last_month = Carbon::today()->subMonth()->startOfDay()->addHours(6);
        //$yesterday = date('Y-m-d',strtotime('-1 Days'));
        $yesterday = Carbon::yesterday()->startOfDay()->addHours(6);

        $data = array();

        // DASHBOARD STATISTICS
        $data['all_time_sales'] = Order::all()->sum('paid');
        $data['all_time_count'] = Order::all()->count();

        if(Carbon::now()->format('H') >= 6){
            $data['yesterday_sales'] = Order::query()->whereBetween('created_at',[$yesterday,Carbon::yesterday()->endOfDay()->addHours(6)])->sum('total') +
                Order::query()->whereBetween('created_at',[$yesterday,Carbon::yesterday()->endOfDay()->addHours(6)])->sum('tips') +
                Order::query()->whereBetween('created_at',[$yesterday,Carbon::yesterday()->endOfDay()->addHours(6)])->sum('delivery');
            $data['yesterday_count'] = Order::query()->whereBetween('created_at',[$yesterday,Carbon::yesterday()->endOfDay()->addHours(6)])->count();
        }else{
            $data['yesterday_sales'] = Order::query()->whereBetween('created_at',[Carbon::yesterday()->startOfDay()->addHours(6)->subDay(),$yesterday])->sum('total') +
            Order::query()->whereBetween('created_at',[Carbon::yesterday()->startOfDay()->addHours(6)->subDay(),$yesterday])->sum('tips') +
            Order::query()->whereBetween('created_at',[Carbon::yesterday()->startOfDay()->addHours(6)->subDay(),$yesterday])->sum('delivery');
            $data['yesterday_count'] = Order::query()->whereBetween('created_at',[Carbon::yesterday()->startOfDay()->addHours(6)->subDay(),$yesterday])->count();
        }

        //dd($yesterday);
        //dd(Carbon::yesterday()->endOfDay()->addHours(6));
        //dd(Order::query()->whereBetween('created_at',[$yesterday,Carbon::yesterday()->endOfDay()->addHours(6)])->sum('paid'));

        $data['last_week_sales'] = Order::query()->where('created_at', '>=', $last_week)
            ->where('created_at', '<=', Carbon::today()->endOfDay()->addHours(6))
            ->sum('total');
        $data['last_week_count'] = count(
            Order::query()->where('created_at', '>=', $last_week)
                ->where('created_at', '<=', Carbon::today()->endOfDay()->addHours(6))->get()
        );

        $data['last_month_sales'] = Order::query()->where('created_at', '>=', $last_month)
            ->where('created_at', '<=', Carbon::today()->endOfDay()->addHours(6))
            ->sum('total');
        $data['last_month_count'] = count(
            Order::query()->where('created_at', '>=', $last_month)
                ->where('created_at', '<=', Carbon::today()->endOfDay()->addHours(6))->get()
        );

        if(Carbon::now()->format('H') >= 6){
            //$data['todays_sales'] = Order::query()->whereBetween('created_at',[$today,Carbon::today()->endOfDay()->addHours(6)])->sum('total');
            $data['todays_sales'] = Order::query()->whereBetween('created_at',[$today,Carbon::today()->endOfDay()->addHours(6)])->sum('total') +
                Order::query()->whereBetween('created_at',[$today,Carbon::today()->endOfDay()->addHours(6)])->sum('tips') +
                Order::query()->whereBetween('created_at',[$today,Carbon::today()->endOfDay()->addHours(6)])->sum('delivery');
            $data['todays_count'] = Order::query()->whereBetween('created_at',[$today,Carbon::today()->endOfDay()->addHours(6)])->count();
        }else{
            $data['todays_sales'] = Order::query()->whereBetween('created_at',[$yesterday,$today])->sum('total');
//            $data['todays_sales'] = Order::query()->whereBetween('created_at',[$yesterday,$today])->sum('total') +
//                Order::query()->whereBetween('created_at',[$yesterday,$today])->sum('tips') +
//                Order::query()->whereBetween('created_at',[$yesterday,$today])->sum('delivery') -
//                Order::query()->whereBetween('created_at',[$yesterday,$today])->sum('discount');
            $data['todays_count'] = Order::query()->whereBetween('created_at',[$yesterday,$today])->count();
        }


        // LAST 10 POS SALES

        $data['latest_sales'] = Order::query()->limit(10)->latest()->get();

        // MOST 10 POPULAR ITEMS

        $data['popular_items'] = ProductOrder::query()->select('item_id', DB::raw('COUNT(item_id) as item_count'))
            ->groupBy('item_id')->orderBy('item_count', 'desc')->limit(10)->get();

        // PRE ORDERS
        $data['orders'] = Order::all()->where('pre_order','<>',null)->sortByDesc('pre_order');
        //dd($data['orders']);

        // LAST 7 DAYS SALES

        $week_sales_dates = [];
        $week_sales_total = [];

        for ($i = 1; $i <= 7; $i++)
        {
            //$date = date('Y-m-d', strtotime("-".$i." Days"));
            $date = Carbon::today()->subDays($i)->format('Y-m-d');

            $sum = Order::query()->where('created_at', '>=', $date)
                ->where('created_at', '<=', Carbon::today()->subDays($i)->endOfDay())
                ->sum('total');

            $sum_final = $sum ? number_format($sum, 3) : 0;
            $final_date = date('l', strtotime($date));

            array_push($week_sales_dates, $final_date);
            array_push($week_sales_total, $sum_final);
        }

        $data['week_sales_dates'] = $week_sales_dates;
        $data['week_sales_total'] = $week_sales_total;


        return view('pos.dashboard',$data);
    }

    public function GetWeeklyGraph()
    {
        $week_sales_dates = [];
        $week_sales_total = [];

        for ($i = 1; $i <= 7; $i++)
        {
            $date = date('Y-m-d', strtotime("-".$i." Days"));
            $sum = Order::where('created_at', '>=', $date . ' 00:00:00')
                ->where('created_at', '<=', $date . ' 23:59:59')
                ->sum('paid');

            $sum_final = $sum ? number_format($sum, 3) : 0;
            $final_date = date('l', strtotime($date));

            array_push($week_sales_dates, $final_date);
            array_push($week_sales_total, $sum_final);
        }

        $data['week_sales_dates'] = $week_sales_dates;
        $data['week_sales_total'] = $week_sales_total;

        return response()->json($data);
    }


    public function GetMonthlyGraph()
    {
        $month_sales_dates = [];
        $month_sales_total = [];

        for ($i = 0; $i < 30; $i++)
        {
            $date = date('Y-m-d', strtotime("-".$i." Days"));
            $sum = Order::where('created_at', '>=', $date . ' 00:00:00')
                ->where('created_at', '<=', $date . ' 23:59:59')
                ->sum('paid');

            $date_final = date('d-m', strtotime($date));
            $sum_final = $sum ? number_format($sum, 3) : 0;

            array_push($month_sales_dates, $date_final);
            array_push($month_sales_total, $sum_final);
        }

        $data['month_sales_dates'] = $month_sales_dates;
        $data['month_sales_total'] = $month_sales_total;

        return response()->json($data);

    }


    public function GetYearlyGraph()
    {
        $year_sales_dates = [];
        $year_sales_total = [];

        for ($i = 0; $i < 12; $i++)
        {
            $e = $i+1;
            $start_date = date('Y-m-d', strtotime("-".$i." Month"));
            $end_date = date('Y-m-d', strtotime("-".$e." Month"));

            $sum = Order::where('created_at', '>=', $end_date . ' 00:00:00')
                ->where('created_at', '<=', $start_date . ' 23:59:59')
                ->sum('paid');


            $date_final = date('F', strtotime($start_date));
            $sum_final = $sum ? number_format($sum, 3) : 0;

            array_push($year_sales_dates, $date_final);
            array_push($year_sales_total, $sum_final);
        }


        $data['year_sales_dates'] = $year_sales_dates;
        $data['year_sales_total'] = $year_sales_total;

        return response()->json($data);
    }


    public function get_products(Request $request)
    {
        $data = DB::table('items')
            ->where('p_name', 'like', '%' . $request->name . '%')
            ->limit(5)
            ->get();

//        return $data;

        $jsonData = [];

        $jsonData['items'] = [];
        $jsonData['total_count'] = 0;

        foreach ($data as $item) {
            $jsonData['items'][] = [
                'id'    => $item->id,
                'name'    => $item->p_name,
                'arabic_name'    => $item->arabic_name,
                'price'    => $item->p_price,
                'image'    => $item->p_image,
            ];
        }

//        return $jsonData;

        return response()->json($jsonData);
    }


    public function pos_report(Request $request)
    {
        $data = null;

        $order_types = DB::table('order_types')->get();

        $payment_methods = DB::table('payment_methods')->get();

        $tables = DB::table('users')->where('is_type', 'table')->get();

        View::share('order_types', $order_types);
        View::share('payment_methods', $payment_methods);
        View::share('tables', $tables);

        if ( count($request->all()) > 0 ) {

            if ($request->has('start_date') && !is_null($request->start_date)) {

                $start = Carbon::parse($request->start_date);
                $end = Carbon::parse($request->end_date);

                $data = Order::whereDate('created_at', '>=', $start)
                    ->whereDate('created_at', '<=', $end);
            }

            $data = $data->get();

            return view('reports.report', compact('data'));

        } else {
            return view('reports.report');
        }



//        return $data;



//        $interval = date_diff($datetime1, $datetime2);
//        $interval = $interval->format('%R%a days');

//        return $interval;
    }
}
