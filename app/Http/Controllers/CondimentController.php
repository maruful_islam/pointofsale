<?php

namespace App\Http\Controllers;

use App\Condiment;
use App\Item;
use App\ProductCategory;
use App\RMCategory;
use DB;
use Illuminate\Http\Request;

class CondimentController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);;
    }

    public function create()
    {
        $categories = ProductCategory::all()->sortBy('name');
        $condiments = Condiment::query()->paginate(20);
        return view('pos.product_condiments',compact('condiments','categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);

        Condiment::create($request->all());
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $condiment = Condiment::query()->findOrFail($request->get('id'));
        $condiment->update($request->all());
        return back()->with(['success'=>'Data Updated']);
    }

    public function destroy($id)
    {
        Condiment::query()->findOrFail($id)->delete();
        return redirect()->back()->with(['success'=>'Data Deleted']);
    }

    public function getCondiments()
    {
        $condiments = Condiment::all();
        $con = '';

        foreach($condiments as $condiment){
            $con .= '<option value="'.$condiment->id.'">'.$condiment->name.'</option>';
        }

        return $con;
    }

    public function condimentList()
    {
        $condiments = Condiment::all()->pluck('name','id');
        $result = '';

        foreach ($condiments as $key => $condiment){
            $result .= '<option value="'.$key.'">'.$condiment.'</option>';
        }

        return $result;
    }

    public function update_product_status($id)
    {
        //$status = DB::table('items')->where('id', $id)->value('is_active');
        $condiment = Condiment::query()->findOrFail($id);

        if ($condiment->status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $condiment->update($data);

        $notification = array(
            'message' => 'This condiment visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }
}
