<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SettingsController extends Controller
{
    public function __construct(){
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
    }
    
    public function showSettingsForm()
    {
        $data = array();

        $data[ 'system_name' ] = DB::table('settings')->where('key', 'system_name')->value('value');
        $data[ 'system_email' ] = DB::table('settings')->where('key', 'system_email')->value('value');
        $data[ 'address' ] = DB::table('settings')->where('key', 'address')->value('value');
        $data[ 'phone' ] = DB::table('settings')->where('key', 'phone')->value('value');
        $data[ 'logo' ] = DB::table('settings')->where('key', 'logo')->value('value');
        $data[ 'currency' ] = DB::table('settings')->where('key', 'currency')->value('value');
        $data[ 'timezone' ] = DB::table('settings')->where('key', 'timezone')->value('value');

        return view('settings.settings_form', $data);
    }

    function save_system_settings( Request $request )
    {
        $name = $request->name;
        DB::table('settings')->where('key', 'system_name')->update([ 'value' => $name ]);

        $email = $request->email;
        DB::table('settings')->where('key', 'system_email')->update([ 'value' => $email ]);

        $phone = $request->phone;
        DB::table('settings')->where('key', 'phone')->update([ 'value' => $phone ]);

        $address = $request->address;
        DB::table('settings')->where('key', 'address')->update([ 'value' => $address ]);

        $currency = $request->currency;
        DB::table('settings')->where('key', 'currency')->update([ 'value' => $currency ]);

        $timezone = $request->timezone;
        DB::table('settings')->where('key', 'timezone')->update([ 'value' => $timezone ]);


        $notification = array(
            'message' => 'Systems Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    function save_system_logo(Request $request)
    {
        $image = Input ::file('logo');
        $filename = 'system_logo.' . $image -> getClientOriginalExtension();

        $path = public_path('uploads/');
        $image->move($path, $filename);

        DB ::table('settings') -> where('key', 'logo') -> update(['value' => 'uploads/'.$filename]);

        $notification = array (
            'message' => 'Systems Logo Saved Successfully!',
            'alert-type' => 'success'
        );

        return back() -> with($notification);
    }
}
