<?php

namespace App\Http\Controllers;

use App\Consumption;
use App\Repositories;
use App\Item;
use App\Purchase;
use App\PurchasedItem;
use App\PurchaseRequisition;
use App\Repositories\InventoryRepository;
use App\RMCategory;
use App\RMItem;
use App\Stock;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller
{
    /**
     * @var InventoryRepository
     */
    private $repository;

    public function __construct(InventoryRepository $repository){
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
        $this->repository = $repository;
    }

    /*
     * RAW MATERIALS CATEGORY
     */
    public function R_M_Category()
    {
        $categories = RMCategory::all();

        return view('inventory.r_m_categories', compact('categories'));
    }

    public function CreateR_M_Category(Request $request)
    {
        $new = new RMCategory();
        $new->name = $request->name;
        $new->title = $request->title;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Category created Successfully');
    }


    public function UpdateR_M_Category(Request $request)
    {
        $new = RMCategory::query()->find($request->id);
        $new->name = $request->name;
        $new->title = $request->title;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Category info updated Successfully');
    }

    public function updateRMCatStatus($id)
    {
        $status = RMCategory::query()->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        RMCategory::query()->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Category info status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function DeleteR_M_Category($id)
    {
        $item_id = RMItem::query()->where('rmc_id', $id)->get();
        foreach ($item_id as $item) {
            PurchasedItem::query()->where('rmi_id', $item_id)->delete();
            Consumption::query()->where('rmi_id', $item_id)->delete();
            Stock::query()->where('rmi_id', $item_id)->delete();
            RMItem::query()->where('id', $item)->delete();
        }
        RMCategory::destroy($id);

        return back()->with('success', 'Category deleted Successfully');
    }

    /*
     * RAW MATERIALS ITEMS FUNCTIONS
     */

    public function R_M_Items()
    {
        $categories = RMCategory::query()->where('is_active', 'active')->get();

        $items = RMItem::with('category')->paginate(20);

        return view('inventory.r_m_Items', compact('items', 'categories'));
    }

    public function CreateR_M_Items(Request $request)
    {
        $new = new RMItem();
        $new->rmc_id = $request->rmc_id;
        $new->name = $request->name;
        $new->title = $request->title;
        $new->measurement = $request->measurement;
        $new->warning_level = $request->warning_level;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Item created Successfully');
    }

    public function UpdateR_M_Items(Request $request)
    {
        $new = RMItem::query()->find($request->id);
        $new->rmc_id = $request->rmc_id;
        $new->name = $request->name;
        $new->title = $request->title;
        $new->measurement = $request->measurement;
        $new->warning_level = $request->warning_level;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Item updated Successfully');
    }

    public function updateRMItemStatus($id)
    {
        $status = RMItem::query()->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        RMItem::query()->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Item info status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function DeleteR_M_Item($id)
    {
        PurchasedItem::query()->where('rmi_id', $id)->delete();
        Consumption::query()->where('rmi_id', $id)->delete();
        Stock::query()->where('rmi_id', $id)->delete();
        RMItem::destroy($id);

        return back()->with('success', 'Item deleted Successfully');
    }


    /*
     * SUPPLIERS FUNCTIONS
     */

    public function Suppliers()
    {
        $suppliers = Supplier::all();

        return view('inventory.Suppliers', compact('suppliers'));
    }

    public function CreateSupplier(Request $request)
    {
        $new = new Supplier();
        $new->name = $request->name;
        $new->phone = $request->phone;
        $new->email = $request->email;
        $new->phone = $request->phone;
        $new->address = $request->address;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Supplier Created Successfully');
    }

    public function UpdateSupplier(Request $request)
    {
        $new = Supplier::query()->find($request->id);
        $new->name = $request->name;
        $new->phone = $request->phone;
        $new->email = $request->email;
        $new->phone = $request->phone;
        $new->address = $request->address;
        $new->is_active = $request->is_active;
        $new->save();

        return back()->with('success', 'Supplier info updated Successfully');
    }

    public function updateSupplierStatus($id)
    {
        $status = Supplier::query()->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        Supplier::query()->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Suppliers info status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function DeleteSupplier($id)
    {
        Supplier::destroy($id);

        return back()->with('success', 'Suppliers Info deleted Successfully');
    }

    /*
     * PURCHASE REQUISITION FUNCTIONS
     */

    public function PurchaseRequisition()
    {
        $data = array();
        $data['categories'] = RMCategory::query()->where('is_active', 'active')->get();

        return view('inventory.purchase_requisition', $data);
    }

    public function SavePurchaseRequisition(Request $request)
    {
        $total_item = count($request->rmc_id);

        $carts = [];

        for ($i = 0; $i < $total_item; $i++) {
            $category_id = $request->rmc_id[$i];
            $item_id = $request->rmi_id[$i];
            $qty = $request->qty[$i];

            $cart = array(
                'rmc_id' => $category_id, 'rmi_id' => $item_id, 'qty' => $qty
            );

            array_push($carts, $cart);
        }

        $new = new PurchaseRequisition();
        $new->requisition_id = $request->requisition_id;
        $new->user_id = auth()->user()->id;
        $new->remarks = $request->note;
        $new->carts = json_encode($carts);
        $new->status = 0;

        $new->save();

        foreach($request->get('rmi_id') as $key => $item){
            $new->items()->attach($item,['qty'=>$request->get('qty')[$key]]);
        }

        $notification = array(
            'message' => 'Purchase Requisition has been submitted for admin approval!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function PurchaseRequisitionList()
    {
        $data = array();
        $data['requisitions'] = PurchaseRequisition::with('user')->latest()->paginate(10);

        return view('inventory.purchase_requisition_list', $data);
    }

    public function ChangeStatusPurchaseRequisition($id, $status)
    {
        if ($status == 0) {
            PurchaseRequisition::query()->where('id', $id)->update(['status' => 1]);
        } else {
            PurchaseRequisition::query()->where('id', $id)->update(['status' => 0]);
        }

        $notification = array(
            'message' => 'Purchase Requisition Status has Changed', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    /*
     * PURCHASE FUNCTIONS
     */

    public function Purchase()
    {
        $data = array();
        $data['categories'] = RMCategory::query()->where('is_active', 'active')->get();
        $data['suppliers'] = Supplier::query()->where('is_active', 'active')->get();
        $data['carts'] = [];
        $repository = $this->repository;

        return view('inventory.Purchase',$data, compact('repository'));
    }

    public function GetItemByCatID($id)
    {
        $items = RMItem::query()->where(['rmc_id' => $id, 'is_active' => 'active'])->get();

        return response()->json($items);
    }

    public function GetItemInfoByItemId($id)
    {
        $items = RMItem::query()->where('id', $id)->first();

        return response()->json($items);
    }

    public function GetUnitByItemId($id)
    {
        $items = RMItem::query()->where('id', $id)->value('measurement');

        return response()->json($items);
    }

    public function StorePurchasedInfo(Request $request)
    {
        $carts = $request->input('data');
        //dd($request->all());
        $query = DB::select(DB::Raw("SHOW TABLE STATUS LIKE 'purchases'"));
        Purchase::query()->create(['total_cost'=>$request->get('total_cost'),'user_id'=>\Auth::id()]);

        $arks = array_keys($request->all());
        $newArks = array_slice($arks,1,-1);
//        dd($newArks);
        $data = [];
        foreach($newArks as $ark){
//            if($ark != '_token'){
//            dd($request->get($ark));
            foreach($request->get($ark) as $key => $value){
//                dd(array_keys($request->get($ark)));
                if(array_key_exists($key,$request->get($ark))){
//                    dd($request->get($ark));
//                    dd(array_key_exists(2,$request->get($ark)));
                    $val = array_column($request->all(),$key);
                    $data1 =  array_combine($newArks,$val);
                    $data2 = ['purchase_id' => $query[0]->Auto_increment];
                    $data3 = ['unit' => null];
                    $data = array_merge($data1,$data2,$data3);
                    //dd($data);
                    PurchasedItem::query()->create($data);
                };
            }
            return redirect()->back();
            //dd(array_combine($newArks,$val));
            //dd(array_combine($newArks,$val));
//            }
        }
        return redirect()->back();
        //dd($data);

//        $total = 0;
//        foreach ($carts as $cart) {
//            $price = $cart['price'];
//            $total += $price;
//        }
//
//        $new = new Purchase();
//        $new->total_cost = $total;
//        $new->user_id = auth()->user()->id;
//
//        $new->save();
//
//        $purchase_id = $new->id;
//
//        foreach ($carts as $cart) {
//            $new = new PurchasedItem();
//
//            $new->purchase_id = $purchase_id;
//            $new->rmc_id = $cart['category_id'];
//            $new->rmi_id = $cart['item_id'];
//            $new->supplier_id = $cart['supplier_id'];
//            $new->quantity = $cart['qty'];
//            $new->unit = $cart['measurement'];
//            $new->price = $cart['price'];
//
//            $new->save();
//
//            $stock = Stock::where('rmi_id', $cart['item_id'])->first();
//
//            if ($stock == null) {
//                $new_stock = new Stock();
//                $new_stock->rmi_id = $cart['item_id'];
//                $new_stock->quantity = $cart['qty'];
//                $new_stock->save();
//            } else {
//                $quantity = array(
//                    'quantity' => $cart['qty'] + $stock->quantity
//                );
//                Stock::where('rmi_id', $cart['item_id'])->update($quantity);
//            }
//
//        }
//
//        return response()->json(true);
    }

    public function PurchaseHistory()
    {
        $histories = Purchase::with('purchased_item', 'user')->latest()->paginate(15);

        return view('inventory.PurchaseHistory', compact('histories'));

    }

    /*
     * CONSUMPTION AND STOCK FUNCTIONS
     */

    public function Consumption()
    {
        $stocks = Stock::with('items')->get();

        return view('inventory.Consumption', compact('stocks'));
    }

    public function StockUpdate(Request $request)
    {
        $total = $request->rmi_id;

        foreach ($total as $key => $item) {
            $item = $request->rmi_id[$key];
            $qty_used = abs($request->qty_used[$key]); //returns positive number

            if ($qty_used != 0) {
                $new = new Consumption();
                $new->rmi_id = $item;
                $new->quantity = $qty_used;
                $new->save();

                $stock = Stock::where('rmi_id', $item)->first();

                $quantity = array(
                    'quantity' => $stock->quantity - $qty_used
                );
                Stock::where('rmi_id', $item)->update($quantity);
            }

        }

        $notification = array(
            'message' => 'Stock Information Updated', 'alert-type' => 'success'
        );


        return redirect()->back()->with($notification);
    }

    public function consumptionHistory()
    {
        $histories = Consumption::with('items')->latest()->paginate(20);

        return view('inventory.consumptionHistory', compact('histories'));
    }

    public function Inventory()
    {
        $stocks = Stock::with('items')->get();

        return view('inventory.Inventory', compact('stocks'));
    }

    public function InventorySearch(Request $request)
    {
        $str = $request->string;

        $item = RMItem::has('item_stock')
            ->where('name', "LIKE", "%" . $str . "%")
            ->value('id');

        if (count($item) > 0) {
            $result = Stock::with('items')->where('rmi_id', $item)->first();

            $cat_id = ($result->items->rmc_id) ? $result->items->rmc_id : 0;

            $cat_id = ($result->items->rmc_id) ? $result->items->rmc_id : 0;

            $info = array(

                'key' => 1,
                'category' => $result->getRMCName($cat_id),
                'item' => $result->items->name,
                'unit' => $result->items->measurement,
                'warning_level' => $result->items->warning_level,
                'qty' => $result->quantity,
                'time' => date('l, dS F Y h:i A', strtotime($result->updated_at) + 3600 * 5)
            );
        } else {
            $info = false;
        }


        return response()->json($info);
    }

    /*
     * PURCHASE & CONSUMPTION REPORT
     */

    public function PurchaseReport(Request $request)
    {

        $items = RMItem::with('category')->get();

        $reports = [];

        if (count($request->all()) > 0) {
            $date1 = Carbon::parse($request->start_date)->format('Y-m-d');
            $date2 = Carbon::parse($request->end_date)->format('Y-m-d');

            $sort_type = $request->sort_type;

            if ($sort_type == 0) {
                foreach ($items as $item) {
                    $report['item_name'] = $item->name;
                    $report['category_name'] = $item->category->name;
                    $report['unit'] = $item->measurement;
                    $report['quantity'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                    $report['total_price'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('price');
                    $report['last_bought'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');


                    if ($report['quantity'] > 0) {
                        array_push($reports, $report);
                    }


                }

                $total_sum = Purchase::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('total_cost');
            } elseif ($sort_type == 1) {
                $category_id = $request->category_id;

                foreach ($items as $item) {

                    if ($item->rmc_id == $category_id) {

                        $report['item_name'] = $item->name;
                        $report['category_name'] = $item->category->name;
                        $report['unit'] = $item->measurement;
                        $report['quantity'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                        $report['total_price'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('price');
                        $report['last_bought'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');


                        if ($report['quantity'] > 0) {
                            array_push($reports, $report);
                        }

                    }

                }

                $total_sum = PurchasedItem::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->where('rmc_id', $category_id)->sum('price');

                $sort_type = 1;
                $category_display = 'inline-block';
            } elseif ($sort_type == 2) {
                $item_id = $request->item_id;

                foreach ($items as $item) {

                    if ($item->id == $item_id) {

                        $report['item_name'] = $item->name;
                        $report['category_name'] = $item->category->name;
                        $report['unit'] = $item->measurement;
                        $report['quantity'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                        $report['total_price'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('price');
                        $report['last_bought'] = PurchasedItem::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');


                        if ($report['quantity'] > 0) {
                            array_push($reports, $report);
                        }

                    }

                }

                $total_sum = PurchasedItem::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->where('rmi_id', $item_id)->sum('price');

                $sort_type = 2;
                $item_display = 'inline-block';
            }


        } else {
            foreach ($items as $item) {
                $report['item_name'] = $item->name;
                $report['category_name'] = $item->category->name;
                $report['unit'] = $item->measurement;
                $report['quantity'] = PurchasedItem::where('rmi_id', $item->id)->sum('quantity');
                $report['total_price'] = PurchasedItem::where('rmi_id', $item->id)->sum('price');
                $report['last_bought'] = PurchasedItem::where('rmi_id', $item->id)->latest()->value('updated_at');

                if ($report['quantity'] > 0) {
                    array_push($reports, $report);
                }

            }

            $total_sum = Purchase::sum('total_cost');

        }

        $categories = RMCategory::where('is_active', 'active')->get();

        return view('inventory.purchaseReport', compact('categories', 'items', 'total_sum', 'reports', 'sort_type', 'category_id', 'category_display', 'item_id', 'item_display'));
    }

    public function ConsumptionReport(Request $request)
    {
        $data = array();

        $items = RMItem::with('category')->get();

        $reports = [];

        if (count($request->all()) > 0) {
            $date1 = Carbon::parse($request->start_date)->format('Y-m-d');
            $date2 = Carbon::parse($request->end_date)->format('Y-m-d');

            $sort_type = $request->sort_type;


            if ($sort_type == 0) {
                foreach ($items as $item) {
                    $report['item_name'] = $item->name;
                    $report['category_name'] = $item->category->name;
                    $report['unit'] = $item->measurement;
                    $report['quantity'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                    $report['last_bought'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');

                    if ($report['quantity'] > 0) {
                        array_push($reports, $report);
                    }
                }
            } elseif ($sort_type == 1) {
                $category_id = $request->category_id;
                foreach ($items as $item) {
                    if ($item->category->id == $category_id) {
                        $report['item_name'] = $item->name;
                        $report['category_name'] = $item->category->name;
                        $report['unit'] = $item->measurement;
                        $report['quantity'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                        $report['last_bought'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');


                        if ($report['quantity'] > 0) {
                            array_push($reports, $report);
                        }
                    }
                }

                $sort_type = 1;
                $category_display = 'inline-block';
            } elseif ($sort_type == 2) {
                $item_id = $request->item_id;
                foreach ($items as $item) {
                    if ($item->id == $item_id) {
                        $report['item_name'] = $item->name;
                        $report['category_name'] = $item->category->name;
                        $report['unit'] = $item->measurement;
                        $report['quantity'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->sum('quantity');
                        $report['last_bought'] = Consumption::where('rmi_id', $item->id)->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->latest()->value('updated_at');


                        if ($report['quantity'] > 0) {
                            array_push($reports, $report);
                        }
                    }
                }

                $sort_type = 2;
                $item_display = 'inline-block';

            }

        } else {
            foreach ($items as $item) {
                $report['item_name'] = $item->name;
                $report['category_name'] = $item->category->name;
                $report['unit'] = $item->measurement;
                $report['quantity'] = Consumption::where('rmi_id', $item->id)->sum('quantity');
                $report['last_bought'] = Consumption::where('rmi_id', $item->id)->latest()->value('updated_at');

                if ($report['quantity'] > 0) {
                    array_push($reports, $report);
                }

            }


        }

        $categories = RMCategory::where('is_active', 'active')->get();

        return view('inventory.consumptionReport', compact('items', 'categories', 'total_sum', 'reports', 'sort_type', 'category_id', 'category_display', 'item_id', 'item_display'));
    }

    /**
     * View requisition items list in purchase order
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function requisitionCall(Request $request)
    {
        $id = $request->get('requisition');
        $requisition = PurchaseRequisition::query()->findOrFail($id);
        //dd($requisition->items());
        $carts = $requisition->items;
        $repository = $this->repository;
        return view('inventory.requisition_list_item',compact('carts','repository'));
    }

}
