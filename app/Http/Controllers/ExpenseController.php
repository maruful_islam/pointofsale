<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExpenseCategory;
use App\Purchase;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class ExpenseController extends Controller
{
    //
    public function __construct(){
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
    }

    public function AllExpenseCategory()
    {
        $data = array();
        $data[ 'expense_categories' ] = ExpenseCategory::all();

        return view('expense.expense_categories', $data);
    }

    public function SaveExpenseCategory( Request $request )
    {
        $new = new ExpenseCategory();
        $new->name = $request->name;
        $new->description = $request->description;
        $new->status = ( $request->status ) ? $request->status : 0;
        $new->save();

        $notification = array(
            'message' => 'Expense Category Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);

    }

    public function UpdateExpenseCategory( Request $request )
    {
        $new = ExpenseCategory::query()->find($request->id);
        $new->name = $request->name;
        $new->description = $request->description;
        $new->status = ( $request->status ) ? $request->status : 0;
        $new->save();

        $notification = array(
            'message' => 'Expense Category Updated Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function DeleteExpenseCategory( $id )
    {
        ExpenseCategory::destroy($id);

        return back()->with('success', 'Expense Category deleted Successfully');
    }

    public function updateExpenseCategoryStatus( $id )
    {
        $status = ExpenseCategory::query()->where('id', $id)->value('status');

        if ($status == 1) {
            $data[ 'status' ] = 0;
        }
        else {
            $data[ 'status' ] = 1;
        }

        ExpenseCategory::query()->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Category info status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function AllExpenses()
    {
        $data = array();
        $data[ 'expense_categories' ] = ExpenseCategory::query()->where('status', 1)->get();
        $data[ 'expenses' ] = Expense::with('expense_category')->paginate(15);

        return view('expense.expenses', $data);
    }

    public function SaveExpense( Request $request )
    {
        $new = new Expense();
        $new->category_id = $request->category_id;
        $new->title = $request->title;
        $new->amount = $request->amount;
        $new->remarks = $request->remarks;
        $new->date = Carbon::parse($request->date);
        $new->save();

        $notification = array(
            'message' => 'Expense Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function UpdateExpense( Request $request )
    {
        $new = Expense::query()->find($request->id);
        $new->category_id = $request->category_id;
        $new->title = $request->title;
        $new->amount = $request->amount;
        $new->remarks = $request->remarks;
        $new->date = Carbon::parse($request->date);
        $new->save();

        $notification = array(
            'message' => 'Expense Information Updated Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function DeleteExpense( $id )
    {
        Expense::destroy($id);

        return back()->with('success', 'Expense Information deleted Successfully');
    }

    public function ExpenseReport( Request $request )
    {
        $data = array();


        if ($request->has('sort_type'))
        {
            $data = $this->ExpenseReportWithFilter($request);
        }
        else {
            $data = $this->ExpenseReportWithoutFilter();
        }

        $data[ 'categories' ] = ExpenseCategory::query()->where('status', 1)->get();
        //dd($data);
        return view('expense.expense_report', $data);
    }

    public function ExpenseReportWithoutFilter()
    {

        //$date = date('Y-m-d');
        $date = Carbon::now();
        //dd($date);
        $data[ 'reports' ] = Expense::with('expense_category')->whereDate('date', $date)->orderBy('date', 'desc')->paginate(50);
        $data[ 'expense_cost' ] = Expense::query()->whereDate('date', $date)->sum('amount');
        $data[ 'purchase_cost' ] = Purchase::query()->whereDate('created_at', $date)->sum('total_cost');
        $data[ 'total_expense' ] = $data[ 'expense_cost' ] + $data[ 'purchase_cost' ];

        return $data;
    }

    public function ExpenseReportWithFilter( Request $request )
    {
        $data = array();

        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date)->addHour(23)->addMinute(59);
        $sort_type = $request->sort_type;

        if ($sort_type == 0) {
            $data[ 'reports' ] = Expense::with('expense_category')->whereBetween('date', [ $start_date, $end_date ])->orderBy('date', 'desc')->paginate(50);
            $data[ 'expense_cost' ] = Expense::query()->whereBetween('date', [ $start_date, $end_date ])->sum('amount');
            $data[ 'purchase_cost' ] = Purchase::query()->whereBetween('created_at', [ $start_date, $end_date ])->sum('total_cost');
            $data[ 'total_expense' ] = $data[ 'expense_cost' ] + $data[ 'purchase_cost' ];

            $data[ 'sort_type' ] = 0;

        }
        elseif ($sort_type == 1) {
            $category_id = $request->category_id;

            if ($category_id > 0) {
                $data[ 'reports' ] = Expense::with('expense_category')->where('category_id', $category_id)->whereBetween('date', [ $start_date, $end_date ])->orderBy('date', 'desc')->paginate(50);
                $data[ 'expense_cost' ] = Expense::query()->where('category_id', $category_id)->whereBetween('date', [ $start_date, $end_date ])->sum('amount');

            }

            else {
                $data[ 'reports' ] = Expense::with('expense_category')->whereBetween('date', [ $start_date, $end_date ])->orderBy('date', 'desc')->paginate(50);
                $data[ 'expense_cost' ] = Expense::query()->whereBetween('date', [ $start_date, $end_date ])->sum('amount');

            }

            $data[ 'purchase_cost' ] = 0;
            $data[ 'total_expense' ] = $data[ 'expense_cost' ] + $data[ 'purchase_cost' ];

            $data[ 'sort_type' ] = 1;
            $data[ 'category_display' ] = 'inline-block';
            $data[ 'category_id' ] = $category_id;
        }
        elseif ($sort_type == 2) {
            $data[ 'expense_cost' ] = 0;
            $data[ 'purchase_cost' ] = Purchase::query()->whereBetween('created_at', [ $start_date, $end_date ])->sum('total_cost');
            $data[ 'total_expense' ] = $data[ 'expense_cost' ] + $data[ 'purchase_cost' ];

            $data[ 'sort_type' ] = 2;
        }

        return $data;
    }

    public function PdfReportOfExpense(Request $request)
    {
        if (count($request->all()) > 0) {
            $data = $this->ExpenseReportWithFilter($request);

            $data['date1'] = $request->start_date;
            $data['date2'] = $request->end_date;
        }

        else {
            $data = $this->ExpenseReportWithoutFilter();
        }

        PDF::setOptions([ 'dpi' => 150, 'defaultFont' => 'sans-serif' ]);

        $pdf = PDF::loadView('pdf.expense_report', $data);

        return $pdf->setPaper('a4', 'landscape')->setWarnings(false)->download('expense_report.pdf');
    }
}
