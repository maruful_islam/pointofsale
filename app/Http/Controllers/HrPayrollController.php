<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\BreakLog;
use App\Country;
use App\Department;
use App\Designation;
use App\Staff;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HrPayrollController extends Controller
{
    public function __construct()
    {
//        date_default_timezone_set('Asia/Dhaka');
       config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
    }

    /*
     * DEPARTMENTS CRUD
     */

    public function Departments()
    {
        $data = array();
        $data['departments'] = Department::latest()->get();
        return view('hr_payroll.departments', $data);
    }


    public function StoreDepartment(Request $request)
    {
        $department = new Department();
        $department->name = $request->name;
        $department->description = $request->description;
        $department->status = $request->status;
        $department->save();

        $notification = array(
            'message' => 'Departments Information Created Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);

    }

    public function UpdateDepartment(Request $request)
    {
        $department = Department::find($request->id);
        $department->name = $request->name;
        $department->description = $request->description;
        $department->status = $request->status;
        $department->save();

        $notification = array(
            'message' => 'Departments Information Updated Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function UpdateDeptStatus($id)
    {
        $status = DB::table('departments')->where('id', $id)->value('status');

        if ($status == 'active') {
            $data['status'] = 'inactive';
        } else {
            $data['status'] = 'active';
        }

        DB::table('departments')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This department\'s visibility status has been updated!',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function DeleteDeptStatus($id)
    {
        Department::find($id)->delete();

        $notification = array(
            'message' => 'This department information deleted!',
            'alert-type' => 'error'
        );

        return back()->with($notification);
    }

    /*
     * DESIGNATIONS CRUD
     */


    public function Designations()
    {
        $data = array();
        $data['designations'] = Designation::all();
        return view('hr_payroll.designations', $data);
    }

    public function StoreDesignation(Request $request)
    {
        $designation = new Designation();
        $designation->title = $request->title;
        $designation->role = $request->role;
        $designation->status = $request->status;
        $designation->save();

        $notification = array(
            'message' => 'Designation Information Created Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);

    }

    public function UpdateDesignation(Request $request)
    {
        $designation = Designation::find($request->id);
        $designation->title = $request->title;
        $designation->role = $request->role;
        $designation->status = $request->status;
        $designation->save();

        $notification = array(
            'message' => 'Departments Information Updated Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function UpdateDesignationStatus($id)
    {
        $status = DB::table('designations')->where('id', $id)->value('status');

        if ($status == 'active') {
            $data['status'] = 'inactive';
        } else {
            $data['status'] = 'active';
        }

        DB::table('designations')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This designation\'s visibility status has been updated!',
            'alert-type' => 'success'
        );

        return back()->with($notification);

    }

    public function DeleteDesignationStatus($id)
    {
        Designation::find($id)->delete();

        $notification = array(
            'message' => 'This designation information deleted!',
            'alert-type' => 'error'
        );

        return back()->with($notification);
    }

    /*
     * STAFF CRUD
     */

    public function CreateStaff()
    {
        $data = array();
        $data['countries'] = Country::all();
        $data['departments'] = Department::where('status', 'active')->get();
        $data['designations'] = Designation::where('status', 'active')->get();
        return view('hr_payroll.add_staff', $data);
    }

    public function StoreStaff(Request $request)
    {
        $this->validate($request,
            [
                'image' => 'nullable|mimes:jpeg,png,jpg,gif,jfif|max:2048',
                'id_card' => 'nullable|mimes:jpeg,png,jpg,gif,jfif|max:2048',
                'cv' => 'nullable|mimes:doc,pdf,docx,zip|max:2048',
                'email' => 'unique:staff',
            ]
        );

        $staff = new Staff();
        $staff->name = $request->name;
        $staff->email = $request->email;
        $staff->phone = $request->phone;
        $staff->f_name = $request->f_name;
        $staff->m_name = $request->m_name;
        $staff->department_id = $request->department_id;
        $staff->designation_id = $request->designation_id;
        $staff->job_type = $request->job_type;
        $staff->country_id = $request->country_id;
        $staff->city = $request->city;
        $staff->address = $request->address;
        $staff->blood_group = $request->blood_group;
        $staff->gender = $request->gender;
        $staff->employee_id = $request->employee_id;
        $staff->d_o_b = $request->d_o_b;
        $staff->join_date = $request->join_date;
        $staff->status = $request->status;
        $staff->password = $request->password;

        /*
         * UPLOAD IMAGE, ID CARD, CV FILE
         */

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/image/');
            $path = '/uploads/staff/image/';
            $image->move($destinationPath, $name);
            $image_path = $path . $name;

            $staff->image = $image_path;
        }

        if ($request->hasFile('id_card')) {
            $id_card = $request->file('id_card');
            $name1 = time() . '.' . $id_card->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/id_card/');
            $path = '/uploads/staff/id_card/';
            $id_card->move($destinationPath, $name1);
            $id_card_path = $path . $name1;

            $staff->id_card = $id_card_path;
        }

        if ($request->hasFile('cv')) {
            $cv = $request->file('cv');
            $name2 = time() . '.' . $cv->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/cv/');
            $path = '/uploads/staff/cv/';
            $cv->move($destinationPath, $name2);
            $cv_path = $path . $name2;

            $staff->cv = $cv_path;
        }

        $staff->save();


        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->login_password);
        $user->save();

        $notification = array(
            'message' => 'Staff Information Added Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);

    }


    public function CheckStaffIdExist($id)
    {
        $check = Staff::where('employee_id', $id)->value('name');

        echo json_encode($check);
    }

    public function manageStaffs()
    {
        $data = array();
        $data['staffs'] = Staff::paginate(15);

        return view('hr_payroll.manage_staff', $data);
    }

    public function UpdateStaffStatus($id)
    {
        $status = DB::table('staff')->where('id', $id)->value('status');

        if ($status == 'active') {
            $data['status'] = 'inactive';
        } else {
            $data['status'] = 'active';
        }

        DB::table('staff')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This Staff\'s status has been updated!',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function DeleteStaff($id)
    {
        Staff::find($id)->delete();

        $notification = array(
            'message' => 'This Staff\'s Information has been deleted!',
            'alert-type' => 'error'
        );

        return back()->with($notification);

    }

    public function EditStaff($id)
    {
        $data = array();
        $data['countries'] = Country::all();
        $data['departments'] = Department::where('status', 'active')->get();
        $data['designations'] = Designation::where('status', 'active')->get();

        $data['info'] = Staff::find($id);
        return view('hr_payroll.edit_staff', $data);
    }

    public function UpdateStaff(Request $request)
    {
        $this->validate($request,
            [
                'image' => 'nullable|mimes:jpeg,png,jpg,gif,jfif|max:2048',
                'id_card' => 'nullable|mimes:jpeg,png,jpg,gif,jfif|max:2048',
                'cv' => 'nullable|mimes:doc,pdf,docx,zip|max:2048',
            ]
        );

        $staff = Staff::find($request->id);
        $staff->name = $request->name;
        $staff->email = $request->email;
        $staff->phone = $request->phone;
        $staff->f_name = $request->f_name;
        $staff->m_name = $request->m_name;
        $staff->department_id = $request->department_id;
        $staff->designation_id = $request->designation_id;
        $staff->job_type = $request->job_type;
        $staff->country_id = $request->country_id;
        $staff->city = $request->city;
        $staff->address = $request->address;
        $staff->blood_group = $request->blood_group;
        $staff->gender = $request->gender;
        $staff->employee_id = $request->employee_id;
        $staff->d_o_b = $request->d_o_b;
        $staff->join_date = $request->join_date;
        $staff->status = $request->status;
        $staff->password = $request->password;

        /*
         * UPLOAD IMAGE, ID CARD, CV FILE
         */

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/image/');
            $path = '/uploads/staff/image/';
            $image->move($destinationPath, $name);
            $image_path = $path . $name;

            $staff->image = $image_path;
        }

        if ($request->hasFile('id_card')) {
            $id_card = $request->file('id_card');
            $name1 = time() . '.' . $id_card->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/id_card/');
            $path = '/uploads/staff/id_card/';
            $id_card->move($destinationPath, $name1);
            $id_card_path = $path . $name1;

            $staff->id_card = $id_card_path;
        }

        if ($request->hasFile('cv')) {
            $cv = $request->file('cv');
            $name2 = time() . '.' . $cv->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/staff/cv/');
            $path = '/uploads/staff/cv/';
            $cv->move($destinationPath, $name2);
            $cv_path = $path . $name2;

            $staff->cv = $cv_path;
        }

        $staff->save();


        $notification = array(
            'message' => 'Staff Information Updated Successfully!',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function StaffProfile($id)
    {
        $data = array();
        $data['info'] = Staff::find($id);
        return view('hr_payroll.staff_profile', $data);
    }

    /*
     * ATTENDANCE & BREAK LOG
     */

    public function attendance_system()
    {
        return view('hr_payroll.attendance_break_monitor');
    }

    public function RecordInTime(Request $request)
    {
        $email = $request->email;
        $date = $request->date;
        $id = auth()->user()->id;

        $password = $request->password;

        $auth_check = DB::table('staff')->where(['email' => $email, 'password' => $password])->first();

        if ($auth_check != null) {

            $row_check = Attendance::where(['emp_id' => $id, 'date' => $date])->first();

            if ($row_check == null) {
                $in_record = new Attendance();
                $in_record->emp_id = $id;
                $in_record->date = $date;
                $in_record->entry_time = $request->entry_time;
                $in_record->current_status = $request->current_status;

                $result = $in_record->save();

                return response()->json($result);
            }
        }


        $returnData = array(
            'status' => 'error',
            'message' => 'Your Password Did not matched!'
        );

        return response()->json($returnData, 403);
    }

    public function RecordOutTime(Request $request)
    {
        $email = $request->email;
        $date = $request->date;
        $password = $request->password;

        $id = auth()->user()->id;

        $auth_check = DB::table('staff')->where(['email' => $email, 'password' => $password])->first();

        if ($auth_check != null) {
            $row_check = Attendance::where(['emp_id' => $id, 'date' => $date])->first();

            if ($row_check != null) {
                $data = array(
                    'leave_time' => $request->leave_time,
                    'current_status' => $request->current_status,
                );

                $result = DB::table('attendances')->where(['emp_id' => $id, 'date' => $date])->update($data);

                return response()->json($result);
            }
        }

        $returnData = array(
            'status' => 'error',
            'message' => 'Your Password Did not matched!'
        );

        return response()->json($returnData, 403);
    }

    public function RecordStartBreak(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $auth_check = DB::table('staff')->where(['email' => $email, 'password' => $password])->first();

        if ($auth_check != null) {
            $id = auth()->user()->id;

            $break = new BreakLog();
            $break->emp_id = $id;
            $break->date = $request->date;
            $break->start_break = $request->start_break;
            $result = $break->save();

            DB::table('attendances')->where('emp_id', $id)->update(['current_status' => $request->current_status]);

            return response()->json($result);
        }


        $returnData = array(
            'status' => 'error',
            'message' => 'Your Password Did not matched!'
        );

        return response()->json($returnData, 403);

    }

    public function RecordEndBreak(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $auth_check = DB::table('staff')->where(['email' => $email, 'password' => $password])->first();

        if ($auth_check != null) {
            $id = auth()->user()->id;
            $date = $request->date;

            $result = DB::table('break_logs')->where(['emp_id' => $id, 'date' => $date])
                ->orderBy('id', 'desc')->limit(1)->update(['end_break' => $request->end_break]);

            DB::table('attendances')->where('emp_id', $id)->update(['current_status' => $request->current_status]);

            return response()->json($result);
        }


        $returnData = array(
            'status' => 'error',
            'message' => 'Your Password Did not matched!'
        );

        return response()->json($returnData, 403);
    }

    public function AccessHistory(Request $request)
    {

        $data = array();

        if (count($request->all()) > 0 )
        {
            $date1 = $request->start_date;
            $date2 = $request->end_date;
            $staff_type = $request->staff_type;

            $data['date1'] = $date1;
            $data['date2'] = $date2;

            if ($staff_type == 1)
            {
                $data['histories'] = Attendance::where('date','>=' ,$date1)->where('date','<=' ,$date2)
                    ->paginate(50);
            }
            else
            {
                $emp_id = $request->emp_id;
                $data['histories'] = Attendance::where('date','>=' ,$date1)->where('date','<=' ,$date2)->where('emp_id',$emp_id)
                    ->paginate(50);

                $data['staff_type'] = 2;
                $data['display'] = 'inline-block';
                $data['emp_id'] = $emp_id;
            }

        }
        else
        {
            $data['histories'] = Attendance::latest()->paginate(50);
        }


        $data['staffs'] = User::where('is_type','.')->get();

        return view('hr_payroll.access_history', $data);
    }

    public function GetBreakLog($date, $emp_id, Request $request)
    {
        $data = array();

        if (count($request->all()) > 0 )
        {
            $emp_id = $request->emp_id;
            $date = $request->date;
        }

        $get_log = BreakLog::where(['date'=>$date, 'emp_id'=>$emp_id])->get();
        $data['break_logs'] = $get_log;

        $data['date'] = $date;
        $data['emp_id'] = $emp_id;
        $data['name'] = User::where('id',$emp_id)->value('name');

        $break_time = new BreakLog();

        $data['total_break'] = $break_time->GetTotalBreakTime($date, $emp_id);



        $data['staffs'] = User::where('is_type','.')->get();

        return view('hr_payroll.break_logs', $data);
    }


    public function UserSettings()
    {
        $info = Staff::where('email', auth()->user()->email)->first();

        return view('hr_payroll.user_settings', compact('info'));
    }

    public function UpdatePassword(Request $request)
    {
        if (!(Hash::check($request->input('current_password'), Auth::user()->password))) {
            $notification = array(
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
                'alert-type' => 'error'
            );
            // The passwords matches
            return redirect()->back()->with($notification);
        }

        if(strcmp($request->input('current_password'), $request->input('password')) == 0){
            $notification = array(
                'message' => 'New Password cannot be same as your current password. Please choose a different password.',
                'alert-type' => 'error'
            );

            //Current password and new password are same
            return redirect()->back()->with($notification);
        }


        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $notification = array(
            'message' => 'Password changed successfully !',
            'alert-type' => 'success'
        );


        return redirect()->back()->with($notification);
    }


    public function UpdateProfileInfo(Request $request)
    {
        $id = auth()->user()->id;
        $email = auth()->user()->email;
        $staff_id = DB::table('staff')->where('email',$email)->value('id');

        $user = User::find($id);
        $user->name = $request->name;
        $user->save();

        $staff = Staff::find($staff_id);
        $staff->name = $request->name;
        $staff->phone = $request->phone;
        $staff->blood_group = $request->blood_group;
        $staff->f_name = $request->f_name;
        $staff->m_name = $request->m_name;
        $staff->d_o_b = $request->d_o_b;
        $staff->city = $request->address;

        $staff->save();

        $notification = array(
            'message' => 'Profile Updated Successfully!',
            'alert-type' => 'success'
        );


        return redirect()->back()->with($notification);
    }
}
