<?php

namespace App\Http\Controllers;

use App\CashStatus;
use App\Condiment;
use App\CurrencyNote;
use App\Customer;
use App\Expense;
use App\Item;
use App\KitchenOrder;
use App\Module;
use App\Order;
use App\Permission;
use App\ProductOrder;
use App\Purchase;
use App\User;
use App\Withdraw;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Return_;

class manageController extends Controller
{
    public function __construct(){
//        config(['app.name'=>'HOT BURGER','app.currency'=>'KD','app.email'=>'','app.phone'=>'99550592']);
        config([
            'app.name'=>systemInfo('system_name'),
            'app.currency'=>systemInfo('currency'),
            'app.email'=>systemInfo('email'),
            'app.phone'=>systemInfo('phone'),
            'app.local_name'=>systemInfo('local_name')
        ]);
    }

    public function order()
    {
        $customers = DB::table('customers')->where('is_active', 'on')->get();
        $tables = DB::table('users')->where('is_type', 'table')->get();

        $categories = DB::table('product_categoy')->where('is_active', 'active')->get();

        $methods = DB::table('payment_methods')->where('is_active', 'active')->get();

        $items = DB::table('items')
            ->join('product_categoy', 'items.cat_id', '=', 'product_categoy.id')
            ->select('items.*', 'product_categoy.name')
            ->where('items.is_active', 'active')
            ->get();

        $condiments = Condiment::all();

        $order_types = DB::table('order_types')->where('is_active', 'active')->get();


        return view('pos.order', compact('categories', 'items', 'order_types', 'methods', 'tables', 'customers','condiments'));
    }

    public function tables()
    {
        $tables = DB::table('users')->where('is_type', 'table')->get();

        return view('pos.tables', compact('tables'));
    }

    public function create_table(Request $request)
    {
        $request->validate([
            'name' => 'required', 'title' => 'required',
        ]);
        DB::table('users')->insert([
            'name' => $request->name, 'email' => time() . "@gmail.com", 'password' => 12345678, 'is_type' => 'table', 'tbl_title' => $request->title,
        ]);


        return back()->withInput()->with('success', 'Data Inserted.');
    }

    public function delete_table($id)
    {
        DB::table('users')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }


    public function taxs()
    {
        $taxs = DB::table('taxs')->get();

        return view('pos.taxs', compact('taxs'));
    }


    public function create_taxt(Request $request)
    {
        $request->validate([
            'name' => 'required', 'title' => 'required', 'percent' => 'required',
        ]);


        DB::table('taxs')->insert([
            'name' => $request->name, 'title' => $request->title, 'percent' => $request->percent, 'is_active' => $request->status,
        ]);


        return back()->withInput()->with('success', 'Data Inserted.');
    }


    public function delete_tax($id)
    {
        DB::table('taxs')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }


    public function discount()
    {
        $discounts = DB::table('discounts')->get();

        return view('pos.discount', compact('discounts'));
    }

    public function create_discount(Request $request)
    {
        $request->validate([
            'name' => 'required', 'title' => 'required', 'percent' => 'required',
        ]);


        DB::table('discounts')->insert([
            'name' => $request->name, 'title' => $request->title, 'percent' => $request->percent, 'is_active' => $request->status,
        ]);

        return back()->withInput()->with('success', 'Data Inserted.');

    }


    public function delete_discount($id)
    {
        DB::table('discounts')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }

    public function order_type()
    {
        $discounts = DB::table('discounts')->where('is_active', 'active')->get();
        $taxs = DB::table('taxs')->where('is_active', 'active')->get();
        $order_types = DB::table('order_types')->get();

        return view('pos.order_type', compact('order_types', 'discounts', 'taxs'));
    }

    public function create_order_type(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
        ]);

        if ($request->discount_type) {

            $dis_type = implode(',', $request->discount_type);
        } else {
            $dis_type = '';
        }
        if ($request->tax_type) {
            $tax_types = implode(',', $request->tax_type);
        } else {
            $tax_types = '';
        }

        // dd($request->all());


        DB::table('order_types')->insert([
            'name' => $request->name, 'dis_type' => $dis_type, 'tax_type' => $tax_types, 'title' => $request->title, 'is_active' => $request->status,
        ]);

        return back()->withInput()->with('success', 'Data Inserted.');

    }

    public function delete_order_type($id)
    {
        DB::table('order_types')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }


    public function payment_methods()
    {
        $methods = DB::table('payment_methods')->get();

        return view('pos.payment_methods', compact('methods'));
    }


    public function create_methods(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);


        DB::table('payment_methods')->insert([
            'method_name' => $request->name, 'title' => $request->title, 'is_active' => $request->status,
        ]);

        return back()->withInput()->with('success', 'Data Inserted.');
    }


    public function delete_method($id)
    {
        DB::table('payment_methods')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }

    public function product_category()
    {
        $categorys = DB::table('product_categoy')->get();

        return view('pos.product_category', compact('categorys'));
    }

    public function create_product_category(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/product/categoy');
            $path = 'product/categoy/';
            $image->move($destinationPath, $name);
            $image_puth = $path . $name;
        } else {
            $image_puth = '';
        }


        DB::table('product_categoy')->insert([
            'name' => $request->name, 'title' => $request->title, 'icon' => $image_puth, 'is_active' => $request->status,
        ]);

        return back()->withInput()->with('success', 'Data Inserted.');
    }


    public function delete_product_category($id)
    {
        DB::table('product_categoy')->where('id', $id)->delete();

        return response()->json(['success' => 'Data has been Deleted'], 201);
    }

    public function pro_item(Request $request)
    {
        $data = array();

        if (count($request->all()) > 0) {
            $search = $request->item_name;

            $items = DB::table('items')->join('product_categoy', 'items.cat_id', '=', 'product_categoy.id')->select('items.*', 'product_categoy.name')->where('items.p_name', "LIKE", "%" . $search . "%")->orWhere('product_categoy.name', "LIKE", "%" . $search . "%")->paginate(15);

            $data['string'] = $search;
        } else {
            $items = DB::table('items')->join('product_categoy', 'items.cat_id', '=', 'product_categoy.id')->select('items.*', 'product_categoy.name')->paginate(15);
        }

        $categorys = DB::table('product_categoy')->where('is_active', 'active')->get();

        return view('pos.product_items', compact('items', 'categorys'));
    }

    public function create_product_item(Request $request)
    {
        $request->validate([
            'cat_id' => 'required', 'name' => 'required', 'price' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/product/item');
            $path = 'product/item/';
            $image->move($destinationPath, $name);
            $image_puth = $path . $name;
        } else {
            $image_puth = '';
        }


        DB::table('items')->insert([
            'cat_id' => $request->cat_id, 'p_name' => $request->name, 'arabic_name' => $request->arabic_name, 'p_title' => $request->title, 'p_price' => $request->price, 'p_image' => $image_puth, 'is_active' => $request->status,
        ]);

        return back()->withInput()->with('success', 'Data Inserted.');
    }


    public function delete_product_item($id)
    {
        DB::table('items')->where('id', $id)->delete();

        return back()->withInput()->with('success', 'Data Deleted.');
    }


    /*JavaScript*/

    public function order_get_support($id)
    {

        $tax = DB::table('order_types')->where('id', $id)->first();
        if (!empty($tax->tax_type)) {
            $arrTax = explode(',', $tax->tax_type);
            $taxs = [];

            foreach ($arrTax as $key => $value) {

                $tax = DB::table('taxs')->where('id', $value)->first();

                $input = [
                    'name' => $tax->name, 'amount' => $tax->percent,
                ];

                array_push($taxs, $input);
            }
        } else {
            $taxs = [];
        }


        $dis = DB::table('order_types')->where('id', $id)->first();

        if (!empty($dis->dis_type)) {
            $arrDis = explode(',', $dis->dis_type);
            $discounts = [];

            foreach ($arrDis as $key => $value) {

                $dis = DB::table('discounts')->where('id', $value)->first();

                $input2 = [
                    'name' => $dis->name, 'amount' => $dis->percent,
                ];

                array_push($discounts, $input2);
            }
        } else {
            $discounts = [];
        }


        return response()->json([
            'taxs' => $taxs, 'discounts' => $discounts,
        ], 200);


    }


    public function get_table_info($id)
    {
        $table = DB::table('users')->where('id', $id)->first();

        return response()->json([
            'table' => $table,
        ], 200);
    }


    public function loard_order_type()
    {


        $order = DB::table('orders')->orderBy('id', 'DESC')->first();
        $method = DB::table('payment_methods')->first();
        $tax = DB::table('order_types')->first();

        if (!$order) {
            $order_id = 0;
        } else {
            $order_id = $order->id;
        }


        if (!empty($tax->tax_type)) {
            $arrTax = explode(',', $tax->tax_type);
            $taxs = [];

            foreach ($arrTax as $key => $value) {

                $tax = DB::table('taxs')->where('id', $value)->first();

                $input = [
                    'name' => $tax->name, 'amount' => $tax->percent,
                ];

                array_push($taxs, $input);
            }
        } else {
            $taxs = [];
        }


        $dis = DB::table('order_types')->first();

        if (!empty($dis->dis_type)) {
            $arrDis = explode(',', $dis->dis_type);
            $discounts = [];

            foreach ($arrDis as $key => $value) {

                $dis = DB::table('discounts')->where('id', $value)->first();

                $input2 = [
                    'name' => $dis->name, 'amount' => $dis->percent,
                ];

                array_push($discounts, $input2);
            }
        } else {
            $discounts = [];
        }

        $order_type_get_id = $tax->id;
        $order_type_get_name = $tax->name;

        $method_id = $method->id;
        $method_name = $method->method_name;

        $method_title = $method->title;

        return response()->json([
            'taxs' => $taxs, 'discounts' => $discounts, 'order_type_get_id' => $order_type_get_id, 'order_type_get_name' => $order_type_get_name, 'method_id' => $method_id, 'method_name' => $method_name, 'order_id' => $order_id, 'method_title' => $method_title,
        ], 200);

    }


    /**
     * Store a newly created Customer in Database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function create_customer(Request $request)
    {
        $post = Customer::create($request->all());

        $customers = DB::table('customers')->where('is_active', 'on')->orderBy('id', 'desc')->get();

        return response()->json([
            'post' => $post, 'customers' => $customers,
        ]);
    }


    /**
     * Displays all Customers
     */

    public function customer_list(Request $request)
    {
        $data = array();

        if (count($request->all()) > 0) {
            $string = $request->string;
            $data['customers'] = Customer::latest()->where('name', "LIKE", "%" . $string . "%")->orWhere('phone', "LIKE", "%" . $string . "%")->orWhere('email', "LIKE", "%" . $string . "%")->orWhere('w_number', "LIKE", "%" . $string . "%")->paginate(20);

            $data['string'] = $string;
        } else {
            $data['customers'] = Customer::latest()->paginate(20);
        }

        return view('pos.customers', $data);
    }

    /**
     * Store a newly created Customer in Database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function update_customer_info(Request $request)
    {
        $data = array(
            'name' => $request->input('name'), 'phone' => $request->input('phone'), 'email' => $request->input('email'), 'address' => $request->input('address'), 'w_number' => $request->input('w_number'), 'is_active' => $request->input('is_active'),
        );

        $id = $request->input('id');

        $result = DB::table('customers')->where('id', $id)->update($data);

        if ($result) {
            $notification = array(
                'message' => 'Customer Information Updated Successfully!', 'alert-type' => 'success'
            );

            return back()->with($notification);


        } else {
            $notification = array(
                'message' => 'You Have Not Changed Any Information!', 'alert-type' => 'info'
            );

            return back()->with($notification);
        }

    }

    public function update_customers_status($id)
    {
        $status = DB::table('customers')->where('id', $id)->value('is_active');

        if ($status == 'on') {
            $data['is_active'] = 'off';
        } else {
            $data['is_active'] = 'on';
        }

        DB::table('customers')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This users activity status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function product_list(Request $request)
    {
        $data = array();

        if (count($request->all()) > 0) {
            $search = $request->item_name;

            $data['products'] = DB::table('items')->join('product_categoy', 'items.cat_id', '=', 'product_categoy.id')->select('items.*', 'product_categoy.name')->where('items.p_name', "LIKE", "%" . $search . "%")->orWhere('product_categoy.name', "LIKE", "%" . $search . "%")->paginate(15);

            $data['string'] = $search;
        } else {
            $data['products'] = DB::table('items')->join('product_categoy', 'items.cat_id', '=', 'product_categoy.id')->select('items.*', 'product_categoy.name')->paginate(15);
        }

        $data['categories'] = DB::table('product_categoy')->where('is_active', 'active')->get();

        return view('pos.products', $data);
    }

    public function product_info_update(Request $request)
    {
        $data = array(
            'p_name' => $request->input('p_name'), 'arabic_name' => $request->input('arabic_name'), 'cat_id' => $request->input('cat_id'), 'p_title' => $request->input('p_title'), 'p_price' => $request->input('p_price'), 'is_active' => $request->input('is_active'),
        );


        if ($request->file('image') != NULL) {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/product/item');
                $path = 'product/item/';
                $image->move($destinationPath, $name);
                $image_path = $path . $name;

                $data['p_image'] = $image_path;
            }
        }


        $id = $request->input('id');

        DB::table('items')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Items Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);


    }


    public function update_product_status($id)
    {
        $status = DB::table('items')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('items')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This product visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function category_info_update(Request $request)
    {
        $data = array(
            'name' => $request->input('name'), 'title' => $request->input('title'), 'is_active' => $request->input('status'),
        );


        if ($request->file('image') != NULL) {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/product/categoy');
                $path = 'product/categoy/';
                $image->move($destinationPath, $name);
                $image_path = $path . $name;

                $data['icon'] = $image_path;
            }
        }


        $id = $request->input('id');

        DB::table('product_categoy')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Category\'s Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);


    }


    public function update_category_status($id)
    {
        $status = DB::table('product_categoy')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('product_categoy')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This category\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function table_info_update(Request $request)
    {
        $data = array(
            'name' => $request->input('name'), 'tbl_title' => $request->input('title'),
        );

        $id = $request->input('id');

        DB::table('users')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This table\'s Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);


    }

    public function tax_info_update(Request $request)
    {
        $data = array(
            'name' => $request->input('name'), 'title' => $request->input('title'), 'percent' => $request->input('percent'), 'is_active' => $request->input('status'),
        );

        $id = $request->input('id');

        DB::table('taxs')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'Tax\'s Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function update_tax_status($id)
    {
        $status = DB::table('taxs')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('taxs')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This tax\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function discount_info_update(Request $request)
    {
        $data = array(
            'name' => $request->input('name'), 'title' => $request->input('title'), 'percent' => $request->input('percent'), 'is_active' => $request->input('status'),
        );

        $id = $request->input('id');

        DB::table('discounts')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'Discount\'s Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function update_discount_status($id)
    {
        $status = DB::table('discounts')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('discounts')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This discount\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function payment_info_update(Request $request)
    {
        $data = array(
            'method_name' => $request->input('name'), 'title' => $request->input('title'), 'is_active' => $request->input('status'),
        );

        $id = $request->input('id');

        DB::table('payment_methods')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'Payment Method\'s Information has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function update_payment_status($id)
    {
        $status = DB::table('payment_methods')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('payment_methods')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'This Payment Method\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function edit_order_type($id)
    {
        $data = array();
        $data['details'] = DB::table('order_types')->where('id', $id)->first();

        $data['discounts'] = DB::table('discounts')->where('is_active', 'active')->get();
        $data['taxs'] = DB::table('taxs')->where('is_active', 'active')->get();

        return view('pos.edit_order_type', $data);
    }


    public function update_order_type(Request $request)
    {

        if ($request->discount_type) {

            $dis_type = implode(',', $request->discount_type);
        } else {
            $dis_type = '';
        }
        if ($request->tax_type) {
            $tax_types = implode(',', $request->tax_type);
        } else {
            $tax_types = '';
        }


        $data = array(
            'name' => $request->name, 'dis_type' => $dis_type, 'tax_type' => $tax_types, 'title' => $request->title, 'is_active' => $request->status,
        );

        $id = $request->id;

        DB::table('order_types')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This order type data has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function update_order_type_status($id)
    {
        $status = DB::table('order_types')->where('id', $id)->value('is_active');

        if ($status == 'active') {
            $data['is_active'] = 'inactive';
        } else {
            $data['is_active'] = 'active';
        }

        DB::table('order_types')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Order Type\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function save_resource(Request $request)
    {
        $method_origin = $request->method_id;
        $payment_method_one = $request->split_bill_one_method_id;
        $payment_method_two = $request->split_bill_two_method_id;
        $payment_methods = [];
        if($payment_method_one){
            array_push($payment_methods,$payment_method_one);
        }
        if($payment_method_two){
            array_push($payment_methods,$payment_method_two);
        }
        if($method_origin){
            array_push($payment_methods,$method_origin);
        }

        $data = [
            'pre_order' => Carbon::parse($request->pre_order),
            'order_type_id' => $request->order_type_id,
            'persone_number' => $request->person,
            'table_id' => $request->table_id,
            'method_id' => $request->method_id,
            'one_method_id' => $request->split_bill_one_method_id,
            'two_method_id' => $request->split_bill_two_method_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'sub_total' => $request->sub_total,
            'total' => $request->total,
            'paid' => $request->paid,
            'pay_one' => $request->pay_one,
            'pay_two' => $request->pay_two,
            'discount' => $request->discount,
            'tax' => $request->taxs,
            'tips' => $request->tips,
            'delivery' => $request->delivery,
            'cmp' => $request->cmp,
            'csk_id' => $request->csk_id,
            'is_kitchen' => $request->is_kitchen,
            //'created_at' => Carbon::now()->toDateTimeString(),
            //'updated_at' => Carbon::now()->toDateTimeString(),
        ];

//        DB::table('orders')->insert([
//            'pre_order' => Carbon::parse($request->pre_order),
//            'order_type_id' => $request->order_type_id,
//            'persone_number' => $request->person,
//            'table_id' => $request->table_id,
//            'method_id' => $request->method_id,
//            'user_id' => $request->user_id,
//            'customer_id' => $request->customer_id,
//            'sub_total' => $request->sub_total,
//            'total' => $request->total,
//            'paid' => $request->paid,
//            'discount' => $request->discount,
//            'tax' => $request->taxs,
//            'tips' => $request->tips,
//            'delivery' => $request->delivery,
//            'cmp' => $request->cmp,
//            'csk_id' => $request->csk_id,
//            'is_kitchen' => $request->is_kitchen,
//            'created_at' => Carbon::now()->toDateTimeString(),
//            'updated_at' => Carbon::now()->toDateTimeString(),
//        ]);

        //dd($request->all());
        //dd($data);

        Order::query()->create($data);

        $id = DB::getPdo()->lastInsertId();
        $order = DB::table('orders')->orderBy('id', 'desc')->first();

        return response()->json($order);
    }

    public function update_resource(Request $request, $csk_id)
    {
        DB::table('product_orders')->where('csk_id', $csk_id)->delete();

        DB::table('orders')->where('csk_id', $csk_id)->update([
            'pre_order' => Carbon::parse($request->pre_order),
            'order_type_id' => $request->order_type_id,
            'persone_number' => $request->person,
            'table_id' => $request->table_id,
            'method_id' => $request->method_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'sub_total' => $request->sub_total,
            'total' => $request->total,
            'paid' => $request->paid,
            'discount' => $request->discount,
            'tax' => $request->taxs,
            'tips' => $request->tips,
            'cmp' => $request->cmp
        ]);

        $id = DB::getPdo()->lastInsertId();

        $order = DB::table('orders')->where('csk_id', $csk_id)->first();

        return response()->json($order);
    }


    public function SaveProductOrderInfo(Request $request)
    {
        $item_id = $request->item_id;
        $cat_id = DB::table('items')->where('id', $item_id)->value('cat_id');

        //$query = DB::select(DB::raw("SHOW TABLE STATUS LIKE 'orders'"));
        //$chk_id = $query[0]->Auto_increment;
        $chk_id = Order::query()->max('id');

        $product_order = new ProductOrder();
        //$product_order->csk_id = $request->csk_id;
        $product_order->csk_id = $chk_id;
        $product_order->item_id = $item_id;
        //$product_order->condiment_id = $request->condiment_id;
        $product_order->cat_id = $cat_id;
        $product_order->item_price = $request->item_price;
        $product_order->qty = $request->qty;
        $product_order->total = $request->total;
        $product_order->discount = $request->discount;

        $product_order->save();

        return response()->json(['success' => true]);

    }


    public function updateProductOrderInfo(Request $request)
    {


        $item_id = $request->item_id;
        $cat_id = DB::table('items')->where('id', $item_id)->value('cat_id');

        $product_order = new ProductOrder();
        $product_order->csk_id = $request->csk_id;
        $product_order->item_id = $item_id;
        //$product_order->condiment = $request->condiment_id;
        $product_order->cat_id = $cat_id;
        $product_order->item_price = $request->item_price;
        $product_order->qty = $request->qty;
        $product_order->total = $request->total;
        $product_order->discount = $request->discount;
        $product_order->created_at = $request->created_at;
        $product_order->updated_at = $request->updated_at;

        $product_order->save();

        return response()->json(['success' => true]);

    }


    public function last_order_data()
    {

        $order = DB::table('orders')->orderBy('id', 'DESC')->first();

        return response()->json($order);


    }


    public function report(Request $request)
    {
        if($request->has('start_date') && $request->has('end_date')){
            $date1 = Carbon::parse($request->start_date)->addHours(6); // 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
            $date2 = Carbon::parse($request->end_date)->endOfDay()->addHours(6); // 1 day and 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
        }else{
            if(Carbon::now()->format('H') >= 6){
                $date1 = Carbon::today()->addHours(6);
                $date2 = Carbon::today()->endOfDay()->addHours(6);
            }else{
                $date1 = Carbon::yesterday()->addHours(6);
                $date2 = Carbon::today()->addHours(6);
            }
        }

        $sort_type = $request->get('sort_type');

        $data = Order::query()->whereBetween('created_at', [$date1,$date2]);

        if ($sort_type == 0) {
            $data = Order::query()->whereBetween('created_at',[$date1,$date2]);
        } elseif ($sort_type == 7) {
            $data->where('cmp',1);
        } elseif ($sort_type == 1) {
            $category_id = $request->get('category_id');
            if($category_id != 0){
                $cskId = ProductOrder::query()->where('cat_id',$category_id)->pluck('csk_id');
                $data->whereIn('csk_id',$cskId);
            }
        } elseif ($sort_type == 2) {
            $item_id = $request->get('item_id');
            if($item_id != 0){
                $cskId = ProductOrder::query()->where('item_id',$item_id)->pluck('csk_id');
                $data->whereIn('csk_id',$cskId);
            }
        } else if ($sort_type == 3) {
            $order_type_id = $request->order_type_id;
            $data->where('order_type_id',$order_type_id);
        } else if ($sort_type == 4) {
            $table_id = $request->table_id;
            $data->where('table_id',$table_id);
        } else if ($sort_type == 5) {
            $method_id = $request->method_id;
            $data->where('method_id',$method_id);
        } else if ($sort_type == 6) {
            $user_id = $request->user_id;
            $data->where('user_id',$user_id);
        }

        $data = $data->get();

        $categories = DB::table('product_categoy')->where('is_active', 'active')->get();
        $items = DB::table('items')->where('is_active', 'active')->get();

        $order_types = DB::table('order_types')->where('is_active', 'active')->get();
        $method_types = DB::table('payment_methods')->where('is_active', 'active')->get();
        $tables = DB::table('users')->where('is_type', 'table')->get();
        $users = DB::table('users')->where('is_type', '.')->get();

        $total = Order::all()->sum('paid');

        return view('pos.report', compact('data','categories','items','order_types','method_types','tables','users','total'));
    }


    public function PdfReport(Request $request)
    {

        if($request->has('start_date') && $request->has('end_date')){
            $date1 = Carbon::parse($request->start_date)->addHours(6); // 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
            $date2 = Carbon::parse($request->end_date)->endOfDay()->addHours(6); // 1 day and 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
        }else{
            if(Carbon::now()->format('H') >= 6){
                $date1 = Carbon::today()->addHours(6);
                $date2 = Carbon::today()->endOfDay()->addHours(6);
            }else{
                $date1 = Carbon::yesterday()->addHours(6);
                $date2 = Carbon::today()->addHours(6);
            }
        }

        $reports['date1'] = $date1;
        $reports['date2'] = $date2;

        $sort_type = $request->get('sort_type');

        $data = Order::query()->whereBetween('created_at', [$date1,$date2]);

        if ($sort_type == 0) {
            $data = Order::query()->whereBetween('created_at',[$date1,$date2]);
        } elseif ($sort_type == 7) {
            $data->where('cmp',1);
        } elseif ($sort_type == 1) {
            $category_id = $request->get('category_id');
            if($category_id != 0){
                $cskId = ProductOrder::query()->where('cat_id',$category_id)->pluck('csk_id');
                $data->whereIn('csk_id',$cskId);
            }
        } elseif ($sort_type == 2) {
            $item_id = $request->get('item_id');
            if($item_id != 0){
                $cskId = ProductOrder::query()->where('item_id',$item_id)->pluck('csk_id');
                $data->whereIn('csk_id',$cskId);
            }
        } else if ($sort_type == 3) {
            $order_type_id = $request->order_type_id;
            $data->where('order_type_id',$order_type_id);
        } else if ($sort_type == 4) {
            $table_id = $request->table_id;
            $data->where('table_id',$table_id);
        } else if ($sort_type == 5) {
            $method_id = $request->method_id;
            $data->where('method_id',$method_id);
        } else if ($sort_type == 6) {
            $user_id = $request->user_id;
            $data->where('user_id',$user_id);
        }

        $reports['reports'] = $data->get();

//        if (count($request->all()) > 0) {
//            $reports = $this->ReportWithFilter($request);
//        } else {
//            $reports = $this->ReportWithoutFilter();
//        }

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);

        $pdf = PDF::loadView('pdf.pdf_report', $reports);

        return $pdf->setPaper('a3', 'portrait')->setWarnings(false)->download('order_report.pdf');
    }

    public function ReportWithoutFilter()
    {

        if(Carbon::now()->format('H') >= 6){
            $date1 = Carbon::today()->addHours(6);
            $date2 = Carbon::today()->endOfDay()->addHours(6);
        }else{
            $date1 = Carbon::yesterday()->addHours(6);
            $date2 = Carbon::today()->addHours(6);
        }

        $data = Order::query()->whereBetween('created_at', [$date1,$date2]);

        $data = $data->get();
        return $data;
    }

    public function ReportWithFilter(Request $request)
    {
        $credit_method_id = [3, 4, 5];

        $date1 = date('Y-m-d');
        $date2 = date('Y-m-d');

        if ($request->has('start_date') && !is_null($request->start_date)) {
            $date1 = Carbon::parse($request->start_date)->addHours(6); // 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
        }

        if ($request->has('end_date') && !is_null($request->end_date)) {
            $date2 = Carbon::parse($request->end_date)->endOfDay()->addHours(6); // 1 day and 6 hours added by smartrahat dated: 30-08-18 time: 02:37 AM
        }

        $sort_type = $request->sort_type;

        $data['date1'] = $date1;
        $data['date2'] = $date2;
        $data = Order::query()->whereBetween('created_at',[$date1,$date2]);


        if ($sort_type == 0) {
            $data = Order::query()->whereBetween('created_at',[$date1,$date2]);
        } elseif ($sort_type == 7) {
            $data->where('cmp',1);
        } elseif ($sort_type == 1) {
            $category_id = $request->category_id;

            $data['reports'] = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->get();

            $net_sales = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->sum('orders.total');

            $gross_sales = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->sum('orders.sub_total');

            $total_discount = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->sum('orders.discount');

            $cmp_discount = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->where('orders.cmp', 1)->sum('orders.discount');

            $tips = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->sum('orders.tips');

            $data['sort_type'] = 1;
            $data['category_display'] = 'inline-block';
            $data['category_id'] = $category_id;

            $credit_card = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->whereIn('method_id', $credit_method_id)->sum('orders.tips');

            $knet = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->where('method_id', 6)->sum('orders.tips');
            $cash = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->where('orders.created_at', '>=', $date1)->where('orders.created_at', '<=', $date2)->where('cat_id', $category_id)->where('method_id', 1)->sum('orders.tips');

            $data['credit_card_total'] = $credit_card;
            $data['knet_total'] = $knet;
            $data['cash_total'] = $cash;

            $carriage = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('cat_id', $category_id)->where('order_type_id', 5)->sum('paid');
            $talabat = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('cat_id', $category_id)->where('order_type_id', 6)->sum('paid');
            $data['carriage_total'] = $carriage;
            $data['talabat_total'] = $talabat;

            $data['product_sold'] = ProductOrder::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->distinct('item_id')->get();
        } elseif ($sort_type == 2) {
            $item_id = $request->item_id;

            if ($item_id == 0) {
                $data['reports'] = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->get();

                $net_sales = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->sum('total');
                $gross_sales = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->sum('sub_total');
                $total_discount = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->sum('discount');
                $cmp_discount = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('cmp', 1)->sum('discount');
                $tips = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->sum('tips');

                $credit_card = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->whereIn('method_id', $credit_method_id)->sum('total');

                $knet = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('method_id', 6)->sum('total');
                $cash = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('method_id', 1)->sum('total');

                $data['credit_card_total'] = $credit_card;
                $data['knet_total'] = $knet;
                $data['cash_total'] = $cash;

                $carriage = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('order_type_id', 5)->sum('total');
                $talabat = Order::where('created_at', '>=', $date1)->where('created_at', '<=', $date2)->where('order_type_id', 6)->sum('total');
                $data['carriage_total'] = $carriage;
                $data['talabat_total'] = $talabat;

                $data['sort_type'] = 2;
                $data['item_display'] = 'inline-block';
                $data['item_id'] = $item_id;

                $data['product_sold'] = ProductOrder::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->distinct('item_id')->get();
            } else {
                $data['reports'] = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->get();

                $net_sales = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->sum('orders.total');

                $gross_sales = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->sum('orders.sub_total');

                $total_discount = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->sum('orders.discount');

                $cmp_discount = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->where('orders.cmp', 1)->sum('orders.discount');

                $tips = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->sum('orders.tips');

                $data['sort_type'] = 2;
                $data['item_display'] = 'inline-block';
                $data['item_id'] = $item_id;

                $credit_card = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->whereIn('method_id', $credit_method_id)->sum('orders.tips');

                $knet = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->where('method_id', 6)->sum('orders.total');
                $cash = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->where('method_id', 1)->sum('orders.total');

                $data['credit_card_total'] = $credit_card;
                $data['knet_total'] = $knet;
                $data['cash_total'] = $cash;

                $carriage = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->where('order_type_id', 5)->sum('orders.paid');;
                $talabat = ProductOrder::join('orders', 'product_orders.csk_id', '=', 'orders.csk_id')->whereDate('orders.created_at', '>=', $date1)->whereDate('orders.created_at', '<=', $date2)->where('item_id', $item_id)->where('order_type_id', 6)->sum('orders.paid');;
                $data['carriage_total'] = $carriage;
                $data['talabat_total'] = $talabat;

                $data['product_sold'] = ProductOrder::query()->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->distinct('item_id')->get();
            }

        } else if ($sort_type == 3) {
            $order_type_id = $request->order_type_id;
            $data->where('order_type_id',$order_type_id);
        } else if ($sort_type == 4) {
            $table_id = $request->table_id;
            $data->where('table_id',$table_id);
        } else if ($sort_type == 5) {
            $method_id = $request->method_id;
            $data->where('method_id',$method_id);
        } else if ($sort_type == 6) {
            $user_id = $request->user_id;
            $data->where('user_id',$user_id);
        }

        $data = $data->get();
        return $data;
    }

    public function ItemReport(Request $request)
    {
        $items = Item::query()->where('is_active', 'active')->get();

        $report_by_item = [];
        $total_item_sold = 0;

        // echo $date1;exit;

        $d = array();
        foreach ($items as $item) {
            $id = $item->id;
            $item_name = $item->p_name;
            $price = ProductOrder::query()->where('item_id', $id)->value('item_price');

            //if (count($request->all()) > 0) {
            if ($request->has('start_date') && $request->has('end_date')) {
                $date1 = Carbon::parse($request->get('start_date'))->addHours(6);
                $date2 = Carbon::parse($request->get('end_date'))->endOfDay()->addHours(6);
                //$date = Carbon::parse($request->date)->format('Y-m-d');

                //$total_qty_sold = ProductOrder::query()->where('item_id', $id)->whereDate('created_at', '>=', $date . ' 00:00:00')->whereDate('created_at', '<=', $date . ' 23:59:59')->sum('qty');
                $total_qty_sold = ProductOrder::query()
                    ->where('item_id', $id)
                    ->whereBetween('created_at',[$date1,$date2])
                    ->sum('qty');

                //$total_discount_by_item = ProductOrder::query()->where('item_id', $id)->whereDate('created_at', '>=', $date . ' 00:00:00')->whereDate('created_at', '<=', $date . ' 23:59:59')->sum('discount');
                $total_discount_by_item = ProductOrder::query()
                    ->where('item_id', $id)
                    ->whereBetween('created_at',[$date1,$date2])
                    ->sum('discount');

                //$date = Carbon::parse($request->date)->format('l, dS F Y');

                //$s['searched_date'] = $request->start_date;
                //$e['searched_date'] = $request->end_date;
                $show = true;

            } else {
                //$date = date('Y-m-d');
                if(Carbon::now()->format('H') >= 6){
                    $date1 = Carbon::today()->addHours(6);
                    $date2 = Carbon::today()->endOfDay()->addHours(6);
                }else{
                    $date1 = Carbon::yesterday()->startOfDay()->addHours(6);
                    $date2 = Carbon::today()->addHours(6);
                }

                $total_qty_sold = ProductOrder::query()
                    ->where('item_id', $id)
                    //->whereDate('created_at', '>=', $date . ' 00:00:00')
                    //->whereDate('created_at', '<=', $date . ' 23:59:59')
                    ->whereBetween('created_at',[$date1,$date2])
                    ->sum('qty');
                $total_discount_by_item = ProductOrder::query()
                    ->where('item_id', $id)
                    //->whereDate('created_at', '>=', $date . ' 00:00:00')
                    //->whereDate('created_at', '<=', $date . ' 23:59:59')
                    ->whereBetween('created_at',[$date1,$date2])
                    ->sum('discount');

                //$date = date('l, dS F Y');
            }


            $earned_by_item = $total_qty_sold * $price;
            $items_net_sale = $earned_by_item - $total_discount_by_item;

            $data['item_name'] = $item_name;
            $data['price'] = $price;
            $data['sold'] = $total_qty_sold;
            $data['earned'] = number_format($earned_by_item, 3);
            $data['discount'] = $total_discount_by_item;
            $data['net_profit_by_item'] = $items_net_sale;

            $total_item_sold += $data['sold'];

            // $total_earned += $data['earned'];
            // $total_discount += $data['discount'];
            // $net_profit += $data['net_profit_by_item'];

            if ($total_qty_sold != 0) {
                array_push($report_by_item, $data);
            }


        }

        return view('pos.item_report', compact('report_by_item', 'start','end', 'data', 'date', 'show'));
    }

    public function CurrencySettings()
    {
        $data = array();
        $data['notes'] = CurrencyNote::all();

        return view('pos.currency_settings', $data);
    }

    public function StoreNote(Request $request)
    {
        $note = new CurrencyNote();
        $note->name = $request->name;
        $note->value = $request->value;
        $note->status = $request->status;
        $note->save();

        $notification = array(
            'message' => 'Currency Note Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function UpdateNote(Request $request)
    {
        $note = CurrencyNote::query()->find($request->id);
        $note->name = $request->name;
        $note->value = $request->value;
        $note->status = $request->status;
        $note->save();

        $notification = array(
            'message' => 'Currency Note Information Updated Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function update_note_status($id)
    {
        $status = DB::table('currency_notes')->where('id', $id)->value('status');

        if ($status == 'active') {
            $data['status'] = 'inactive';
        } else {
            $data['status'] = 'active';
        }

        DB::table('currency_notes')->where('id', $id)->update($data);

        $notification = array(
            'message' => 'This Coin/Note\'s visibility status has been updated!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function DeleteNote($id)
    {
        DB::table('currency_notes')->where('id', $id)->delete();

        $notification = array(
            'message' => 'This Coin/Note\'s Information deleted!', 'alert-type' => 'error'
        );

        return back()->with($notification);
    }


    public function EndOfDay(Request $request)
    {
        $items = Item::query()->where('is_active', 'active')->get();

        $report_by_item = [];
        $total_item_sold = 0;

        $d = array();
        foreach ($items as $item) {
            $id = $item->id;
            $item_name = $item->p_name;
            $price = ProductOrder::query()->where('item_id', $id)->value('item_price');

            if (count($request->all()) > 0) {
                //$date = Carbon::parse($request->date)->format('Y-m-d');
                $date = Carbon::parse($request->date); // added by smartrahat

                //$total_qty_sold = ProductOrder::query()->where('item_id', $id)->whereDate('created_at', '>=', $date . ' 00:00:00')->whereDate('created_at', '<=', $date . ' 23:59:59')->sum('qty');
                $total_qty_sold = ProductOrder::query()
                    ->where('item_id', $id)
                    ->whereBetween('created_at', [Carbon::parse($request->date)->addHours(6),Carbon::parse($request->date)->endOfDay()->addHours(6)])
                    ->sum('qty');

                $total_discount_by_item = ProductOrder::query()
                    ->where('item_id', $id)
                    //->whereBetween('created_at', [$date->addHours(6),$date->addHours(6)->addDay()])
                    ->whereBetween('created_at', [Carbon::parse($request->date)->addHours(6),Carbon::parse($request->date)->endOfDay()->addHours(6)])
                    ->sum('discount');

                $date = Carbon::parse($request->date)->format('l, dS F Y');

                $d['searched_date'] = $request->date;
                $show = true;

            } else {
                //$date = date('Y-m-d');
                $date = Carbon::today(); // added by smartrahat

                if(Carbon::now()->format('H') >= 6){
                    $total_qty_sold = ProductOrder::query()
                        ->where('item_id', $id)
                        ->whereBetween('created_at', [Carbon::today()->addHours(6),Carbon::today()->endOfDay()->addHours(6)])
                        ->sum('qty');
                    $total_discount_by_item = ProductOrder::query()
                        ->where('item_id', $id)
                        ->whereBetween('created_at', [Carbon::today()->addHours(6),Carbon::today()->endOfDay()->addHours(6)])
                        ->sum('discount');
                }else{
                    $total_qty_sold = ProductOrder::query()
                        ->where('item_id', $id)
                        ->whereBetween('created_at', [Carbon::yesterday()->startOfDay()->addHours(6),Carbon::today()->addHours(6)])
                        ->sum('qty');
                    $total_discount_by_item = ProductOrder::query()
                        ->where('item_id', $id)
                        ->whereBetween('created_at', [Carbon::yesterday()->startOfDay()->addHours(6),Carbon::today()->addHours(6)])
                        ->sum('discount');
                }

                $date = date('l, dS F Y');
            }

            $earned_by_item = $total_qty_sold * $price;
            $items_net_sale = $earned_by_item - $total_discount_by_item;

            $data['item_name'] = $item_name;
            $data['price'] = $price;
            $data['sold'] = $total_qty_sold;
            $data['earned'] = number_format($earned_by_item, 3);
            $data['discount'] = $total_discount_by_item;
            $data['net_profit_by_item'] = $items_net_sale;

            $total_item_sold += $data['sold'];

            // $total_earned += $data['earned'];
            // $total_discount += $data['discount'];
            // $net_profit += $data['net_profit_by_item'];

            if ($total_qty_sold != 0) {
                array_push($report_by_item, $data);
            }


        }

        if (count($request->all()) > 0) {
            //$date = Carbon::parse($request->date)->format('Y-m-d');
            $date = Carbon::parse($request->date); // added by smartrahat
            $date1 = Carbon::parse($request->date)->addHours(6);
            $date2 = Carbon::parse($request->date)->endOfDay()->addHours(6);

            $cash_status = CashStatus::query()->whereBetween('created_at', [$date1,$date2])->first();

            $total_withdraw = Withdraw::query()->whereBetween('created_at', [$date1,$date2])->sum('amount');

            $withdraws = Withdraw::query()->whereBetween('created_at', [$date1,$date2])->get();

            $total_earned = Order::query()->whereBetween('created_at', [$date1,$date2])->sum('sub_total');
            $total_discount = Order::query()->whereBetween('created_at', [$date1,$date2])->sum('discount');
            $net_profit = $total_earned - $total_discount;

            $credit_card = Order::query()->whereBetween('created_at', [$date1,$date2])
                ->where('method_id', 3)
                ->orWhere('method_id', 4)
                ->orWhere('method_id', 5)
                ->sum('paid');

            $knet = Order::query()->whereBetween('created_at', [$date1,$date2])->where('method_id', 6)->sum('paid');
        } else {
            //$date = date('Y-m-d');
            $date = Carbon::today(); // added by smartrahat
            if(Carbon::now()->format('H') >= 6){
                $date1 = Carbon::today()->addHours(6);
                $date2 = Carbon::today()->endOfDay()->addHours(6);
            }else{
                $date1 = Carbon::yesterday()->startOfDay()->addHours(6);
                $date2 = Carbon::today()->addHours(6);
            }

            $cash_status = CashStatus::query()->whereBetween('created_at', [$date1,$date2])->first();

            $total_withdraw = Withdraw::query()->whereBetween('created_at', [$date1,$date2])->sum('amount');

            $withdraws = Withdraw::query()->whereBetween('created_at', [$date1,$date2])->get();

            //$date1 = date('Y-m-d') . ' 00:00:00';
            //$date2 = date('Y-m-d') . ' 23:59:59';
            //$date1 = Carbon::today()->addHours(6);
            //$date2 = Carbon::today()->endOfDay()->addHours(6);

            $total_earned = Order::query()->whereBetween('created_at', [$date1,$date2])->sum('sub_total');
            $total_discount = Order::query()->whereBetween('created_at', [$date1,$date2])->sum('discount');
            $net_profit = $total_earned - $total_discount;

            $credit_card = Order::query()
                ->whereBetween('created_at', [$date1,$date2])
                ->where('method_id', 3)
                ->orWhere('method_id', 4)
                ->orWhere('method_id', 5)
                ->sum('paid');

            $knet = Order::query()
                ->whereBetween('created_at', [$date1,$date2])
                ->where('method_id', 6)
                ->sum('paid');

        }

        $start_day_cash = ($cash_status != null ? $cash_status->start_day_cash : 0);
        $end_day_cash = ($cash_status != null ? $cash_status->end_day_cash : 0);

        $start_day_notes = ($cash_status != null ? json_decode($cash_status->start_day_notes) : 0);
        $end_day_notes = ($cash_status != null ? json_decode($cash_status->end_day_notes) : 0);

        $current_cash = $start_day_cash + $net_profit - $total_withdraw;

        // dd($cash_status);

        $notes = CurrencyNote::query()->where('status', 'active')->get();

        $withdraws = (count($withdraws) > 0 ? $withdraws : NULL);

        return view('pos.EndOfDay', compact('d', 'start_day_cash', 'end_day_cash', 'current_cash', 'total_withdraw', 'date','date1', 'report_by_item', 'total_item_sold', 'total_earned', 'total_discount', 'net_profit', 'notes', 'start_day_notes', 'end_day_notes', 'withdraws', 'show', 'credit_card', 'knet'));
    }


    public function CalculateStartDayCash(Request $request)
    {
        ini_set("precision", 3);

        $notes = $request->note;
        $qtys = $request->quantity;

        $total = 0;

        $noteArr = [];

        foreach ($notes as $key => $note) {
            $note = $notes[$key];
            $qty = $qtys[$key];

            $note_status = array(
                'note_val' => $note, 'qty' => $qty
            );

            array_push($noteArr, $note_status);

            $temp = $notes[$key] * $qtys[$key];

            $total += $temp;
        }

        $exists = CashStatus::query()->where('date', $request->date)->first();

        if ($exists != NULL) {
            $id = $exists->id;
            $cash_status = CashStatus::query()->find($id);
        } else {
            $cash_status = new CashStatus();
        }

        $cash_status->start_day_cash = $total;
        $cash_status->start_day_notes = json_encode($noteArr);
        $cash_status->date = $request->date;
        $cash_status->save();

        $notification = array(
            'message' => 'Your Start Day Cash Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function StoreWithdrawCash(Request $request)
    {
        $withdraw = new Withdraw();
        $withdraw->amount = $request->withdraw_amount;
        $withdraw->note = $request->withdraw_note;
        $withdraw->staff_id = $request->withdraw_issued_by;
        $withdraw->save();

        $notification = array(
            'message' => 'Withdrawal Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }

    public function get_customer()
    {
        $customers = DB::table('customers')->where('is_active', 'on')->orderBy('id', 'desc')->get();

        return response()->json([
            'customers' => $customers,
        ]);
    }

    public function StoreEndDayCash(Request $request)
    {
        ini_set("precision", 3);

        $notes = $request->note;
        $qtys = $request->quantity;

        $total = 0;

        $noteArr = [];

        foreach ($notes as $key => $note) {
            $note = $notes[$key];
            $qty = $qtys[$key];

            $note_status = array(
                'note_val' => $note, 'qty' => $qty
            );

            array_push($noteArr, $note_status);

            $temp = $notes[$key] * $qtys[$key];

            $total += $temp;
        }

        $exists = CashStatus::query()->where('date', $request->date)->first();

        if ($exists != NULL) {
            $id = $exists->id;
            $cash_status = CashStatus::query()->find($id);
        } else {
            $cash_status = new CashStatus();
        }

        $cash_status->end_day_cash = $total;
        $cash_status->end_day_notes = json_encode($noteArr);
        $cash_status->date = $request->date;
        $cash_status->save();

        $notification = array(
            'message' => 'Your End Day Cash Information Saved Successfully!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    }


    public function GetOrderStatus()
    {
        return view('pos.order_status');
    }


    public function get_close_check()
    {

        $date = date('Y-m-d');

        if(Carbon::now()->format('H') >= 6){
            $date1 = Carbon::today()->addHours(6);
            $date2 = Carbon::today()->endOfDay()->addHours(6);
        }else{
            $date1 = Carbon::yesterday()->startOfDay()->addHours(6);
            $date2 = Carbon::today()->addHours(6);
        }

        $orders = DB::table('orders')
            ->leftJoin('users', 'orders.table_id', '=', 'users.id')
            ->leftJoin('payment_methods', 'orders.method_id', '=', 'payment_methods.id')
            ->leftJoin('order_types', 'orders.order_type_id', '=', 'order_types.id')
            ->select('orders.*', 'users.name as table_name', 'payment_methods.method_name', 'order_types.name as order_type_name', 'order_types.title', 'order_types.id as order_id')
            //->whereDate('orders.created_at', '>=', $date . ' 00:00:00')
            //->whereDate('orders.created_at', '<=', $date . ' 23:59:59')
            ->whereBetween('orders.created_at',[$date1,$date2])
            ->orderBy('orders.id', 'desc')
            ->limit(20)->get();


        $close_order = [];

        foreach ($orders as $order) {
            $items = [];
            $all_items = DB::table('product_orders')
                ->join('items', 'product_orders.item_id', '=', 'items.id')
                ->select('product_orders.*', 'items.p_name', 'items.id as p_id')
                ->where('product_orders.csk_id', $order->csk_id)
                ->get();


            foreach ($all_items as $all_item) {
                $item_input = [
                    'id' => $all_item->p_id, 'name' => $all_item->p_name, 'price' => $all_item->item_price, 'count' => $all_item->qty, 'discount' => $all_item->discount,
                ];

                array_push($items, $item_input);
            }


            $date = date('d-m-Y', strtotime($order->created_at));
            $time = date('h:i A', strtotime($order->created_at));

            $input_arr = [
                'sub_total' => $order->sub_total,
                'total' => $order->total,
                'paid' => $order->paid,
                'discount' => $order->discount,
                'tax' => $order->tax,
                'csk_id' => $order->csk_id,
                'tips' => $order->tips,
                'order_type' => $order->order_type_name,
                'order_id' => $order->order_id,
                'method_name' => $order->method_name,
                'method_title' => $order->title,
                'table_name' => $order->table_name,
                'id' => $order->id,
                'date' => $date,
                'time' => $time,
                'cart' => $items,
                'delivery' => $order->delivery,
                'grand_total' => $order->total + $order->delivery + $order->tips,
            ];

            array_push($close_order, $input_arr);
        }


        return response()->json([
            'close_order' => $close_order,
        ]);
    }


    public function GetClosedReport()
    {
        $date = Carbon::parse(date('Y-m-d'));

        if(Carbon::now()->format('H') >= 6){
            $date1 = Carbon::today()->addHours(6);
            $date2 = Carbon::today()->endOfDay()->addHours(6);
        }else{
            $date1 = Carbon::yesterday()->startOfDay()->addHours(6);
            $date2 = Carbon::today()->addHours(6);
        }

        $orders = Order::query()
            //->whereDate('orders.created_at', '>=', $date . ' 00:00:00')
            //->whereDate('orders.created_at', '<=', $date . ' 23:59:00')
            ->whereBetween('created_at',[$date1,$date2])
            ->get();

        $u_orders = [];
        foreach ($orders as $order) {
            $data['csk_id'] = $order->csk_id;
            $data['order_type'] = $order->GetOrderType($order->order_type_id);
            $data['table_name'] = $order->GetTableName($order->table_id);
            $data['time'] = $order->created_at->format('d F Y h:i A');
            $data['sub_total'] = $order->sub_total;
            $data['discount'] = $order->discount;
            $data['paid'] = $order->paid;

            array_push($u_orders, $data);
        }

        return response()->json($u_orders);
    }


    public function UserPermission()
    {
        $users = User::query()->where('is_type', '=', '.')->get();
        $modules = Module::all();

        foreach ($users as $user) {
            $id = $user->id;
            $permission = Permission::query()->where('user_id', $id)->get();

            if (count($permission) == 0) {
                // Inserting Permissions default value 0 for every module
                foreach ($modules as $module) {
                    $module_id = $module->id;

                    $permission = new Permission();
                    $permission->user_id = $id;
                    $permission->module_id = $module_id;
                    $permission->status = 0;
                    $permission->save();
                }

            }

        }

        return view('pos.UserPermission', compact('users', 'modules'));
    }


    public function SaveUserPermission(Request $request, $id)
    {

        for ($i = 0; $i < count($request->module_id); $i++) {
            $module_id = $request->module_id[$i];
            $status = $request->input('status_' . $module_id);
            DB::table('permissions')->where(['user_id' => $id, 'module_id' => $module_id])->update(['status' => $status]);
        }

        $notification = array(
            'message' => 'Access Information Saved', 'alert-type' => 'success'
        );

        return back()->with($notification);

    }

    public function get_staff_passcode()
    {
        $staffs_passcode = DB::table('staff')->select('password')->orderBy('id', 'desc')->get();

        return response()->json([
            'staffs_passcode' => $staffs_passcode,
        ]);
    }

    public function delete_close_check($csk_id)
    {
        DB::table('orders')->where('csk_id', $csk_id)->delete();
        DB::table('product_orders')->where('csk_id', $csk_id)->delete();

        return response()->json($csk_id);

    }

    public function profit_loss_calculation(Request $request)
    {
        $data = [];
        if ($request->has('start_date')) {
            $data = $this->profit_loss_report($request);
        }

        return view('pos.profit_loss', $data);
    }

    public function PDF_profit_loss_calculation(Request $request)
    {
        $data = [];
        if ($request->has('start_date')) {
            $data = $this->profit_loss_report($request);
        }

//        return view('pdf.profit_report_pdf', $data);

        PDF::setOptions([ 'dpi' => 150, 'defaultFont' => 'sans-serif']);

        $pdf = PDF::loadView('pdf.profit_report_pdf', $data);

        return $pdf->setPaper('a4', 'portrait')->setWarnings(false)->download('profit_loss_report.pdf');
    }

    public function profit_loss_report($request)
    {
        $data = array();

        //$start_date = Carbon::parse($request->start_date);
        //$end_date = Carbon::parse($request->end_date)->addHour(23)->addMinute(59);
        $start_date = Carbon::parse($request->start_date)->addHours(6);
        $end_date = Carbon::parse($request->end_date)->endOfDay()->addHours(6);

        $data['total_order'] = count(Order::query()->whereBetween('created_at', [$start_date, $end_date])->get());
        $data['total_income'] = Order::query()->whereBetween('created_at', [$start_date, $end_date])->sum('paid');

        $data['purchase_expenses'] = Purchase::query()->whereBetween('created_at', [$start_date, $end_date])->sum('total_cost');
        $data['other_expenses'] = Expense::query()->whereBetween('created_at', [$start_date, $end_date])->sum('amount');

        $data['total_expense'] = $data['purchase_expenses'] + $data['other_expenses'];

        return $data;
    }

    public function kitchen(){
        return view('kitchen.kitchen');
    }


    public function get_kitchen_check()
    {

        $date = date('Y-m-d');


        $orders = DB::table('kitchen_orders')
            ->orderBy('id', 'desc')
            ->get();
        $close_order = [];

        foreach ($orders as $order) {
            $items = [];
            $all_items = DB::table('kitchen_products')
                ->join('items', 'kitchen_products.item_id', '=', 'items.id')
                ->select('kitchen_products.*', 'items.p_name', 'items.id as p_id')
                //->where('kitchen_products.csk_id', $order->csk_id)
                ->where('kitchen_products.kitchen_order_id',$order->id)
                ->get();


            foreach ($all_items as $all_item) {
                $item_input = [
                    'id' => $all_item->p_id, 'name' => $all_item->p_name, 'count' => $all_item->qty,
                ];

                array_push($items, $item_input);
            }


            $date = date('d-m-Y', strtotime($order->created_at));
            $time = date('h:i A', strtotime($order->created_at));



            $input_arr = [
                'csk_id' => $order->csk_id,
                'id' => $order->id,
                'date' => $date,
                'time' => $time,
                'cart' => $items,
                'created_at' => $order->created_at,
                'is_kitchen' => $order->is_kitchen,

            ];

            array_push($close_order, $input_arr);
        }


        return response()->json([
            'close_order' => $close_order,
        ]);
    }

    public function kitchen_finish($id){
        DB::table('kitchen_orders')->where('id',$id)->update([
            'is_kitchen'=> 3,
        ]);
        return response()->json(true);
    }

    public function kitchen_processing($id){
        DB::table('kitchen_orders')->where('id',$id)->update([
            'is_kitchen'=> 2,
        ]);
        return response()->json(true);
    }



    //Kitchen Save Data

    public function save_kitchen(Request $request){
        DB::table('kitchen_orders')->insert([
            'csk_id' => $request->csk_id,
            'cmp' => $request->cmp,
            'is_kitchen' => $request->is_kitchen,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        $id = DB::getPdo()->lastInsertId();
        $order = DB::table('kitchen_orders')->orderBy('id', 'desc')->first();
        return response()->json($order);
    }

    public function SaveKitchenProductOrderInfo(Request $request){

        $item_id = $request->item_id;
        DB::table('kitchen_products')->insert([
            'kitchen_order_id' => KitchenOrder::query()->max('id'),
            'csk_id' => $request->csk_id,
            'item_id' => $item_id,
            'qty' => $request->qty,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        return response()->json([ 'success' => true ]);
    }

    public function kitchen_list(Request $request){
        if (count($request->all()) > 0) {
            $date1 = Carbon::parse($request->start_date)->format('Y-m-d');
            $date2 = Carbon::parse($request->end_date)->format('Y-m-d');

            $orders = DB::table('kitchen_orders')
                ->whereDate('created_at', '>=', $date1)
                ->whereDate('created_at', '<=', $date2)
                ->orderBy('id', 'desc')
                ->get();

        }else{
            $date = date('Y-m-d');
            $orders = DB::table('kitchen_orders')
                ->whereDate('created_at', '>=', $date . ' 00:00:00')
                ->whereDate('created_at', '<=', $date . ' 23:59:59')
                ->orderBy('id', 'desc')
                ->get();
        }



        $close_order = [];

        foreach ($orders as $order) {
            $items = [];
            $all_items = DB::table('kitchen_products')
                ->join('items', 'kitchen_products.item_id', '=', 'items.id')
                ->select('kitchen_products.*', 'items.p_name', 'items.id as p_id')
                //->where('kitchen_products.csk_id', $order->csk_id)
                ->where('kitchen_products.kitchen_order_id',$order->id)
                ->get();

            foreach ($all_items as $all_item) {
                $item_input = [
                    'id' => $all_item->p_id, 'name' => $all_item->p_name, 'count' => $all_item->qty,
                ];

                array_push($items, $item_input);
            }

            $date = date('d-m-Y', strtotime($order->created_at));
            $time = date('h:i A', strtotime($order->created_at));

            $input_arr = [
                'csk_id' => $order->csk_id,
                'id' => $order->id,
                'date' => $date,
                'time' => $time,
                'cart' => $items,
                'created_at' => $order->created_at,
                'is_kitchen' => $order->is_kitchen,

            ];

            array_push($close_order, $input_arr);
        }

        // dd($close_order);

        return view('kitchen.kitchen_list', compact('close_order'));
    }

    public function getCustomer(Request $request)
    {
        //dd($request->all());
        $id = $request->get('id');
        $customer = Customer::query()->findOrFail($id);
        return $customer;
    }

}
