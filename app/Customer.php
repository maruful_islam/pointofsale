<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name','phone','email','address','area','street','street_no','block','building','floor','flat','w_number','is_active'];
}
