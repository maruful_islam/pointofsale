<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BreakLog extends Model
{

    public function GetTotalBreakTime($date, $emp_id)
    {
        $get_log = BreakLog::where(['date'=>$date, 'emp_id'=>$emp_id])->get();

        $total = 0;

        foreach ($get_log as $row)
        {
            $to = strtotime($row->end_break);
            $from = strtotime($row->start_break);

            $total += round(abs($to - $from) / 60,2);
        }

        $data['total_break'] = $total;

        return $total;
    }
}
