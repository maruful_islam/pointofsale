<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequisition extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function items()
    {
        return $this->belongsToMany(RMItem::class,'item_requisition','requisition_id','item_id')->withPivot('qty');
    }
}
