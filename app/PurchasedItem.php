<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasedItem extends Model
{
    protected $fillable = ['purchase_id','rmc_id','rmi_id','supplier_id','quantity','unit','price'];
    public function purchase()
    {
        return $this->belongsTo(Purchase::class, 'id', 'purchase_id');
    }

    public function items()
    {
        return $this->belongsTo(Item::class,'id', 'rmi_id');
    }

    public function categories()
    {
        return $this->belongsTo(RMCategory::class, 'id', 'rmi_id');
    }

    public function suppliers()
    {
        return $this->belongsTo(Supplier::class, 'id', 'supplier_id');
    }
}
