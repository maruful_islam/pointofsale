<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RMCategory extends Model
{
    //
    public function items()
    {
        return $this->hasMany(Item::class, 'rmc_id', 'id');
    }
}
