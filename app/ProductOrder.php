<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductOrder extends Model
{

    public function GetOrderType($type_id)
    {
        return DB::table('order_types')->where('id',$type_id)->value('name');
    }

    public function GetOrderItems($csk_id)
    {
        return DB::table('product_orders')->join('items','product_orders.item_id','=','items.id')
            ->where('csk_id',$csk_id)->get();
    }

    public function GetTableName($id)
    {
        return DB::table('users')->where(['is_type'=> 'table','id'=>$id])->value('name');
    }

    public function GetMethodName($id)
    {
        return DB::table('payment_methods')->where(['id'=>$id])->value('method_name');
    }

    public function getItemName($item_id)
    {
        return Item::where('id', $item_id)->value('p_name');
    }
}
