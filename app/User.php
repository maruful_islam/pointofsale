<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDesignationTitle($id)
    {
        $email = User::where('id',$id)->value('email');
        $designation_id = Staff::where('email', $email)->value('designation_id');
        $title = Designation::where('id',$designation_id)->value('title');

        return $title;
    }
}
