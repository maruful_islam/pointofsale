<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumption extends Model
{
    public function items()
    {
        return $this->belongsTo(RMItem::class,'rmi_id', 'id');
    }

    public function getRMCName($id)
    {
        return RMCategory::where('id',$id)->value('name');
    }
}
