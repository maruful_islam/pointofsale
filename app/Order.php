<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    protected $dates = ['pre_order'];

    protected $fillable = [
        "pre_order",
        "order_type_id",
        "persone_number",
        "table_id",
        "method_id",
        "user_id",
        "customer_id",
        "sub_total",
        "total",
        "paid",
        "discount",
        "tax",
        "tips",
        "delivery",
        "cmp",
        "csk_id",
        "is_kitchen",
        "one_method_id",
        "two_method_id",
        "pay_one",
        "pay_two"
    ];

    public function methods()
    {
        return $this->belongsToMany(Method::class);
    }

    public function method()
    {
        return $this->belongsTo(Method::class);
    }

    public function methodOne()
    {
        return $this->belongsTo(Method::class,'one_method_id');
    }

    public function methodTwo()
    {
        return $this->belongsTo(Method::class,'two_method_id');
    }

    public function GetOrderType($type_id)
    {
        return DB::table('order_types')->where('id',$type_id)->value('name');
    }

    public function GetOrderItems($csk_id)
    {
        return DB::table('product_orders')->join('items','product_orders.item_id','=','items.id')
            ->where('csk_id',$csk_id)->get();
    }

    public function GetTableName($id)
    {
        return DB::table('users')->where(['is_type'=> 'table','id'=>$id])->value('name');
    }

    public function GetMethodName($id)
    {
        return DB::table('payment_methods')->where(['id'=>$id])->value('method_name');
    }
}
