-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2018 at 01:21 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `re_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_time` datetime DEFAULT NULL,
  `leave_time` datetime DEFAULT NULL,
  `current_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `emp_id`, `date`, `entry_time`, `leave_time`, `current_status`, `created_at`, `updated_at`) VALUES
(3, 7, '06.03.2018', '2018-03-06 07:04:51', '2018-03-06 17:04:51', 'Working', '2018-03-06 01:04:51', '2018-03-06 01:04:51'),
(6, 9, '06.03.2018', '2018-03-06 11:27:07', '2018-03-06 19:27:07', 'Day Work Finished', '2018-03-06 05:27:07', '2018-03-06 05:27:07'),
(11, 8, '06.03.2018', '2018-03-06 12:41:02', '2018-03-06 20:41:02', 'Day Work Finished', '2018-03-06 06:41:02', '2018-03-06 06:41:02'),
(13, 8, '07.03.2018', '2018-03-07 04:21:35', '2018-03-07 14:21:35', 'Day Work Finished', '2018-03-06 22:21:35', '2018-03-06 22:21:35'),
(14, 9, '07.03.2018', '2018-03-07 04:54:04', '2018-03-07 14:54:04', 'Day Work Finished', '2018-03-06 22:54:04', '2018-03-06 22:54:04'),
(17, 7, '07.03.2018', '2018-03-07 05:23:26', NULL, 'Working', '2018-03-06 23:23:26', '2018-03-06 23:23:26'),
(18, 7, '08.03.2018', '2018-03-08 04:11:36', NULL, 'Working', '2018-03-07 22:11:44', '2018-03-07 22:11:44'),
(19, 7, '10.03.2018', '2018-03-10 04:18:19', NULL, 'Working', '2018-03-09 22:18:26', '2018-03-09 22:18:26'),
(20, 6, '11.03.2018', '2018-03-11 04:21:48', NULL, 'Working', '2018-03-10 22:21:53', '2018-03-10 22:21:53'),
(21, 6, '12.03.2018', '2018-03-12 04:13:05', NULL, 'Working', '2018-03-11 22:13:12', '2018-03-11 22:13:12'),
(22, 6, '13.03.2018', '2018-03-13 03:58:09', NULL, 'Working', '2018-03-12 21:58:34', '2018-03-12 21:58:34');

-- --------------------------------------------------------

--
-- Table structure for table `break_logs`
--

CREATE TABLE `break_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_break` datetime DEFAULT NULL,
  `end_break` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `break_logs`
--

INSERT INTO `break_logs` (`id`, `emp_id`, `date`, `start_break`, `end_break`, `created_at`, `updated_at`) VALUES
(2, 9, '06.03.2018', '2018-03-06 11:42:55', '2018-03-06 11:55:55', '2018-03-06 05:42:55', '2018-03-06 05:42:55'),
(3, 9, '06.03.2018', '2018-03-06 12:45:55', '2018-03-06 12:52:55', '2018-03-06 05:42:55', '2018-03-06 05:42:55'),
(5, 9, '06.03.2018', '2018-03-06 12:13:25', '2018-03-06 12:33:25', '2018-03-06 06:13:25', '2018-03-06 06:13:25'),
(8, 8, '07.03.2018', '2018-03-07 04:35:21', '2018-03-07 04:45:21', '2018-03-06 22:35:21', '2018-03-06 22:35:21'),
(9, 9, '07.03.2018', '2018-03-07 04:36:35', '2018-03-07 04:56:35', '2018-03-06 22:56:35', '2018-03-06 22:56:35'),
(12, 7, '07.03.2018', '2018-03-07 05:23:53', '2018-03-07 05:33:53', '2018-03-06 23:23:53', '2018-03-06 23:23:53'),
(13, 7, '07.03.2018', '2018-03-07 06:07:00', '2018-03-07 06:17:00', '2018-03-07 00:07:00', '2018-03-07 00:07:00'),
(14, 7, '07.03.2018', '2018-03-07 09:32:57', '2018-03-07 09:35:57', '2018-03-07 03:33:03', '2018-03-07 03:33:03'),
(15, 7, '08.03.2018', '2018-03-08 09:41:30', '2018-03-08 12:16:15', '2018-03-08 03:41:41', '2018-03-08 03:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `cash_drawers`
--

CREATE TABLE `cash_drawers` (
  `id` int(10) UNSIGNED NOT NULL,
  `1_4_diner` int(11) DEFAULT NULL,
  `1_2_diner` int(11) DEFAULT NULL,
  `1_diner` int(11) DEFAULT NULL,
  `5_diner` int(11) DEFAULT NULL,
  `1_0_diner` int(11) DEFAULT NULL,
  `2_0_diner` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cash_statuses`
--

CREATE TABLE `cash_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_day_cash` int(11) DEFAULT NULL,
  `withdraw` int(11) DEFAULT NULL,
  `end_day_cash` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `phonecode`) VALUES
(1, 'Afghanistan', 93),
(2, 'Albania', 355),
(3, 'Algeria', 213),
(4, 'American Samoa', 1684),
(5, 'Andorra', 376),
(6, 'Angola', 244),
(7, 'Anguilla', 1264),
(8, 'Antarctica', 0),
(9, 'Antigua and Barbuda', 1268),
(10, 'Argentina', 54),
(11, 'Armenia', 374),
(12, 'Aruba', 297),
(13, 'Australia', 61),
(14, 'Austria', 43),
(15, 'Azerbaijan', 994),
(16, 'Bahamas', 1242),
(17, 'Bahrain', 973),
(18, 'Bangladesh', 880),
(19, 'Barbados', 1246),
(20, 'Belarus', 375),
(21, 'Belgium', 32),
(22, 'Belize', 501),
(23, 'Benin', 229),
(24, 'Bermuda', 1441),
(25, 'Bhutan', 975),
(26, 'Bolivia', 591),
(27, 'Bosnia and Herzegovina', 387),
(28, 'Botswana', 267),
(29, 'Bouvet Island', 0),
(30, 'Brazil', 55),
(31, 'British Indian Ocean Territory', 246),
(32, 'Brunei Darussalam', 673),
(33, 'Bulgaria', 359),
(34, 'Burkina Faso', 226),
(35, 'Burundi', 257),
(36, 'Cambodia', 855),
(37, 'Cameroon', 237),
(38, 'Canada', 1),
(39, 'Cape Verde', 238),
(40, 'Cayman Islands', 1345),
(41, 'Central African Republic', 236),
(42, 'Chad', 235),
(43, 'Chile', 56),
(44, 'China', 86),
(45, 'Christmas Island', 61),
(46, 'Cocos (Keeling) Islands', 672),
(47, 'Colombia', 57),
(48, 'Comoros', 269),
(49, 'Congo', 242),
(50, 'Congo, the Democratic Republic of the', 242),
(51, 'Cook Islands', 682),
(52, 'Costa Rica', 506),
(53, 'Cote D\'Ivoire', 225),
(54, 'Croatia', 385),
(55, 'Cuba', 53),
(56, 'Cyprus', 357),
(57, 'Czech Republic', 420),
(58, 'Denmark', 45),
(59, 'Djibouti', 253),
(60, 'Dominica', 1767),
(61, 'Dominican Republic', 1809),
(62, 'Ecuador', 593),
(63, 'Egypt', 20),
(64, 'El Salvador', 503),
(65, 'Equatorial Guinea', 240),
(66, 'Eritrea', 291),
(67, 'Estonia', 372),
(68, 'Ethiopia', 251),
(69, 'Falkland Islands (Malvinas)', 500),
(70, 'Faroe Islands', 298),
(71, 'Fiji', 679),
(72, 'Finland', 358),
(73, 'France', 33),
(74, 'French Guiana', 594),
(75, 'French Polynesia', 689),
(76, 'French Southern Territories', 0),
(77, 'Gabon', 241),
(78, 'Gambia', 220),
(79, 'Georgia', 995),
(80, 'Germany', 49),
(81, 'Ghana', 233),
(82, 'Gibraltar', 350),
(83, 'Greece', 30),
(84, 'Greenland', 299),
(85, 'Grenada', 1473),
(86, 'Guadeloupe', 590),
(87, 'Guam', 1671),
(88, 'Guatemala', 502),
(89, 'Guinea', 224),
(90, 'Guinea-Bissau', 245),
(91, 'Guyana', 592),
(92, 'Haiti', 509),
(93, 'Heard Island and Mcdonald Islands', 0),
(94, 'Holy See (Vatican City State)', 39),
(95, 'Honduras', 504),
(96, 'Hong Kong', 852),
(97, 'Hungary', 36),
(98, 'Iceland', 354),
(99, 'India', 91),
(100, 'Indonesia', 62),
(101, 'Iran, Islamic Republic of', 98),
(102, 'Iraq', 964),
(103, 'Ireland', 353),
(104, 'Israel', 972),
(105, 'Italy', 39),
(106, 'Jamaica', 1876),
(107, 'Japan', 81),
(108, 'Jordan', 962),
(109, 'Kazakhstan', 7),
(110, 'Kenya', 254),
(111, 'Kiribati', 686),
(112, 'Korea, Democratic People\'s Republic of', 850),
(113, 'Korea, Republic of', 82),
(114, 'Kuwait', 965),
(115, 'Kyrgyzstan', 996),
(116, 'Lao People\'s Democratic Republic', 856),
(117, 'Latvia', 371),
(118, 'Lebanon', 961),
(119, 'Lesotho', 266),
(120, 'Liberia', 231),
(121, 'Libyan Arab Jamahiriya', 218),
(122, 'Liechtenstein', 423),
(123, 'Lithuania', 370),
(124, 'Luxembourg', 352),
(125, 'Macao', 853),
(126, 'Macedonia, the Former Yugoslav Republic of', 389),
(127, 'Madagascar', 261),
(128, 'Malawi', 265),
(129, 'Malaysia', 60),
(130, 'Maldives', 960),
(131, 'Mali', 223),
(132, 'Malta', 356),
(133, 'Marshall Islands', 692),
(134, 'Martinique', 596),
(135, 'Mauritania', 222),
(136, 'Mauritius', 230),
(137, 'Mayotte', 269),
(138, 'Mexico', 52),
(139, 'Micronesia, Federated States of', 691),
(140, 'Moldova, Republic of', 373),
(141, 'Monaco', 377),
(142, 'Mongolia', 976),
(143, 'Montserrat', 1664),
(144, 'Morocco', 212),
(145, 'Mozambique', 258),
(146, 'Myanmar', 95),
(147, 'Namibia', 264),
(148, 'Nauru', 674),
(149, 'Nepal', 977),
(150, 'Netherlands', 31),
(151, 'Netherlands Antilles', 599),
(152, 'New Caledonia', 687),
(153, 'New Zealand', 64),
(154, 'Nicaragua', 505),
(155, 'Niger', 227),
(156, 'Nigeria', 234),
(157, 'Niue', 683),
(158, 'Norfolk Island', 672),
(159, 'Northern Mariana Islands', 1670),
(160, 'Norway', 47),
(161, 'Oman', 968),
(162, 'Pakistan', 92),
(163, 'Palau', 680),
(164, 'Palestinian Territory, Occupied', 970),
(165, 'Panama', 507),
(166, 'Papua New Guinea', 675),
(167, 'Paraguay', 595),
(168, 'Peru', 51),
(169, 'Philippines', 63),
(170, 'Pitcairn', 0),
(171, 'Poland', 48),
(172, 'Portugal', 351),
(173, 'Puerto Rico', 1787),
(174, 'Qatar', 974),
(175, 'Reunion', 262),
(176, 'Romania', 40),
(177, 'Russian Federation', 70),
(178, 'Rwanda', 250),
(179, 'Saint Helena', 290),
(180, 'Saint Kitts and Nevis', 1869),
(181, 'Saint Lucia', 1758),
(182, 'Saint Pierre and Miquelon', 508),
(183, 'Saint Vincent and the Grenadines', 1784),
(184, 'Samoa', 684),
(185, 'San Marino', 378),
(186, 'Sao Tome and Principe', 239),
(187, 'Saudi Arabia', 966),
(188, 'Senegal', 221),
(189, 'Serbia and Montenegro', 381),
(190, 'Seychelles', 248),
(191, 'Sierra Leone', 232),
(192, 'Singapore', 65),
(193, 'Slovakia', 421),
(194, 'Slovenia', 386),
(195, 'Solomon Islands', 677),
(196, 'Somalia', 252),
(197, 'South Africa', 27),
(198, 'South Georgia and the South Sandwich Islands', 0),
(199, 'Spain', 34),
(200, 'Sri Lanka', 94),
(201, 'Sudan', 249),
(202, 'Suriname', 597),
(203, 'Svalbard and Jan Mayen', 47),
(204, 'Swaziland', 268),
(205, 'Sweden', 46),
(206, 'Switzerland', 41),
(207, 'Syrian Arab Republic', 963),
(208, 'Taiwan, Province of China', 886),
(209, 'Tajikistan', 992),
(210, 'Tanzania, United Republic of', 255),
(211, 'Thailand', 66),
(212, 'Timor-Leste', 670),
(213, 'Togo', 228),
(214, 'Tokelau', 690),
(215, 'Tonga', 676),
(216, 'Trinidad and Tobago', 1868),
(217, 'Tunisia', 216),
(218, 'Turkey', 90),
(219, 'Turkmenistan', 7370),
(220, 'Turks and Caicos Islands', 1649),
(221, 'Tuvalu', 688),
(222, 'Uganda', 256),
(223, 'Ukraine', 380),
(224, 'United Arab Emirates', 971),
(225, 'United Kingdom', 44),
(226, 'United States', 1),
(227, 'United States Minor Outlying Islands', 1),
(228, 'Uruguay', 598),
(229, 'Uzbekistan', 998),
(230, 'Vanuatu', 678),
(231, 'Venezuela', 58),
(232, 'Viet Nam', 84),
(233, 'Virgin Islands, British', 1284),
(234, 'Virgin Islands, U.s.', 1340),
(235, 'Wallis and Futuna', 681),
(236, 'Western Sahara', 212),
(237, 'Yemen', 967),
(238, 'Zambia', 260),
(239, 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `w_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `email`, `w_number`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'sadek', '4444', 'rjsadek@gmail.com', '4452', 'on', '2018-03-01 06:40:29', '2018-03-01 06:40:29'),
(2, 'sa', '333', 'admin@gmail.com', '333', 'on', '2018-03-03 07:57:54', '2018-03-03 07:57:54'),
(3, 'New Customer', '01775552255', 'admin@gmail.com', '5522222', 'on', '2018-03-03 08:05:16', '2018-03-03 08:05:16'),
(4, 'Movin', '555', 'rjsadek@gmail.com', '55555', 'on', '2018-03-03 08:15:54', '2018-03-03 08:15:54'),
(5, 'sadek hossain', '0177552222', 'rjsadek@gmail.com', '555555555', 'on', '2018-03-03 08:27:08', '2018-03-03 08:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sales Department', 'Worker will sell our product', 'active', '2018-03-04 04:55:32', '2018-03-04 04:55:32'),
(2, 'Finance Department', 'In this department, employee will track report of sales', 'active', '2018-03-04 04:58:50', '2018-03-04 04:58:50'),
(3, 'IT Department', 'This departments worker will monitor our system and will take back up of files.', 'active', '2018-03-04 05:00:02', '2018-03-05 00:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `title`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Senior Marketing Executive', 'Some Description Goes here', 'active', '2018-03-04 05:33:11', '2018-03-04 05:33:11'),
(2, 'Software Engineer', 'Some Description Goes here 11', 'active', '2018-03-04 05:36:15', '2018-03-04 05:36:44');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percent` double(8,2) NOT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `title`, `percent`, `is_active`) VALUES
(7, 'dis', 'dis', 2.00, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `p_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `cat_id`, `p_name`, `p_title`, `p_price`, `p_image`, `is_active`) VALUES
(9, 9, 'Burger', 'Burger', '2', 'product/item/1519911594.jpg', 'active'),
(10, 9, 'Burger 2', 'Burger 2', '3', 'product/item/1519911616.jpg', 'active'),
(11, 10, 'Rice', 'Rice', '5', 'product/item/1519911644.jpg', 'active'),
(12, 11, 'Food 1', 'Food 1', '2', 'product/item/1519911680.jpg', 'active'),
(13, 11, 'Food 2', 'Food 1', '3', 'product/item/1519911703.jpg', 'active'),
(14, 10, 'Food 3', 'Food 3', '5', 'product/item/1519911735.jpg', 'active'),
(15, 12, 'juice', 'juice', '3', 'product/item/1519911766.jpg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_02_24_094547_create-taxs-table', 2),
(9, '2018_02_24_114411_create-discounts-table', 3),
(10, '2018_02_25_050256_create-payment_methods-table', 4),
(12, '2018_02_25_055022_create-orders_type-table', 5),
(13, '2018_02_25_082830_create-product_category-table', 6),
(14, '2018_02_25_102126_create-product_item-table', 7),
(15, '2018_03_01_051414_create_customers_table', 8),
(18, '2018_03_03_105729_create-orders-table', 9),
(19, '2018_03_04_104657_create_departments_table', 10),
(20, '2018_03_04_112413_create_designations_table', 11),
(21, '2018_03_05_042508_create_staff_table', 12),
(22, '2018_03_05_101834_create_attendances_table', 13),
(23, '2018_03_05_101849_create_break_logs_table', 13),
(24, '2018_03_10_054303_create_product_orders_table', 14),
(25, '2018_03_10_112915_create_cash_drawers_table', 15),
(26, '2018_03_10_112939_create_cash_statuses_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `csk_id` bigint(11) NOT NULL,
  `tips` float(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_type_id`, `persone_number`, `table_id`, `method_id`, `sub_total`, `total`, `paid`, `discount`, `tax`, `csk_id`, `tips`, `created_at`, `updated_at`) VALUES
(178, '13', '1', '4', '1', '2.000', '2.000', '2.000', '0.000', '0.000', 1623027, NULL, '2018-03-13 05:47:03', '2018-03-13 05:47:03'),
(181, '13', '1', '3', '1', '2.000', '2.000', '2.000', '0.000', '0.000', 2017500, NULL, '2018-03-13 05:54:08', '2018-03-13 05:54:08'),
(182, '13', '1', '4', '1', '5.000', '5.000', '5.000', '0.000', '0.000', 2026051, NULL, '2018-03-13 05:54:08', '2018-03-13 05:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `order_types`
--

CREATE TABLE `order_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dis_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_types`
--

INSERT INTO `order_types` (`id`, `name`, `dis_type`, `tax_type`, `title`, `is_active`) VALUES
(13, 'Take Away', '', '', 'Take Away', 'active'),
(14, 'Delivery', '', '', 'Delivery', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `method_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `method_name`, `title`, `is_active`) VALUES
(1, 'Cash', 'Cash', 'active'),
(2, 'DBBL', NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product_categoy`
--

CREATE TABLE `product_categoy` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categoy`
--

INSERT INTO `product_categoy` (`id`, `name`, `title`, `icon`, `is_active`) VALUES
(9, 'Breakfast', 'Breakfast', 'product/categoy/1519911348.jpg', 'active'),
(10, 'Lunch', 'Lunch', 'product/categoy/1519911367.jpg', 'active'),
(11, 'Dinner', 'Dinner', 'product/categoy/1519911395.jpg', 'active'),
(12, 'Drinks', 'Drinks', 'product/categoy/1519911413.jpg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `csk_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `item_price` double(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` double(8,2) NOT NULL,
  `discount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `csk_id`, `item_id`, `cat_id`, `item_price`, `qty`, `total`, `discount`, `created_at`, `updated_at`) VALUES
(95, 1623027, 9, 9, 2.00, 1, 2.00, 0.00, '2018-03-13 05:47:04', '2018-03-13 05:47:04'),
(96, 2026051, 11, 10, 5.00, 1, 5.00, 0.00, '2018-03-13 05:54:08', '2018-03-13 05:54:08'),
(97, 2017500, 9, 9, 2.00, 1, 2.00, 0.00, '2018-03-13 05:54:08', '2018-03-13 05:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `blood_group` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `d_o_b` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `id_card` text COLLATE utf8mb4_unicode_ci,
  `cv` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `password` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `department_id`, `designation_id`, `country_id`, `employee_id`, `name`, `email`, `phone`, `f_name`, `m_name`, `job_type`, `city`, `address`, `blood_group`, `gender`, `d_o_b`, `join_date`, `image`, `id_card`, `cv`, `status`, `password`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 226, '1', 'Mr. X', 'x@gmail.com', '11111111', 'Mr X Father', 'Mr X Mother', 'Full Time', 'New York', 'some address', 'A+', 'Male', '01.03.1990', '01.03.2018', '/uploads/staff/image/1520229789.jfif', '/uploads/staff/id_card/1520229789.png', '/uploads/staff/cv/1520229789.docx', 'active', 123456, '2018-03-05 00:03:09', '2018-03-05 00:03:09'),
(2, 3, 2, 18, '2', 'Mrs Anika', 'anika@gmail.com', '2222222222', 'Anika\'s Father', 'Anika\'s Mother', 'Full Time', 'Dhaka', 'Uttara', 'A+', 'Female', '01.09.1992', '01.03.2018', '/uploads/staff/image/1520229989.png', '/uploads/staff/id_card/1520229989.png', '/uploads/staff/cv/1520229989.docx', 'active', 123456, '2018-03-05 00:06:29', '2018-03-05 00:06:29'),
(3, 3, 2, 18, '3', 'Mr Khairul', 'khairul@gmail.com', '12345671234', 'Khairul\'s Father', 'Khairul\'s Mother', 'Full Time', 'Dhaka', 'Khilgaon', 'A+', 'Male', '11.11.1982', '01.03.2018', '/uploads/staff/image/1520243390.jfif', '/uploads/staff/id_card/1520243390.png', '/uploads/staff/cv/1520243390.docx', 'active', 123456, '2018-03-05 03:49:50', '2018-03-05 03:49:50'),
(4, 3, 2, 18, '3', 'Admin', 'admin@gmail.com', '12345671234', 'Admins Father', 'Admins Mother', 'Full Time', 'Dhaka', 'Khilgaon', 'A+', 'Male', '11.11.1982', '01.03.2018', '/uploads/staff/image/1520243390.jfif', '/uploads/staff/id_card/1520243390.png', '/uploads/staff/cv/1520243390.docx', 'active', 123456, '2018-03-05 03:49:50', '2018-03-05 03:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `taxs`
--

CREATE TABLE `taxs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percent` double(8,2) NOT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '.',
  `is_rule` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N/A',
  `permission` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `tbl_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N/A',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_type`, `is_rule`, `permission`, `tbl_title`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'rjsadek', 'rjsadek@gmail.com', '$2y$10$j1JPIerUzTsGZ0.9wOw2OOYoxoqxYqNMS5VssbIF8QKu0HD3zTQ8i', '.', 'N/A', '0', 'N/A', 'tXRhI8GNvBCCsi38Dcqsdcc6SzcHjsE5JhLF05wW8CtYhjT2dlPyjy3tu2Sq', '2018-02-24 00:20:49', '2018-02-24 00:20:49'),
(2, 'test', 'test@gmail.com', '$2y$10$gsPlbI08wYo9CiY3fM0z1uyzB680pjpIQGxGmsh28z9kIwAnUOp9i', '.', 'N/A', '0', 'N/A', 'MV7AXsBa8gZDbPu0OPfTIxHVckkvfLIsXtUD6uIBinmpGiKPkfH5tP4zQ2ts', '2018-02-24 00:41:52', '2018-02-24 00:41:52'),
(3, 'Table 1', '1519459914@gmail.com', '12345678', 'table', 'N/A', '0', 'asdf', NULL, NULL, NULL),
(4, 'Table 2', '1519460014@gmail.com', '12345678', 'table', 'N/A', '0', 'asdf', NULL, NULL, NULL),
(5, 'Table 3', '1519463954@gmail.com', '12345678', 'table', 'N/A', '0', 'asdf', NULL, NULL, NULL),
(6, 'admin', 'admin@gmail.com', '$2y$10$tPfV/PvWZvLFZjz8ZT/1ee9Gy7yZNGc.Fojon1oV3n7vTTg40Qp/.', '.', 'N/A', '0', 'N/A', 'rE3MYmr2moEiXRDtFVAFGznJXjdjJcRZjh22KtgMFtr0NU4X0RYmo4crzo5m', '2018-03-01 04:10:59', '2018-03-01 04:10:59'),
(7, 'Mr. X', 'x@gmail.com', '$2y$10$5Co8vj.kvApyno7dSqY6K.vPdnUiIgXy8aNnETvGqUq8tgDGw1zK2', '.', 'N/A', '0', 'N/A', 'NE5BRmYlBqweD4ZktO4xlK5WexjNwRtHyKk8xFUE4u2iMzQqn0UKKpeTCmnM', '2018-03-05 00:03:09', '2018-03-05 00:03:09'),
(8, 'Mrs Anika', 'anika@gmail.com', '$2y$10$5Co8vj.kvApyno7dSqY6K.vPdnUiIgXy8aNnETvGqUq8tgDGw1zK2', '.', 'N/A', '0', 'N/A', 'nK00aXnK03sT0Q0BFYeknCZLJH8vtzfq7VML9YmMHetuUBTU2JBUdTKsy05O', '2018-03-05 00:13:09', '2018-03-05 00:13:09'),
(9, 'Mr Khairul', 'khairul@gmail.com', '$2y$10$5Co8vj.kvApyno7dSqY6K.vPdnUiIgXy8aNnETvGqUq8tgDGw1zK2', '.', 'N/A', '0', 'N/A', 'o8DAaAlTYvARxrLlJwWKPVqfYydOHja7nh3qjkffLUi5W6HpOLEAHN16PKqA', '2018-03-05 03:49:50', '2018-03-05 03:49:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `break_logs`
--
ALTER TABLE `break_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_drawers`
--
ALTER TABLE `cash_drawers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_statuses`
--
ALTER TABLE `cash_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_cat_id_foreign` (`cat_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_types`
--
ALTER TABLE `order_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categoy`
--
ALTER TABLE `product_categoy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `taxs`
--
ALTER TABLE `taxs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `break_logs`
--
ALTER TABLE `break_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cash_drawers`
--
ALTER TABLE `cash_drawers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cash_statuses`
--
ALTER TABLE `cash_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `order_types`
--
ALTER TABLE `order_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_categoy`
--
ALTER TABLE `product_categoy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `taxs`
--
ALTER TABLE `taxs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `product_categoy` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
