-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 06, 2018 at 12:32 AM
-- Server version: 5.6.23-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `optimuma_annapoorna`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leave_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `break_logs`
--

CREATE TABLE `break_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_break` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_break` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cash_statuses`
--

CREATE TABLE `cash_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_day_cash` int(11) DEFAULT NULL,
  `start_day_notes` text COLLATE utf8mb4_unicode_ci,
  `withdraw` int(11) DEFAULT NULL,
  `end_day_cash` int(11) DEFAULT NULL,
  `end_day_notes` text COLLATE utf8mb4_unicode_ci,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consumptions`
--

CREATE TABLE `consumptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `rmi_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `phonecode`) VALUES
(1, 'Afghanistan', 93),
(2, 'Albania', 355),
(3, 'Algeria', 213),
(4, 'American Samoa', 1684),
(5, 'Andorra', 376),
(6, 'Angola', 244),
(7, 'Anguilla', 1264),
(8, 'Antarctica', 0),
(9, 'Antigua and Barbuda', 1268),
(10, 'Argentina', 54),
(11, 'Armenia', 374),
(12, 'Aruba', 297),
(13, 'Australia', 61),
(14, 'Austria', 43),
(15, 'Azerbaijan', 994),
(16, 'Bahamas', 1242),
(17, 'Bahrain', 973),
(18, 'Bangladesh', 880),
(19, 'Barbados', 1246),
(20, 'Belarus', 375),
(21, 'Belgium', 32),
(22, 'Belize', 501),
(23, 'Benin', 229),
(24, 'Bermuda', 1441),
(25, 'Bhutan', 975),
(26, 'Bolivia', 591),
(27, 'Bosnia and Herzegovina', 387),
(28, 'Botswana', 267),
(29, 'Bouvet Island', 0),
(30, 'Brazil', 55),
(31, 'British Indian Ocean Territory', 246),
(32, 'Brunei Darussalam', 673),
(33, 'Bulgaria', 359),
(34, 'Burkina Faso', 226),
(35, 'Burundi', 257),
(36, 'Cambodia', 855),
(37, 'Cameroon', 237),
(38, 'Canada', 1),
(39, 'Cape Verde', 238),
(40, 'Cayman Islands', 1345),
(41, 'Central African Republic', 236),
(42, 'Chad', 235),
(43, 'Chile', 56),
(44, 'China', 86),
(45, 'Christmas Island', 61),
(46, 'Cocos (Keeling) Islands', 672),
(47, 'Colombia', 57),
(48, 'Comoros', 269),
(49, 'Congo', 242),
(50, 'Congo, the Democratic Republic of the', 242),
(51, 'Cook Islands', 682),
(52, 'Costa Rica', 506),
(53, 'Cote D\'Ivoire', 225),
(54, 'Croatia', 385),
(55, 'Cuba', 53),
(56, 'Cyprus', 357),
(57, 'Czech Republic', 420),
(58, 'Denmark', 45),
(59, 'Djibouti', 253),
(60, 'Dominica', 1767),
(61, 'Dominican Republic', 1809),
(62, 'Ecuador', 593),
(63, 'Egypt', 20),
(64, 'El Salvador', 503),
(65, 'Equatorial Guinea', 240),
(66, 'Eritrea', 291),
(67, 'Estonia', 372),
(68, 'Ethiopia', 251),
(69, 'Falkland Islands (Malvinas)', 500),
(70, 'Faroe Islands', 298),
(71, 'Fiji', 679),
(72, 'Finland', 358),
(73, 'France', 33),
(74, 'French Guiana', 594),
(75, 'French Polynesia', 689),
(76, 'French Southern Territories', 0),
(77, 'Gabon', 241),
(78, 'Gambia', 220),
(79, 'Georgia', 995),
(80, 'Germany', 49),
(81, 'Ghana', 233),
(82, 'Gibraltar', 350),
(83, 'Greece', 30),
(84, 'Greenland', 299),
(85, 'Grenada', 1473),
(86, 'Guadeloupe', 590),
(87, 'Guam', 1671),
(88, 'Guatemala', 502),
(89, 'Guinea', 224),
(90, 'Guinea-Bissau', 245),
(91, 'Guyana', 592),
(92, 'Haiti', 509),
(93, 'Heard Island and Mcdonald Islands', 0),
(94, 'Holy See (Vatican City State)', 39),
(95, 'Honduras', 504),
(96, 'Hong Kong', 852),
(97, 'Hungary', 36),
(98, 'Iceland', 354),
(99, 'India', 91),
(100, 'Indonesia', 62),
(101, 'Iran, Islamic Republic of', 98),
(102, 'Iraq', 964),
(103, 'Ireland', 353),
(104, 'Israel', 972),
(105, 'Italy', 39),
(106, 'Jamaica', 1876),
(107, 'Japan', 81),
(108, 'Jordan', 962),
(109, 'Kazakhstan', 7),
(110, 'Kenya', 254),
(111, 'Kiribati', 686),
(112, 'Korea, Democratic People\'s Republic of', 850),
(113, 'Korea, Republic of', 82),
(114, 'Kuwait', 965),
(115, 'Kyrgyzstan', 996),
(116, 'Lao People\'s Democratic Republic', 856),
(117, 'Latvia', 371),
(118, 'Lebanon', 961),
(119, 'Lesotho', 266),
(120, 'Liberia', 231),
(121, 'Libyan Arab Jamahiriya', 218),
(122, 'Liechtenstein', 423),
(123, 'Lithuania', 370),
(124, 'Luxembourg', 352),
(125, 'Macao', 853),
(126, 'Macedonia, the Former Yugoslav Republic of', 389),
(127, 'Madagascar', 261),
(128, 'Malawi', 265),
(129, 'Malaysia', 60),
(130, 'Maldives', 960),
(131, 'Mali', 223),
(132, 'Malta', 356),
(133, 'Marshall Islands', 692),
(134, 'Martinique', 596),
(135, 'Mauritania', 222),
(136, 'Mauritius', 230),
(137, 'Mayotte', 269),
(138, 'Mexico', 52),
(139, 'Micronesia, Federated States of', 691),
(140, 'Moldova, Republic of', 373),
(141, 'Monaco', 377),
(142, 'Mongolia', 976),
(143, 'Montserrat', 1664),
(144, 'Morocco', 212),
(145, 'Mozambique', 258),
(146, 'Myanmar', 95),
(147, 'Namibia', 264),
(148, 'Nauru', 674),
(149, 'Nepal', 977),
(150, 'Netherlands', 31),
(151, 'Netherlands Antilles', 599),
(152, 'New Caledonia', 687),
(153, 'New Zealand', 64),
(154, 'Nicaragua', 505),
(155, 'Niger', 227),
(156, 'Nigeria', 234),
(157, 'Niue', 683),
(158, 'Norfolk Island', 672),
(159, 'Northern Mariana Islands', 1670),
(160, 'Norway', 47),
(161, 'Oman', 968),
(162, 'Pakistan', 92),
(163, 'Palau', 680),
(164, 'Palestinian Territory, Occupied', 970),
(165, 'Panama', 507),
(166, 'Papua New Guinea', 675),
(167, 'Paraguay', 595),
(168, 'Peru', 51),
(169, 'Philippines', 63),
(170, 'Pitcairn', 0),
(171, 'Poland', 48),
(172, 'Portugal', 351),
(173, 'Puerto Rico', 1787),
(174, 'Qatar', 974),
(175, 'Reunion', 262),
(176, 'Romania', 40),
(177, 'Russian Federation', 70),
(178, 'Rwanda', 250),
(179, 'Saint Helena', 290),
(180, 'Saint Kitts and Nevis', 1869),
(181, 'Saint Lucia', 1758),
(182, 'Saint Pierre and Miquelon', 508),
(183, 'Saint Vincent and the Grenadines', 1784),
(184, 'Samoa', 684),
(185, 'San Marino', 378),
(186, 'Sao Tome and Principe', 239),
(187, 'Saudi Arabia', 966),
(188, 'Senegal', 221),
(189, 'Serbia and Montenegro', 381),
(190, 'Seychelles', 248),
(191, 'Sierra Leone', 232),
(192, 'Singapore', 65),
(193, 'Slovakia', 421),
(194, 'Slovenia', 386),
(195, 'Solomon Islands', 677),
(196, 'Somalia', 252),
(197, 'South Africa', 27),
(198, 'South Georgia and the South Sandwich Islands', 0),
(199, 'Spain', 34),
(200, 'Sri Lanka', 94),
(201, 'Sudan', 249),
(202, 'Suriname', 597),
(203, 'Svalbard and Jan Mayen', 47),
(204, 'Swaziland', 268),
(205, 'Sweden', 46),
(206, 'Switzerland', 41),
(207, 'Syrian Arab Republic', 963),
(208, 'Taiwan, Province of China', 886),
(209, 'Tajikistan', 992),
(210, 'Tanzania, United Republic of', 255),
(211, 'Thailand', 66),
(212, 'Timor-Leste', 670),
(213, 'Togo', 228),
(214, 'Tokelau', 690),
(215, 'Tonga', 676),
(216, 'Trinidad and Tobago', 1868),
(217, 'Tunisia', 216),
(218, 'Turkey', 90),
(219, 'Turkmenistan', 7370),
(220, 'Turks and Caicos Islands', 1649),
(221, 'Tuvalu', 688),
(222, 'Uganda', 256),
(223, 'Ukraine', 380),
(224, 'United Arab Emirates', 971),
(225, 'United Kingdom', 44),
(226, 'United States', 1),
(227, 'United States Minor Outlying Islands', 1),
(228, 'Uruguay', 598),
(229, 'Uzbekistan', 998),
(230, 'Vanuatu', 678),
(231, 'Venezuela', 58),
(232, 'Viet Nam', 84),
(233, 'Virgin Islands, British', 1284),
(234, 'Virgin Islands, U.s.', 1340),
(235, 'Wallis and Futuna', 681),
(236, 'Western Sahara', 212),
(237, 'Yemen', 967),
(238, 'Zambia', 260),
(239, 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `currency_notes`
--

CREATE TABLE `currency_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double(8,3) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_notes`
--

INSERT INTO `currency_notes` (`id`, `name`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, '1/4 Kuwaiti Dinar', 0.250, 'active', '2018-03-15 00:39:10', '2018-03-15 00:57:14'),
(2, '1/2 Kuwaiti Dinar', 0.500, 'active', '2018-03-15 00:40:29', '2018-03-15 00:40:29'),
(3, '1 Kuwaiti Dinar', 1.000, 'active', '2018-03-15 00:42:00', '2018-03-15 00:42:00'),
(4, '5 Kuwaiti Dinar', 5.000, 'active', '2018-03-15 00:42:17', '2018-03-15 00:42:17'),
(5, '10 Kuwaiti Dinar', 10.000, 'active', '2018-03-15 00:43:01', '2018-03-15 00:43:01'),
(6, '20 Kuwaiti Dinar', 20.000, 'active', '2018-03-15 00:43:13', '2018-03-15 00:43:13');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `w_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `title`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cashier', 'will receive payments', 'active', '2018-03-20 04:33:43', '2018-03-20 04:33:43'),
(2, 'Waiters', 'will serve ordered product', 'active', '2018-03-20 04:34:01', '2018-03-20 04:34:01'),
(3, 'System Admin', 'will get all the permissions', 'active', '2018-03-20 04:34:23', '2018-03-20 04:34:23');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percent` double(8,2) NOT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `p_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arabic_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_price` double(8,3) DEFAULT NULL,
  `p_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_24_094547_create-taxs-table', 1),
(4, '2018_02_24_114411_create-discounts-table', 1),
(5, '2018_02_25_050256_create-payment_methods-table', 1),
(6, '2018_02_25_055022_create-orders_type-table', 1),
(7, '2018_02_25_082830_create-product_category-table', 1),
(8, '2018_02_25_102126_create-product_item-table', 1),
(9, '2018_03_01_051414_create_customers_table', 1),
(10, '2018_03_03_105729_create-orders-table', 1),
(11, '2018_03_04_104657_create_departments_table', 1),
(12, '2018_03_04_112413_create_designations_table', 1),
(13, '2018_03_05_042508_create_staff_table', 1),
(14, '2018_03_05_101834_create_attendances_table', 1),
(15, '2018_03_05_101849_create_break_logs_table', 1),
(16, '2018_03_10_054303_create_product_orders_table', 1),
(17, '2018_03_10_112939_create_cash_statuses_table', 1),
(18, '2018_03_15_063209_create_currency_notes_table', 1),
(19, '2018_03_15_105308_create_withdraws_table', 1),
(20, '2018_03_20_070905_create_modules_table', 2),
(21, '2018_03_20_071005_create_permissions_table', 2),
(22, '2018_04_16_025444_create_r_m_categories_table', 3),
(23, '2018_04_16_033305_create_r_m_items_table', 4),
(24, '2018_04_16_045338_create_suppliers_table', 5),
(25, '2018_04_18_082748_create_purchases_table', 6),
(26, '2018_04_18_082851_create_purchased_items_table', 6),
(27, '2018_04_18_092238_create_stocks_table', 6),
(28, '2018_04_18_092326_create_consumptions_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `slug`, `created_at`, `updated_at`) VALUES
(5, 'Order Management', 'manage order, order status, customer, sync offline data', 'order', '2018-03-20 01:20:48', '2018-03-20 01:20:48'),
(6, 'User Management', 'manage staff, permission, Staffs, daily access report', 'user-management', '2018-03-20 01:20:48', '2018-03-20 01:20:48'),
(7, 'System Configuration', 'manage category, Item, Staffs, daily access report', 'system-configuration', '2018-03-20 01:20:48', '2018-03-20 01:20:48'),
(8, 'Reports', 'Checkout order reports, end of day reports', 'reports', '2018-03-20 01:20:48', '2018-03-20 01:20:48'),
(9, 'End of Day', 'End of day report', 'end-of-day', '2018-03-24 08:35:00', '2018-03-24 08:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `sub_total` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `csk_id` bigint(11) NOT NULL,
  `tips` float(8,2) DEFAULT NULL,
  `cmp` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_type_id`, `persone_number`, `table_id`, `method_id`, `user_id`, `customer_id`, `sub_total`, `total`, `paid`, `discount`, `tax`, `csk_id`, `tips`, `cmp`, `created_at`, `updated_at`) VALUES
(28, '1', '1', NULL, '3', 1, NULL, '2.100', '2.1', '2.1', '0', '0', 1121862, 25.00, 0, '2018-03-15 01:05:31', '2018-03-19 01:05:31'),
(29, '1', '1', NULL, '3', 1, NULL, '3.200', '3.2', '3.2', '0', '0', 1121861, 44.00, 0, '2018-03-15 01:06:46', '2018-03-19 01:06:46'),
(30, '1', '1', '5', '3', 1, 2, '2.000', '2', '2', '0', '0', 2474982, 0.00, 0, '2018-03-15 01:07:36', '2018-03-19 01:07:36'),
(31, '1', '1', '5', '3', 1, 2, '4.200', '4.2', '4.2', '0', '0', 2474981, 66.00, 0, '2018-03-16 01:07:53', '2018-03-19 01:07:53'),
(32, '1', '1', '2', '1', NULL, NULL, '2.100', '2.1', '2.1', '0', '0', 3873222, NULL, 0, '2018-03-16 01:19:47', '2018-03-19 01:19:47'),
(33, '1', '1', '2', '1', NULL, NULL, '3.000', '3', '3', '0', '0', 3873221, NULL, 0, '2018-03-16 01:19:48', '2018-03-19 01:19:48'),
(34, '1', '1', NULL, '1', 1, NULL, '12.000', '12.000', '12.000', '0.000', '0.000', 18919, 0.00, 0, '2018-03-17 01:20:19', '2018-03-19 01:20:19'),
(35, '1', '1', NULL, '1', NULL, NULL, '6.200', '6.200', '6.200', '0.000', '0.000', 96750, NULL, 0, '2018-03-17 01:33:51', '2018-03-19 01:33:51'),
(36, '1', '1', '4', '1', 1, 2, '5.200', '5.200', '5.200', '0.000', '0.000', 116780, NULL, 0, '2018-03-17 01:33:52', '2018-03-19 01:33:52'),
(37, '1', '1', '2', '1', NULL, NULL, '6.600', '6.600', '6.600', '0.000', '0.000', 631183, NULL, 0, '2018-03-18 01:33:53', '2018-03-19 01:33:53'),
(38, '1', '1', '6', '1', NULL, NULL, '6.200', '6.200', '6.200', '0.000', '0.000', 922462, NULL, 0, '2018-03-18 01:35:38', '2018-03-19 01:35:38'),
(39, '1', '1', NULL, '1', NULL, NULL, '13.250', '13.250', '13.250', '0.000', '0.000', 27254, NULL, 0, '2018-03-18 01:41:57', '2018-03-19 01:41:57'),
(40, '1', '1', NULL, '1', 1, NULL, '6.200', '6.200', '6.200', '0.000', '0.000', 196726, NULL, 0, '2018-03-18 01:41:59', '2018-03-19 01:41:59'),
(41, '1', '1', '5', '1', 1, 2, '6.200', '6.200', '6.200', '0.000', '0.000', 225086, NULL, 0, '2018-03-19 01:42:00', '2018-03-19 01:42:00'),
(42, '1', '1', NULL, '1', 1, NULL, '5.200', '5.200', '5.200', '0.000', '0.000', 365350, 0.00, 0, '2018-03-19 01:42:45', '2018-03-19 01:42:45'),
(43, '1', '1', '5', '1', 1, 1, '7.200', '7.200', '7.200', '0.000', '0.000', 381661, 0.00, 0, '2018-03-19 01:43:02', '2018-03-19 01:43:02'),
(44, '1', '1', '3', '1', 1, NULL, '8.500', '8.500', '8.500', '0.000', '0.000', 678643, NULL, 0, '2018-03-19 05:41:39', '2018-03-19 05:41:39'),
(45, '1', '1', '3', '1', 1, NULL, '7.350', '7.350', '7.350', '0.000', '0.000', 982219, 0.00, 0, '2018-03-19 22:43:02', '2018-03-19 22:43:02'),
(46, '1', '1', '3', '1', 1, NULL, '7.700', '7.700', '7.700', '0.000', '0.000', 592701, 0.00, 0, '2018-03-19 22:53:13', '2018-03-19 22:53:13'),
(47, '1', '1', '4', '1', 1, NULL, '7.300', '7.300', '7.300', '0.000', '0.000', 605797, 0.00, 0, '2018-03-19 22:53:26', '2018-03-19 22:53:26'),
(48, '1', '1', '5', '1', 1, NULL, '3.350', '3.350', '3.350', '0.000', '0.000', 614267, 0.00, 0, '2018-03-19 22:53:34', '2018-03-19 22:53:34'),
(49, '1', '1', '6', '1', 1, NULL, '7.350', '7.350', '7.350', '0.000', '0.000', 631019, 0.00, 0, '2018-03-19 22:53:51', '2018-03-19 22:53:51'),
(50, '1', '1', '7', '1', 1, NULL, '7.350', '7.350', '7.350', '0.000', '0.000', 656980, 0.00, 0, '2018-03-19 22:54:17', '2018-03-19 22:54:17'),
(51, '1', '1', '2', '1', 1, NULL, '3.200', '3.200', '3.200', '0.000', '0.000', 668332, 0.00, 0, '2018-03-19 22:54:28', '2018-03-19 22:54:28'),
(52, '1', '1', '2', '1', 1, NULL, '2.000', '2.000', '2.000', '0.000', '0.000', 681940, 0.00, 0, '2018-03-20 22:54:42', '2018-03-19 22:54:42'),
(53, '1', '1', NULL, '1', 1, NULL, '4.200', '4.200', '4.200', '0.000', '0.000', 633294, 0.00, 0, '2018-03-20 22:07:13', '2018-03-21 22:07:13'),
(54, '1', '1', '2', '1', 1, NULL, '10.250', '10.250', '10.250', '0.000', '0.000', 662475, 0.00, 0, '2018-03-20 22:24:23', '2018-03-21 22:24:23'),
(55, '1', '1', '3', '1', 1, NULL, '5.350', '5.350', '5.350', '0.000', '0.000', 680132, 0.00, 0, '2018-03-21 22:24:40', '2018-03-21 22:24:40'),
(56, '1', '1', '3', '1', 1, NULL, '3.400', '3.400', '3.400', '0.000', '0.000', 695504, 0.00, 0, '2018-03-21 22:24:56', '2018-03-21 22:24:56'),
(57, '1', '1', '3', '1', 1, NULL, '4.500', '4.500', '4.500', '0.000', '0.000', 729824, 0.00, 0, '2018-03-21 22:25:30', '2018-03-21 22:25:30'),
(58, '1', '1', '3', '1', 1, NULL, '6.450', '6.450', '6.450', '0.000', '0.000', 745362, 0.00, 0, '2018-03-21 22:25:45', '2018-03-21 22:25:45'),
(59, '1', '1', NULL, '1', 1, NULL, '3.100', '3.100', '3.100', '0.000', '0.000', 554080, 0.00, 0, '2018-03-22 05:02:34', '2018-03-22 05:02:34'),
(60, '1', '1', NULL, '1', 1, NULL, '4.950', '4.950', '4.950', '0.000', '0.000', 607856, 0.00, 0, '2018-03-22 05:03:28', '2018-03-22 05:03:28'),
(61, '1', '1', '3', '1', 1, NULL, '5.850', '5.850', '5.850', '0.000', '0.000', 155316, 0.00, 0, '2018-03-24 08:15:56', '2018-03-24 08:15:56'),
(62, '1', '1', '3', '1', 1, NULL, '2.500', '2.500', '2.500', '0.000', '0.000', 309161, 0.00, 0, '2018-03-24 08:18:29', '2018-03-24 08:18:29'),
(63, '1', '1', NULL, '2', 1, NULL, '5.000', '5.000', '5.000', '0.000', '0.000', 798634, 0.00, 0, '2018-03-24 08:26:39', '2018-03-24 08:26:39'),
(64, '1', '1', NULL, '2', 1, NULL, '6.000', '6.000', '6.000', '0.000', '0.000', 675072, 0.00, 0, '2018-03-25 02:27:55', '2018-03-25 02:27:55'),
(65, '1', '1', NULL, '2', 1, NULL, '6.000', '6.000', '6.000', '0.000', '0.000', 217264, 0.00, 0, '2018-03-27 05:10:17', '2018-03-27 05:10:17'),
(66, '1', '1', NULL, '2', 1, NULL, '6.300', '6.300', '6.300', '0.000', '0.000', 557247, 0.00, 0, '2018-04-01 08:02:37', '2018-04-01 08:02:37'),
(67, '1', '1', NULL, '2', 1, NULL, '12.250', '12.250', '12.250', '0.000', '0.000', 73772, 0.00, 0, '2018-04-05 04:24:34', '2018-04-05 04:24:34'),
(68, '1', '1', NULL, '2', 1, NULL, '11.500', '11.500', '11.500', '0.000', '0.000', 171291, 0.00, 0, '2018-04-07 06:26:11', '2018-04-07 06:26:11'),
(69, '1', '1', NULL, '2', 1, NULL, '7.950', '7.950', '7.950', '0.000', '0.000', 184510, 0.00, 0, '2018-04-07 06:26:25', '2018-04-07 06:26:25'),
(70, '1', '1', NULL, '2', 1, NULL, '9.450', '9.450', '9.450', '0.000', '0.000', 195304, 0.00, 0, '2018-04-07 06:26:35', '2018-04-07 06:26:35'),
(71, '1', '1', NULL, '2', 1, NULL, '8.350', '8.350', '8.350', '0.000', '0.000', 205817, 0.00, 0, '2018-04-07 06:26:46', '2018-04-07 06:26:46'),
(72, '1', '1', NULL, '2', 1, NULL, '7.000', '7.000', '7.000', '0.000', '0.000', 215834, 0.00, 0, '2018-04-07 06:26:56', '2018-04-07 06:26:56'),
(73, '1', '1', NULL, '2', 1, NULL, '2.200', '2.200', '2.200', '0.000', '0.000', 224147, 0.00, 0, '2018-04-07 06:27:04', '2018-04-07 06:27:04'),
(74, '1', '1', NULL, '2', 1, NULL, '7.350', '7.350', '7.350', '0.000', '0.000', 332240, 0.00, 0, '2018-04-07 06:28:52', '2018-04-07 06:28:52'),
(75, '1', '1', NULL, '2', 1, NULL, '7.000', '7.000', '7.000', '0.000', '0.000', 343595, 0.00, 0, '2018-04-07 06:29:04', '2018-04-07 06:29:04'),
(76, '1', '1', NULL, '2', 1, NULL, '4.200', '4.200', '4.200', '0.000', '0.000', 464971, NULL, 0, '2018-04-07 07:05:31', '2018-04-07 07:05:31'),
(77, '1', '1', NULL, '2', 1, NULL, '7.300', '7.300', '7.300', '0.000', '0.000', 479257, NULL, 0, '2018-04-07 07:05:32', '2018-04-07 07:05:32'),
(78, '1', '1', NULL, '2', 1, NULL, '6.750', '5.000', '5.000', '1.750', '0.000', 420948, 0.00, 1, '2018-04-07 07:37:01', '2018-04-07 07:37:01'),
(79, '1', '1', NULL, '2', 1, NULL, '9.800', '9.000', '9.000', '0.800', '0.000', 898194, 0.00, 1, '2018-04-07 07:44:58', '2018-04-07 07:44:58'),
(80, '1', '1', NULL, '2', 1, NULL, '3.100', '3.100', '3.100', '0.000', '0.000', 87265, 0.00, 0, '2018-04-12 01:24:47', '2018-04-12 01:24:47'),
(81, '1', '1', NULL, '2', 1, NULL, '4.450', '4.450', '4.450', '0.000', '0.000', 102570, 0.00, 0, '2018-04-12 01:25:03', '2018-04-12 01:25:03'),
(82, '1', '1', NULL, '2', 1, NULL, '7.750', '7.750', '7.750', '0.000', '0.000', 115136, 0.00, 0, '2018-04-12 01:25:15', '2018-04-12 01:25:15'),
(83, '1', '1', NULL, '2', 1, NULL, '7.350', '7.350', '7.350', '0.000', '0.000', 800936, 0.00, 0, '2018-04-12 06:36:41', '2018-04-12 06:36:41'),
(84, '1', '1', NULL, '2', 1, NULL, '3.350', '3.350', '3.350', '0.000', '0.000', 881777, 0.00, 0, '2018-04-12 06:38:02', '2018-04-12 06:38:02'),
(85, '1', '1', NULL, '2', 1, NULL, '10.900', '8.720', '8.720', '2.180', '0.000', 625607, 0.00, 1, '2018-04-21 21:07:06', '2018-04-21 21:07:06'),
(86, '1', '1', NULL, '2', 1, NULL, '6.000', '5.400', '5.400', '0.600', '0.000', 731309, 0.00, 1, '2018-04-21 21:08:51', '2018-04-21 21:08:51'),
(87, '2', '1', '3', '2', 1, 8, '2.850', '2.565', '2.565', '0.285', '0.000', 783390, 0.00, 1, '2018-04-21 21:09:44', '2018-04-21 21:09:44'),
(88, '2', '1', NULL, '2', 1, 9, '5.600', '5.040', '5.040', '0.560', '0.000', 620352, 0.00, 0, '2018-04-27 21:33:40', '2018-04-27 21:33:40'),
(89, '2', '1', NULL, '2', 1, 10, '3.200', '2.880', '2.880', '0.320', '0.000', 725922, 0.00, 0, '2018-04-27 21:35:26', '2018-04-27 21:35:26'),
(90, '1', '1', NULL, '2', 1, NULL, '14.550', '13.095', '13.095', '1.455', '0.000', 246972, 0.00, 0, '2018-05-05 02:14:07', '2018-05-05 02:14:07'),
(91, '1', '1', '3', '2', 1, NULL, '16.500', '14.025', '14.025', '2.475', '0.000', 274878, 0.00, 1, '2018-05-05 02:14:35', '2018-05-05 02:14:35'),
(92, '1', '1', '3', '2', 1, NULL, '13.100', '11.135', '11.135', '1.965', '0.000', 298655, 0.00, 0, '2018-05-05 02:14:59', '2018-05-05 02:14:59'),
(93, '1', '1', '3', '2', 1, NULL, '10.750', '10.213', '10.213', '0.537', '0.000', 326298, 0.00, 1, '2018-05-05 02:15:26', '2018-05-05 02:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `order_types`
--

CREATE TABLE `order_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dis_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_types`
--

INSERT INTO `order_types` (`id`, `name`, `dis_type`, `tax_type`, `title`, `is_active`) VALUES
(1, 'Take Away', '', '', 'Take Away', 'active'),
(2, 'Delivery', '', '', 'Delivery', 'active'),
(4, 'Dine In', '', '', 'Dine In', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `method_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `method_name`, `title`, `is_active`) VALUES
(1, 'Cash', 'Cash on Order', 'active'),
(2, 'Check', 'Check Number', 'active'),
(3, 'Visa', 'Visa Number', 'active'),
(4, 'Master Card', 'Master Card Number', 'active'),
(5, 'American Express', 'American Express Number', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `module_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, '2018-03-24 10:00:00', '2018-03-24 10:00:00'),
(2, 1, 6, 1, '2018-03-24 10:00:00', '2018-03-24 10:00:00'),
(3, 1, 7, 1, '2018-03-24 10:00:00', '2018-03-24 10:00:00'),
(4, 1, 8, 1, '2018-03-24 10:00:00', '2018-03-24 10:00:00'),
(5, 1, 9, 1, '2018-03-24 10:00:00', '2018-03-24 10:00:00'),
(6, 8, 5, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(7, 8, 6, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(8, 8, 7, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(9, 8, 8, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(10, 8, 9, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(11, 9, 5, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(12, 9, 6, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(13, 9, 7, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(14, 9, 8, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(15, 9, 9, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(16, 10, 5, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(17, 10, 6, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(18, 10, 7, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(19, 10, 8, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30'),
(20, 10, 9, 0, '2018-03-24 07:36:30', '2018-03-24 07:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_categoy`
--

CREATE TABLE `product_categoy` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `csk_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `item_price` double(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` double(8,2) NOT NULL,
  `discount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `csk_id`, `item_id`, `cat_id`, `item_price`, `qty`, `total`, `discount`, `created_at`, `updated_at`) VALUES
(26, 985251, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:13:06', '2018-03-19 00:13:06'),
(27, 985251, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:13:06', '2018-03-19 00:13:06'),
(28, 15692, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:13:36', '2018-03-19 00:13:36'),
(29, 15692, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:13:36', '2018-03-19 00:13:36'),
(30, 15692, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:13:36', '2018-03-19 00:13:36'),
(31, 32255, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 00:13:53', '2018-03-19 00:13:53'),
(32, 32255, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:13:53', '2018-03-19 00:13:53'),
(33, 468085, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:21:08', '2018-03-19 00:21:08'),
(34, 468085, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:21:08', '2018-03-19 00:21:08'),
(35, 468085, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:21:09', '2018-03-19 00:21:09'),
(36, 525058, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:22:05', '2018-03-19 00:22:05'),
(37, 525058, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:22:05', '2018-03-19 00:22:05'),
(38, 525058, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:22:06', '2018-03-19 00:22:06'),
(39, 1180, 12, 12, 3.75, 2, 7.50, 0.00, '2018-03-19 00:30:01', '2018-03-19 00:30:01'),
(40, 1180, 11, 12, 3.50, 2, 7.00, 0.00, '2018-03-19 00:30:01', '2018-03-19 00:30:01'),
(41, 1180, 10, 12, 0.75, 3, 2.25, 0.00, '2018-03-19 00:30:02', '2018-03-19 00:30:02'),
(42, 1180, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:30:02', '2018-03-19 00:30:02'),
(43, 122623, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:38:03', '2018-03-19 00:38:03'),
(44, 122623, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:38:04', '2018-03-19 00:38:04'),
(45, 197915, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:38:04', '2018-03-19 00:38:04'),
(46, 197915, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:38:04', '2018-03-19 00:38:04'),
(47, 744419, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:46:31', '2018-03-19 00:46:31'),
(48, 744419, 3, 11, 1.10, 3, 3.30, 0.00, '2018-03-19 00:46:31', '2018-03-19 00:46:31'),
(49, 744419, 5, 11, 1.00, 4, 4.00, 0.00, '2018-03-19 00:46:32', '2018-03-19 00:46:32'),
(50, 764842, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:46:32', '2018-03-19 00:46:32'),
(51, 764842, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:46:32', '2018-03-19 00:46:32'),
(52, 764842, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:46:32', '2018-03-19 00:46:32'),
(53, 857131, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:46:33', '2018-03-19 00:46:33'),
(54, 857131, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:46:33', '2018-03-19 00:46:33'),
(55, 857131, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:46:33', '2018-03-19 00:46:33'),
(56, 413320, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 00:53:33', '2018-03-19 00:53:33'),
(57, 413320, 5, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:53:34', '2018-03-19 00:53:34'),
(58, 413320, 4, 11, 1.00, 6, 6.00, 0.00, '2018-03-19 00:53:34', '2018-03-19 00:53:34'),
(59, 452328, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 00:54:13', '2018-03-19 00:54:13'),
(60, 452328, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:54:13', '2018-03-19 00:54:13'),
(61, 452328, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:54:13', '2018-03-19 00:54:13'),
(62, 534457, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 00:55:35', '2018-03-19 00:55:35'),
(63, 534457, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:55:35', '2018-03-19 00:55:35'),
(64, 534457, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 00:55:35', '2018-03-19 00:55:35'),
(65, 556657, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 00:55:57', '2018-03-19 00:55:57'),
(66, 556657, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 00:55:57', '2018-03-19 00:55:57'),
(67, 556657, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:55:57', '2018-03-19 00:55:57'),
(68, 667727, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 00:57:48', '2018-03-19 00:57:48'),
(69, 667727, 4, 11, 1.00, 4, 4.00, 0.00, '2018-03-19 00:57:48', '2018-03-19 00:57:48'),
(70, 689224, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 00:58:09', '2018-03-19 00:58:09'),
(71, 689224, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:58:09', '2018-03-19 00:58:09'),
(72, 689224, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:58:10', '2018-03-19 00:58:10'),
(73, 701302, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:58:21', '2018-03-19 00:58:21'),
(74, 701302, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 00:58:22', '2018-03-19 00:58:22'),
(75, 1121862, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 01:05:32', '2018-03-19 01:05:32'),
(76, 1121862, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:05:32', '2018-03-19 01:05:32'),
(77, 1121861, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:06:46', '2018-03-19 01:06:46'),
(78, 1121861, 3, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:06:47', '2018-03-19 01:06:47'),
(79, 2474982, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:07:36', '2018-03-19 01:07:36'),
(80, 2474982, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:07:36', '2018-03-19 01:07:36'),
(81, 2474981, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:07:53', '2018-03-19 01:07:53'),
(82, 2474981, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:07:53', '2018-03-19 01:07:53'),
(83, 3873222, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 01:19:47', '2018-03-19 01:19:47'),
(84, 3873222, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:19:47', '2018-03-19 01:19:47'),
(85, 3873221, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:19:48', '2018-03-19 01:19:48'),
(86, 3873221, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:19:48', '2018-03-19 01:19:48'),
(87, 18919, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:20:19', '2018-03-19 01:20:19'),
(88, 18919, 5, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 01:20:20', '2018-03-19 01:20:20'),
(89, 18919, 6, 11, 1.10, 5, 5.50, 0.00, '2018-03-19 01:20:20', '2018-03-19 01:20:20'),
(90, 18919, 10, 12, 0.75, 2, 1.50, 0.00, '2018-03-19 01:20:20', '2018-03-19 01:20:20'),
(91, 96750, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:33:51', '2018-03-19 01:33:51'),
(92, 96750, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:33:51', '2018-03-19 01:33:51'),
(93, 96750, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:33:52', '2018-03-19 01:33:52'),
(94, 116780, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:33:52', '2018-03-19 01:33:52'),
(95, 116780, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:33:52', '2018-03-19 01:33:52'),
(96, 116780, 3, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:33:52', '2018-03-19 01:33:52'),
(97, 631183, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-19 01:33:53', '2018-03-19 01:33:53'),
(98, 631183, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:33:53', '2018-03-19 01:33:53'),
(99, 631183, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:33:53', '2018-03-19 01:33:53'),
(100, 631183, 10, 12, 0.75, 2, 1.50, 0.00, '2018-03-19 01:33:54', '2018-03-19 01:33:54'),
(101, 922462, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:35:38', '2018-03-19 01:35:38'),
(102, 922462, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:35:39', '2018-03-19 01:35:39'),
(103, 922462, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:35:39', '2018-03-19 01:35:39'),
(104, 27254, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:41:58', '2018-03-19 01:41:58'),
(105, 27254, 12, 12, 3.75, 1, 3.75, 0.00, '2018-03-19 01:41:58', '2018-03-19 01:41:58'),
(106, 27254, 11, 12, 3.50, 2, 7.00, 0.00, '2018-03-19 01:41:58', '2018-03-19 01:41:58'),
(107, 27254, 10, 12, 0.75, 2, 1.50, 0.00, '2018-03-19 01:41:58', '2018-03-19 01:41:58'),
(108, 196726, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 01:41:59', '2018-03-19 01:41:59'),
(109, 196726, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 01:41:59', '2018-03-19 01:41:59'),
(110, 196726, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:42:00', '2018-03-19 01:42:00'),
(111, 225086, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:42:00', '2018-03-19 01:42:00'),
(112, 225086, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:42:00', '2018-03-19 01:42:00'),
(113, 225086, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:42:00', '2018-03-19 01:42:00'),
(114, 365350, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:42:46', '2018-03-19 01:42:46'),
(115, 365350, 5, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 01:42:46', '2018-03-19 01:42:46'),
(116, 381661, 4, 11, 1.00, 2, 2.00, 0.00, '2018-03-19 01:43:02', '2018-03-19 01:43:02'),
(117, 381661, 3, 11, 1.10, 2, 2.20, 0.00, '2018-03-19 01:43:02', '2018-03-19 01:43:02'),
(118, 381661, 5, 11, 1.00, 3, 3.00, 0.00, '2018-03-19 01:43:02', '2018-03-19 01:43:02'),
(119, 678643, 26, 14, 2.25, 1, 2.25, 0.00, '2018-03-19 05:41:39', '2018-03-19 05:41:39'),
(120, 678643, 27, 14, 2.25, 1, 2.25, 0.00, '2018-03-19 05:41:39', '2018-03-19 05:41:39'),
(121, 678643, 28, 14, 1.75, 1, 1.75, 0.00, '2018-03-19 05:41:40', '2018-03-19 05:41:40'),
(122, 678643, 29, 14, 2.25, 1, 2.25, 0.00, '2018-03-19 05:41:40', '2018-03-19 05:41:40'),
(123, 982219, 16, 13, 2.45, 1, 2.45, 0.00, '2018-03-19 22:43:02', '2018-03-19 22:43:02'),
(124, 982219, 17, 13, 2.45, 1, 2.45, 0.00, '2018-03-19 22:43:03', '2018-03-19 22:43:03'),
(125, 982219, 18, 13, 2.45, 1, 2.45, 0.00, '2018-03-19 22:43:03', '2018-03-19 22:43:03'),
(126, 592701, 14, 13, 3.25, 1, 3.25, 0.00, '2018-03-19 22:53:13', '2018-03-19 22:53:13'),
(127, 592701, 15, 13, 2.00, 1, 2.00, 0.00, '2018-03-19 22:53:13', '2018-03-19 22:53:13'),
(128, 592701, 16, 13, 2.45, 1, 2.45, 0.00, '2018-03-19 22:53:13', '2018-03-19 22:53:13'),
(129, 605797, 29, 14, 2.25, 1, 2.25, 0.00, '2018-03-19 22:53:26', '2018-03-19 22:53:26'),
(130, 605797, 35, 14, 2.25, 1, 2.25, 0.00, '2018-03-19 22:53:26', '2018-03-19 22:53:26'),
(131, 605797, 30, 14, 1.55, 1, 1.55, 0.00, '2018-03-19 22:53:26', '2018-03-19 22:53:26'),
(132, 605797, 36, 14, 1.25, 1, 1.25, 0.00, '2018-03-19 22:53:27', '2018-03-19 22:53:27'),
(133, 614267, 77, 16, 1.60, 1, 1.60, 0.00, '2018-03-19 22:53:34', '2018-03-19 22:53:34'),
(134, 614267, 76, 16, 1.75, 1, 1.75, 0.00, '2018-03-19 22:53:35', '2018-03-19 22:53:35'),
(135, 631019, 73, 16, 2.95, 1, 2.95, 0.00, '2018-03-19 22:53:51', '2018-03-19 22:53:51'),
(136, 631019, 74, 16, 1.85, 1, 1.85, 0.00, '2018-03-19 22:53:51', '2018-03-19 22:53:51'),
(137, 631019, 75, 16, 2.55, 1, 2.55, 0.00, '2018-03-19 22:53:52', '2018-03-19 22:53:52'),
(138, 656980, 73, 16, 2.95, 1, 2.95, 0.00, '2018-03-19 22:54:17', '2018-03-19 22:54:17'),
(139, 656980, 74, 16, 1.85, 1, 1.85, 0.00, '2018-03-19 22:54:17', '2018-03-19 22:54:17'),
(140, 656980, 75, 16, 2.55, 1, 2.55, 0.00, '2018-03-19 22:54:18', '2018-03-19 22:54:18'),
(141, 668332, 1, 11, 1.20, 1, 1.20, 0.00, '2018-03-19 22:54:29', '2018-03-19 22:54:29'),
(142, 668332, 4, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 22:54:29', '2018-03-19 22:54:29'),
(143, 668332, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-19 22:54:29', '2018-03-19 22:54:29'),
(144, 681940, 83, 15, 0.75, 1, 0.75, 0.00, '2018-03-19 22:54:42', '2018-03-19 22:54:42'),
(145, 681940, 82, 15, 1.25, 1, 1.25, 0.00, '2018-03-19 22:54:42', '2018-03-19 22:54:42'),
(146, 633294, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-21 22:07:14', '2018-03-21 22:07:14'),
(147, 633294, 6, 11, 1.10, 2, 2.20, 0.00, '2018-03-21 22:07:14', '2018-03-21 22:07:14'),
(148, 662475, 7, 12, 2.50, 1, 2.50, 0.00, '2018-03-21 22:24:23', '2018-03-21 22:24:23'),
(149, 662475, 8, 12, 5.25, 1, 5.25, 0.00, '2018-03-21 22:24:23', '2018-03-21 22:24:23'),
(150, 662475, 9, 12, 1.75, 1, 1.75, 0.00, '2018-03-21 22:24:24', '2018-03-21 22:24:24'),
(151, 662475, 10, 12, 0.75, 1, 0.75, 0.00, '2018-03-21 22:24:24', '2018-03-21 22:24:24'),
(152, 680132, 80, 15, 1.85, 1, 1.85, 0.00, '2018-03-21 22:24:40', '2018-03-21 22:24:40'),
(153, 680132, 81, 15, 1.50, 1, 1.50, 0.00, '2018-03-21 22:24:41', '2018-03-21 22:24:41'),
(154, 680132, 82, 15, 1.25, 1, 1.25, 0.00, '2018-03-21 22:24:41', '2018-03-21 22:24:41'),
(155, 680132, 83, 15, 0.75, 1, 0.75, 0.00, '2018-03-21 22:24:41', '2018-03-21 22:24:41'),
(156, 695504, 1, 11, 1.20, 1, 1.20, 0.00, '2018-03-21 22:24:56', '2018-03-21 22:24:56'),
(157, 695504, 2, 11, 1.10, 1, 1.10, 0.00, '2018-03-21 22:24:56', '2018-03-21 22:24:56'),
(158, 695504, 3, 11, 1.10, 1, 1.10, 0.00, '2018-03-21 22:24:57', '2018-03-21 22:24:57'),
(159, 729824, 29, 14, 2.25, 1, 2.25, 0.00, '2018-03-21 22:25:30', '2018-03-21 22:25:30'),
(160, 729824, 35, 14, 2.25, 1, 2.25, 0.00, '2018-03-21 22:25:30', '2018-03-21 22:25:30'),
(161, 745362, 1, 11, 1.20, 1, 1.20, 0.00, '2018-03-21 22:25:46', '2018-03-21 22:25:46'),
(162, 745362, 8, 12, 5.25, 1, 5.25, 0.00, '2018-03-21 22:25:46', '2018-03-21 22:25:46'),
(163, 554080, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-22 05:02:34', '2018-03-22 05:02:34'),
(164, 554080, 5, 11, 1.00, 2, 2.00, 0.00, '2018-03-22 05:02:34', '2018-03-22 05:02:34'),
(165, 607856, 76, 16, 1.75, 1, 1.75, 0.00, '2018-03-22 05:03:28', '2018-03-22 05:03:28'),
(166, 607856, 77, 16, 1.60, 2, 3.20, 0.00, '2018-03-22 05:03:28', '2018-03-22 05:03:28'),
(167, 155316, 5, 11, 1.00, 1, 1.00, 0.00, '2018-03-24 08:15:56', '2018-03-24 08:15:56'),
(168, 155316, 6, 11, 1.10, 1, 1.10, 0.00, '2018-03-24 08:15:56', '2018-03-24 08:15:56'),
(169, 155316, 12, 12, 3.75, 1, 3.75, 0.00, '2018-03-24 08:15:56', '2018-03-24 08:15:56'),
(170, 309161, 9, 12, 1.75, 1, 1.75, 0.00, '2018-03-24 08:18:29', '2018-03-24 08:18:29'),
(171, 309161, 10, 12, 0.75, 1, 0.75, 0.00, '2018-03-24 08:18:30', '2018-03-24 08:18:30'),
(177, 675072, 5, 11, 1.00, 3, 3.00, 0.00, '2018-03-25 02:27:55', '2018-03-25 02:27:55'),
(178, 675072, 4, 11, 1.00, 3, 3.00, 0.00, '2018-03-25 02:27:55', '2018-03-25 02:27:55'),
(179, 798634, 9, 12, 1.75, 2, 3.50, 0.00, '2018-03-24 08:26:39', '2018-03-24 08:26:39'),
(180, 798634, 10, 12, 0.75, 2, 1.50, 0.00, '2018-03-24 08:26:39', '2018-03-24 08:26:39'),
(181, 217264, 1, 11, 1.20, 5, 6.00, 0.00, '2018-03-27 05:10:17', '2018-03-27 05:10:17'),
(182, 557247, 2, 11, 1.10, 1, 1.10, 0.00, '2018-04-01 08:02:38', '2018-04-01 08:02:38'),
(183, 557247, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-01 08:02:38', '2018-04-01 08:02:38'),
(184, 557247, 4, 11, 1.00, 1, 1.00, 0.00, '2018-04-01 08:02:38', '2018-04-01 08:02:38'),
(185, 557247, 5, 11, 1.00, 2, 2.00, 0.00, '2018-04-01 08:02:38', '2018-04-01 08:02:38'),
(186, 557247, 6, 11, 1.10, 1, 1.10, 0.00, '2018-04-01 08:02:39', '2018-04-01 08:02:39'),
(187, 73772, 17, 13, 2.45, 1, 2.45, 0.00, '2018-04-05 04:24:34', '2018-04-05 04:24:34'),
(188, 73772, 18, 13, 2.45, 2, 4.90, 0.00, '2018-04-05 04:24:35', '2018-04-05 04:24:35'),
(189, 73772, 24, 13, 2.45, 2, 4.90, 0.00, '2018-04-05 04:24:35', '2018-04-05 04:24:35'),
(190, 171291, 5, 11, 1.00, 1, 1.00, 0.00, '2018-04-07 06:26:12', '2018-04-07 06:26:12'),
(191, 171291, 11, 12, 3.50, 3, 10.50, 0.00, '2018-04-07 06:26:12', '2018-04-07 06:26:12'),
(192, 184510, 9, 12, 1.75, 2, 3.50, 0.00, '2018-04-07 06:26:25', '2018-04-07 06:26:25'),
(193, 184510, 15, 13, 2.00, 1, 2.00, 0.00, '2018-04-07 06:26:25', '2018-04-07 06:26:25'),
(194, 184510, 17, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 06:26:26', '2018-04-07 06:26:26'),
(195, 195304, 13, 12, 1.75, 1, 1.75, 0.00, '2018-04-07 06:26:35', '2018-04-07 06:26:35'),
(196, 195304, 14, 13, 3.25, 1, 3.25, 0.00, '2018-04-07 06:26:36', '2018-04-07 06:26:36'),
(197, 195304, 15, 13, 2.00, 1, 2.00, 0.00, '2018-04-07 06:26:36', '2018-04-07 06:26:36'),
(198, 195304, 16, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 06:26:36', '2018-04-07 06:26:36'),
(199, 205817, 6, 11, 1.10, 1, 1.10, 0.00, '2018-04-07 06:26:46', '2018-04-07 06:26:46'),
(200, 205817, 12, 12, 3.75, 1, 3.75, 0.00, '2018-04-07 06:26:46', '2018-04-07 06:26:46'),
(201, 205817, 11, 12, 3.50, 1, 3.50, 0.00, '2018-04-07 06:26:46', '2018-04-07 06:26:46'),
(202, 215834, 9, 12, 1.75, 1, 1.75, 0.00, '2018-04-07 06:26:56', '2018-04-07 06:26:56'),
(203, 215834, 8, 12, 5.25, 1, 5.25, 0.00, '2018-04-07 06:26:56', '2018-04-07 06:26:56'),
(204, 224147, 2, 11, 1.10, 1, 1.10, 0.00, '2018-04-07 06:27:04', '2018-04-07 06:27:04'),
(205, 224147, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-07 06:27:05', '2018-04-07 06:27:05'),
(206, 332240, 18, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 06:28:53', '2018-04-07 06:28:53'),
(207, 332240, 24, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 06:28:53', '2018-04-07 06:28:53'),
(208, 332240, 17, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 06:28:53', '2018-04-07 06:28:53'),
(209, 343595, 13, 12, 1.75, 1, 1.75, 0.00, '2018-04-07 06:29:04', '2018-04-07 06:29:04'),
(210, 343595, 14, 13, 3.25, 1, 3.25, 0.00, '2018-04-07 06:29:04', '2018-04-07 06:29:04'),
(211, 343595, 15, 13, 2.00, 1, 2.00, 0.00, '2018-04-07 06:29:04', '2018-04-07 06:29:04'),
(212, 464971, 4, 11, 1.00, 1, 1.00, 0.00, '2018-04-07 07:05:32', '2018-04-07 07:05:32'),
(213, 464971, 10, 12, 0.75, 1, 0.75, 0.00, '2018-04-07 07:05:32', '2018-04-07 07:05:32'),
(214, 464971, 16, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:05:32', '2018-04-07 07:05:32'),
(215, 479257, 6, 11, 1.10, 1, 1.10, 0.00, '2018-04-07 07:05:33', '2018-04-07 07:05:33'),
(216, 479257, 12, 12, 3.75, 1, 3.75, 0.00, '2018-04-07 07:05:33', '2018-04-07 07:05:33'),
(217, 479257, 18, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:05:33', '2018-04-07 07:05:33'),
(218, 420948, 29, 14, 2.25, 1, 2.25, 0.00, '2018-04-07 07:37:01', '2018-04-07 07:37:01'),
(219, 420948, 35, 14, 2.25, 2, 4.50, 0.00, '2018-04-07 07:37:01', '2018-04-07 07:37:01'),
(220, 898194, 16, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:44:58', '2018-04-07 07:44:58'),
(221, 898194, 22, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:44:59', '2018-04-07 07:44:59'),
(222, 898194, 23, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:44:59', '2018-04-07 07:44:59'),
(223, 898194, 24, 13, 2.45, 1, 2.45, 0.00, '2018-04-07 07:44:59', '2018-04-07 07:44:59'),
(224, 87265, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-12 01:24:48', '2018-04-12 01:24:48'),
(225, 87265, 4, 11, 1.00, 1, 1.00, 0.00, '2018-04-12 01:24:48', '2018-04-12 01:24:48'),
(226, 87265, 5, 11, 1.00, 1, 1.00, 0.00, '2018-04-12 01:24:48', '2018-04-12 01:24:48'),
(227, 102570, 2, 11, 1.10, 1, 1.10, 0.00, '2018-04-12 01:25:03', '2018-04-12 01:25:03'),
(228, 102570, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-12 01:25:03', '2018-04-12 01:25:03'),
(229, 102570, 29, 14, 2.25, 1, 2.25, 0.00, '2018-04-12 01:25:03', '2018-04-12 01:25:03'),
(230, 115136, 74, 16, 1.85, 1, 1.85, 0.00, '2018-04-12 01:25:15', '2018-04-12 01:25:15'),
(231, 115136, 75, 16, 2.55, 1, 2.55, 0.00, '2018-04-12 01:25:16', '2018-04-12 01:25:16'),
(232, 115136, 76, 16, 1.75, 1, 1.75, 0.00, '2018-04-12 01:25:16', '2018-04-12 01:25:16'),
(233, 115136, 77, 16, 1.60, 1, 1.60, 0.00, '2018-04-12 01:25:16', '2018-04-12 01:25:16'),
(234, 800936, 16, 13, 2.45, 1, 2.45, 0.00, '2018-04-12 06:36:41', '2018-04-12 06:36:41'),
(235, 800936, 17, 13, 2.45, 1, 2.45, 0.00, '2018-04-12 06:36:41', '2018-04-12 06:36:41'),
(236, 800936, 18, 13, 2.45, 1, 2.45, 0.00, '2018-04-12 06:36:42', '2018-04-12 06:36:42'),
(237, 881777, 77, 16, 1.60, 1, 1.60, 0.00, '2018-04-12 06:38:02', '2018-04-12 06:38:02'),
(238, 881777, 76, 16, 1.75, 1, 1.75, 0.00, '2018-04-12 06:38:02', '2018-04-12 06:38:02'),
(239, 625607, 6, 11, 1.10, 2, 2.20, 0.00, '2018-04-21 21:07:06', '2018-04-21 21:07:06'),
(240, 625607, 5, 11, 1.00, 2, 2.00, 0.00, '2018-04-21 21:07:06', '2018-04-21 21:07:06'),
(241, 625607, 11, 12, 3.50, 1, 3.50, 0.00, '2018-04-21 21:07:06', '2018-04-21 21:07:06'),
(242, 625607, 10, 12, 0.75, 1, 0.75, 0.00, '2018-04-21 21:07:07', '2018-04-21 21:07:07'),
(243, 625607, 16, 13, 2.45, 1, 2.45, 0.00, '2018-04-21 21:07:07', '2018-04-21 21:07:07'),
(244, 731309, 1, 11, 1.20, 5, 6.00, 0.00, '2018-04-21 21:08:51', '2018-04-21 21:08:51'),
(245, 783390, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-21 21:09:44', '2018-04-21 21:09:44'),
(246, 783390, 4, 11, 1.00, 1, 1.00, 0.00, '2018-04-21 21:09:44', '2018-04-21 21:09:44'),
(247, 783390, 10, 12, 0.75, 1, 0.75, 0.00, '2018-04-21 21:09:44', '2018-04-21 21:09:44'),
(248, 620352, 1, 11, 1.20, 1, 1.20, 0.00, '2018-04-27 21:33:41', '2018-04-27 21:33:41'),
(249, 620352, 2, 11, 1.10, 2, 2.20, 0.00, '2018-04-27 21:33:41', '2018-04-27 21:33:41'),
(250, 620352, 3, 11, 1.10, 2, 2.20, 0.00, '2018-04-27 21:33:41', '2018-04-27 21:33:41'),
(251, 725922, 3, 11, 1.10, 1, 1.10, 0.00, '2018-04-27 21:35:26', '2018-04-27 21:35:26'),
(252, 725922, 2, 11, 1.10, 1, 1.10, 0.00, '2018-04-27 21:35:27', '2018-04-27 21:35:27'),
(253, 725922, 4, 11, 1.00, 1, 1.00, 0.00, '2018-04-27 21:35:27', '2018-04-27 21:35:27'),
(254, 246972, 2, 11, 1.10, 1, 1.10, 0.00, '2018-05-05 02:14:07', '2018-05-05 02:14:07'),
(255, 246972, 8, 12, 5.25, 1, 5.25, 0.00, '2018-05-05 02:14:07', '2018-05-05 02:14:07'),
(256, 246972, 14, 13, 3.25, 1, 3.25, 0.00, '2018-05-05 02:14:08', '2018-05-05 02:14:08'),
(257, 246972, 16, 13, 2.45, 1, 2.45, 0.00, '2018-05-05 02:14:08', '2018-05-05 02:14:08'),
(258, 246972, 10, 12, 0.75, 1, 0.75, 0.00, '2018-05-05 02:14:08', '2018-05-05 02:14:08'),
(259, 246972, 9, 12, 1.75, 1, 1.75, 0.00, '2018-05-05 02:14:08', '2018-05-05 02:14:08'),
(260, 274878, 21, 13, 2.95, 1, 2.95, 0.00, '2018-05-05 02:14:35', '2018-05-05 02:14:35'),
(261, 274878, 27, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:14:35', '2018-05-05 02:14:35'),
(262, 274878, 28, 14, 1.75, 1, 1.75, 0.00, '2018-05-05 02:14:36', '2018-05-05 02:14:36'),
(263, 274878, 29, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:14:36', '2018-05-05 02:14:36'),
(264, 274878, 30, 14, 1.55, 1, 1.55, 0.00, '2018-05-05 02:14:36', '2018-05-05 02:14:36'),
(265, 274878, 36, 14, 1.25, 1, 1.25, 0.00, '2018-05-05 02:14:36', '2018-05-05 02:14:36'),
(266, 274878, 35, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:14:36', '2018-05-05 02:14:36'),
(267, 274878, 34, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:14:37', '2018-05-05 02:14:37'),
(268, 298655, 18, 13, 2.45, 1, 2.45, 0.00, '2018-05-05 02:14:59', '2018-05-05 02:14:59'),
(269, 298655, 20, 13, 2.25, 1, 2.25, 0.00, '2018-05-05 02:14:59', '2018-05-05 02:14:59'),
(270, 298655, 19, 13, 5.45, 1, 5.45, 0.00, '2018-05-05 02:15:00', '2018-05-05 02:15:00'),
(271, 298655, 21, 13, 2.95, 1, 2.95, 0.00, '2018-05-05 02:15:00', '2018-05-05 02:15:00'),
(272, 326298, 14, 13, 3.25, 1, 3.25, 0.00, '2018-05-05 02:15:27', '2018-05-05 02:15:27'),
(273, 326298, 26, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:15:27', '2018-05-05 02:15:27'),
(274, 326298, 27, 14, 2.25, 1, 2.25, 0.00, '2018-05-05 02:15:27', '2018-05-05 02:15:27'),
(275, 326298, 28, 14, 1.75, 1, 1.75, 0.00, '2018-05-05 02:15:28', '2018-05-05 02:15:28'),
(276, 326298, 36, 14, 1.25, 1, 1.25, 0.00, '2018-05-05 02:15:28', '2018-05-05 02:15:28');

-- --------------------------------------------------------

--
-- Table structure for table `purchased_items`
--

CREATE TABLE `purchased_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchase_id` int(10) UNSIGNED NOT NULL,
  `rmc_id` int(10) UNSIGNED NOT NULL,
  `rmi_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `total_cost` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r_m_categories`
--

CREATE TABLE `r_m_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r_m_items`
--

CREATE TABLE `r_m_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `rmc_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `measurement` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warning_level` int(3) DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `blood_group` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `d_o_b` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `id_card` text COLLATE utf8mb4_unicode_ci,
  `cv` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT 'inactive',
  `password` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `department_id`, `designation_id`, `country_id`, `employee_id`, `name`, `email`, `phone`, `f_name`, `m_name`, `job_type`, `city`, `address`, `blood_group`, `gender`, `d_o_b`, `join_date`, `image`, `id_card`, `cv`, `status`, `password`, `created_at`, `updated_at`) VALUES
(1, 0, 3, 18, '1', 'Admin', 'admin@gmail.com', '12345671234', 'Admins Father', 'Admins Mother', 'Full Time', 'Sugondha', 'Khilgaon', 'A+', 'Male', '11.11.1982', '01.03.2018', '/uploads/staff/image/1520243390.jfif', '/uploads/staff/id_card/1520243390.png', '/uploads/staff/cv/1520243390.docx', 'active', 123456, '2018-03-05 03:49:50', '2018-03-21 02:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `rmi_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxs`
--

CREATE TABLE `taxs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percent` double(8,2) NOT NULL,
  `is_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '.',
  `is_rule` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N/A',
  `permission` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `tbl_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N/A',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_type`, `is_rule`, `permission`, `tbl_title`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$G0jrpqceCdLaH3ed46CEgeYriQPUG.lIpK7pkOy8iIjHb36k0Z4l2', '.', 'N/A', '0', 'N/A', 'PpsO9Z8vIVaV6xCdLMQxW6WHeVFk4BoW6ApPmpugtDb6CJNlqdUMbG3JRpvn', '2018-03-01 04:10:59', '2018-03-21 02:11:32'),
(2, 'Table 1', '1521118079@gmail.com', '12345678', 'table', 'N/A', '0', 'Table 1', NULL, NULL, NULL),
(3, 'Table 2', '1521118085@gmail.com', '12345678', 'table', 'N/A', '0', 'Table 2', NULL, NULL, NULL),
(4, 'Table 3', '1521118092@gmail.com', '12345678', 'table', 'N/A', '0', 'Table 3', NULL, NULL, NULL),
(5, 'Table 4', '1521118099@gmail.com', '12345678', 'table', 'N/A', '0', 'Table 4', NULL, NULL, NULL),
(6, 'Table 5', '1521118105@gmail.com', '12345678', 'table', 'N/A', '0', 'Table 5', NULL, NULL, NULL),
(7, 'Table 6', '1521141818@gmail.com', '12345678', 'table', 'N/A', '0', 'Table6', 'hSfQnqUsRwn0lPfhBh6LL2xIQxU0sSkPJ1dpIM05YpMT6NWNf6VIqub4H9Wg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `break_logs`
--
ALTER TABLE `break_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_statuses`
--
ALTER TABLE `cash_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumptions`
--
ALTER TABLE `consumptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_notes`
--
ALTER TABLE `currency_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_cat_id_foreign` (`cat_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_types`
--
ALTER TABLE `order_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categoy`
--
ALTER TABLE `product_categoy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchased_items`
--
ALTER TABLE `purchased_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r_m_categories`
--
ALTER TABLE `r_m_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r_m_items`
--
ALTER TABLE `r_m_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_m_items_rmc_id_foreign` (`rmc_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxs`
--
ALTER TABLE `taxs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `break_logs`
--
ALTER TABLE `break_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cash_statuses`
--
ALTER TABLE `cash_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `consumptions`
--
ALTER TABLE `consumptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `currency_notes`
--
ALTER TABLE `currency_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `order_types`
--
ALTER TABLE `order_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_categoy`
--
ALTER TABLE `product_categoy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT for table `purchased_items`
--
ALTER TABLE `purchased_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `r_m_categories`
--
ALTER TABLE `r_m_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `r_m_items`
--
ALTER TABLE `r_m_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `taxs`
--
ALTER TABLE `taxs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `product_categoy` (`id`);

--
-- Constraints for table `r_m_items`
--
ALTER TABLE `r_m_items`
  ADD CONSTRAINT `r_m_items_rmc_id_foreign` FOREIGN KEY (`rmc_id`) REFERENCES `r_m_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
