<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->middleware('guest');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Route::get('/dashboard', function () {
//    return view('pos.dashboard');
//});

Route::get('site.manifest_v1', function()
{
    return Response::make(View::make('site.manifest_v1')->render(), 200,
        array(
            'Content-Type' => 'text/cache-manifest',
            'Cache-Control' => 'no-cache, must-revalidate, no-store',
            'Expires' => 'Sat, 26 Jul 1997 01:00:00 GMT'
        )
    );
});


Route::group(['middleware' => ['auth']], function () {

    /*
     * ATTENDANCE & BREAK LOG
     */

    Route::get('/res/attendance-and-break-monitor', ['as' => 'attendance.break', 'uses' => 'HrPayrollController@attendance_system']);
    Route::get('/res/record-in-time', ['as' => 'emp.in.time', 'uses' => 'HrPayrollController@RecordInTime']);
    Route::get('/res/record-out-time', ['as' => 'emp.out.time', 'uses' => 'HrPayrollController@RecordOutTime']);
    Route::get('/res/record-start-break', ['as' => 'emp.start.break', 'uses' => 'HrPayrollController@RecordStartBreak']);
    Route::get('/res/record-end-break', ['as' => 'emp.end.break', 'uses' => 'HrPayrollController@RecordEndBreak']);

    Route::get('/user-settings', ['as' => 'user.settings', 'uses' => 'HrPayrollController@UserSettings']);
    Route::post('/update-password', ['as' => 'update.password', 'uses' => 'HrPayrollController@UpdatePassword']);
    Route::post('/update-profile-info', ['as' => 'update.profile', 'uses' => 'HrPayrollController@UpdateProfileInfo']);


    //Route::group(['middleware' => ['AttendanceMiddleware']], function (){

        Route::get('res/order', ['as' => 'order', 'uses' => 'ManageController@order'])->middleware('order');
        Route::get('res/tables', 'ManageController@tables')->middleware('configuration');
        Route::post('res/create-table', 'ManageController@create_table');
        Route::get('res/delete-table/{id}', 'ManageController@delete_table');
        Route::get('/res/taxs', 'ManageController@taxs')->middleware('configuration');
        Route::post('/res/create-taxt', 'ManageController@create_taxt');
        Route::get('res/delete-tax/{id}', 'ManageController@delete_tax');
        Route::get('res/discount', 'ManageController@discount')->middleware('configuration');
        Route::post('res/create-discount', 'ManageController@create_discount');
        Route::get('res/delete-discount/{id}', 'ManageController@delete_discount');
        Route::get('res/order-type', 'ManageController@order_type')->middleware('configuration');
        Route::get('res/payment-methods', 'ManageController@payment_methods')->middleware('configuration');
        Route::post('res/create-methods', 'ManageController@create_methods');
        Route::get('/res/delete-method/{id}', 'ManageController@delete_method');
        Route::post('/res/create-order-type', 'ManageController@create_order_type')->middleware('configuration');
        Route::get('res/delete-order_type/{id}', 'ManageController@delete_order_type');
        Route::get('/res/pro-category', 'ManageController@product_category')->middleware('configuration');
        Route::post('res/create-product-category', 'ManageController@create_product_category');
        Route::get('res/delete-product-category/{id}', 'ManageController@delete_product_category');
        Route::get('res/pro-item', 'ManageController@pro_item')->middleware('configuration');
        Route::post('/res/create-product-item', 'ManageController@create_product_item');
        Route::get('res/delete-product-item/{id}', 'ManageController@delete_product_item');

        /** Condiment Routes */
        Route::get('condiments','CondimentController@create');
        Route::post('condiment/store','CondimentController@store');
        Route::post('condiment/update','CondimentController@update');
        Route::get('condiment/destroy/{id}','CondimentController@destroy');
        Route::get('res/get-condiments','CondimentController@getCondiments');
        Route::get('res/condiment-list','CondimentController@condimentList');
        Route::get('/res/update-condiment-status/{id}', ['as' => 'product.status.update', 'uses' => 'CondimentController@update_product_status']);

        Route::post('/res/create-customer', ['as' => 'customer.store', 'uses' => 'ManageController@create_customer']);
        Route::get('/res/customer-list', ['as' => 'customer.list', 'uses' => 'ManageController@customer_list'])->middleware('order');
        Route::post('/res/update-customer-info', ['as' => 'customer.update', 'uses' => 'ManageController@update_customer_info']);
        Route::get('/res/update-customers-status/{id}', ['as' => 'customer.status.update', 'uses' => 'ManageController@update_customers_status']);

        Route::get('/res/product-list', ['as' => 'product.list', 'uses' => 'ManageController@product_list'])->middleware('configuration');
        Route::post('/res/product-info-update', ['as' => 'product.update', 'uses' => 'ManageController@product_info_update']);
        Route::get('/res/update-product-status/{id}', ['as' => 'product.status.update', 'uses' => 'ManageController@update_product_status']);

        Route::post('/res/category-info-update', ['as' => 'category.update', 'uses' => 'ManageController@category_info_update']);
        Route::get('/res/update-category-status/{id}', ['as' => 'category.status.update', 'uses' => 'ManageController@update_category_status']);

        Route::post('/res/table-info-update', ['as' => 'table.update', 'uses' => 'ManageController@table_info_update']);
        Route::post('/res/tax-info-update', ['as' => 'tax.update', 'uses' => 'ManageController@tax_info_update']);
        Route::get('/res/update-tax-status/{id}', ['as' => 'tax.status.update', 'uses' => 'ManageController@update_tax_status']);

        Route::post('/res/discount-info-update', ['as' => 'discount.update', 'uses' => 'ManageController@discount_info_update']);
        Route::get('/res/update-discount-status/{id}', ['as' => 'discount.status.update', 'uses' => 'ManageController@update_discount_status']);

        Route::post('/res/payment-info-update', ['as' => 'payment.update', 'uses' => 'ManageController@payment_info_update']);
        Route::get('/res/update-payment-status/{id}', ['as' => 'payment.status.update', 'uses' => 'ManageController@update_payment_status']);

        Route::get('/res/edit-order-type/{id}', ['as' => 'edit.order-type', 'uses' => 'ManageController@edit_order_type']);
        Route::post('/res/update-order-type', ['as' => 'update.order-type', 'uses' => 'ManageController@update_order_type']);
        Route::get('/res/update-order-type-status/{id}', ['as' => 'order-type.status.update', 'uses' => 'ManageController@update_order_type_status']);


        /*
         * HR & PAYROLL ROUTES
         */

        Route::get('/res/department-list', ['as' => 'departments', 'uses' => 'HrPayrollController@Departments'])->middleware('user_management');
        Route::post('/res/create-department', ['as' => 'store.department', 'uses' => 'HrPayrollController@StoreDepartment']);
        Route::post('/res/update-department', ['as' => 'update.department', 'uses' => 'HrPayrollController@UpdateDepartment']);
        Route::get('/res/update-department-status/{id}', ['as' => 'update.department.status', 'uses' => 'HrPayrollController@UpdateDeptStatus']);
        Route::get('/res/delete-department/{id}', ['as' => 'delete.department', 'uses' => 'HrPayrollController@DeleteDeptStatus']);


        Route::get('/res/designation-list', ['as' => 'designations', 'uses' => 'HrPayrollController@Designations'])->middleware('user_management');
        Route::post('/res/create-designation', ['as' => 'store.designation', 'uses' => 'HrPayrollController@StoreDesignation']);
        Route::post('/res/update-designation', ['as' => 'update.designation', 'uses' => 'HrPayrollController@UpdateDesignation']);
        Route::get('/res/update-designation-status/{id}', ['as' => 'update.designation.status', 'uses' => 'HrPayrollController@UpdateDesignationStatus']);
        Route::get('/res/delete-designation/{id}', ['as' => 'delete.designation', 'uses' => 'HrPayrollController@DeleteDesignationStatus']);


        Route::get('/res/add-staff', ['as' => 'add.staff', 'uses' => 'HrPayrollController@CreateStaff'])->middleware('user_management');
        Route::post('/res/create-staff', ['as' => 'store.staff', 'uses' => 'HrPayrollController@StoreStaff']);
        Route::get('/res/get-emp-id/{id}', ['as' => 'check.staff', 'uses' => 'HrPayrollController@CheckStaffIdExist']);
        Route::get('/res/staff-list', ['as' => 'staffs', 'uses' => 'HrPayrollController@manageStaffs'])->middleware('user_management');
        Route::get('/res/update-staff-status/{id}', ['as' => 'update.staff.status', 'uses' => 'HrPayrollController@UpdateStaffStatus']);
        Route::get('/res/delete-staff/{id}', ['as' => 'delete.staff', 'uses' => 'HrPayrollController@DeleteStaff']);
        Route::get('/res/edit-staff/{id}', ['as' => 'edit.staff', 'uses' => 'HrPayrollController@EditStaff']);
        Route::post('/res/update-staff', ['as' => 'update.staff', 'uses' => 'HrPayrollController@UpdateStaff']);
        Route::get('/res/staff-profile/{id}', ['as' => 'staff.profile', 'uses' => 'HrPayrollController@StaffProfile'])->middleware('user_management');

        Route::get('/res/access-history', ['as' => 'access.history', 'uses' => 'HrPayrollController@AccessHistory'])->middleware('report');
        Route::get('/res/break-log/{date}/{id}', ['as' => 'break.log', 'uses' => 'HrPayrollController@GetBreakLog']);

        Route::get('res/report', ['as' => 'get.report', 'uses' => 'ManageController@report'])->middleware('report');
        Route::get('res/item-report', ['as' => 'item.report', 'uses' => 'ManageController@ItemReport'])->middleware('report');
        Route::get('res/pdf_report', ['as' => 'pdf', 'uses' => 'ManageController@PdfReport'])->middleware('report');

        Route::get('/orders-status', ['as' => 'orders.status', 'uses' => 'ManageController@GetOrderStatus'])->middleware('order');

        Route::get('/end-of-day', ['as' => 'end.of.day', 'uses' => 'ManageController@EndOfDay'])->middleware('end-of-day');
        Route::get('/currency-settings', ['as' => 'currency.settings', 'uses' => 'ManageController@CurrencySettings'])->middleware('configuration');
        Route::post('/save-note', ['as' => 'save.note', 'uses' => 'ManageController@StoreNote']);
        Route::post('/update-note', ['as' => 'note.update', 'uses' => 'ManageController@UpdateNote']);
        Route::get('/update-note-status/{id}', ['as' => 'update.note.status', 'uses' => 'ManageController@update_note_status']);
        Route::get('/delete-note/{id}', ['as' => 'delete.note', 'uses' => 'ManageController@DeleteNote']);

        Route::post('/calculate-start-day-cash', ['as' => 'calculate.cash', 'uses' => 'ManageController@CalculateStartDayCash'])->middleware('report');
        Route::post('/store-withdraw-cash', ['as' => 'store.withdraw', 'uses' => 'ManageController@StoreWithdrawCash']);
        Route::post('/store-end-day-cash', ['as' => 'store.end.day.cash', 'uses' => 'ManageController@StoreEndDayCash']);


        /*
         * INVENTORY AND PURCHASE
         */

        Route::get('/raw-materials-category', ['as' => 'r.m.category', 'uses' => 'InventoryController@R_M_Category']);

        Route::post('/raw-materials-category', ['as' => 'r.m.category', 'uses' => 'InventoryController@CreateR_M_Category']);
        Route::post('/update-raw-materials-category', ['as' => 'update.r.m.category', 'uses' => 'InventoryController@UpdateR_M_Category']);
        Route::get('/update-status-raw-materials-category/{id}', ['as' => 'status.update.r.m.category', 'uses' => 'InventoryController@updateRMCatStatus']);
        Route::get('/delete-raw-materials-category/{id}', ['as' => 'delete.r.m.category', 'uses' => 'InventoryController@DeleteR_M_Category']);

        Route::get('/raw-materials-item', ['as' => 'r.m.items', 'uses' => 'InventoryController@R_M_Items']);

        Route::post('/raw-materials-item', ['as' => 'r.m.item', 'uses' => 'InventoryController@CreateR_M_Items']);
        Route::post('/update-raw-materials-item', ['as' => 'update.r.m.item', 'uses' => 'InventoryController@UpdateR_M_Items']);
        Route::get('/update-status-raw-materials-item/{id}', ['as' => 'status.update.r.m.item', 'uses' => 'InventoryController@updateRMItemStatus']);
        Route::get('/delete-raw-materials-item/{id}', ['as' => 'delete.r.m.item', 'uses' => 'InventoryController@DeleteR_M_Item']);

        // SUPPLIER

        Route::get('/Suppliers', ['as' => 'suppliers', 'uses' => 'InventoryController@Suppliers']);

        Route::post('/Suppliers', ['as' => 'create.supplier', 'uses' => 'InventoryController@CreateSupplier']);
        Route::post('/update-supplier', ['as' => 'update.supplier', 'uses' => 'InventoryController@UpdateSupplier']);
        Route::get('/update-supplier/{id}', ['as' => 'status.supplier', 'uses' => 'InventoryController@updateSupplierStatus']);
        Route::get('/delete-supplier/{id}', ['as' => 'delete.supplier', 'uses' => 'InventoryController@DeleteSupplier']);

        // PURCHASE REQUISITION
        Route::get('/purchase-requisition', ['as' => 'purchase.requisition', 'uses' => 'InventoryController@PurchaseRequisition']);
        Route::post('/purchase-requisition', ['as' => 'purchase.requisition', 'uses' => 'InventoryController@SavePurchaseRequisition']);

        Route::get('/purchase-requisition-list', ['as' => 'purchase.requisition.list', 'uses' => 'InventoryController@PurchaseRequisitionList']);
        Route::get('/change-status-purchase-requisition/{id}/{status}', ['as' => 'purchase.requisition.status', 'uses' => 'InventoryController@ChangeStatusPurchaseRequisition']);

        // PURCHASE

        Route::get('/add-purchase', ['as' => 'purchase', 'uses' => 'InventoryController@Purchase']);
        Route::get('/purchase-history', ['as' => 'purchase.history', 'uses' => 'InventoryController@PurchaseHistory']);

        Route::get('/get-item-by-category/{id}', ['as' => 'get.item.by.id', 'uses' => 'InventoryController@GetItemByCatID']);
        Route::get('/get-item-unit-by-id/{id}', ['as' => 'get.unit.by.item..id', 'uses' => 'InventoryController@GetUnitByItemId']);
        Route::get('/get-item-info-by-id/{id}', ['as' => 'get.info.by.item..id', 'uses' => 'InventoryController@GetItemInfoByItemId']);

        Route::get('/consumption', ['as' => 'consumption', 'uses' => 'InventoryController@Consumption']);
        Route::post('/stock-update', ['as' => 'stock.update', 'uses' => 'InventoryController@StockUpdate']);

        Route::get('/consumption-history', ['as' => 'consumption.history', 'uses' => 'InventoryController@ConsumptionHistory']);

        Route::get('/inventory-information', ['as' => 'inventory', 'uses' => 'InventoryController@Inventory']);
        Route::get('/inventory/search', ['as' => 'inventory.search', 'uses' => 'InventoryController@InventorySearch']);

        Route::post('/store-purchased-info', ['as' => 'store.purchase', 'uses' => 'InventoryController@StorePurchasedInfo']);
        Route::post('/store-purchased-item-info', ['as' => 'store.purchased.item', 'uses' => 'InventoryController@StorePurchasedItemInfo']);
        Route::post('/store-purchased-item-info', ['as' => 'store.purchased.item', 'uses' => 'InventoryController@StorePurchasedItemInfo']);

        Route::get('/purchase-report', ['as' => 'purchase.report', 'uses' => 'InventoryController@PurchaseReport']);
        Route::get('/consumption-report', ['as' => 'consumption.report', 'uses' => 'InventoryController@ConsumptionReport']);

        Route::post('requisition-call','InventoryController@requisitionCall');


        /*
         * Access Role System
         */
        Route::get('/user-permissions', ['as' => 'user.permissions', 'uses' => 'ManageController@UserPermission'])->middleware('user_management');
        Route::post('/save-user-permission/{id}', ['as' => 'user.permission', 'uses' => 'ManageController@SaveUserPermission']);

        /*JavaScript*/

        Route::get('get-closed-report',['as' => 'get.closed.report', 'uses'=>'ManageController@GetClosedReport']);

        Route::get('order-get-support/{id}', 'ManageController@order_get_support');
        Route::get('get-table-info/{id}', 'ManageController@get_table_info');

        Route::get('loard-order-type', 'ManageController@loard_order_type');
        Route::get('delete-close-check/{id}', 'ManageController@delete_close_check');

        Route::post('save_resource', 'ManageController@save_resource');
        Route::post('update_resource/{id}', 'ManageController@update_resource');

        Route::get('last-order-data', 'ManageController@last_order_data');
        Route::get('get-customer', 'ManageController@get_customer');
        Route::get('get-close-check', 'ManageController@get_close_check');
        Route::get('get-staff-passcode', 'ManageController@get_staff_passcode');

        Route::post('/save-product-order', ['as' => 'store.product.order', 'uses' => 'ManageController@SaveProductOrderInfo']);
        Route::post('/update-product-order', ['as' => 'update.product.order', 'uses' => 'ManageController@updateProductOrderInfo']);

        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/get_products', 'HomeController@get_products')->name('get_products');

        Route::get('/pos_report', 'HomeController@pos_report')->name('pos_report');

        Route::get('/get-weekly-graph', ['as' => 'get.weekly.graph', 'uses' => 'HomeController@GetWeeklyGraph']);
        Route::get('/get-monthly-graph', ['as' => 'get.monthly.graph', 'uses' => 'HomeController@GetMonthlyGraph']);
        Route::get('/get-yearly-graph', ['as' => 'get.yearly.graph', 'uses' => 'HomeController@GetYearlyGraph']);


        Route::get('/system-settings', ['as' => 'system.settings', 'uses' => 'SettingsController@showSettingsForm']);
        Route::post('/system-settings', ['as' => 'system.settings', 'uses' => 'SettingsController@save_system_settings']);
        Route::post('/save-system-logo', ['as' => 'save.logo', 'uses' => 'SettingsController@save_system_logo']);

        Route::get('/expense-categories', ['as' => 'expense.categories', 'uses' => 'ExpenseController@AllExpenseCategory']);
        Route::post('/save-expense-category', ['as' => 'save.expense.category', 'uses' => 'ExpenseController@SaveExpenseCategory']);
        Route::post('/update-expense-category', ['as' => 'update.expense.category', 'uses' => 'ExpenseController@UpdateExpenseCategory']);
        Route::get('/delete-expense-category/{id}', ['as' => 'delete.expense.category', 'uses' => 'ExpenseController@DeleteExpenseCategory']);
        Route::get('/update-exp-cat-status/{id}', ['as' => 'status.update.exp.cat', 'uses' => 'ExpenseController@updateExpenseCategoryStatus']);

        Route::get('/expenses', ['as' => 'expenses', 'uses' => 'ExpenseController@AllExpenses']);
        Route::post('/save-expense', ['as' => 'save.expense', 'uses' => 'ExpenseController@SaveExpense']);
        Route::post('/update-expense', ['as' => 'update.expense', 'uses' => 'ExpenseController@UpdateExpense']);
        Route::get('/delete-expense/{id}', ['as' => 'delete.expense', 'uses' => 'ExpenseController@DeleteExpense']);

        Route::get('/expense-report', ['as' => 'expense.report', 'uses' => 'ExpenseController@ExpenseReport']);
        Route::get('/pdf-expense-report', ['as' => 'pdf.expense.report', 'uses' => 'ExpenseController@PdfReportOfExpense']);

        Route::get('/profit-loss-report', ['as' => 'profit.loss.report', 'uses' => 'ManageController@profit_loss_calculation']);
        Route::get('/pdf-profit-report', ['as' => 'profit.loss.pdf.report', 'uses' => 'ManageController@PDF_profit_loss_calculation']);

         Route::get('/kitchen-information', ['as' => 'kitchen', 'uses' => 'ManageController@kitchen']);
         
         Route::get('get_kitchen_check', 'ManageController@get_kitchen_check');
         Route::get('kitchen-finish/{id}', 'ManageController@kitchen_finish');
         Route::get('kitchen-processing/{id}', 'ManageController@kitchen_processing');
         Route::post('save_kitechen', 'ManageController@save_kitechen');
         Route::post('/save-kitchen-product-order', ['as' => 'store.kitchen.product.order', 'uses' => 'ManageController@SaveKitchenProductOrderInfo']);
         Route::get('res/kitchen_list', 'ManageController@kitchen_list');
         
    //});

Route::post('get-customer-for-bill','ManageController@getCustomer');

Route::get('merge-customer',function(){
    $customers = \App\Customer::all();
    foreach($customers as $customer){
        $id = $customer->id;
        $area = 'Area:';
        $street = 'Street Name:';
        $street_no = 'Street No:';
        $block = 'Block:';
        $building = 'Building';
        $floor = 'Floor:';
        $flat = 'Flat:';

        $data['area'] = strstr(substr($customer->address,strpos($customer->address,$area)+strlen($area)+1),',',true);
        $data['street'] = strstr(substr($customer->address,strpos($customer->address,$street)+strlen($street)+1),',',true);
        $data['street_no'] = strstr(substr($customer->address,strpos($customer->address,$street_no)+strlen($street_no)+1),',',true);
        $data['block'] = strstr(substr($customer->address,strpos($customer->address,$block)+strlen($block)+1),',',true);
        $data['building'] = strstr(substr($customer->address,strpos($customer->address,$building)+strlen($building)+2),',',true);
        $data['floor'] = strstr(substr($customer->address,strpos($customer->address,$floor)+strlen($floor)+1),',',true);
        $data['flat'] = substr($customer->address,strpos($customer->address,$flat)+strlen($flat)+1);

        $cust = \App\Customer::query()->findOrFail($id);
        $cust->update($data);

    }

    dd('customer database updated');

});


//Clear Cache facade value:
    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');

        $notification = array(
            'message' => 'Cache Cleared!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    });


//Clear Cache facade value:
    Route::get('/clear-view', function() {
        Artisan::call('view:clear');

        $notification = array(
            'message' => 'View Cleared!', 'alert-type' => 'success'
        );

        return back()->with($notification);
    });


});

