var is_clear_type = localStorage.getItem('is_clear_ok23');

if (is_clear_type === null) {
    prev_hold = [];
    localStorage.setItem('all_hold_orders', JSON.stringify(prev_hold));
    localStorage.setItem('is_clear_ok23',true);
    localStorage.setItem('hold_id','1');
    localStorage.setItem('order_id','1');

    var c_date = new Date();
    var c_day = c_date.getDate();
    localStorage.setItem('c_day',c_day);
}

function set_hold_default(){
    var get_day = localStorage.getItem('c_day');
    var today = new Date();
    var dd = today.getDate();
    if(Number(get_day) != Number(dd)){
        localStorage.setItem('hold_id','1');
        var c_date = new Date();
        var c_day = c_date.getDate();
        localStorage.setItem('c_day',c_day);
    }

}

set_hold_default();





all_cash_remove();

function all_cash_remove(){
    cart = [];
    saveCart();
    loadOrderTypeData();

    $('#inputDiscount').val('');
    $('#input-discount').val('');


}


saveTableName(' ');


$('#product_search').select2({
    placeholder: 'Search Item',
    ajax: {
        url: '../get_products',
        dataType: 'json',
        data: function (params) {

            // Query parameters will be ?search=[term]&type=public
            return {
                name: params.term,
                type: 'public'
            };
        },
        processResults: function (data) {

            var programs = [];
            data.items.forEach(function (item) {
                programs.push({
                    'id': item.id,
                    'name': item.name,
                    'arabic_name': item.arabic_name,
                    'price': item.price,
                    'text': 'Name: ' + item.name +  ', Price: ' + item.price
                })
            });

            // Tranforms the top-level key of the response object from 'items' to 'results'

            return {
                results: programs
            };
        }
    }
});



$('#product_search').on('select2:select', function (e) {
    var data = e.params.data;
    console.log(data.arabic_name);
    addItemToCart(data.id,data.name, data.price, 1, 0, data.arabic_name, data.condiment);
    displayCart();


    $('#product_search').val(null).trigger('change');
    $('#product_search').select2('open');
});



$('.add-to-cart').click(function(event){
    event.preventDefault();
    id = Number($(this).attr('data-id'));
    name = $(this).attr('data-name');
    condiment = $(this).attr('data-condiment');
    arabic_name = $(this).attr('data-arabic_name');
    price = Number($(this).attr('data-price'));
    //console.log(arabic_name);

    // $(".pos-pro-body-qit-input").keyup(function(){
    //     displayCart();
    // });

    addItemToCart(id,name, price, 1, 0,arabic_name,condiment);
    displayCart();

    // $(".pos-pro-body-qit-input").keyup(function(){
    //     var qty = Number($(this).val());
    //     alert(qty);
    //     var id = Number($(this).attr('data-id'));
    //
    //     var name = $(this).attr('data-name');
    //     var arabic_name = $(this).attr('data-arabic_name');
    //     var condiment = $("#condiment").val();
    //
    //     updateItemToCart(id,name, 0, qty, 0,arabic_name,condiment);
    //     getOrderyTypeData(strogOrderData);
    //     displayCart();
    // });

});



function displayCart(){


    displayHoldCart();

    var cartArray = listCart();
    var output = '';
    var output_hold = '';

    var x = 1;
    for(var i in cartArray){
        var ser = cartArray[i].price > 0 ? x++ : '';
        output  +="<tr>";
        output  +="<th scope='row' width='5.3%'>"+(ser)+"</th>";

        output  +="<td width='30%' class='custom-proce-stye'>"+ cartArray[i].name +"<br/></td>";

        output  +="<td class='text-center cat-price-custon' width='13%'>";
        if(cartArray[i].price > 0 ){
            output  +="<div class='pos-pro-body-qit'>";
            output  +="<button class='subtract-item' data-name='"+cartArray[i].name+"' data-id='"+cartArray[i].id+"'><span class='fa fa-minus'></span></button>";
            output  +="<input type='text'value='"+ cartArray[i].count +"' class='pos-pro-body-qit-input'  data-name='"+cartArray[i].name+"' data-id='"+cartArray[i].id+"'>";
            output  +="<button class='plus-item' data-name='"+cartArray[i].name+"' data-id='"+cartArray[i].id+"'><span class='fa fa-plus '></span></button>";
            output  +="</div>";
        }
        output  +="</td>";
        output  +="<td width='5%' class='text-center custom-class'><div class='pos-pro-list-table-dis'>";
        if(cartArray[i].price > 0){
            output += "<input type='text' class='discount-value-change' data-name='"+cartArray[i].name+"' value='"+cartArray[i].discount+"'>";
        }
        output += "</td>";
        output +="<td widht='10%' class='text-center custom-price'>";
        if(cartArray[i].price > 0){
            output  += ((cartArray[i].price * cartArray[i].count) - cartArray[i].discount).toFixed(3);
        }
        output += "</td>";
        output +="<td class='text-center custom-delect'><a href='#' data-toggle='tooltip' title='Delete' class='delete-item' data-name='"+cartArray[i].name+"'><span class='fa fa-trash'></span></a></td>";
        output +="</tr>";

        output_hold  +="<tr>";
        output_hold  +="<th scope='row' width='2%' >"+(ser)+"</th>";
        output_hold  +="<td width='32%' style='font-size:11px'>"+ cartArray[i].name +"</td>";

        output_hold  +="<td width='17.5%' class='text-center' >";
        output_hold  +="<div class='pos-pro-body-qit'>";
        output_hold  +="<input type='text'value='"+ cartArray[i].count +"' class='pos-pro-body-qit-input'>";
        output_hold  +="</div>";
        output_hold  +="</td>";

        output_hold  +="<td width='15%' class='text-center'>"+ Number((cartArray[i].price)).toFixed(3) +"</td>";
        output_hold  +="<td width='14%' class='text-center'>"+ Number((cartArray[i].price * cartArray[i].count) - cartArray[i].discount).toFixed(3) +"</td>";

        output_hold  +="</tr>";
        output_hold  +="</tr>";
        output_hold  +="</tr>";



    }

    $('#show-cart').html(output);
    $('.select2-search').select2(); // re-run select2 after adding item in cart
    $('#show-cart-hold').html(output_hold);

    $('#total-cart-hold').html(totalCart());

    //$('#total-cart-discount-hold').html(totalCartDiscount().toFixed(3));
    $('#total-cart-discount-hold').html(totalCartDiscount());


    $('#total-cart').html(totalCart());

    $('#count-Cart-hold').html(countCart());
    $('#count-Cart').html(countCart());

    $('#total-cart-discount').html(totalCartDiscount().toFixed(3));

    /*split order function call*/
    splitOrder();
    $storeOrderTypeData = JSON.parse(localStorage.getItem('OrderData'));

    if($storeOrderTypeData != null){
        getOrderyTypeData(strogOrderData);
    }

    $("#grand-total").text(grandTotal());
    $("#grand-total").val(grandTotal());

    $(document).on("change","select",function(){
        $("option[value=" + this.value + "]", this)
            .attr("selected", true).siblings()
            .removeAttr("selected")
    });

    $(".pos-pro-body-qit-input").keyup(function(){
        var qty = Number($(this).val());
        var id = Number($(this).attr('data-id'));

        var name = $(this).attr('data-name');
        var arabic_name = $(this).attr('data-arabic_name');
        var condiment = $("#condiment").val();

        updateItemToCart(id,name, 0, qty, 0,arabic_name,condiment);
        getOrderyTypeData(strogOrderData);
        displayCart();
    });
}

$('#clear-cart').click(function(){
    clearCart();
    $('#inputDiscount').val(0.000);
    $('#input-discount').val(0.000);
    $("#input-discount").text(0.000);
    $('#action_hold').val(0.000);
    $("#tip-amount-input").val(0.000);
    $("#delivery-charge-input").val(0.000);
    $("#grand-total").text(grandTotal());
    $("#grand-total").val(grandTotal());
    getOrderyTypeData(strogOrderData);
});

$('#show-cart').on("click", ".plus-item", function(event){
    var id = Number($(this).attr('data-id'));

    var name = $(this).attr('data-name');
    var arabic_name = $(this).attr('data-arabic_name');
    var condiment = $("#condiment").val();

    addItemToCart(id,name, 0, 1,0,arabic_name,condiment);
    getOrderyTypeData(strogOrderData);
    displayCart();

    $(".pos-pro-body-qit-input").keyup(function(){
        var qty = Number($(this).val());
        var id = Number($(this).attr('data-id'));

        var name = $(this).attr('data-name');
        var arabic_name = $(this).attr('data-arabic_name');
        var condiment = $("#condiment").val();

        updateItemToCart(id,name, 0, qty, 0,arabic_name,condiment);
        getOrderyTypeData(strogOrderData);
        displayCart();
    });

});

$('#show-cart').on("click", ".subtract-item", function(event){
    var id = Number($(this).attr('data-id'));

    var name = $(this).attr('data-name');


    removeItemFromCart(name);
    displayCart();
});

$('#show-cart').on("click", ".delete-item", function(event){
    var name = $(this).attr('data-name');
    removeItemFromCartAll(name);
    displayCart();
});

/************* split Calculator **************/

$('.cal-button-split').click(function() {
    var button_value = $(this).val();

    var tipAmount = $('#tip-amount-input-split').val();
    var deliveryCharge = $('#delivery-charge-input-split').val();
    var latestInput = $('#input-current-amount-split').val();
    var currentValue = latestInput+button_value;
    var paymentValue = $('#payment-total-split').val();

    var paymentChange = parseFloat(currentValue-(paymentValue+deliveryCharge)).toFixed(3);
    $('#input-current-amount-split').val(currentValue);

    var complateChangeValue = parseFloat(paymentChange-(tipAmount+deliveryCharge)).toFixed(3);
    console.log(tipamount);
    $('#payment-change-split').html(complateChangeValue);

    if(complateChangeValue<0){
        $('#confirm-payment-split').attr('disabled','disabled');
    }else{
        $('#confirm-payment-split').attr('disabled',false);
    }



});


$('#input-current-amount-split').keyup(function(){

    var tipAmount = $('#tip-amount-input-split').val();
    var deliveryCharge = $("#delivery-charge-input-split").val();
    var latestInput = $('#input-current-amount-split').val();


    var paymentValue = $('#payment-total-split').val();

    var paymentChange = (Number(latestInput)-Number(paymentValue)).toFixed(3);

    var complateChangeValue = parseFloat(paymentChange-(tipAmount+deliveryCharge)).toFixed(3);

    $('#payment-change-split').html(complateChangeValue);


    if(complateChangeValue<0){
        $('#confirm-payment-split').attr('disabled','disabled');
    }else{
        $('#confirm-payment-split').attr('disabled',false);
    }

});


/* set split bill information in localStorage */
/* added by smartrahat 2019.06.25 4:30 PM */
$('#payment-one-method').on('click','.payment-one-method-type', function(event){
    var data_split_bill_one_method_name = $(this).attr('data-split-bill-one-method-name');
    var data_split_bill_one_method_id = $(this).attr('data-split-bill-one-method-id');
    var data_split_bill_one_method_title = $(this).attr('data-split-bill-one-method-title');

    $('#show-split-bill-one-payment-method-type').html(data_split_bill_one_method_name);
    $('#show-split-bill-one-payment-method-id').val(data_split_bill_one_method_id);

    localStorage.setItem('data_split_bill_one_method_name', data_split_bill_one_method_name);
    localStorage.getItem('orderTypeName');

    localStorage.setItem('data_split_bill_one_method_id', data_split_bill_one_method_id);
    localStorage.getItem('data_split_bill_one_method_id');

    localStorage.setItem('data_split_bill_one_method_title', data_split_bill_one_method_title);
    localStorage.getItem('data_split_bill_one_method_title');

    localStorage.setItem('data_method_id', ''); // clear the original method id
});

$('#payment-two-method').on('click','.payment-two-method-type', function(event){
    var data_split_bill_two_method_name = $(this).attr('data-split-bill-two-method-name');
    var data_split_bill_two_method_id = $(this).attr('data-split-bill-two-method-id');
    var data_split_bill_two_method_title = $(this).attr('data-split-bill-two-method-title');

    $('#show-split-bill-two-payment-method-type').html(data_split_bill_two_method_name);
    $('#show-split-bill-two-payment-method-id').val(data_split_bill_two_method_id);

    localStorage.setItem('data_split_bill_two_method_name', data_split_bill_two_method_name);
    localStorage.getItem('data_split_bill_two_method_name');

    localStorage.setItem('data_split_bill_two_method_id', data_split_bill_two_method_id);
    localStorage.getItem('data_split_bill_two_method_id');

    localStorage.setItem('data_split_bill_two_method_title', data_split_bill_two_method_title);
    localStorage.getItem('data_split_bill_two_method_title');

    localStorage.setItem('data_method_id', ''); // clear the original method id
});
/* setting localStorage ends */

$('#payment-method-split').on('click','.payment-method-type', function(event){
    var data_method_name = $(this).attr('data-method-name');
    var data_method_id = $(this).attr('data-method-id');
    var data_method_title = $(this).attr('data-method-title');

    $('#show-payment-method-type-split').html(data_method_name);
    $('#show-payment-method-id-split').val(data_method_id);

    localStorage.setItem('data_method_name', data_method_name);
    localStorage.getItem('orderTypeName');

    localStorage.setItem('data_method_id', data_method_id);
    localStorage.getItem('data_method_id');

    localStorage.setItem('data_method_title', data_method_title);
    localStorage.getItem('data_method_title');

    //if(data_method_name != 'Cash'){
    var total =  $('#payment-total-split').val();
    $('#input-current-amount-split').val(total);
    $('#payment-change-split').html(Number(0.000).toFixed(3));
    $('#confirm-payment-split').attr('disabled',false);
    // $('#tip-amount-input-split').val('');

    //}

});

$('#remove-cal-char-split').click(function() {
    var tipAmount = $('#tip-amount-input-split').val();
    var deliveryCharge = $('#delivery-charge-input-split').val();
    var latestInputs = $('#input-current-amount-split').val();
    var currentValues = latestInputs.slice(0,-1);
    $('#input-current-amount-split').val(currentValues);
    paymentValue = $('#payment-total-split').val();
    var paymentChange = parseFloat(currentValues-paymentValue).toFixed(3);
    var complateChangeValue = parseFloat(paymentChange-(tipAmount+deliveryCharge)).toFixed(3);
    $('#payment-change-split').html(complateChangeValue);

    if(complateChangeValue<0){
        $('#confirm-payment-split').attr('disabled','disabled');
    }else{
        $('#confirm-payment-split').attr('disabled',false);
    }

});


$('#tip-amount-input-split').keyup(function(){
    var  latestInputs = Number($('#input-current-amount-split').val());

    var total = Number($('#payment-total-split').val());

    var paymentChange = Number($('#payment-change-split').text());
    var tipAmount = Number($('#tip-amount-input-split').val());
    var deliveryCharge = Number($('#delivery-charge-input-split').val());
    var complateChangeValue = (latestInputs-(tipAmount+total+deliveryCharge));

    // var complateChangeValue = (paymentChange-tipAmount).toFixed(3);
    $('#payment-change-split').html(complateChangeValue);

    if(complateChangeValue<0){
        $('#confirm-payment-split').attr('disabled','disabled');
    }else{
        $('#confirm-payment-split').attr('disabled',false);
    }

});

$('#new-cal-clear-split').click(function() {
    var tipAmount = Number($('#tip-amount-input-split').val());
    var deliveryCharge = Number($('#delivery-charge-input-split')).val();
    paymentValue = Number($('#payment-total-split').val());
    $('#payment-change-split').html('-'+parseFloat(paymentValue+tipAmount+deliveryCharge).toFixed(3));
    $('#input-current-amount-split').val('');

});





/*end split order*/



/*Calculator Start*/
$('.cal-button').click(function() {
    var button_value = $(this).val();
    var tipAmount = $('#tip-amount-input').val();
    var deliveryCharge = $('#delivery-charge-input').val();
    var latestInput = $('#input-current-amount').val();
    var currentValue = latestInput+button_value;
    var  paymentValue = $('#payment-total').val();

    var paymentChange = parseFloat(currentValue-paymentValue).toFixed(3);
    $('#input-current-amount').val(currentValue);

    var complateChangeValue = parseFloat(paymentChange-(tipAmount+deliveryCharge)).toFixed(3);
    console.log(tipamount);
    $('#payment-change').html(complateChangeValue);

    if(complateChangeValue<0){
        $('#confirm-payment').attr('disabled','disabled');
    }else{
        $('#confirm-payment').attr('disabled',false);
    }



});


// $('#input-current-amount').keyup(function(){
//
//     var tipAmount = $('#tip-amount-input').val();
//     var deliveryCharge = $('#delivery-charge-input').val();
//     var latestInput = $('#input-current-amount').val();
//     var discount = $("#inputDiscount").val();
//     //var paymentValue = $('#payment-total').val();
//     var paymentValue = Number($("#total-cart-hold").text());
//
//     var paymentChange = (Number(latestInput)-Number(paymentValue)).toFixed(3);
//
//     var completeChangeValue = ((Number(paymentChange) + Number(discount)) - (Number(tipAmount) + Number(deliveryCharge))).toFixed(3);
//
//     $('#payment-change').html(completeChangeValue);
//
//
//     if(completeChangeValue<0){
//         $('#confirm-payment').attr('disabled','disabled');
//     }else{
//         $('#confirm-payment').attr('disabled',false);
//     }
//
// });






/*hold cal*/

$('.cal-button-hold').click(function() {

    var button_value = $(this).val();
    var latestInput = $('#input-current-amount-hold').val();
    var currentValue = latestInput+button_value;

    $('#input-current-amount-hold').val(currentValue);


});


$('#remove-cal-char-hold').click(function() {

    var latestInputs = $('#input-current-amount-hold').val();
    var currentValues = latestInputs.slice(0,-1);
    $('#input-current-amount-hold').val(currentValues);


});

$('#hold-ac-btn').click(function(){
    $('#input-current-amount-hold').val('');
});





$('#new-cal-clear').click(function() {
    var tipAmount = Number($('#tip-amount-input').val());
    var deliveryCharge = Number($('#delivery-charge-input')).val();
    paymentValue = Number($('#payment-total').val());
    $('#payment-change').html('-'+(paymentValue+tipAmount+deliveryCharge).toFixed(3));
    $('#input-current-amount').val('');

});

$('#remove-cal-char').click(function() {
    var tipAmount = $('#tip-amount-input').val();
    var deliveryCharge = $('#delivery-charge-input').val();
    var latestInputs = $('#input-current-amount').val();
    var currentValues = latestInputs.slice(0,-1);
    $('#input-current-amount').val(currentValues);
    paymentValue = $('#payment-total').val();
    var paymentChange = (currentValues-(paymentValue+deliveryCharge)).toFixed(3);
    var complateChangeValue = (paymentChange-(tipAmount+deliveryCharge)).toFixed(3);
    $('#payment-change').html(complateChangeValue);

    if(complateChangeValue<0){
        $('#confirm-payment').attr('disabled','disabled');
    }else{
        $('#confirm-payment').attr('disabled',false);
    }

});

// $('#tip-amount-input').keyup(function(){
//
//     var inputDis = $('#inputDiscount').val();
//     var dis2 = Number(totalCartDiscount());
//     var discount_type = localStorage.getItem('discount_type');
//     var totalcart = Number(totalCart()).toFixed(3);
//     var total = totalcart-dis2;
//
//     if(discount_type === 'Percent'){
//         var inputdisvalue = Number((total*inputDis)/100);
//         $('#show-kd').html('%');
//     }else{
//         var inputdisvalue = Number($('#inputDiscount').val());
//         $('#show-kd').html('KD');
//     }
//
//
//
//     var latestInputs = Number($('#input-current-amount').val());
//     var tipAmount = Number($('#tip-amount-input').val());
//     var deliveryCharge = Number($('#delivery-charge-input').val());
//     var discount = inputdisvalue;
//     //var total = Number($('#payment-total').val());
//     var total = Number($("#total-cart-hold").text());
//
//     var paymentChange = Number($('#payment-change').text());
//     var grandTotal = (total + tipAmount + deliveryCharge - discount).toFixed(3);
//     var completeChangeValue = (latestInputs - grandTotal).toFixed(3);
//
//     // var completeChangeValue = (paymentChange-tipAmount).toFixed(3);
//     $("#tip-hold").text(tipAmount.toFixed(3));
//     $("#delivery-hold").text(deliveryCharge.toFixed(3));
//     $("#grand-total-hold").text(grandTotal);
//     $("#payment-total").val(grandTotal);
//     $('#payment-change').html(completeChangeValue);
//     if(completeChangeValue<0){
//         $('#confirm-payment').attr('disabled','disabled');
//     }else{
//         $('#confirm-payment').attr('disabled',false);
//     }
//
// });

$('#delivery-charge-input, #tip-amount-input, #input-current-amount').keyup(function(){

    var inputDis = $('#inputDiscount').val();
    var dis2 = Number(totalCartDiscount());
    var discount_type = localStorage.getItem('discount_type');
    var totalcart = Number(totalCart()).toFixed(3);
    var total = totalcart-dis2;

    if(discount_type === 'Percent'){
        var inputdisvalue = Number((total*inputDis)/100);
        $('#show-kd').html('%');
    }else{
        var inputdisvalue = Number($('#inputDiscount').val());
        $('#show-kd').html('KD');
    }

    var inputDiscount = inputdisvalue; //added by smartrahat 2018.11.03


    var latestInputs = Number($('#input-current-amount').val());
    var tipAmount = Number($('#tip-amount-input').val());
    var deliveryCharge = Number($('#delivery-charge-input').val());
    deliveryCharge = isNaN(deliveryCharge) ? 0 : deliveryCharge;
    var discount = inputdisvalue;
    //var total = Number($('#payment-total').val());
    var total = Number($("#total-cart-hold").text());
    //var paymentChange = Number($('#payment-change').text());
    var grandTotal = (total + tipAmount + deliveryCharge - discount).toFixed(3);
    var completeChangeValue = (latestInputs - grandTotal).toFixed(3);

    // var complateChangeValue = (paymentChange-tipAmount).toFixed(3);
    $("#tip-hold").text(tipAmount.toFixed(3));
    $("#delivery-hold").text(deliveryCharge.toFixed(3));
    $("#grand-total-hold").text(grandTotal);
    $("#payment-total").val(grandTotal);
    $('#payment-change').html(completeChangeValue);

    if(completeChangeValue<0){
        $('#confirm-payment').attr('disabled','disabled');
    }else{
        $('#confirm-payment').attr('disabled',false);
    }

});



$('#show-cart').on("change", ".discount-value-change", function(event){
    var name = $(this).attr('data-name');
    var disValue = Number($(this).val());
    discountUpdate(name,disValue);
    displayCart();
});


$('#all-clear-cash').click(function(){
    prev_hold = [];
    localStorage.setItem('all_hold_orders', JSON.stringify(prev_hold));

    prev_split = [];
    localStorage.setItem('split_arr', JSON.stringify(prev_split));
});




$('#inputDiscount').keyup(function(){

    var discount_type = localStorage.getItem('discount_type');
    var inputDis = $('#inputDiscount').val();
    var tip_amount = $("#tip-amount-input").val();
    var delivery_charge = $("#delivery-charge-input").val();
    var input_current_amount = $("#input-current-amount").val();
    //var grand_total = $('#grand-total').text();

    $('#dis-type-show').html(discount_type);


    var totalcart = Number(totalCart()).toFixed(3);

    var dis2 = Number(totalCartDiscount());
    var total = totalcart-dis2;

    if(discount_type === 'Percent'){
        var inputdisvalue = Number((total*inputDis)/100);
        $('#show-kd').html('%');
    }else{
        var inputdisvalue = Number($('#inputDiscount').val());
        $('#show-kd').html('KD');
    }

    inputdisvalue = inputdisvalue.toFixed(3);

    var alldiscount = (Number(dis2) + Number(inputdisvalue)).toFixed(3);
    var paymentChange = (Number(input_current_amount) - grandTotal()).toFixed(3);

    var grandTotalValue = (Number(totalcart)-Number(alldiscount) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);

    $('#inputDiscount_2').val(inputdisvalue);

    $('#input-discount').html(inputdisvalue);
    $('#input-discount-hold').html(inputdisvalue);

    $('#split-order-discount').html(alldiscount);

    $('#grand-total').html(grandTotalValue);
    $("#grand-total-hold").html(grandTotal());
    $("#payment-total").val(grandTotal());
    $("#payment-change").html(paymentChange);

    $('#split-order-total').html(grandTotalValue);

    localStorage.setItem('split_order_total_discount', alldiscount);
    localStorage.setItem('split_order_total', totalcart);

    //getOrderyTypeData(strogOrderData);






});


function saveHoldData(inputHold){

    var prev_hold = JSON.parse(localStorage.getItem('all_hold_orders'));

    prev_hold.push(inputHold);

    localStorage.setItem('all_hold_orders', JSON.stringify(prev_hold));

    displayHoldCart();
}



$('#table_id').change(function(){
    var table_id = ($('#table_id').val());

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../get-table-info' + '/' + table_id,
        success: function (data) {
            saveTableName(data.table.name);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });



});



$('#create-hold-order').click(function(){
    var orderTypeName = localStorage.getItem('orderTypeName');

    if ($('#table_id').val() == null && orderTypeName == "Dine In" ) {
        alert('Table Is Empty.');
    }else{
        if (cart.length == 0) {
            alert('Cart is Empty.');
        }else{
            chk_id_generate();
            var csk_id = localStorage.getItem('hold_id');

            var holdCode = Number($('#input-current-amount-hold').val());
            // var order_type_id = Number($('#show-payment-method-id').val());
            var order_type_id = localStorage.getItem('orderTypeId');

            var current_user_name = $('#current-user-name').val();
            var person = $('#person').val();
            var table_id = $('#table_id').val();
            var tip_amount = $('#tip-amount-input').val();
            var delivery_charge = $('#delivery-charge-input');
            var customer_id = $('#customer_id').val();
            var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
            var dis2 = Number(totalCartDiscount());
            var dis3 = Number($('#inputDiscount').val());
            var alldiscount = Number(dis1+dis2+dis3).toFixed(3);
            var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);

            var totalcart = Number(totalCart()).toFixed(3);


            var grandTotalValue = (Number(totalcart)+Number(tax1)-Number(alldiscount) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);


            var d = new Date();
            var currentDate = d.getDate() + "-" +(d.getMonth()+1) + "-" + d.getFullYear();
            var dt = new Date();
            var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var holdArray = listCart();

            inputHold ={
                'current_user_name' : current_user_name,
                'customer_id'       : customer_id,
                'holdName'          : orderTypeName,
                'holdCartItem'      : holdArray,
                'order_type_id'     : order_type_id,
                'person'            : person,
                'table_id'          : table_id,
                'table_name'        : get_table_Name,
                'alldiscount'       : alldiscount,
                'subTotal'          : totalcart,
                'grandTotal'        : grandTotalValue,
                'currentDate'       : currentDate,
                'currentTime'       : currentTime,
                'totalTax'          : tax1,
                'holdCode'          : holdCode,
                'is_hold'           : 1,
                'csk_id'            : csk_id
            };

            //todo:: check for unique open check

            // console.log('All Hold Orders');
            // console.log(is_open_check);
            // console.log(localStorage.getItem('all_hold_orders'));
            // var allHoldOrder = JSON.parse(localStorage.getItem('all_hold_orders'));
            // console.log('single');
            // //console.log(allHoldOrder);
            // for(var k in allHoldOrder){
            //     console.log(allHoldOrder[k].csk_id + '-' + csk_id);
            // }

            // end for check unique open check

            saveHoldData(inputHold);
            displayHoldCart();
            cart = [];
            saveCart();
            loadOrderTypeData();

            $('#inputDiscount').val('');
            $('#input-discount').val('');
            displayCart();

            toastr.success('Hold Successfully Added.', 'Success Alert', {timeOut: 1000});

            $(".Popupmodel").css("display", "none");

            $('#input-current-amount-hold').val('');

            set_hold_id();
        }
    }

});


function displayHoldCart(){
    var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
    var holdOutput = '';
    var hold = "Hold Code : ";
    for(i in allHoldCart){
        var tbl = allHoldCart[i].table_name ;
        if(tbl == " "){
            var holdtypeview = allHoldCart[i].holdName;

        }else{
            var holdtypeview = tbl;

        }
        // console.log(allHoldCart[i].holdCartItem);

        holdOutput += "<div class='col-xs-12 colsm-6 col-md-6'>";
        holdOutput += "<a class='view-single-hold' data-hold-index-number="+i+" href='#'>";
        holdOutput += "<div class='hold-list-inner'>";
        holdOutput += "<div class='item'>";
        holdOutput += "<div class='hold-stock'>"+holdtypeview+"</div>";
        holdOutput += "<div class='hold-item-dec'>";
        holdOutput += "<a href='#'><h4>"+allHoldCart[i].csk_id+"</h4></a>";
        holdOutput += "<div class='hold-meta'>";
        holdOutput += "<ul>";
        holdOutput += "<li class='single-hold-delete' data-hold-index-number="+i+"><a href='#'";
        holdOutput += "><span>Delete</span></a>";
        holdOutput += "</li>";
        holdOutput += "<li class='single-hold-display' data-hold-index-number="+i+" ><a href='#' class='popupclase' ><span";
        holdOutput += " ></span> Add Item  </a></li>";
        holdOutput += "<li class='single-hold-bill-print hold-additem ' data-hold-index-number="+i+" ><a href='#'  ><span";
        holdOutput += " ></span> Pay Now  </a></li>";
        holdOutput += "</ul>";
        holdOutput += "</div>";
        holdOutput += "</div>";
        holdOutput += "</div>";
        holdOutput += "</div>";
        holdOutput += "</a>";
        holdOutput += "</div>";
    }

    $('#show-all-hold').html(holdOutput);


}

$('#tip-amount').click(function(){
    var tip_amount = ($('#tip-amount-input').val());
    $("#tipamount").css("display", "none");
});
$('#delivery-charge').click(function(){
    var delivery_charge = ($('#delivery-charge-input').val());
    $("#deliverycharge").css("display", "none");
});




$('#show-all-hold').on('click','.single-hold-display', function(event){


    var holdIndexNumber = $(this).attr('data-hold-index-number');

    var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
    for(i in allHoldCart){
        if(i==holdIndexNumber){
            var hh_id = allHoldCart[i].csk_id;
            $('#action_hold').val(hh_id);
            var holdArray = (allHoldCart[i].holdCartItem);
            var is_open_check = localStorage.setItem('is_open_check',1);
            console.log(holdArray);
            cart = holdArray;
            saveCart();
            var holdOutput = '';

            for(var i in holdArray){
                var ser = 1 + Number(i);
                holdOutput  +="<tr>";
                holdOutput  +="<th scope='row' width='5.3%'>"+(ser)+"</th>";
                holdOutput  +="<td width='50%'>"+ holdArray[i].name +"<br/></td>";
                holdOutput  +="<td class='text-center' width='13%'>";
                holdOutput  +="<div class='pos-pro-body-qit'>";
                holdOutput  +="<button class='subtract-item' data-name='"+holdArray[i].name+"' data-id='"+holdArray[i].id+"'><span class='fa fa-minus'></span></button>";
                holdOutput  +="<input type='text'value='"+ holdArray[i].count +"' class='pos-pro-body-qit-input'  data-name='"+holdArray[i].name+"' data-id='"+holdArray[i].id+"'>";
                holdOutput  +="<button class='plus-item' data-name='"+holdArray[i].name+"' data-id='"+holdArray[i].id+"'><span class='fa fa-plus '></span></button>";
                holdOutput  +="</div>";
                holdOutput  +="</td>";
                holdOutput  +="<td class='text-center'><div class='pos-pro-list-table-dis'><input type='text' class='discount-value-change' data-name='"+holdArray[i].name+"' value='"+holdArray[i].discount+"'></td>";
                holdOutput  +="<td class='text-center'>"+ ((holdArray[i].price * holdArray[i].count) - holdArray[i].discount).toFixed(3) +"</td>";
                holdOutput  +="<td class='text-center'><a href='#' data-toggle='tooltip' title='Delete' class='delete-item' data-name='"+holdArray[i].name+"'><span class='fa fa-trash'></span></a></td>";
                holdOutput  +="</tr>"
            }

            $('#show-cart').html(holdOutput);
            $('#total-cart').html(totalCart());
            $('#count-Cart').html(countCart());

            $('#total-cart-discount').html(totalCartDiscount().toFixed(3));
            /*split order function call*/
            splitOrder();

            getOrderyTypeData(strogOrderData);

            for(i in allHoldCart){
                if(i==holdIndexNumber){
                    // allHoldCart.splice(i,1);
                    localStorage.setItem('all_hold_orders', JSON.stringify(allHoldCart));
                    displayHoldCart();
                    break;
                }

            }
            console.log(JSON.parse(localStorage.getItem('all_hold_orders')));


        }

    }

    $(".Popupmodel").css("display", "none");



});



// $('#show-all-hold').on('click','.single-hold-delete',function(event){
//
//         var holdIndexNumber = $(this).attr('data-hold-index-number');
//         var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
//         for(i in allHoldCart){
//             if(i==holdIndexNumber){
//                 allHoldCart.splice(i,1);
//                 localStorage.setItem('all_hold_orders', JSON.stringify(allHoldCart));
//                 displayHoldCart();
//                 break;
//             }
//
//         }
//         console.log(JSON.parse(localStorage.getItem('all_hold_orders')));
//     });





$('#show-all-hold').on('click','.view-single-hold', function(event){

    var holdIndexNumber = $(this).attr('data-hold-index-number');
    var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
    for(i in allHoldCart){
        if(i==holdIndexNumber){

            $('#holdAuthor').html(allHoldCart[i].current_user_name);
            $('#holdDate').html(allHoldCart[i].currentDate);
            $('#holdTime').html(allHoldCart[i].currentTime);
            $('#holdSubTotal').html(allHoldCart[i].subTotal);
            $('#holdDiscount').html(allHoldCart[i].alldiscount);
            $('#holdTax').html(allHoldCart[i].totalTax);
            $('#holdGrandTotal').html(allHoldCart[i].grandTotal);
            $('#holdCode').html(allHoldCart[i].holdCode);

            var arrayHold = allHoldCart[i].holdCartItem;
            var singleHoldOutput = '';
            console.log(arrayHold);
            for(p in arrayHold){
                singleHoldOutput += "<tr>";
                singleHoldOutput += "<td class='text-left'>"+arrayHold[p].name+"</td>";
                singleHoldOutput += "<td class='text-center'>"+arrayHold[p].count+"</td>";
                singleHoldOutput += "<td class='text-right'>"+Number(arrayHold[p].discount).toFixed(3)+"</td>";
                singleHoldOutput += "<td class='text-right'>"+Number(arrayHold[p].price).toFixed(3)+"</td>";
                singleHoldOutput += "<td class='text-right'>"+Number(arrayHold[p].price * arrayHold[p].count).toFixed(3)+"</td>";
                singleHoldOutput += "</tr>"

            }

            $('#show-hold-data').html(singleHoldOutput);
        }

    }


});












/*calculator End*/

/****** Split Order ***********/

function splitOrder(){

    var splitArray = listCart();


    localStorage.setItem('split_arr', JSON.stringify(splitArray));


    var splitOutput = '';
    for(var i in splitArray){
        splitOutput += "<tr split-id='"+ splitArray[i].id +"' split-arabic_name='"+ splitArray[i].arabic_name +"'  split-name='"+ splitArray[i].name +"' split-price='"+ splitArray[i].price +"' split-count='"+ splitArray[i].count +"' split-discount='"+ splitArray[i].discount +"' >";
        splitOutput += "<td width='50%'>"+ splitArray[i].name +"<input type='text'></td>";
        splitOutput += "<td width='15%' class='text-center'>"+ splitArray[i].count +"</td>";
        splitOutput += "<td width='35%' class='text-center'>"+ (splitArray[i].count*splitArray[i].price).toFixed(3) +" KD </td>";
        splitOutput += "</tr>"
    }

    /*loadOrderData();

     console.log('strogOrderData ='+ strogOrderData);*/


    // var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
    var dis2 = Number(totalCartDiscount());
    var dis3 = Number($('#inputDiscount').val());
    var tip_amount = $("#tip-amount-input").val();
    var delivery_charge = $("#delivery-charge-input").val();

    var alldiscount = Number(dis2+dis3).toFixed(3);

    // var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);


    var totalcart = Number(totalCart()).toFixed(3);


    var grandTotalValue = (Number(totalcart)-Number(alldiscount) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);

    $('#table-split-list-left').html(splitOutput);



    $('#split-order-discount').html(alldiscount);
    $('#split-order-total').html(totalcart);

    localStorage.setItem('split_order_total_discount', alldiscount);
    localStorage.setItem('split_order_total', grandTotalValue);

}



function re_displaySplitOrder(splitArray){


    localStorage.setItem('split_arr', JSON.stringify(splitArray));


    var splitOutput = '';
    for(var i in splitArray){
        splitOutput += "<tr split-id='"+ splitArray[i].id +"'  split-name='"+ splitArray[i].name +"' split-price='"+ splitArray[i].price +"' split-count='"+ splitArray[i].count +"' split-discount='"+ splitArray[i].discount +"' >";
        splitOutput += "<td>"+ splitArray[i].name +"</td>";
        splitOutput += "<td class='text-center'>"+ splitArray[i].count +"</td>";
        splitOutput += "<td class='text-center'>"+ (splitArray[i].count*splitArray[i].price).toFixed(3) +" KD </td>";
        splitOutput += "</tr>"
    }

    $('#table-split-list-left').html(splitOutput);

}




function splitOrder_right(){

    var splitOutput = '';
    for(var i in split_arr_2){
        splitOutput += "<tr split-id='"+ split_arr_2[i].id +"' split-arabic_name='"+ split_arr_2[i].arabic_name +"'  split-name='"+ split_arr_2[i].name +"' split-price='"+ split_arr_2[i].price +"' split-count='"+ split_arr_2[i].count +"' split-discount='"+ split_arr_2[i].discount +"' >";
        splitOutput += "<td>"+ split_arr_2[i].name +"</td>";
        splitOutput += "<td class='text-center'>"+ split_arr_2[i].count +"</td>";
        splitOutput += "<td class='text-center'>"+ (split_arr_2[i].count*split_arr_2[i].price).toFixed(3) +" KD </td>";
        splitOutput += "</tr>"
    }

    $('#table-split-list-right').html(splitOutput);

}



$('#table-split-list-left').on('click','tr',function(){

    $('#table-split-list-left tr').removeClass('split-bg');

    $(this).addClass("split-bg");

});

$('#table-split-list-right').on('click','tr',function(){

    $('#table-split-list-right tr').removeClass('split-bg-2');

    $(this).addClass("split-bg-2");

});


var split_arr_2 = [];

$('#table-split-list-left').on('dblclick','tr',function(){

    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));


    var id = ($(this).attr('split-id'));
    var name = ($(this).attr('split-name'));
    var price = ($(this).attr('split-price'));
    var count = ($(this).attr('split-count'));
    var discount = ($(this).attr('split-discount'));

    var split_data = {
        'id': id,
        'name': name,
        'price': price,
        'count': count,
        'discount': discount,
    };

    for(var i in get_split_arr){
        if (get_split_arr[i].name == name) {
            get_split_arr.splice(i, 1);
        }
    }

    split_arr_2.push(split_data);
    splitOrder_right();


    re_displaySplitOrder(get_split_arr);


    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);


});

function leftCalculation(left_arr){
    var split_order_total = localStorage.getItem('split_order_total');
    var split_order_total_discount = localStorage.getItem('split_order_total_discount');

    var left_total = 0;
    var left_current_discount = 0;
    for(var l in left_arr){
        left_total += (left_arr[l].price*left_arr[l].count);
    }

    left_current_discount = ((left_total*split_order_total_discount)/split_order_total);

    $('#split-order-discount').html(left_current_discount.toFixed(3));
    $('#split-order-total').html((left_total-left_current_discount).toFixed(3));

    var leftTotalArray = [{
        'discount' : left_current_discount,
        'total' : left_total-left_current_discount,
    }];

    return leftTotalArray;


}


function rightCalculation(split_arr_2){
    var split_order_total = localStorage.getItem('split_order_total');
    var split_order_total_discount = localStorage.getItem('split_order_total_discount');

    var right_total = 0;
    var right_current_discount = 0;
    for(var r in split_arr_2){
        right_total += (split_arr_2[r].price*split_arr_2[r].count);
    }

    right_current_discount = ((right_total*split_order_total_discount)/split_order_total);

    $('#split-order-discount-right').html(right_current_discount.toFixed(3));
    $('#split-order-total-right').html((right_total-right_current_discount).toFixed(3));

    var leftTotalArray = [{
        'discount' : right_current_discount,
        'total' : right_total-right_current_discount,
    }];

    return leftTotalArray;


}



$('#angle-single-right').click(function(){
    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));


    var id = ($('.split-bg').attr('split-id'));
    var name = ($('.split-bg').attr('split-name'));
    var price = ($('.split-bg').attr('split-price'));
    var count = ($('.split-bg').attr('split-count'));
    var discount = ($('.split-bg').attr('split-discount'));

    var split_data = {
        'id': id,
        'name': name,
        'price': price,
        'count': 1,
        'discount': discount,
    };

    if (name == undefined){
        var split_data_arr = {
            'id': get_split_arr[0].id,
            'name': get_split_arr[0].name,
            'price': get_split_arr[0].price,
            'count': 1,
            'discount': get_split_arr[0].discount,
        };


        get_split_arr[0].count--;

        if(get_split_arr[0].count == 0){
            get_split_arr.splice(0, 1);
        }



        split_arr_2.push(split_data_arr);
        splitOrder_right();
        re_displaySplitOrder(get_split_arr);

    }else{
        for(var i in get_split_arr){
            if (get_split_arr[i].name == name) {
                get_split_arr[i].count--;
                if(get_split_arr[i].count ==0){
                    get_split_arr.splice(i, 1);
                }
            }
        }

        split_arr_2.push(split_data);
        splitOrder_right();
        re_displaySplitOrder(get_split_arr);
    }


    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);



});

$('#angle-double-right').click(function(){
    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));


    var id = ($('.split-bg').attr('split-id'));
    var name = ($('.split-bg').attr('split-name'));
    var price = ($('.split-bg').attr('split-price'));
    var count = ($('.split-bg').attr('split-count'));
    var discount = ($('.split-bg').attr('split-discount'));

    var split_data = {
        'id': id,
        'name': name,
        'price': price,
        'count': count,
        'discount': discount,
    };

    if (name == undefined){
        var split_data_arr = {
            'id': get_split_arr[0].id,
            'name': get_split_arr[0].name,
            'price': get_split_arr[0].price,
            'count': get_split_arr[0].count,
            'discount': get_split_arr[0].discount,
        };

        get_split_arr.splice(0, 1);




        split_arr_2.push(split_data_arr);
        splitOrder_right();
        re_displaySplitOrder(get_split_arr);

    }else{
        for(var i in get_split_arr){
            if (get_split_arr[i].name == name) {
                get_split_arr.splice(i, 1);
            }
        }

        split_arr_2.push(split_data);
        splitOrder_right();
        re_displaySplitOrder(get_split_arr);
    }

    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);



});





$('#payment-split-left').click(function(){
    var val_chk = $('#left-chk').text();
    $('#payment-show-chk-id').html('CHK Number : '+val_chk);

    $('#split-array-type').val('left');

    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    $('#popsplitdor').css('display','block');
    var output_split = '';
    console.log(left_arr);

    for(var i in left_arr){
        var ser = Number(i)+1;
        output_split  +="<tr>";
        output_split  +="<th scope='row' width='2%' >"+ser+"</th>";
        output_split  +="<td width='32%' style='font-size:11px'>"+ left_arr[i].name +"</td>";

        output_split  +="<td width='17.5%' class='text-center' >";
        output_split  +="<div class='pos-pro-body-qit'>";
        output_split  +="<input type='text'value='"+ left_arr[i].count +"' class='pos-pro-body-qit-input'>";
        output_split  +="</div>";
        output_split  +="</td>";

        output_split  +="<td width='15%' class='text-center'>"+ Number((left_arr[i].price)).toFixed(3) +"</td>";
        output_split  +="<td width='14%' class='text-center'>"+ Number((left_arr[i].price * left_arr[i].count) - left_arr[i].discount).toFixed(3) +"</td>";

        output_split  +="</tr>";
        output_split  +="</tr>";
        output_split  +="</tr>"
    }

    $('#show-order-split-cart').html(output_split);

    var right_total = leftCalculation(left_arr);

    $('#input-discount-split').html((right_total[0].discount).toFixed(3));
    $('#grand-total-split').html((right_total[0].total).toFixed(3));

    $('#payment-total-split').val((right_total[0].total).toFixed(3));






});


$('#payment-split-right').click(function(){

    var val_chk = $('#right-chk').text();
    $('#payment-show-chk-id').html('CHK Number : '+val_chk);

    $('#split-array-type').val('right');

    $('#popsplitdor').css('display','block');

    var output_split = '';
    console.log(split_arr_2);

    for(var i in split_arr_2){
        var ser = Number(i)+1;
        output_split  +="<tr>";
        output_split  +="<th scope='row' width='2%' >"+ser+"</th>";
        output_split  +="<td width='32%' style='font-size:11px'>"+ split_arr_2[i].name +"</td>";

        output_split  +="<td width='17.5%' class='text-center' >";
        output_split  +="<div class='pos-pro-body-qit'>";
        output_split  +="<input type='text'value='"+ split_arr_2[i].count +"' class='pos-pro-body-qit-input'>";
        output_split  +="</div>";
        output_split  +="</td>";

        output_split  +="<td width='15%' class='text-center'>"+ Number((split_arr_2[i].price)).toFixed(3) +"</td>";
        output_split  +="<td width='14%' class='text-center'>"+ Number((split_arr_2[i].price * split_arr_2[i].count) - split_arr_2[i].discount).toFixed(3) +"</td>";

        output_split  +="</tr>";
        output_split  +="</tr>";
        output_split  +="</tr>"
    }

    $('#show-order-split-cart').html(output_split);

    var left_total = rightCalculation(split_arr_2);

    $('#input-discount-split').html((left_total[0].discount).toFixed(3));
    $('#grand-total-split').html((left_total[0].total).toFixed(3));

    $('#payment-total-split').val((left_total[0].total).toFixed(3));






});









$('#table-split-list-right').on('dblclick','tr',function(){
    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));
    console.log(get_split_arr);


    var id = ($(this).attr('split-id'));
    var name = ($(this).attr('split-name'));
    var arabic_name = ($(this).attr('split-arabic_name'));
    var price = ($(this).attr('split-price'));
    var count = ($(this).attr('split-count'));
    var discount = ($(this).attr('split-discount'));

    var split_data = {
        'id': id,
        'name': name,
        'arabic_name': arabic_name,
        'price': price,
        'count': count,
        'discount': discount,
    };

    for(var i in split_arr_2){
        if (split_arr_2[i].name == name) {
            split_arr_2.splice(i, 1);
        }
    }


    get_split_arr.push(split_data);

    // console.log(split_arr_2);
    splitOrder_right();

    re_displaySplitOrder(get_split_arr);

    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);


});



$('#angle-single-left').click(function(){
    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));
    console.log(get_split_arr);


    var id = ($('.split-bg-2').attr('split-id'));
    var name = ($('.split-bg-2').attr('split-name'));
    var price = ($('.split-bg-2').attr('split-price'));
    var count = ($('.split-bg-2').attr('split-count'));
    var discount = ($('.split-bg-2').attr('split-discount'));


    var split_data = {
        'id': id,
        'name': name,
        'price': price,
        'count': 1,
        'discount': discount,
    };




    if (name == undefined){
        var split_data_2 = {
            'id': split_arr_2[0].id,
            'name': split_arr_2[0].name,
            'price': split_arr_2[0].price,
            'count': 1,
            'discount': split_arr_2[0].discount,
        };


        split_arr_2[0].count--;

        if(split_arr_2[0].count == 0){
            split_arr_2.splice(0, 1);
        }

        get_split_arr.push(split_data_2);

        // console.log(split_arr_2);
        splitOrder_right();

        re_displaySplitOrder(get_split_arr);

    }else{
        for(var i in split_arr_2){
            if (split_arr_2[i].name == name) {
                split_arr_2[i].count--;
                if(split_arr_2[i].count == 0){
                    split_arr_2.splice(i, 1);
                }
            }
        }


        get_split_arr.push(split_data);

        // console.log(split_arr_2);
        splitOrder_right();

        re_displaySplitOrder(get_split_arr);
    }


    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);



});

$('#angle-double-left').click(function(){

    var  get_split_arr  = JSON.parse(localStorage.getItem('split_arr'));
    console.log(get_split_arr);


    var id = ($('.split-bg-2').attr('split-id'));
    var name = ($('.split-bg-2').attr('split-name'));
    var price = ($('.split-bg-2').attr('split-price'));
    var count = ($('.split-bg-2').attr('split-count'));
    var discount = ($('.split-bg-2').attr('split-discount'));


    var split_data = {
        'id': id,
        'name': name,
        'price': price,
        'count': count,
        'discount': discount,
    };




    if (name == undefined){
        var split_data_2 = {
            'id': split_arr_2[0].id,
            'name': split_arr_2[0].name,
            'price': split_arr_2[0].price,
            'count': split_arr_2[0].count,
            'discount': split_arr_2[0].discount,
        };


        split_arr_2.splice(0, 1);


        get_split_arr.push(split_data_2);

        // console.log(split_arr_2);
        splitOrder_right();

        re_displaySplitOrder(get_split_arr);

    }else{
        for(var i in split_arr_2){
            if (split_arr_2[i].name == name) {

                split_arr_2.splice(i, 1);

            }
        }


        get_split_arr.push(split_data);

        // console.log(split_arr_2);
        splitOrder_right();

        re_displaySplitOrder(get_split_arr);
    }

    localStorage.setItem('split_arr', JSON.stringify(get_split_arr));
    var  left_arr  = JSON.parse(localStorage.getItem('split_arr'));

    leftCalculation(left_arr);
    var right_arr = split_arr_2;
    rightCalculation(right_arr);


});



/* $('#table-split-list-left tr').click(function(){
 $('#table-split-list-left tr').removeClass('split-bg');

 $(this).addClass("split-bg");

 console.log($(this).attr('split-id'));
 console.log($(this).attr('split-name'));
 console.log($(this).attr('split-price'));
 console.log($(this).attr('split-count'));
 console.log($(this).attr('split-discount'));
 console.log('------------------------------');
 });

 $('#table-split-list-right tr').click(function(){
 $('#table-split-list-right tr').removeClass('split-bg');

 $(this).addClass("split-bg");



 });

 $('#table-split-list-right tr').dblclick(function(){
 console.log($(this).attr('split-id'));
 console.log($(this).attr('split-name'));
 console.log($(this).attr('split-price'));
 console.log($(this).attr('split-count'));
 console.log($(this).attr('split-discount'));
 console.log('------------------------------');
 });*/



/************End Solit Order*************/



/*OrderyType Start */
loadOrderTypeData();

function loadOrderTypeData(){

    var taxs_show = '';
    var discount_show = '';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../loard-order-type',
        success: function (data) {
            var orderyTypeName  = data.order_type_get_name;
            var orderTypeId  = data.order_type_get_id;


            saveOrderType(orderyTypeName,orderTypeId);
            saveMethodType(data.method_name,data.method_id, data.method_title);

            // localStorage.setItem('order_id', (data.order_id+1));
            localStorage.setItem('order_id', (Number(data.order_id)+Number(1)));
            localStorage.getItem('order_id');

            console.log(data);

            for(t = 0; t < data.taxs.length; t++){


                taxs_show += "<li>";
                taxs_show += "<strong>"+data.taxs[t].name+" : </strong>";
                taxs_show += "<span>"+Number(data.taxs[t].amount).toFixed(3)+"</span>";
                taxs_show += " KD ";
                taxs_show += "</li>";



            }
            for(d = 0; d < data.discounts.length; d++){

                discount_show += "<li>";
                discount_show += "<strong>"+data.discounts[d].name+" : </strong>";
                discount_show += "<span>"+Number(data.discounts[d].amount).toFixed(3)+"</span>";
                discount_show += "  KD ";
                discount_show += "</li>";

            }

            $('#show-all-tax').html(taxs_show);
            $('#show-all-discount').html(discount_show);
            $('#show-all-tax-hold').html(taxs_show);
            $('#show-all-discount-hold').html(discount_show);

            saveOrderData(data);
            getOrderyTypeData(data);
            loadOrderData();


        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

/*active Class */

$('.nav2 li a').click(function(e) {


    $('.nav2 li a').removeClass('active');

    var $this = $(this);

    if (!$this.hasClass('active')) {
        $this.addClass('active');
    }

});



$('.clickOrderType').click(function(){



    var orderyTypeName  = ($(this).attr('data-order-name'));
    var orderTypeId  = ($(this).attr('data-order-id'));

    saveOrderType(orderyTypeName,orderTypeId);

    var order_id = Number($(this).attr('data-order-id'));
    var taxs_show = '';
    var discount_show = '';
    var totalTax = 0;
    var totalDiscount = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })

    $.ajax({
        type: "GET",

        url: '../order-get-support' + '/' + order_id,
        success: function (data) {


            for(t = 0; t < data.taxs.length; t++){
                totalTax += data.taxs[t].amount;

                taxs_show += "<li>";
                taxs_show += "<strong>"+data.taxs[t].name+" : </strong>";
                taxs_show += "<span>"+Number(data.taxs[t].amount).toFixed(3);+"</span>";
                taxs_show += " KD ";
                taxs_show += "</li>";



            }
            for(d = 0; d < data.discounts.length; d++){
                totalDiscount += data.discounts[d].amount;

                discount_show += "<li>";
                discount_show += "<strong>"+data.discounts[d].name+" : </strong>";
                discount_show += "<span>"+Number(data.discounts[d].amount).toFixed(3)+"</span>";
                discount_show += " KD ";
                discount_show += "</li>";

            }

            $('#show-all-tax').html(taxs_show);
            $('#show-all-discount').html(discount_show);
            $('#show-all-tax-hold').html(taxs_show);
            $('#show-all-discount-hold').html(discount_show);

            saveOrderData(data);
            getOrderyTypeData(data);
            loadOrderData();




        },
        error: function (data) {
            console.log('Error:', data);
        }
    });


});

/*Storage Table Name*/

function saveTableName(name){
    localStorage.setItem('tableName', name);
    get_table_Name = localStorage.getItem('tableName');

}

function saveMethodType(name,id,title){
    localStorage.setItem('data_method_name', name);
    order_Type_Name = localStorage.getItem('data_method_name');
    localStorage.setItem('data_method_id', id);
    order_Type_Id = localStorage.getItem('data_method_id');
    localStorage.setItem('data_method_title', title);
    data_method_title = localStorage.getItem('data_method_title');
}

/*Storage Order Type Name*/
function saveOrderType(name,id){
    localStorage.setItem('orderTypeName', name);
    order_Type_Name = localStorage.getItem('orderTypeName');
    localStorage.setItem('orderTypeId', id);
    order_Type_Id = localStorage.getItem('orderTypeId');
}

function saveOrderData(data){

    localStorage.setItem('OrderData', JSON.stringify(data))
}




function loadOrderData(){
    strogOrderData  = JSON.parse(localStorage.getItem('OrderData'))
}




function getOrderyTypeData(data){
    var totalTax = 0;
    var totalDiscount = 0;
    for(t = 0; t < data.taxs.length; t++){
        totalTax += Number(data.taxs[t].amount);
    }
    for(d = 0; d < data.discounts.length; d++){
        totalDiscount += Number(data.discounts[d].amount);

    }
    //var inputDiscount = Number($('#inputDiscount_2').val());

    var inputDis = Number($('#inputDiscount').val());

    var dis2 = Number(totalCartDiscount());
    var discount_type = localStorage.getItem('discount_type');
    var totalcart = Number(totalCart()).toFixed(3);
    var total = totalcart - dis2;

    if(discount_type === 'Percent'){
        var inputdisvalue = Number((total*inputDis)/100);
        $('#show-kd').html('%');
    }else{
        var inputdisvalue = Number($('#inputDiscount').val());
        $('#show-kd').html('KD');
    }

    var inputDiscount = inputdisvalue; //added by smartrahat 2018.11.03
    var deliveryCharge = Number($("#delivery-charge-input").val());
    deliveryCharge = isNaN(deliveryCharge) ? 0 : deliveryCharge;
    var inputTip = Number($("#tip-amount-input").val());

    var suppingTotal = Number(totalCart());
    var grandTotal = (Number(suppingTotal+totalTax+Number(deliveryCharge)+inputTip)-(Number(totalDiscount)+Number(totalCartDiscount())+inputDiscount));
    var latestInputs = Number($('#input-current-amount').val());
    var completeChangeValue = (latestInputs - grandTotal).toFixed(3);
    grandTotal = grandTotal.toFixed(3);



    $('#grand-total').html(grandTotal);
    $('#grand-total-hold').html(grandTotal);
    $('#payment-total').val(grandTotal);
    $('#payment-change').html(completeChangeValue);



}

function orderTypeTotalDiscount(data){
    //displayCart();
    var orderTypeTotalDiscount = 0;
    for(ddd = 0; ddd < data.discounts.length; ddd++){
        orderTypeTotalDiscount += Number(data.discounts[ddd].amount);

    }
    return orderTypeTotalDiscount;
}


function orderTypeTotalTax(data){
    //displayCart();
    var orderTypeTotalTax = 0;

    for(ttt = 0; ttt < data.taxs.length; ttt++){

        orderTypeTotalTax += Number(data.taxs[ttt].amount);
    }

    return orderTypeTotalTax;
}

$('#payment-method').on('click','.payment-method-type', function(event){
    var data_method_name = $(this).attr('data-method-name');
    var data_method_id = $(this).attr('data-method-id');
    var data_method_title = $(this).attr('data-method-title');


    $('#show-payment-method-type').html(data_method_name);
    $('#show-payment-method-id').val(data_method_id);

    localStorage.setItem('data_method_name', data_method_name);
    localStorage.getItem('orderTypeName');

    localStorage.setItem('data_method_id', data_method_id);
    localStorage.getItem('data_method_id');

    localStorage.setItem('data_method_title', data_method_title);
    localStorage.getItem('data_method_title');

    localStorage.setItem('data_split_bill_one_method_id', ''); // clear the method one id
    localStorage.setItem('data_split_bill_two_method_id', ''); // clear the method two id

    //if(data_method_name != 'Cash'){
    var total =  $('#payment-total').val();
    $('#input-current-amount').val(total);
    $('#payment-change').html(Number(0.000).toFixed(3));
    $('#confirm-payment').attr('disabled',false);

    //}


});


function getOrderIdFn(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../last-order-data',
        success: function (data) {

            localStorage.setItem('order_id', (data.id+1));
            localStorage.getItem('order_id');

        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}




/*OrderyType End*/


//Shopping Cart....

var cart = [];

var Item = function (id, name, price, count, discount, arabic_name, condiment){
    this.id = id;
    this.name = name;
    this.condiment = condiment;
    this.price = price;
    this.count = count;
    this.discount = discount;
    this.arabic_name = arabic_name;
};



function updateItemToCart(id, name, price, count, discount, arabic_name, condiment){
    for(var i in cart){
        if(cart[i].name === name){
            cart[i].count = count;
            saveCart();
            return true;
        }

    }

    // var item = new Item(id, name, price, count, discount, arabic_name, condiment);
    // cart.push(item);
    // saveCart();

}

function addItemToCart(id, name, price, count, discount, arabic_name, condiment){
    for(var i in cart){
        //if(cart[i].name === name){
        if(cart[i].name === name && cart[i].condiment === undefined){
            cart[i].count += count;
            saveCart();
            return true;
        }

    }
    var item = new Item(id, name, price, count, discount, arabic_name, condiment);
    cart.push(item);
    saveCart();

}

function removeItemFromCart(name){
    for(var i in cart){
        if (cart[i].name === name) {
            cart[i].count--;
            if (cart[i].count <= 0) {
                cart.splice(i, 1);
            }
            saveCart();
            break;
        }

    }

    saveCart();
}



function removeItemFromCartAll(name){
    for(var i in cart){
        if(cart[i].name === name){
            cart.splice(i, 1);
            saveCart();
            break;
        }
    }

    saveCart();
}



function clearCart(){
    cart = [];
    $('#input-discount').val(0.000);
    $('#input-discount').text(0.000);
    saveCart();
    displayCart();
}

function countCart(){
    var  totalCount = 0;
    for(var i in cart){
        totalCount += cart[i].count;
    }
    return totalCount;
}

function totalCart(){
    var  totalCost = 0;
    for(var i in cart){
        totalCost += cart[i].price * cart[i].count;
    }
    return totalCost.toFixed(3);
}

function totalCartDiscount(){
    var  totalCartDis = 0;
    for(var i in cart){
        totalCartDis += cart[i].discount;
    }
    return parseFloat(totalCartDis);
}

function discountUpdate(name, disValue){
    for(i in cart){
        if(cart[i].name === name){
            cart[i].discount = disValue;
            saveCart();
            return true;
        }

    }
}




function listCart(){
    //console.log(cart);
    var cartCopy = [];
    for(var i in cart){
        var item = cart[i];
        var itemCopy = {};
        for( var p in item){
            itemCopy[p] = item[p];
        }
        cartCopy.push(itemCopy);
    }
    return cartCopy;
}

function saveCart(){
    localStorage.setItem('shoppingCart', JSON.stringify(cart))
}


function loadCart(){
    cart  = JSON.parse(localStorage.getItem('shoppingCart'))
}


loadCart();
loadOrderData();
displayCart();

/*******  POPUP ********/

$('#split-popup').click(function (){
    // chk_id_generate();
    var chk_id = localStorage.getItem('order_id');
    var orderTypeName = localStorage.getItem('orderTypeName');

    $('#left-chk').html(chk_id+"-1");
    $('#right-chk').html(chk_id+"-2");
    $('#left-chk-value').val(chk_id+"1");
    $('#right-chk-value').val(chk_id+"2");

    if ($('#table_id').val() == null && orderTypeName == "Dine In") {
        alert('Table Is Empty.');
    }else{
        $('#Splitorder').css('display','block');
    }

});

$('#confirm-popup').click(function (){
    var orderTypeName = localStorage.getItem('orderTypeName');
    if ($('#table_id').val() == null && orderTypeName == "Dine In") {
        alert('Table Is Empty.');
    }else{
        $('#popUp').css('display','block');
        $('#is_hold').val(0);
        displayCart();
    }

});

function chk_id_generate(){

    var timestamp = ''+ Date.now() + '';
    var csk = timestamp.slice(7);

    localStorage.setItem('csk_id',csk);
}

function getCustomer(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../get-customer',
        success: function (data) {
            localStorage.setItem('customers', JSON.stringify(data.customers));
            displayCustomer(data.customers);

        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

function displayCustomer(customer){
    var customers_output = "<option value='0' selected='selected' disabled=''>Select</option>";

    for(var i in customer){
        customers_output += "<option class='new_cus' value='"+customer[i].id+"'> "+customer[i].name+": "+ customer[i].phone +"</option>"
    }

    $('#customer_id').html(customers_output);
}

$('#customer_id').change(function(){
    var cus_id = $(this).val();
    localStorage.setItem('customer_id', cus_id);
});

getCustomer();

/******* close check *********/

function getCloseCheck(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../get-close-check',
        success: function (data) {

            localStorage.setItem('close_orders', JSON.stringify(data.close_order));
            close_orders  = JSON.parse(localStorage.getItem('close_orders'));

            displayCloseCheck(close_orders);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function displayCloseCheck(closes_check){
    var close_output = '';
    console.log(closes_check);

    for(i in closes_check){

        close_output += "<div class='col-xs-12 colsm-6 col-md-6' >";
        close_output += "<div class='hold-list-inner'>";
        close_output += "<div class='item'>";
        close_output += "<div class='hold-stock single-close-order' data-close-id='"+i+"' >"+closes_check[i].order_type+"</div>";
        close_output += "<div class='hold-item-dec'>";
        close_output += "<a ><strong> CHK: </strong> "+closes_check[i].csk_id+"</a>";
        if(closes_check[i].table_name != null){

            close_output += "<a ><strong> Table:</strong> "+closes_check[i].table_name+"</a>"
        }else{
            close_output += "<a ><strong> Time: </strong> "+closes_check[i].time+"</a>"
        }
        close_output += "<a ><strong> Date: </strong> "+closes_check[i].date+"</a>";
        // close_output += "<a ><strong> Time: </strong> "+closes_check[i].time+"</a>"


        close_output += "<button class='btn btn-button reprint' data-close-id-re-print='"+i+"' >Re-print</button>";
        close_output += "<button class='btn btn-button re-edit-order' data-close-edit-id='"+i+"' >Edit</button>";
        close_output += "<button class='btn btn-button re-delete-order' data-close-delete-id='"+i+"' >Cancel</button>";

        /*close_output += "<div class='hold-metad'>"
         close_output += "<ul>"
         close_output += "<li><a class='deletes'><span class='fa fa-trash'></span></a></li>"
         close_output += "<li><a class='prints'><span class='fa fa-print'></span></a></li>"
         close_output += "<li class='reprint' data-close-edit-id='"+i+"' ><a class='moneys'><span class='fa fa-fa fa-money'></span></a></li>"
         close_output += "</ul>"
         close_output += "</div>"*/



        close_output += " </div> </div> </div> </div>"

    }
    $('#show-all-close-check').html(close_output);
}


getCloseCheck();


$('#show-all-close-check').on('click','.single-close-order', function(){
    var closeIndex = Number($(this).attr('data-close-id'));
    close_orders  = JSON.parse(localStorage.getItem('close_orders'));

    $('.close-paid-bg').css('display','block');


    for( i in close_orders ){
        if(i == closeIndex){
            var view_close_output = '';
            var close_cart_arr = close_orders[i].cart;
            for(p in close_cart_arr){
                view_close_output += "<tr>";
                view_close_output += "<td class='text-left'>"+close_cart_arr[p].name+"</td>";
                view_close_output += "<td class='text-center'>"+close_cart_arr[p].count+"</td>";
                view_close_output += "<td class='text-center'>"+Number(close_cart_arr[p].discount).toFixed(3)+"</td>";
                view_close_output += "<td class='text-right'>"+Number(close_cart_arr[p].price).toFixed(3)+"</td>";
                view_close_output += "<td class='text-center'>"+Number(close_cart_arr[p].count*close_cart_arr[p].price).toFixed(3)+"</td>";
                view_close_output += "</tr>"


            }

            $('#show-close-chk-number').html((close_orders[i].csk_id));
            $('#show-close-date').html(close_orders[i].date);
            $('#show-close-order-type').html(close_orders[i].order_type);
            $('#show-close-time').html(close_orders[i].time);

            $('#show-close-subtotal').html(Number(close_orders[i].sub_total).toFixed(3));
            $('#show-close-discount').html(Number(close_orders[i].discount).toFixed(3));
            $('#show-close-charges').html(Number(close_orders[i].tax).toFixed(3));
            $('#show-close-tips').html(Number(close_orders[i].tips).toFixed(3));
            $('#show-close-total').html(Number(close_orders[i].total).toFixed(3));

            $('#view-single-close-order').html(view_close_output);


        }
    }
});


$('#open-check').click(function(){
    $('#holdpop').css('display','block');
    $('#hold-show-name').html('Open Check');
});


$('#hold-show-p').click(function(){
    $('#holdpop').css('display','block');
    $('#hold-show-name').html('Hold');
});

$('#show-all-close-check').on('click','.re-delete-order', function(){
    var closeIndex = Number($(this).attr('data-close-delete-id'));
    $('#close-index-number').val(closeIndex);
    $('#holdorder').css('display','block');
    $('#close-re-order-type').val('delete');

});

$('#show-all-close-check').on('click','.reprint', function(){
    var closeIndex = Number($(this).attr('data-close-id-re-print'));
    $('#close-index-number').val(closeIndex);
    $('#holdorder').css('display','block');
    $('#close-re-order-type').val('print');
});


$("#show-all-hold").on('click','.single-hold-delete',function(){
    var holdIndexNumber = $(this).attr('data-hold-index-number');
    $("#input-current-amount-hold").val();
    $('#close-index-number').val(holdIndexNumber);
    $('#holdorder').css('display','block');
    $('#close-re-order-type').val('hold-delete');
});


$('#show-all-close-check').on('click','.re-edit-order', function(){
    var closeIndex = Number($(this).attr('data-close-edit-id'));
    $('#close-index-number').val(closeIndex);
    $('#holdorder').css('display','block');
    $('#close-re-order-type').val('edit');
});

$('#re-print-close-order').click(function(){
    var inputPass = Number($('#input-current-amount-hold').val());
    var staffs_arr  = JSON.parse(localStorage.getItem('staffs_passcode'));
    var close_arr  = JSON.parse(localStorage.getItem('close_orders'));
    var closeIndex = $('#close-index-number').val();
    var staff_len = staffs_arr.length;
    var count_i = 0;

    var close_re_order_type = $('#close-re-order-type').val();

    if(close_re_order_type === 'hold-delete'){
        if(inputPass === 111333){
            $('.Popupmodel').css('display', 'none');

            var holdIndexNumber = $('#close-index-number').val();
            //alert($('#close-index-number').val());
            var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
            for(i in allHoldCart){
                if(i==holdIndexNumber){
                    allHoldCart.splice(i,1);
                    localStorage.setItem('all_hold_orders', JSON.stringify(allHoldCart));
                    displayHoldCart();
                    toastr.success('Open order Successfully Deleted.', 'Success Alert', {timeOut: 3000});
                    break;
                }

            }
        }
    }

    if(close_re_order_type === 'print'){
        for( i in staffs_arr){
            count_i++;
            if(staffs_arr[i].password == inputPass){
                for(c in close_arr){
                    if(closeIndex == c){
                        var outputreport = '';
                        var riportArray = close_orders[c].cart;
                        var tip_amount = Number($('#tip-amount-input').val()).toFixed(3);
                        var delivery_charge = Number($('#delivery-charge-input').val()).toFixed(3);
                        console.log(riportArray);

                        for (var i in riportArray) {
                            var ser = 1 + Number(i);
                            var i_total = (Number(riportArray[i].price)*Number(riportArray[i].count))-Number(riportArray[i].discount);
                            outputreport += "<tr>";
                            outputreport += "<td>" + ser + "</td>";
                            outputreport += "<td>" + riportArray[i].name + "</td>";
                            outputreport += "<td>" + riportArray[i].count + "</td>";
                            outputreport += "<td>" + Number(riportArray[i].price).toFixed(3) + "</td>";
                            outputreport += "<td>" + Number(i_total).toFixed(3) + "</td>";
                            outputreport += "</tr>"
                        }

                        $('#show-bill-order_number').html(close_orders[c].csk_id);

                        $('#show-bill-order-type').html(close_orders[c].order_type);
                        $('#show-bill-person').html(1);
                        $('#show-bill-table-number').html(close_orders[c].table_name);


                        // $('#show-bill-table_id').html(table_id);

                        $('#show-bill-current-time').html(close_orders[c].time);
                        $('#show-bill-current-date').html(close_orders[c].date);

                        $('#show-bill-print').html(outputreport);

                        $('#bill-discount').html(close_orders[c].discount.toFixed(3));
                        $('#bill-tax').html(close_orders[c].tax.toFixed(3));
                        $('#bill-grand-total').html(close_orders[c].total.toFixed(3));
                        $('#bill-sub-total').html(close_orders[c].sub_total.toFixed(3));

                        $('#bill-changes-balance').html(Number(0.000).toFixed(3));
                        $('#bill-payment-type-amount').html(close_orders[c].total.toFixed(3));
                        $('#bill-method-name').html(close_orders[c].method_name);

                        $('#bill-tips-amount').html(close_orders[c].tips.toFixed(3));
                        $("#bill-delivery-charge").html(close_orders[c].delivery.toFixed(3));
                        $("#bill-new-grand-total").html(close_orders[c].grand_total.toFixed(3));



                        $(".Popupmodel").css("display", "none");
                        $(".poscat-list-page").css("display", "none");

                        $(".header-area").css("display", "none");
                        $("#holdorder").css("display", "none");

                        $(".invoice-page").css("display", "block");

                        print();



                        $(".poscat-list-page").css("display", "block");


                        $(".header-area").css("display", "block");
                        $(".invoice-page").css("display", "none");
                        $(".Popupmodel").css("display", "none");

                        $("#finalpayclose").css("display", "block");



                        $('#inputDiscount').val('');
                        $('#input-discount').val('');
                        $('#input-current-amount-hold').val('');




                    }
                }
                break;

            }

            if(count_i == staff_len){
                $('#holdorder').css('display','none');
                toastr.error('Your Pass Code Incorrect.', 'Error Alert', {timeOut: 1000});
            }

        }
    }
    if(close_re_order_type === 'edit'){
        if(inputPass === 111333){
            $('.Popupmodel').css('display', 'none');


            var output_close = '';


            for (i in close_arr) {
                if (i == closeIndex) {
                    var count_hold_cart = 0;
                    var close_arr_singel = (close_arr[i].cart);
                    console.log(close_arr_singel);

                    var holdArray = (close_arr[i].cart);

                    cart = holdArray;
                    saveCart();
                    var output_close = '';


                    for(var p in holdArray){
                        var ser = 1 + Number(i);
                        output_close  +="<tr>";
                        output_close  +="<th scope='row' width='5.3%'>"+(ser)+"</th>";
                        output_close  +="<td width='50%'>"+ holdArray[p].name +"<br/></td>";
                        output_close  +="<td class='text-center' width='13%'>";
                        output_close  +="<div class='pos-pro-body-qit'>";
                        output_close  +="<button class='subtract-item' data-name='"+holdArray[p].name+"' data-id='"+holdArray[p].id+"'><span class='fa fa-minus'></span></button>";
                        output_close  +="<input type='text'value='"+ holdArray[p].count +"' class='pos-pro-body-qit-input'  data-name='"+holdArray[i].name+"' data-id='"+holdArray[i].id+"'>";
                        output_close  +="<button class='plus-item' data-name='"+holdArray[p].name+"' data-id='"+holdArray[p].id+"'><span class='fa fa-plus '></span></button>";
                        output_close  +="</div>";
                        output_close  +="</td>";
                        output_close  +="<td class='text-center'><div class='pos-pro-list-table-dis'><input type='text' class='discount-value-change' data-name='"+holdArray[p].name+"' value='"+holdArray[p].discount+"'></td>";
                        output_close  +="<td class='text-center'>"+ ((holdArray[p].price * holdArray[p].count) - holdArray[p].discount).toFixed(3) +"</td>";
                        output_close  +="<td class='text-center'><a href='#' data-toggle='tooltip' title='Delete' class='delete-item' data-name='"+holdArray[p].name+"'><span class='fa fa-trash'></span></a></td>";
                        output_close  +="</tr>"
                    }



                    var num = 0.000;
                    $('#show-cart').html(output_close);
                    $('#inputDiscount').val((close_arr[i].discount - totalCartDiscount()).toFixed(3));
                    $('#input-discount').html((close_arr[i].discount - totalCartDiscount()).toFixed(3));

                    $('#total-cart').html(totalCart());
                    $('#count-Cart').html(countCart());

                    //$('#total-cart-discount').html(totalCartDiscount().toFixed(3));
                    $('#total-cart-discount').html(totalCartDiscount());
                    var orderyTypeName  = ($(this).attr('data-order-name'));




                    var order_id = close_arr[i].order_id;

                    var taxs_show = '';
                    var discount_show = '';
                    var totalTax = 0;
                    var totalDiscount = 0;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "GET",

                        url: '../order-get-support' + '/' + order_id,
                        success: function (data) {


                            for(t = 0; t < data.taxs.length; t++){
                                totalTax += data.taxs[t].amount;

                                taxs_show += "<li>";
                                taxs_show += "<strong>"+data.taxs[t].name+" : </strong>";
                                taxs_show += "<span>"+Number(data.taxs[t].amount).toFixed(3);+"</span>";
                                taxs_show += " KD ";
                                taxs_show += "</li>";

                            }
                            for(d = 0; d < data.discounts.length; d++){
                                totalDiscount += data.discounts[d].amount;

                                discount_show += "<li>";
                                discount_show += "<strong>"+data.discounts[d].name+" : </strong>";
                                discount_show += "<span>"+Number(data.discounts[d].amount).toFixed(3)+"</span>";
                                discount_show += " KD ";
                                discount_show += "</li>";

                            }

                            $('#show-all-tax').html(taxs_show);
                            $('#show-all-discount').html(discount_show);
                            $('#show-all-tax-hold').html(taxs_show);
                            $('#show-all-discount-hold').html(discount_show);

                            // $('#bill-tips-amount').html(data.tips.toFixed(3));
                            // $("#bill-delivery-charge").html(data.delivery.toFixed(3));
                            // $("#bill-new-grand-total").html(data.grand_total.toFixed(3));
                            //
                            // $("#tip-amount-input").val(data.tips);
                            // $("#delivery-charge-input").val(data.delivery);

                            saveOrderData(data);
                            getOrderyTypeData(data);
                            loadOrderData();

                            console.log(data);





                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            }


        }
    }

    if(close_re_order_type === 'delete'){

        if(inputPass === 111333){
            $('.Popupmodel').css('display', 'none');



            for (i in close_arr) {
                if (i == closeIndex) {



                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "GET",

                        url: '../delete-close-check/'+close_arr[i].csk_id,
                        success: function (data) {
                            $('#input-current-amount-hold').val('');
                            toastr.success('Closed order Successfully Deleted.', 'Success Alert', {timeOut: 3000});
                            getCloseCheck();
                        },
                        error: function (data) {
                            $('#input-current-amount-hold').val('');
                            console.log('Error:', data);
                        }
                    });

                }

            }



        }
    }

    $(document).on("change","select",function(){
        $("option[value=" + this.value + "]", this)
            .attr("selected", true).siblings()
            .removeAttr("selected")
    });



});

$('#colose-re-print-popup').click(function(){
    $('#close-re-order-type').val('');
    $('#holdorder').css('display','none');

});


function getStaffPassCode(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../get-staff-passcode',
        success: function (data) {
            localStorage.setItem('staffs_passcode', JSON.stringify(data.staffs_passcode));
            staffs_passcode  = JSON.parse(localStorage.getItem('staffs_passcode'))


        },
        error: function (data) {
            console.log('Error:', data);
        }
    });

}

getStaffPassCode();


$('.dis-type-select').change(function(){
    var dis = ($(this).val());
    discountType(dis);
});

function discountType(dis){
    localStorage.setItem('discount_type',dis);
    var discount_type = localStorage.getItem('discount_type');
    var tip_amount = $('#tip-amount-input').val();
    var delivery_charge = $("#delivery-charge-input");

    $('#dis-type-show').html(discount_type);


    var inputDis = $('#inputDiscount').val();
    var grand_total = $('#grand-total').text();




    var totalcart = Number(totalCart()).toFixed(3);

    var dis2 = Number(totalCartDiscount());
    var total = totalcart - dis2;


    if(discount_type == 'Percent'){
        var inputdisvalue = Number((total*inputDis)/100);
        $('#show-kd').html('%');
        /*$('#input-discount').text(Number(persent_dicount).toFixed(3));
         $('#inputDiscount_2').text(Number(persent_dicount).toFixed(3));*/
    }else{
        var inputdisvalue = Number($('#inputDiscount').val());
        $('#show-kd').html('KD');
        /*$('#input-discount').text(Number(inputDis).toFixed(3));
         $('#inputDiscount_2').text(Number(inputDis).toFixed(3));*/
    }

    inputdisvalue = inputdisvalue.toFixed(3);

    var alldiscount = (Number(dis2) + Number(inputdisvalue)).toFixed(3);

    var grandTotalValue = (Number(totalcart) - Number(alldiscount) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);


    $('#inputDiscount_2').val(inputdisvalue);

    $('#input-discount').html(inputdisvalue);
    $('#input-discount-hold').html(inputdisvalue);

    $('#split-order-discount').html(alldiscount);

    $('#grand-total').html(grandTotal());

    $('#split-order-total').html(grandTotal());

    localStorage.setItem('split_order_total_discount', alldiscount);
    localStorage.setItem('split_order_total', totalcart);

    getOrderyTypeData(strogOrderData);


}


localStorage.setItem('customer_id', 0);


function customerInfoShow(cus_id){
    $('.customer-info').css('display','block');
    var customers  = JSON.parse(localStorage.getItem('customers'));
    console.log(customers);
    for(i in customers){
        if(customers[i].id == cus_id){
            $('#cus-bill-name').html(customers[i].name);
            $('#cus-bill-phone').html(customers[i].phone);
            $('#cus-bill-w_number').html(customers[i].w_number);
            $('#cus-bill-address').html(customers[i].address);
            $('#cus-bill-area').html(customers[i].area);
            $('#cus-bill-street').html(customers[i].street);
            $('#cus-bill-street-no').html(customers[i].street_no);
            $('#cus-bill-block').html(customers[i].block);
            $('#cus-bill-building').html(customers[i].building);
            $('#cus-bill-flat').html(customers[i].flat);
            $('#cus-bill-floor').html(customers[i].floor);
        }
    }
}


function getOrderIdFn(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",

        url: '../last-order-data',
        success: function (data) {

            var get_id = Number(data.id);
            var final_id = get_id+Number(1);

            localStorage.setItem('order_id',final_id);

            localStorage.getItem('order_id');

        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function set_hold_id(){
    var hold_id = localStorage.getItem('hold_id');
    hold_id = (Number(hold_id))+1;
    localStorage.setItem('hold_id',hold_id);
}

function grandTotal() {
    var discount_type = localStorage.getItem('discount_type');
    var inputDis = $('#inputDiscount').val();
    var tip_amount = $("#tip-amount-input").val();
    var delivery_charge = $("#delivery-charge-input").val();
    delivery_charge = isNaN(delivery_charge) ? 0 : delivery_charge;
    //var grand_total = $('#grand-total').text();

    $('#dis-type-show').html(discount_type);


    var totalcart = Number(totalCart()).toFixed(3);

    var dis2 = Number(totalCartDiscount());
    var total = totalcart-dis2;


    if(discount_type == 'Percent'){
        var inputdisvalue = Number((total*inputDis)/100);
        $('#show-kd').html('%');

    }else{
        var inputdisvalue = Number($('#inputDiscount').val());
        $('#show-kd').html('KD');
    }

    inputdisvalue = inputdisvalue.toFixed(3);

    var alldiscount = (Number(dis2)+ Number(inputdisvalue)).toFixed(3);

    return (Number(totalcart)-Number(alldiscount) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);
}


// function condimentList(){
//     $.ajax({
//         url: '../res/condiment-list',
//         type: 'get'
//     }).done(function (e) {
//         $(".condiment-input").append(e);
//
//         $(document).on("change","select",function(){
//             $("option[value=" + this.value + "]", this)
//                 .attr("selected", true).siblings()
//                 .removeAttr("selected")
//         });
//
//     })
// }
//
// $("#condiment").change(function(){
//
// });





          