
var SplitOrder = (function ( $ ) {

    function privateMethod1(){
        // jQ(".container").html("test");
        console.log('from module')
    }

    function remove_items_from_split(idxs) {
        var current_items = localStorage.getItem('shoppingCart') ? JSON.parse(localStorage.getItem('shoppingCart')) : [];

        console.log(current_items);

        if (idxs) {
            idxs.forEach(function (idx) {
                current_items.splice(idx, 1);
            });
        }


        localStorage.setItem('shoppingCart', JSON.stringify(current_items));
    }

    return{
        remove_items_from_split: remove_items_from_split
    };

})( jQuery);

var multiselect_box = $('#split_order_items');

multiselect_box.multiSelect();

$('#split_order_btn').on('click', function (e) {
    e.preventDefault();
    $('#Splitorder').css({
        'display' : 'block'
    });

    console.log('split');

    multiselect_box.multiSelect('refresh');

    var cart_items  = JSON.parse(localStorage.shoppingCart);



    console.log(cart_items);

    if (cart_items) {
        cart_items.forEach(function(value, key) {
            multiselect_box.multiSelect('addOption', { value: value.id, text: 'Name: ' + value.name + ', Qty: ' + value.count + ', Price: ' + value.price});
        });
    }

});

$('#split_order_submit').on('click', function (e) {
    e.preventDefault();
    $('#split_order_form').submit();
});

$('#split_order_close').on('click', function (e) {
    e.preventDefault();
    multiselect_box.html('');
    $('#Splitorder').css({
        'display' : 'none'
    });
});