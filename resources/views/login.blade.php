<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Dictator Soft">
    <title>Point of Sale</title>
    <link href="{{ asset('pos/images/favicon.png') }}" rel="icon"/>

    <!-- bootstrap css -->
    <link href="{{ asset('pos/libraries/bootsrap/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- / bootstrap css -->

    <!-- owl carousel css -->
    <link href="{{ asset('pos/libraries/owlcarousel/owl.carousel.css') }}" rel="stylesheet"/>
    <!-- / owl carousel css -->
    <!-- bootsnav css -->
    <link href="{{ asset('pos/libraries/bootsnav/bootsnav.css') }}" rel="stylesheet"/>
    <!-- / bootsnav css -->
    <!--  icon css -->
    <link href="{{ asset('pos/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <!-- / icon css -->

    <!-- animations css -->
    <link href="{{ asset('pos/libraries/animations/animate.css') }}" rel="stylesheet"/>
    <!-- / animations css -->

    <link href="{{ asset('pos/css/style.css') }}" rel="stylesheet"/>
    <!-- / style css -->

    <!--  media css -->
    <link href="{{ asset('pos/css/media.css') }}" rel="stylesheet"/>
    <!-- / media css -->

    <!-- / font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:300,300i,400,400i,500,700,900|Roboto:400,700|Roboto:300,300i,400,400i,500,700,900"
          rel="stylesheet">
    <!-- / font css -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
            href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

</head>
<body>
<div class="main">
    <div class="login-page">
        <div class="top-hadding  bg-color text-center">
            <div class="container">
                <div class="top-hadding"><h2><span class="co-white">Point Of Sale</span></h2></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-3 col-md-offset-3 col-sm-6 col-md-6 col-offset-3">
                    <div class="login-inner">
                        <h2 class="page-hadding bg-color text-left">Login to system</h2>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{csrf_field()}}

                            <div class="allinfo"><h3><span>Please complete all <span
                                                class="nor-color">information</span></span></h3></div>
                            <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="lgFormGroupInput" class="col-sm-3 col-form-label col-form-label-lg">Email
                                    :</label>
                                <div class="col-sm-9">

                                    <input type="email" name="email" class="form-control form-control-lg"
                                           placeholder="Enter Your Email...." required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="smFormGroupInput" class="col-sm-3 col-form-label col-form-label-sm" >Password
                                    :</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control form-control-sm" name="password"
                                           placeholder="Enter your password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button class="btn-button login-button bg-color">submit</button>
                            </div>
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember Me
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer-section bg-color">
    <div class="container">
        <p class="copyringt text-center"><span class="fa fa-copyright"></span> All Rights Reserved <a
                    data-toggle="tooltip" data-placement="top" title="Ringer! Soft your Support partner"
                    href="http://ringersoft.com/">Dictator Soft</a> 2018.</p>
    </div>
</footer>
<script src="{{ asset('pos/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('pos/libraries/bootsrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('pos/libraries/vendor/modernizr-2.8.3.min.js') }}"></script>
<script src="{{ asset('pos/libraries/bootsnav/bootsnav.js') }}"></script>
<script src="{{ asset('pos/libraries/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('pos/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('pos/libraries/animations/wow.min.js') }}"></script>


<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
            @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";

    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif

        toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'slideUp';
    toastr.options.closeMethod = 'slideUp';
    toastr.options.closeButton = true;
</script>
</body>

</html>