@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="report-header">
            <div class="row">
                <div class="kcol-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="prport-tatepiker">
                        <form action="" method="get">
                            <table class="table">
                                <thead>
                                <tr>

                                    <th width="90%" class="pull-right">Search By Item Name/Category Name: <input type="text" class="span2"
                                                                                              value="@if(isset($string)) {{ $string }} @endif" name="item_name"
                                                                                              style="width: 50%;">
                                    </th>
                                    <th>
                                        <div class="report-search pull-left">
                                            <button class="btn btn-button">Search</button>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Item List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products  as $key => $item)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            <img src='{{asset($item->p_image)}}' width="80px">
                                        </td>
                                        <td>{{$item->p_name}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->p_price}} {{ config('app.currency')}}</td>
                                        <td>{{$item->p_title}}</td>
                                        <td>
                                            @if($item->is_active === "active")
                                                <a data-id="{{ $item->id }}"
                                                   data-status="{{ $item->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $item->id }}"
                                                   data-status="{{ $item->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $item->id }}" data-name="{{ $item->p_name }}"
                                                   data-category="{{ $item->cat_id }}"
                                                   data-title="{{ $item->p_title }}"
                                                   data-price="{{ $item->p_price }}"
                                                   data-image="{{ $item->p_image }}"
                                                   data-arabic_name="{{ $item->arabic_name }}"
                                                   data-status="{{ $item->is_active }}"
                                                   data-target="#edit-item"
                                                   class="user-edits" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/res/delete-product-item/{{$item->id}}" class="user-removed"
                                                   data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-trash"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            @if(count($products) > 0)

                                <div class="alert alert-info">
                                    Showing {{ $products->firstItem() }} to {{ $products->lastItem() }} Product of
                                    Total {{ $products->total() }} Products.
                                </div>

                                <nav aria-label="Page navigation">
                                    @if ($products->lastPage() > 1)
                                        <ul class="pagination pagination-sm">
                                            <li class="page-item {{ ($products->currentPage() == 1) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $products->url(1) }}">Previous</a>
                                            </li>
                                            @for ($i = 1; $i <= $products->lastPage(); $i++)
                                                <li class="page-item {{ ($products->currentPage() == $i) ? 'page active' : '' }}">
                                                    <a class="page-link" href="{{ $products->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item {{ ($products->currentPage() == $products->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link"
                                                   href="{{ $products->url($products->currentPage()+1) }}">Next</a>
                                            </li>
                                        </ul>
                                    @endif
                                </nav>
                            @else
                                <div class="alert alert-danger">
                                    No Result Found!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Customer Edit Pop Up -->

    <div id="edit-item" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Item Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('product.update') }}" method="post" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Edit Item</h2>

                            <input type="hidden" name="id" id="item-id">

                            <div class="form-group">
                                <label for="usr">Item Category:</label>
                                <select class="form-control" name="cat_id" id="item-cat-id">
                                    <option disabled="" selected="">Select Item</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="p_name" id="item-name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Arabic Name:</label>
                                <input type="text" name="arabic_name" id="arabic_name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">price:</label>
                                <input type="text" name="p_price" id="item-price" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="p_title" id="item-title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Image:</label>
                                <input type="file" name="image" class="form-control" onchange="readURL(this);">
                            </div>

                            <div class="form-group">
                                <img id="blah" src="http://placehold.it/300x200" height="50"
                                     class="img-responsive img-thumbnail item-image" alt="your image"/>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="is_active" id="item-status" value="active"
                                           checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>

                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this product information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $('#edit-item').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var arabic_name = $(e.relatedTarget).data('arabic_name');
            var price = $(e.relatedTarget).data('price');
            var title = $(e.relatedTarget).data('title');
            var cat_id = $(e.relatedTarget).data('category');
            var image = $(e.relatedTarget).data('image');
            var status = $(e.relatedTarget).data('status');


            $('#item-id').val(id);
            $('#item-name').val(name);
            $('#arabic_name').val(arabic_name);
            $('#item-price').val(price);
            $('#item-title').val(title);
            $('select#item-cat-id').val(cat_id);

            $('img.item-image').attr('src', '{{ url('/') }}/' + image);

            if (status === 'on') {
                $('#item-status').prop('checked', true);
            }

        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-product-status')}}/' + id);


        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


@endsection