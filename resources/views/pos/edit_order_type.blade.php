@extends('admin_layouts.default')
@section('content')

    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <form action="{{ route('update.order-type') }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="{{ $details->id }}">

                        <div class="user-inputbox">

                            <h2 class="user-list-title">Update Order Type</h2>


                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h2 class="user-list-title"><strong>Tax</strong></h2>
                                    <div class="form-group">
                                        @foreach($taxs as $tax)
                                            <input type="checkbox" name="tax_type[]"

                                                   @php
                                                    $taxs = explode(',',$details->tax_type);
                                                    foreach ($taxs as $t)
                                                   {
                                                        if ($t == $tax->id){
                                                            echo 'checked';
                                                        }
                                                   }
                                                   @endphp

                                                   value="{{$tax->id}}"> {{$tax->name}}:  <span
                                                    class="order-span-price"> {{$tax->percent}} </span><br>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h2 class="user-list-title"><strong>Discount</strong></h2>
                                    <div class="form-group">
                                        @foreach($discounts as $discount)
                                            <input type="checkbox" name="discount_type[]"


                                                   @php
                                                       $discounts = explode(',',$details->dis_type);
                                                       foreach ($discounts as $d)
                                                      {
                                                           if ($d == $discount->id){
                                                               echo 'checked';
                                                           }
                                                      }
                                                   @endphp


                                                   value="{{$discount->id}}"> {{$discount->name}}: <span
                                                    class="order-span-price"> {{$discount->percent}} </span><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" value="{{ $details->name }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="title" value="{{ $details->title }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection