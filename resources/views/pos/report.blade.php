@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

    <style>
        .btn-group-sm > .btn, .btn-sm {
            font-size: 9px;
            border-radius: 100px;
        }

        .btn-primary {
            background-color: #fb787e;
            border-color: #fb787e;
            margin: 3px;
        }

        .invoice-page .invo-header-info {
            width: 100%;
        }
    </style>

@endsection

@section('content')
    <div id="report-desc" class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">


                            <div class="kcol-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="prport-tatepiker">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th>Form Date: <input type="text" class="span2 date_input" name="start_date"
                                                                  @if(isset($_GET['start_date'])) value="{{ $_GET['start_date'] }}"
                                                                  @endif required></th>

                                            <th>To Date: <input type="text" class="span2 date_input" name="end_date"
                                                                @if(isset($_GET['end_date'])) value="{{ $_GET['end_date'] }}"
                                                                @endif required></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="report-search text-right">
                                    <br>
                                    <select class="re-monthly" name="sort_type" data-role="sorter" id="sort-sales">
                                        <option value="0"
                                                @if(isset($sort_type)) @if($sort_type == 0) selected @endif @endif>Sales
                                            By
                                        </option>

                                        <option value="7"
                                                @if(isset($sort_type)) @if($sort_type == 7) selected @endif @endif>
                                            Sales By Complementary Discount
                                        </option>

                                        <option value="1"
                                                @if(isset($sort_type)) @if($sort_type == 1) selected @endif @endif>Sales
                                            by Category
                                        </option>
                                        <option value="2"
                                                @if(isset($sort_type)) @if($sort_type == 2) selected @endif @endif>Sales
                                            by Item
                                        </option>
                                        <option value="3"
                                                @if(isset($sort_type)) @if($sort_type == 3) selected @endif @endif>Sales
                                            by Order Type
                                        </option>
                                        <option value="4"
                                                @if(isset($sort_type)) @if($sort_type == 4) selected @endif @endif>Sales
                                            by Table
                                        </option>
                                        <option value="5"
                                                @if(isset($sort_type)) @if($sort_type == 5) selected @endif @endif>Sales
                                            by Payment Type
                                        </option>

                                        <option value="6"
                                                @if(isset($sort_type)) @if($sort_type == 6) selected @endif @endif>Sales
                                            by Staff
                                        </option>
                                    </select>

                                    <!-- Category List -->

                                    <select class="re-monthly" name="category_id" data-role="sorter"
                                            style="display: @if(isset($category_display)) {{ $category_display }}@else none @endif"
                                            id="category-list">
                                        <option selected="selected">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                    @if(isset($category_display))
                                                    @if(isset($category_id))
                                                    @if($category_id == $category->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $category->name }}</option>
                                        @endforeach
                                    </select>


                                    <!-- Item List -->

                                    <select class="re-monthly" name="item_id" data-role="sorter"
                                            style="display: @if(isset($item_display)) {{ $item_display }}@else none @endif"
                                            id="item-list">
                                        <option selected="selected">Select Item</option>
                                        <option selected="0">All Item</option>
                                        @foreach($items as $item)
                                            <option value="{{ $item->id }}"
                                                    @if(isset($item_display))
                                                    @if(isset($item_id))
                                                    @if($item_id == $item->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $item->p_name }}</option>
                                        @endforeach
                                    </select>


                                    <!-- Order Type List -->

                                    <select class="re-monthly" name="order_type_id" data-role="sorter"
                                            style="display: @if(isset($order_type_display)) {{ $order_type_display }}@else none @endif"
                                            id="order-type-list">
                                        <option selected="selected">Select Order Type</option>
                                        @foreach($order_types as $order_type)
                                            <option value="{{ $order_type->id }}"
                                                    @if(isset($order_type_display))
                                                    @if(isset($order_type_id))
                                                    @if($order_type_id == $order_type->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $order_type->name }}</option>
                                        @endforeach
                                    </select>


                                    <!-- Table List -->

                                    <select class="re-monthly" name="table_id" data-role="sorter"
                                            style="display: @if(isset($table_display)) {{ $table_display }}@else none @endif"
                                            id="table-list">
                                        <option selected="selected">Select Table</option>
                                        @foreach($tables as $table)
                                            <option value="{{ $table->id }}"
                                                    @if(isset($table_display))
                                                    @if(isset($table_id))
                                                    @if($table_id == $table->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $table->name }}</option>
                                        @endforeach
                                    </select>


                                    <!-- Method List -->

                                    <select class="re-monthly" name="method_id" data-role="sorter"
                                            style="display: @if(isset($method_type_display)) {{ $method_type_display }}@else none @endif"
                                            id="method-list">
                                        <option selected="selected">Select Payment Method</option>
                                        @foreach($method_types as $method_type)
                                            <option value="{{ $method_type->id }}"
                                                    @if(isset($method_type_display))
                                                    @if(isset($method_id))
                                                    @if($method_id == $method_type->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $method_type->method_name }}</option>
                                        @endforeach
                                    </select>

                                    <!-- Staff List -->

                                    <select class="re-monthly" name="user_id" data-role="sorter"
                                            style="display: @if(isset($user_display)) {{ $user_display }}@else none @endif"
                                            id="user-list">
                                        <option selected="selected">Select Staff</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}"
                                                    @if(isset($user_display))
                                                    @if(isset($user_id))
                                                    @if($user_id == $user->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $user->name }}</option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-button">Search</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h2>Orders Report</h2><br>
                        <img src="{{ asset('pos/images/ddddd.png') }}" width="150"/>
                        <h4>{{ strtoupper(systemInfo('system_name'))}}</h4>
                        <h4>{{ systemInfo('local_name') }}</h4><br>
                        @if(isset($date1))
                            <h5>Report From: <b>{{ date('l, dS F Y', strtotime($date1)) }} </b>
                            <!--to <b>{{ date('l, dS F Y', strtotime($date2)) }}</b></h5>-->
                                to <b>{{ $date2->subDay()->format('l, dS F Y') }}</b></h5>
                        @else
                            @if(Carbon\Carbon::now()->format('H') >= 6)
                                <h5>Date: {{ date('l, dS F Y') }} </h5>
                            @else
                                <h5>Date: {{ Carbon\Carbon::yesterday()->format('l, dS F Y') }}</h5>
                            @endif
                        @endif
                    </div>
                    <div class="report-table2">
                        <!--Table-->

                        @if($data->count() > 0)
                            <table class="table">

                                <!--Table head-->
                                <thead class="blue-grey lighten-4">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">CHK</th>
                                    <th class="text-center">Order Type</th>
                                    <th class="text-center">Order Items</th>
                                    <th class="text-center">Table Name</th>
                                    <th class="text-center">Payment Method</th>
                                    <th class="text-center" width="8%">Total</th>
                                    <th class="text-center" width="8%">Tips</th>
                                    <th class="text-center" width="8%">Delivery</th>
                                    <th class="text-center" width="8%">Discount</th>
                                    <th class="text-center" width="8%">CMP. Discount</th>
                                    <th class="text-center" width="8%">Grand Total</th>
                                    <th class="text-center" width="8%">Paid</th>
                                    <th class="text-center" width="8%">Time</th>
                                </tr>
                                </thead>
                                <!--Table head-->

                                <!--Table body-->
                                <tbody>

                                @foreach($data as $key=>$report)

                                    <tr>
                                        <th scope="row" class="text-center">{{ $key+1 }}</th>

                                        {{--                                        <th scope="row" class="text-center">{{ $report->csk_id }}</th>--}}
                                        <th scope="row" class="text-center">{{ $report->id }}</th>

                                        <td class="text-center">
                                            {{ $report->GetOrderType($report->order_type_id) }}
                                        </td>

                                        <td>
                                            @php
                                                $products = $report->GetOrderItems($report->id);
                                            @endphp

                                            @foreach ($products as $product)
                                                @php
                                                    echo '
                                                    <button class="btn btn-primary btn-sm">'.$product->p_name.'
                                                        ('.$product->qty.')
                                                    </button> ';
                                                @endphp
                                            @endforeach
                                        </td>

                                        <td class="text-center">
                                            @php $name = $report->GetTableName($report->table_id);
                                            $table = ($name == NULL) ? 'N/A' : $name;
                                            echo $table;
                                            @endphp
                                        </td>

                                        <td class="text-center">
                                            {{--@php--}}
                                                {{--if ($report->method_id){--}}
                                                    {{--echo $report->GetMethodName($report->method_id);--}}
                                                {{--}else{--}}
                                                    {{--echo 'No Info Found!';--}}
                                                {{--}--}}
                                            {{--@endphp--}}
                                            @if($report->method_id)
                                                <span class="badge">{{ $report->method->method_name }}</span>
                                            @endif
                                            @if($report->one_method_id)
                                                <span class="badge">{{ $report->methodOne->method_name }}</span>
                                            @endif
                                            @if($report->two_method_id)
                                                <span class="badge">{{ $report->methodTwo->method_name }}</span>
                                            @endif
                                            {{--@foreach($report->methods as $key => $method)--}}
                                                {{--<span class="badge">--}}
                                                    {{--{{ $method->method_name }}<br>--}}
                                                {{--</span>--}}
                                            {{--@endforeach--}}
                                        </td>

                                        <td class="text-center">
                                            {{ $report->sub_total }} {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            {{--                                            @php $tips = ($report->tips == NULL) ? 0 : $report->tips; echo $tips; @endphp--}}
                                            {{ $report->tips or '0.000' }}
                                            {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            {{--                                            @php $tips = ($report->tips == NULL) ? 0 : $report->tips; echo $tips; @endphp--}}
                                            {{ $report->delivery or '0.000' }}
                                            {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            @php $discount = ($report->discount == NULL) ? 0 : $report->discount; echo $discount; @endphp
                                            {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            @if($report->cmp == 1)
                                                <div class="label label-warning">Yes</div>
                                            @else
                                                <div class="label label-info">No</div>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            {{ number_format(($report->total + $report->tips + $report->delivery),3) }}  {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            @if($report->paid)
                                                {{ number_format($report->paid,3) }}  {{ config('app.currency')}}
                                            @else
                                                {{ number_format(($report->pay_one + $report->pay_two),3) }}  {{ config('app.currency')}}
                                            @endif
                                        </td>


                                        <td class="text-center">
                                            {{-- date('h:i A', strtotime($report->updated_at)) --}}
                                            {{ $report->updated_at->format('d.m.Y h:i A') }}
                                        </td>

                                    </tr>

                                @endforeach
                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Restaurant Sales</u></b></td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Cash Sales:</td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',1)->sum('sub_total') + $data->where('one_method_id',1)->sum('pay_one') + $data->where('two_method_id',1)->sum('pay_two') + $data->where('method_id',1)->sum('tips') + $data->where('method_id',1)->sum('delivery')) - ($data->where('method_id',1)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Credit Card Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->whereIn('method_id',[3,4,5])->sum('sub_total') + $data->whereIn('method_id', [3,4,5])->sum('tips') + $data->whereIn('method_id', [3,4,5])->sum('delivery'))-($data->whereIn('method_id',[3,4,5])->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">K-Net Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',6)->sum('sub_total') + $data->where('one_method_id',6)->sum('pay_one') + $data->where('two_method_id',6)->sum('pay_two') + $data->where('method_id',6)->sum('tips') + $data->where('method_id',6)->sum('delivery')) - ($data->where('method_id',6)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">City Ledger Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{-- number_format(($data->where('method_id',12)->sum('sub_total') + $data->where('method_id',12)->sum('tips') + $data->where('method_id',12)->sum('delivery')) - ($data->where('method_id',12)->sum('discount')),3) --}} 0.000 KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Makan Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Makan Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('order_type_id',5)->sum('sub_total') + $data->where('order_type_id',5)->sum('tips') + $data->where('order_type_id',5)->sum('delivery')) - ($data->where('order_type_id',5)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',12)->sum('sub_total') + $data->where('one_method_id',12)->sum('pay_one') + $data->where('two_method_id',12)->sum('pay_two') + $data->where('method_id',12)->sum('tips') + $data->where('method_id',12)->sum('delivery')) - ($data->where('method_id',12)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',9)->sum('sub_total') + $data->where('one_method_id',9)->sum('pay_one') + $data->where('two_method_id',9)->sum('pay_two') + $data->where('method_id',9)->sum('tips') + $data->where('method_id',9)->sum('delivery')) - ($data->where('method_id',9)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',13)->sum('sub_total') + $data->where('one_method_id',13)->sum('pay_one') + $data->where('two_method_id',13)->sum('pay_two') + $data->where('method_id',13)->sum('tips') + $data->where('method_id',13)->sum('delivery')) - ($data->where('method_id',13)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <!-- Deliveroo Start -->
                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Deliveroo Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('order_type_id',7)->sum('sub_total') + $data->where('one_method_id',7)->sum('pay_one') + $data->where('two_method_id',7)->sum('pay_two') + $data->where('order_type_id',7)->sum('tips') + $data->where('order_type_id',7)->sum('delivery')) - ($data->where('order_type_id',7)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style ="font-weight: bold;">Deliveroo-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',15)->sum('sub_total') + $data->where('one_method_id',15)->sum('pay_one') + $data->where('two_method_id',15)->sum('pay_two') + $data->where('one_method_id',15)->sum('pay_one') + $data->where('two_method_id',15)->sum('pay_two') + $data->where('one_method_id',15)->sum('tips') + $data->where('two_method_id',15)->sum('delivery')) - ($data->where('method_id',15)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',16)->sum('sub_total') + $data->where('one_method_id',16)->sum('pay_one') + $data->where('two_method_id',16)->sum('pay_two') + $data->where('method_id',16)->sum('tips') + $data->where('method_id',16)->sum('delivery')) - ($data->where('method_id',16)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',17)->sum('sub_total') + $data->where('one_method_id',17)->sum('pay_one') + $data->where('two_method_id',17)->sum('pay_two') + $data->where('method_id',17)->sum('tips') + $data->where('method_id',17)->sum('delivery')) - ($data->where('method_id',17)->sum('discount')),3) }} KD
                                    </td>
                                </tr>
                                <!-- Deliveroo End -->

                                <!-- Carriage Start -->
                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Carriage Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('order_type_id',8)->sum('sub_total') + $data->where('order_type_id',8)->sum('tips') + $data->where('order_type_id',8)->sum('delivery')) - ($data->where('order_type_id',8)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style ="font-weight: bold;">Carriage-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',18)->sum('sub_total') + $data->where('one_method_id',18)->sum('pay_one') + $data->where('two_method_id',18)->sum('pay_two') + $data->where('method_id',18)->sum('tips') + $data->where('method_id',18)->sum('delivery')) - ($data->where('method_id',18)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',19)->sum('sub_total') + $data->where('one_method_id',19)->sum('pay_one') + $data->where('two_method_id',19)->sum('pay_two') + $data->where('method_id',19)->sum('tips') + $data->where('method_id',19)->sum('delivery')) - ($data->where('method_id',19)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',20)->sum('sub_total') + $data->where('one_method_id',20)->sum('pay_one') + $data->where('two_method_id',20)->sum('pay_two') + $data->where('method_id',20)->sum('tips') + $data->where('method_id',20)->sum('delivery')) - ($data->where('method_id',20)->sum('discount')),3) }} KD
                                    </td>
                                </tr>
                                <!-- Carriage End -->

                                <!-- Talabat Start -->
                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Talabat Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Talabat Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('order_type_id',6)->sum('sub_total') + $data->where('order_type_id',6)->sum('tips') + $data->where('order_type_id',6)->sum('delivery')) - ($data->where('order_type_id',6)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style ="font-weight: bold;">Talabat-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',10)->sum('sub_total') + $data->where('one_method_id',10)->sum('pay_one') + $data->where('two_method_id',10)->sum('pay_two') + $data->where('method_id',10)->sum('tips') + $data->where('method_id',10)->sum('delivery')) - ($data->where('method_id',10)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Talabat-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">

                                        {{ number_format(($data->where('method_id',8)->sum('sub-total') + $data->where('one_method_id',8)->sum('pay_one') + $data->where('two_method_id',8)->sum('pay_two') + $data->where('method_id',8)->sum('tips') + $data->where('method_id',8)->sum('delivery')) - ($data->where('method_id',8)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Talabat-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->where('method_id',14)->sum('sub_total') + $data->where('one_method_id',14)->sum('pay_one') + $data->where('two_method_id',14)->sum('pay_two') + $data->where('method_id',14)->sum('tips') + $data->where('method_id',14)->sum('delivery')) - ($data->where('method_id',14)->sum('discount')),3) }} KD
                                    </td>
                                </tr>
                                <!-- Talabat End -->

                                <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Total Sales Information</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Gross Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->sum('sub_total') + $data->sum('tips') + $data->sum('delivery')),3) }} KD
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Total Complementary
                                        Discount:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        @php echo number_format($data->where('cmp', 1)->sum('discount'), 3).' KD'; @endphp
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Total Discount:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        @php echo number_format($data->sum('discount'), 3).' KD'; @endphp
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Total Tips:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        @php echo number_format($data->sum('tips'), 3).' KD'; @endphp
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Total Delivery:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        @php echo number_format($data->sum('delivery'), 3).' KD'; @endphp
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Net Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($data->sum('total') + $data->sum('tips') + $data->sum('delivery')),3) }} KD
                                    </td>
                                </tr>


                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Credit Card Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($credit_card_total, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">K-Net Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($knet_total, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Carriage Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($carriage_total, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Talabat Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($talabat_total, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Cash Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($cash_total, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Gross Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($gross_sales, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Total Complementary--}}
                                {{--Discount:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($cmp_discount, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Total Discount:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($total_discount, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Total Tips:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($total_tips, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td colspan="11" class="text-right" style="font-weight: bold;">Net Sales:--}}
                                {{--</td>--}}
                                {{--<td colspan="3" style="font-weight: bold;" class="text-center">--}}
                                {{--@php echo number_format($net_sales, 3). ' '.config('app.currency'); @endphp--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                </tbody>
                                <!--Table body-->
                            </table>
                        @else
                            <div class="alert alert-danger">
                                No Result Found!
                            </div>
                        @endif
                        @if($data->count() > 0)

                            <div class="alert alert-info">
                                Total {{ $data->count() }} Orders Information Found.
                            </div>
                    @endif
                    <!--Table-->
                    </div>
                </div>

                @php $current_url = Request::getPathInfo() . (Request::getQueryString() ? ('?' . Request::getQueryString()) : '');
                    $query_string = substr($current_url,11);
                @endphp
                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color"
                       href="{{ url('res/pdf_report'.$query_string) }}">Save
                        As PDF</a>
                </div>

                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                       onclick="printReport()">Thermal Print</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="thermal-print" class="invoice-page" style="display: none;">
            <div class="invoice-area">
                <div class="inv-header">
                    <div class="invo-logo text-center">
                        <strong>{{ strtoupper(config('app.name'))}}</strong>

                    </div>
                    <div class="invo-header-content" id="clipPolygon">
                        <div class="invo-header-info">
                            <ul>
                                <li><span id="show-bill-phone"> Phone : {{ config('app.phone')}}</span></li>
                                <li><span id="show-bill-email"> Email : {{ config('app.email')}}</span></li>
                                <li><span id="show-bill-email"> @if(isset($_GET['start_date']))
                                            <p>Report From: {{ $_GET['start_date'] }}
                                                to {{ $_GET['end_date'] }}</p>@endif</span></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="invoice-center">

                    <hr>

                    <ul style="padding-left: 10px;">
                        <li><b>Date:</b> <span id="show-bill-current-date">{{ date('d M Y') }}</span></li>
                        <li><b>Time:</b> <span id="show-bill-current-time">{{ date('h:i A')  }}</span></li>

                    </ul>

                    <hr>

                </div>
                <div class="voice-tables">

                    <div class="pos-total-counts">
                        <div class="pos-to-right ">
                            <ul>


                                <li>
                                    <div class="free">
                                        <strong>Total Item Sold </strong>
                                    </div>
                                    <div class="text-right">
                                        {{-- <span class="text-right"> <span id="bill-discount">  {{ count($product_sold) }}</span> Item </span> --}}
                                    </div>
                                </li>


                                <li>
                                    <div class="free">
                                        <strong>Cash Sales </strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-discount">  @php echo number_format($data->where('method_id', 1)->sum('total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>


                                <li>
                                    <div class="free">
                                        <strong>Credit Card Sales </strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-discount">  @php echo number_format($data->whereIn('method_id', [3,4,5])->sum('total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>K-Net Sales</strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax">  @php echo number_format($data->where('method_id', 6)->sum('total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>Makan Sales</strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax">  @php echo number_format($data->where('order_type_id', 5)->sum('total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>Talabat Sales</strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax">  @php echo number_format($data->where('order_type_id', 6)->sum('total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>Total Complementary Discount</strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax"> @php echo number_format($data->where('cmp', 1)->sum('discount'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>Total Discount </strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"><span
                                                    id="bill-sub-total"> @php echo number_format($data->sum('discount'), 3); @endphp</span> {{ config('app.currency')}}</span>
                                    </div>
                                </li>


                                <li>
                                    <div class="free">
                                        <strong>Total Tips</strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax">  @php echo number_format($data->sum('tips'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                                <li>
                                    <div class="free">
                                        <strong>Gross Sales </strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-tax">  @php echo number_format($data->sum('sub_total'), 3); @endphp</span> {{ config('app.currency')}} </span>
                                    </div>
                                </li>

                            </ul>

                            <ul class="total">
                                <li><strong>Net Sales :</strong> <span
                                            id="bill-grand-total">  @php echo number_format($data->sum('total'), 3); @endphp</span>
                                    {{ config('app.currency')}}
                                </li>
                            </ul>

                        </div>
                        <p>Thank you</p>
                        <p>Visit Us Again</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        function printReport() {

            $('#thermal-print').css('display', 'block');
            $('#report-desc').css('display', 'none');
            window.print();
            $('#thermal-print').css('display', 'none');
            $('#report-desc').css('display', 'block');
        }
    </script>
    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #thermal-print, #thermal-print * {
                visibility: visible;
            }

            #thermal-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .invoice-center ul li {
                width: 48%;
            }

            .inv-header {
                border-bottom: none;
            }

        }
    </style>

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#sort-sales').on('change', function () {
                var sort_type = $(this).val();

                if (sort_type == 1) {
                    $('#category-list').css('display', 'inline-block');
                    $('#item-list').css('display', 'none');

                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'none');

                }
                else if (sort_type == 2) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'inline-block');

                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'none');
                }
                else if (sort_type == 3) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');

                    $('#order-type-list').css('display', 'inline-block');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'none');
                }
                else if (sort_type == 4) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');

                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'inline-block');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'none');
                }
                else if (sort_type == 5) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');

                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'inline-block');
                    $('#user-list').css('display', 'none');
                }
                else if (sort_type == 6) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');

                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'inline-block');
                }
                else {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');
                    $('#order-type-list').css('display', 'none');
                    $('#table-list').css('display', 'none');
                    $('#method-list').css('display', 'none');
                    $('#user-list').css('display', 'none');
                }

            });
        });
    </script>

@endsection