@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="/res/create-order-type" method="post">
                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Order Type</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h2 class="user-list-title"><strong>Tax</strong></h2>
                                    <div class="form-group">
                                        @foreach($taxs as $tax)
                                            <input type="checkbox" name="tax_type[]"
                                                   value="{{$tax->id}}"> {{$tax->name}}:  <span
                                                    class="order-span-price"> {{$tax->percent}} </span><br>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <h2 class="user-list-title"><strong>Discount</strong></h2>
                                    <div class="form-group">
                                        @foreach($discounts as $discount)
                                            <input type="checkbox" name="discount_type[]"
                                                   value="{{$discount->id}}"> {{$discount->name}}: <span
                                                    class="order-span-price"> {{$discount->percent}} </span><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Order Types</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Note</th>
                                    <th>Discounts</th>
                                    <th>Tax's</th>
                                    <th>Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order_types  as $key => $order_type)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$order_type->name}}</td>
                                        <td>{{$order_type->title}}</td>
                                        <td>
                                            @if($order_type->dis_type != NULL)
                                                @php  $discounts = explode(',', $order_type->dis_type);
                                               foreach ($discounts as $discount)
                                               {
                                                   echo '<button class="btn btn-primary pos-small-btn bg-color ">'
                                                   .DB::table('discounts')->where('id',$discount)->value('name').
                                                   '</button>&nbsp;';
                                               }
                                                @endphp
                                            @endif
                                        </td>
                                        <td>
                                            @if($order_type->tax_type != NULL)
                                                @php $taxs = explode(',', $order_type->tax_type);
                                               foreach ($taxs as $tax)
                                               {
                                                   echo '<button class="btn btn-primary pos-small-btn bg-red ">'
                                                   .DB::table('taxs')->where('id',$tax)->value('name').
                                                   '</button>&nbsp;';
                                               }
                                                @endphp
                                            @endif
                                        </td>
                                        <td>
                                            @if($order_type->is_active === "active")
                                                <a data-id="{{ $order_type->id }}"
                                                   data-status="{{ $order_type->is_active }}"
                                                   data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $order_type->id }}"
                                                   data-status="{{ $order_type->is_active }}"
                                                   data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="user-action">
                                                <a href="{{ route('edit.order-type',['id'=>$order_type->id] ) }}"
                                                   class="user-edits" data-toggle="tooltip" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/res/delete-order_type/{{$order_type->id}}"
                                                   class="user-removed" data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Order Type Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are You Sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this order type information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-order-type-status')}}/' + id);


        });
    </script>
@endsection