@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

@endsection

@section('content')
    <div class="container">
        <div class="report-page2">
            <div class="container">

                <div class="report-table2">
                    <!--Table-->

                    <table class="table">

                        <!--Table head-->
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th colspan="8" class="text-center" style="padding: 20px; font-size: 16px; color: white; f
                            ont-weight: bold; background: #ff403c">Open Orders</th>
                        </tr>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Order Type</th>
                            <th class="text-center">Table Name</th>
                            <th class="text-center">Persons</th>
                            <th class="text-center">Order Time</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Discount</th>
                            <th class="text-center">Total</th>
                        </tr>
                        </thead>
                        <!--Table head-->

                        <!--Table body-->
                        <tbody id="open-orders">

                        <tr class="text-center">
                            <td colspan="8">Currently there is no open orders available!</td>
                        </tr>


                        </tbody>
                        <!--Table body-->
                    </table>

                    <!--Table-->
                </div>


                <div class="report-table2" id="table2">
                    <!--Table-->

                    <table class="table">

                        <!--Table head-->
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th colspan="9" class="text-center" style="padding: 20px; font-size: 16px; color: white; font-weight: bold; background: #1ab394">Closed Orders</th>
                        </tr>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">CHK ID</th>
                            <th class="text-center">Order Type</th>
                            <th class="text-center">Table Name</th>
                            <th class="text-center">Order Time</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">Discount</th>
                            <th class="text-center">Paid</th>
                        </tr>
                        </thead>
                        <!--Table head-->

                        <!--Table body-->
                        <tbody id="closed-orders">

                        <tr class="text-center">
                            <td colspan="8">Currently there is no open orders available!</td>
                        </tr>


                        </tbody>
                        <!--Table body-->
                    </table>

                    <!--Table-->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
        });
    </script>

    <style>
        .prport-tatepiker .table>thead>tr>th input {
            width: 60%;
        }

        select.re-monthly, select.re-yearly {
            width: 65%;
        }
    </style>

    <script>
        $(document).ready(function () {
            var holdOrders = JSON.parse(localStorage.getItem('all_hold_orders'));
            var rows = '';

            for(i=0; i<holdOrders.length; i++){
                var discount = holdOrders[i].alldiscount;
                var date = holdOrders[i].currentDate;
                var time = holdOrders[i].currentTime;
                var total = holdOrders[i].grandTotal;
                var cart = holdOrders[i].holdCartItem;
                var holdName = holdOrders[i].holdName;
                var person = holdOrders[i].person;
                var subTotal = holdOrders[i].subTotal;
                var table_name = holdOrders[i].table_name;


                CardDetails = [];
                $.each(cart, function (index, value) {
                   var Details = 'Item: '+ value.name + ' - Quantity: '+ value.count + '<br>';
                    CardDetails.push(Details);
                });

                cart = CardDetails.toString();

                cart = cart.replace(/,/g, "");


                rows += '<tr class="text-center">';
                rows += '<td>'+ (i+1) +'</td>';
                rows += '<td>'+ holdName +'</td>';
                rows += '<td>'+ table_name +'</td>';
                rows += '<td>'+ person +'</td>';
                rows += '<td>'+ date + ' ' + time + '</td>';
                rows += '<td class="alert alert-danger"> <a href="#"' +
                    ' data-table="'+ table_name +'"  data-person="'+ person +'"' +
                    ' data-cart = "'+ cart +'" data-time="'+ date +' '+ time +'"' +
                    ' data-total="'+ subTotal +'" data-order-type = "'+ holdName +'" '  +
                    ' data-toggle="modal" data-target="#orderDetails" ' +
                    '> Open  <a/> </td>';
                rows += '<td>'+ discount +' {{ config('app.currency')}}</td>';
                rows += '<td>'+ subTotal +' {{ config('app.currency')}}</td>';
                rows += '</td>';
                rows += '</tr>';

            }
            //for loop end

            if(holdOrders.length === 0)
            {
                rows += '<tr class="text-center">';
                rows += '<td colspan="8">Currently there is no open orders available!</td>';
                rows += '</tr>';
                $("#open-orders").html(rows);
            }
            else
            {
                $("#open-orders").html(rows);
            }


            var rows1 = '';

            $.ajax({
               type: 'get',
                url: '{{ route('get.closed.report') }}',
                dataType: "json",
                async: false,
                success: function(data){

                    for(i=0; i<data.length; i++){
                        var csk_id = data[i].csk_id;
                        var order_type = data[i].order_type;
                        var table_name = data[i].table_name;
                        var date = data[i].time;
                        var discount = data[i].discount;
                        var sub_total = data[i].sub_total;
                        var total = data[i].paid;




                        rows1 += '<tr class="text-center">';
                        rows1 += '<td>'+ (i+1) +'</td>';
                        rows1 += '<td>'+ csk_id +'</td>';
                        rows1 += '<td>'+ order_type +'</td>';
                        rows1 += '<td>'+ table_name +'</td>';
                        rows1 += '<td>'+ date + '</td>';
                        rows1 += '<td class="alert alert-success"> <a href="#"' +
                            ' ' +
                            '> Closed  <a/> </td>';
                        rows1 += '<td>'+ sub_total +' {{ config('app.currency')}}</td>';
                        rows1 += '<td>'+ discount +' {{ config('app.currency')}}</td>';
                        rows1 += '<td>'+ total +' {{ config('app.currency')}}</td>';
                        rows1 += '</td>';
                        rows1 += '</tr>';

                    }

                    if(data.length === 0)
                    {
                        rows1 += '<tr class="text-center">';
                        rows1 += '<td colspan="8">Currently there is no closed orders available!</td>';
                        rows1 += '</tr>';
                        $("#closed-orders").html(rows1);
                    }
                    else
                    {
                        $("#closed-orders").html(rows1);
                    }

                },
                error: function (data) {
                    console.log(data);
                }

            });


            // ORDER DETAILS ON MODAL

            $('#orderDetails').on('show.bs.modal', function (e) {
                var table = $(e.relatedTarget).data('table');
                var person = $(e.relatedTarget).data('person');
                var cart = $(e.relatedTarget).data('cart');
                var time = $(e.relatedTarget).data('time');
                var total = $(e.relatedTarget).data('total');
                var order_type = $(e.relatedTarget).data('order-type');

                $('#table').html(table);
                $('#person').html(person);
                $('#cart').html(cart);
                $('#total').html(total+' {{ config('app.currency')}}');
                $('#time').html(time);
                $('#order_type').html(order_type);


            });


        });


    </script>


    <!-- Modal -->
    <div id="orderDetails" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Order Details</h4>
                </div>
                <div class="modal-body">
                    <hr>
                    <p>Table: <b id="table"></b></p>
                    <p>Order Type: <b id="order_type"></b></p>
                    <p>Person: <b id="person"></b></p>

                    <hr>
                    <p style="font-weight: bolder">Order Details</p>
                    <b id="cart"></b>
                    <hr>

                    <p>Due Amount: <b id="total"></b></p>
                    <p>Order Time: <b id="time"></b></p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection