@extends('admin_layouts.default')

@section('content')
    <div class="container page-padding-top">
        <div class="report-header">
            <div class="row">
                <div class="kcol-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="prport-tatepiker">
                        <form action="" method="get">
                            <table class="table">
                                <thead>
                                <tr>

                                    <th width="90%" class="pull-right">Search By Item Name/Category Name: <input type="text" class="span2" value="@if(isset($string)) {{ $string }} @endif" name="item_name" style="width: 50%;">
                                    </th>
                                    <th>
                                        <div class="report-search pull-left">
                                            <button class="btn btn-button">Search</button>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="{{ action('CondimentController@store') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Condiments</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
                        @endif

                        <!-- Success and error Message End -->


                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Item Name:</label>--}}
                                {{--<select class="form-control" name="cat_id">--}}
                                    {{--<option disabled="" selected="">Select Item</option>--}}
                                    {{--@foreach($categories as $category)--}}
                                        {{--<option value="{{$category->id}}">{{$category->name}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}



                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="usr">Local Name:</label>
                                <input type="text" name="local_name" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="note" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="1" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>

                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Condiment List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($condiments  as $key => $condiment)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$condiment->name}}</td>
                                        <td>
                                            @if($condiment->status == "1")
                                                <a data-id="{{ $condiment->id }}"
                                                   data-status="{{ $condiment->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $condiment->id }}"
                                                   data-status="{{ $condiment->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $condiment->id }}"
                                                   data-name="{{ $condiment->name }}"
                                                   {{--data-category="{{ $condiment->cat_id }}"--}}
                                                   {{--data-title="{{ $condiment->p_title }}"--}}
                                                   data-note="{{ $condiment->note }}"
                                                   {{--data-image="{{ $condiment->p_image }}"--}}
                                                   data-status="{{ $condiment->status }}"
                                                   data-local_name="{{ $condiment->local_name }}"
                                                   data-target="#edit-item"
                                                   class="user-edits" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="{{ action('CondimentController@destroy',$condiment->id) }}" class="user-removed" data-toggle="tooltip" title="Delete" onclick="return confirm('Are You Sure Delete This Item?');"><span class="fa fa-trash"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            @if(count($condiments) > 0)

                                <div class="alert alert-info">
                                    Showing {{ $condiments->firstItem() }} to {{ $condiments->lastItem() }} Product of
                                    Total {{ $condiments->count() }} Products.
                                </div>

                            {{ $condiments->links() }}

                                {{--<nav aria-label="Page navigation">--}}
                                    {{--@if ($items->lastPage() > 1)--}}
                                        {{--<ul class="pagination pagination-sm">--}}
                                            {{--<li class="page-item {{ ($items->currentPage() == 1) ? ' disabled' : '' }}">--}}
                                                {{--<a class="page-link" href="{{ $items->url(1) }}">Previous</a>--}}
                                            {{--</li>--}}
                                            {{--@for ($i = 1; $i <= $items->lastPage(); $i++)--}}
                                                {{--<li class="page-item {{ ($items->currentPage() == $i) ? 'page active' : '' }}">--}}
                                                    {{--<a class="page-link" href="{{ $items->url($i) }}">{{ $i }}</a>--}}
                                                {{--</li>--}}
                                            {{--@endfor--}}
                                            {{--<li class="page-item {{ ($items->currentPage() == $items->lastPage()) ? ' disabled' : '' }}">--}}
                                                {{--<a class="page-link"--}}
                                                   {{--href="{{ $items->url($items->currentPage()+1) }}">Next</a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--@endif--}}
                                {{--</nav>--}}
                            @else
                                <div class="alert alert-danger">
                                    No Result Found!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Customer Edit Pop Up -->

    <div id="edit-item" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Condiment Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ action('CondimentController@update') }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Edit Condiment</h2>

                            <input type="hidden" name="id" id="id">

                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Item Category:</label>--}}
                                {{--<select class="form-control" name="cat_id" id="item-cat-id">--}}
                                    {{--<option disabled="" selected="">Select Item</option>--}}
                                    {{--@foreach($categories as $category)--}}
                                        {{--<option value="{{$category->id}}">{{$category->name}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="usr">Local Name:</label>
                                <input type="text" name="local_name" id="local_name" class="form-control">
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="pwd">price:</label>--}}
                                {{--<input type="text" name="p_price" id="item-price" class="form-control">--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="note" id="note" class="form-control">
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="pwd">Image:</label>--}}
                                {{--<input type="file" name="image" class="form-control" onchange="readURL(this);">--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<img id="blah" src="http://placehold.it/300x200" height="50"--}}
                                     {{--class="img-responsive img-thumbnail item-image" alt="your image"/>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" id="status" value="1">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>

                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this product information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $('#edit-item').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var local_name = $(e.relatedTarget).data('local_name');
            var note = $(e.relatedTarget).data('note');
            //var title = $(e.relatedTarget).data('title');
            //var cat_id = $(e.relatedTarget).data('category');
            //var image = $(e.relatedTarget).data('image');
            var status = $(e.relatedTarget).data('status');

            $('#id').val(id);
            $('#name').val(name);
            $('#local_name').val(local_name);
            $('#note').val(note);
            //$('#item-title').val(title);
            //$('select#item-cat-id').val(cat_id);

            //$('img.item-image').attr('src', '{{ url('/') }}/' + image);

            if (status === 1) {
                $('#status').prop('checked', true);
            }else{
                $('#status').prop('checked',false);
            }
        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-condiment-status')}}/' + id);


        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection