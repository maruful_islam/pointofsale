@extends('admin_layouts.default')

@section('content')

{{--@php--}}
    {{----}}
    {{--config(['app.name'=>'HOT BURGER','app.currency'=>'KD']);--}}
        {{----}}
            {{--@endphp--}}


    <div class="earnings mar-top-50">
        <div class="container">
            <div class="row">
                <div class="erniing-items">
                    <div class="erniing-item">
                        <ul>
                            <li class="days">today</li>
                            <li class="count">@php echo $todays_count ? $todays_count : 0 @endphp</li>
                            <li class="amount">@php echo $todays_sales ? number_format($todays_sales, 3) : 0 @endphp
                                {{ config('app.currency')}}
                            </li>
                        </ul>
                    </div>
                    <div class="erniing-item">
                        <ul>
                            <li class="days">Yesterday</li>
                            <li class="count">@php echo $yesterday_count ? $yesterday_count : 0 @endphp</li>
                            <li class="amount">@php echo $yesterday_sales ? number_format($yesterday_sales, 3) : 0 @endphp
                                {{ config('app.currency')}}
                            </li>
                        </ul>
                    </div>
                    <div class="erniing-item">
                        <ul>
                            <li class="days">Last 7 Days</li>
                            <li class="count">@php echo $last_week_count? $last_week_count : 0 @endphp</li>
                            <li class="amount">@php echo $last_week_sales ? number_format($last_week_sales, 3) : 0 @endphp
                                {{ config('app.currency')}}
                            </li>
                        </ul>
                    </div>
                    <div class="erniing-item">
                        <ul>
                            <li class="days">Last 30 Days</li>
                            <li class="count">@php echo $last_month_count ? $last_month_count : 0 @endphp</li>
                            <li class="amount">@php echo $last_month_sales ? number_format($last_month_sales, 3) : 0 @endphp
                                {{ config('app.currency')}}
                            </li>
                        </ul>
                    </div>
                    <div class="erniing-item">
                        <ul>
                            <li class="days">Total Sales</li>
                            <li class="count">@php echo $all_time_count ? $all_time_count : 0 @endphp</li>
                            <li class="amount">@php echo $all_time_sales ? number_format($all_time_sales, 3) : 0 @endphp
                                {{ config('app.currency')}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="monthly-saleing-area  mar-top-50">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="monthly-sale">
                        <h3> Last 7 Days Sale</h3>
                        <p>Online and pos order</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mo-day-list">
                        <ul>
                            <li id="weekly_active" class="active"><span><a href="javascript:void(0)" id="week-graph">Last 7 days</a></span></li>
                            <li id="monthly_active"><span><a href="javascript:void(0)" id="month-graph">Last 30 days</a></span></li>
                            <li id="yearly_active"><span><a href="javascript:void(0)" id="year-graph">Last 12 month</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="saling-graps" style="max-height: 420px;">
                    <canvas id="weekChart" height="100"></canvas>
                    <canvas id="monthChart" height="100"></canvas>
                    <canvas id="yearChart" height="100"></canvas>
                </div>
                <div class="alert alert-info">
                    In X axis = Day/Date/Month Name <br> In Y axis = Total Income in {{ config('app.currency')}}
                </div>
            </div>
        </div>
    </div>
    <!-- Pre Order List -->
<div class="pos-product-list-sale mar-top-50">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="pos-list-table">
                    <div class="pos-list-title">
                        <h5> Latest Pre Order List </h5>
                        <div class="pos-list-tools">
                            <a class="collapse-link" id="pos-list-table-click">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="pos-list-content" id="pos-list-table-show">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Order Date</th>
                                    <th class="text-center">Delivery Date</th>
                                    <th class="text-center">Discount</th>
                                    <th class="text-center">Total Amount</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($orders as $key=>$order)
                                    <tr class="text-center">

                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $order->created_at->format('dS F Y') }}</td>
                                        <td>{{ $order->pre_order->format('dS F Y') }}</td>
                                        <td>{{ number_format($order->discount, 3) }} {{ config('app.currency')}}</td>
                                        <td>{{ number_format($order->paid, 3) }} {{ config('app.currency')}}</td>

                                        <td>
                                            <a href="" class="btn btn-success pos-small-btn bg-color">Completed</a>
                                        </td>


                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- list pos sale -->
    <div class="pos-product-list-sale mar-top-50">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="pos-list-table">
                        <div class="pos-list-title">
                            <h5> Last 10 POS Sales </h5>
                            <div class="pos-list-tools">
                                <a class="collapse-link" id="pos-list-table-click">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="pos-list-content" id="pos-list-table-show">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Sales Date</th>
                                        <th class="text-center">Discount</th>
                                        <th class="text-center">Total Amount</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($latest_sales as $key=>$latest_sale)
                                        <tr class="text-center">

                                            <td>{{ $key+1 }}</td>
                                            <td> {{ $latest_sale->created_at->format('dS F Y h:i A') }}</td>
                                            <td> {{ number_format($latest_sale->discount, 3) }} {{ config('app.currency')}}</td>
                                            <td> {{ number_format($latest_sale->paid, 3) }} {{ config('app.currency')}}</td>

                                            <td>
                                                <a href="" class="btn btn-success pos-small-btn bg-color">Completed</a>
                                            </td>


                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="top-sale-items pos-list-table">
                        <div class="pos-list-title">
                            <h5> Top 10 Sale Items</h5>
                            <div class="pos-list-tools">
                                <a class="collapse-link" id="top-sale-items-click">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" id="top-sale-items-show">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Sales</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($popular_items as $key=>$item)
                                        <tr>
                                            <td style="padding: 13px">{{ $key+1 }}</td>
                                            <td style="padding: 13px">{{ $item->getItemName($item->item_id) }}</td>
                                            <td style="padding: 13px">{{ $item->item_count }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End list pos sale -->
    

@endsection

@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

    <script>
        var ctx = document.getElementById("weekChart").getContext('2d');
        var weekChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [@foreach($week_sales_dates as $date) "{{$date}}", @endforeach],
                datasets: [{
                    label: '# of Sales',
                    data: [@foreach($week_sales_total as $total) {{$total}}, @endforeach],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255,99,132,1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });


        $(document).ready(function () {

            $('#week-graph').on('click', function () {
                $('#weekly_active').addClass('active');
                $('#monthly_active').removeClass('active');
                $('#yearly_active').removeClass('active');

                $('canvas#monthChart').fadeOut().remove();
                $('canvas#yearChartChart').fadeOut().remove();

                $('.saling-graps').html('<canvas id="weekChart" height="100"></canvas>');

                $.ajax({
                    type: "get",
                    url: '{{ route('get.weekly.graph') }}',
                    dataType: "json",
                    success: function (data) {


                        var ctx = document.getElementById("weekChart").getContext('2d');
                        var weekChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: data.week_sales_dates,
                                datasets: [{
                                    label: '# of Sales',
                                    data: data.week_sales_total,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                        });



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });

            $('#month-graph').on('click', function () {
                $('#weekly_active').removeClass('active');
                $('#monthly_active').addClass('active');
                $('#yearly_active').removeClass('active');

                $('canvas#weekChart').fadeOut().remove();
                $('canvas#monthChart').fadeOut().remove();

                $('.saling-graps').html('<canvas id="monthChart" height="100"></canvas>');

                $.ajax({
                    type: "get",
                    url: '{{ route('get.monthly.graph') }}',
                    dataType: "json",
                    success: function (data) {


                        var ctx = document.getElementById("monthChart").getContext('2d');
                        var monthChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: data.month_sales_dates,
                                datasets: [{
                                    label: '# of Sales',
                                    data: data.month_sales_total,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                        });



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });


            $('#year-graph').on('click', function () {
                $('#weekly_active').removeClass('active');
                $('#monthly_active').removeClass('active');
                $('#yearly_active').addClass('active');

                $('canvas#monthChart').fadeOut().remove();
                $('canvas#weekChart').fadeOut().remove();

                $('.saling-graps').html('<canvas id="yearChart" height="100"></canvas>');

                $.ajax({
                    type: "get",
                    url: '{{ route('get.yearly.graph') }}',
                    dataType: "json",
                    success: function (data) {


                        var ctx = document.getElementById("yearChart").getContext('2d');
                        var yearChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: data.year_sales_dates,
                                datasets: [{
                                    label: '# of Sales',
                                    data: data.year_sales_total,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                        });



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            });
        })
    </script>

@endsection