    
    @extends('admin_layouts.default')
    @section('content')
       <div class="container page-padding-top">
            <div class="user-list-boxarea">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <form action="/res/create-taxt" method="post">
                        {{csrf_field()}}
                            <div class="user-inputbox">
                                 <h2 class="user-list-title">Add Charges</h2>

									 <!-- Success and error Message Start -->

                                  @if ($errors->any())
                                    <div class="alert alert-danger error-message-show">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                  @endif
                                    @if(Session::has('success'))
                                        <div class="alert alert-success success-message-show"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
                                    @endif

                                <!-- Success and error Message End -->


                                <div class="form-group">
                                  <label for="usr">Name:</label>
                                  <input type="text" name="name" class="form-control" >
                                </div>
                               

                                <div class="form-group">
                                  <label for="pwd">Discount:</label>
                                  <input type="taxt" name="percent" class="form-control" >
                                </div>

                                 <div class="form-group">
                                  <label for="pwd">Note:</label>
                                  <input type="text" name="title" class="form-control" >
                                </div>
                               
                                 <div class="form-group">
                                    <label class="container">
                                          <input type="checkbox" name="status" value="active" checked="checke">
                                          Status
                                    </label>
                                </div>
                                <div class="form-group">
                                  <button class="btn-submit btn-primary" type="submit">submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                     <div class="col-xs-12 col-sm-12 col-md-6">
                         <div class="user-list">
                             <div class="user-list-table table-responsive">
                                <h2 class="user-list-title">Charges List</h2>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Title</th>
                                            <th>Discount</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($discounts  as $key => $discount)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$discount->name}}</td>
                                            <td>{{$discount->percent}}</td>
                                             <td>{{$discount->title}}</td>
                                            <td>
                                                <div class="user-action">
                                                    <a href="/res/delete-discount/{{$discount->id}}" class="user-removed" data-toggle="tooltip" title="Delete" onclick="return confirm('Are You Sure Delete This Item?');"><span class="fa fa-times"></span></a>
                                                    <a href="" class="user-edits" data-toggle="tooltip" title="Edit"><span class="fa fa-edit"></span></a>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                         </div>
                     </div>
                </div>
            </div>
        </div> 

    @endsection

    @section('js')

    @endsection