@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

    <style>
        .btn-group-sm > .btn, .btn-sm {
            font-size: 9px;
            border-radius: 100px;
        }

        .btn-primary {
            background-color: #fb787e;
            border-color: #fb787e;
            margin: 3px;
        }

        .invoice-page .invo-header-info {
            width: 100%;
        }

        .custom > thead:first-child > tr:first-child > th {
            border-top: 1px solid #b6bdc1 !important;
        }
    </style>

@endsection

@section('content')
    <div id="report-desc" class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">


                            <div class="kcol-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="prport-tatepiker">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th>Form Date: <input type="text" class="span2 date_input" name="start_date"
                                                                  @if(isset($_GET['start_date'])) value="{{ $_GET['start_date'] }}"
                                                                  @endif required></th>

                                            <th>To Date: <input type="text" class="span2 date_input" name="end_date"
                                                                @if(isset($_GET['end_date'])) value="{{ $_GET['end_date'] }}"
                                                                @endif required></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="report-search text-right">
                                    <br>
                                    <button class="btn btn-button">Search</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                @if(isset($_GET['start_date']))
                    <div id="print">
                        <div class="text-center" style="padding-top: 20px">
                            <h2>Profit/Loss Statement</h2><br>
                            <img src="{{ asset('pos/images/ddddd.png') }}" width="150"/>
                        <h4>{{ strtoupper(systemInfo('system_name'))}}</h4>
                        <h4>{{ systemInfo('local_name') }}</h4><br>
                            @if(isset($_GET['start_date']))
                                <h5>Report From: <b>{{ date('l, dS F Y', strtotime($_GET['start_date'])) }} </b>
                                    to <b>{{ date('l, dS F Y', strtotime($_GET['end_date'])) }}</b></h5>
                            @else
                                <h5>Date: {{ date('l, dS F Y') }} </h5>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="col-md-6 col-xs-12" id="income">
                                    <div class="report-table2">

                                        <h3 class="text-center" style="padding-top: 15px">Income</h3>
                                        <hr>

                                        <table class="table">

                                            <!--Table head-->
                                            <thead class="blue-grey lighten-4">
                                            <tr>
                                                <th class="text-center">Total Order</th>
                                                <th class="text-center">Total Payment Received</th>
                                                <th class="text-center">Date Range</th>

                                            </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>

                                            <tr>
                                                <th scope="row" class="text-center">{{ $total_order }}</th>

                                                <td class="text-center">
                                                    {{ $total_income }}  {{ config('app.currency')}}
                                                </td>

                                                <td class="text-center">
                                                    {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                                                    to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                                                </td>

                                            </tr>

                                            </tbody>
                                            <!--Table body-->
                                        </table>

                                        <!--Table-->
                                    </div>
                                </div>


                                <div class="col-md-6 col-xs-12" id="expense">
                                    <div class="report-table2">

                                        <h3 class="text-center" style="padding-top: 15px">Expense</h3>
                                        <hr>

                                        <table class="table">

                                            <!--Table head-->
                                            <thead class="blue-grey lighten-4">
                                            <tr>
                                                <th class="text-center">Expense Type</th>
                                                <th class="text-center">Total Payment</th>
                                                <th class="text-center">Date Range</th>

                                            </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>

                                            <tr>
                                                <th scope="row" class="text-center">Purchase Expense</th>

                                                <td class="text-center">
                                                    {{ $purchase_expenses }}  {{ config('app.currency')}}
                                                </td>

                                                <td class="text-center">
                                                    {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                                                    to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row" class="text-center">Other Expense</th>

                                                <td class="text-center">
                                                    {{ $other_expenses }}  {{ config('app.currency')}}
                                                </td>

                                                <td class="text-center">
                                                    {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                                                    to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                                                </td>

                                            </tr>

                                            </tbody>
                                            <!--Table body-->
                                        </table>

                                        <!--Table-->
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12 col-xs-12">

                                <div class="report-table2" id="final">

                                    <h3 class="text-center" style="padding-top: 15px">Final Statement</h3>
                                    <hr>

                                    <table class="table custom">

                                        <!--Table head-->
                                        <thead class="blue-grey lighten-4">
                                        <tr>
                                            <th class="text-center">Total Income</th>
                                            <th class="text-center"
                                                style="background: #fff">{{ $total_income }}  {{ config('app.currency')}}</th>

                                        </tr>

                                        <tr>
                                            <th class="text-center">Total Expense</th>
                                            <th class="text-center"
                                                style="background: #fff">{{ $total_expense }}  {{ config('app.currency')}}</th>

                                        </tr>

                                        @php $calculation = $total_income - $total_expense; @endphp
                                        <tr>
                                            @if($calculation > 0)
                                                <th class="text-center">Total Profit</th>
                                            @else
                                                <th class="text-center">Total Loss</th>
                                            @endif
                                            <th class="text-center"
                                                style="background: #fff">{{ $calculation }}  {{ config('app.currency')}}</th>

                                        </tr>

                                        </thead>
                                        <!--Table head-->

                                    </table>

                                    <!--Table-->
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="text-center">
                        <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                           onclick="window.print()">Print This Report</a>
                    </div>

                    @php $current_url = Request::getPathInfo() . (Request::getQueryString() ? ('?' . Request::getQueryString()) : '');
                    $query_string = substr($current_url,19);
                    @endphp
                    <div class="text-center">
                        <a class="btn btn-primary pos-small-btn bg-color"
                           href="{{ url('/pdf-profit-report'.$query_string) }}">Save
                            As PDF</a>
                    </div>

                @endif
            </div>
        </div>
    </div>


@endsection

@section('js')

    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #print, #print * {
                visibility: visible;
            }

            #print {
                position: absolute;
                left: 0;
                top: 0;
            }


        }
    </style>

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true
        });
    </script>

@endsection