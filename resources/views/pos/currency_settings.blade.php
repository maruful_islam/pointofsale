@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="{{ route('save.note') }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Coin/Notes</h2>


                            <div class="form-group">
                                <label for="usr">Coin/Note Name:</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>


                            <div class="form-group">
                                <label for="pwd">Coin/Note Value:</label>
                                <input type="text" name="value" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Coin/Note List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Coin/Note Name</th>
                                    <th>Value</th>
                                    <th>Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notes  as $key => $note)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$note->name}}</td>
                                        <td>{{$note->value}}</td>
                                        <td>
                                            @if($note->status === "active")
                                                <a data-id="{{ $note->id }}"
                                                   data-status="{{ $note->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $note->id }}"
                                                   data-status="{{ $note->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="user-action">
                                                <a href="javascript:void(0)" class="user-edits"
                                                   data-id = "{{ $note->id }}"
                                                   data-name = "{{ $note->name }}"
                                                   data-value = "{{ $note->value }}"
                                                   data-status = "{{ $note->status }}"
                                                   data-toggle="modal" data-target="#edit-note" title="Edit"><span
                                                            class="fa fa-edit"></span></a>

                                                <a href="{{ route('delete.note', ['id' => $note->id]) }}" class="user-removed" data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-times"></span></a>

                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <div id="edit-note" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Coin/Note Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('note.update') }}" method="post">
                        {{csrf_field()}}

                        <input type="hidden" name="id" id="id">

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Update Information</h2>


                            <div class="form-group">
                                <label for="usr">Coin/Note Name:</label>
                                <input type="text" name="name" class="form-control" id="name" required>
                            </div>


                            <div class="form-group">
                                <label for="pwd">Coin/Note Value:</label>
                                <input type="text" name="value" class="form-control" id="value" required>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Tax Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this currency note information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#edit-note').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var value = $(e.relatedTarget).data('value');
            var status = $(e.relatedTarget).data('status');

            $('#id').val(id);
            $('#name').val(name);
            $('#value').val(value);

            if (status === 'active') {
                $('#d-status').prop('checked', true);
            } else {
                $('#d-status').prop('checked', false);
            }


        });


        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');


            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/update-note-status')}}/' + id);


        });
    </script>

@endsection