@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="/res/create-product-category" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Category</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Icon:</label>
                                <input type="file" name="image" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Category List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Icon</th>
                                    <th>Name</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categorys  as $key => $category)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            <img src='{{asset($category->icon)}}' width="80px">
                                        </td>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->title}}</td>
                                        <td>
                                            @if($category->is_active === "active")
                                                <a data-id="{{ $category->id }}"
                                                   data-status="{{ $category->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $category->id }}"
                                                   data-status="{{ $category->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>

                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $category->id }}" data-name="{{ $category->name }}"
                                                   data-title="{{ $category->title }}"
                                                   data-icon="{{ $category->icon }}"
                                                   data-status="{{ $category->is_active }}"
                                                   data-target="#edit-category"
                                                   class="user-edits"
                                                   data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/res/delete-product-category/{{$category->id}}"
                                                   class="user-removed" data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Category Edit Pop Up -->

    <div id="edit-category" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Category Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('category.update') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">

                            <input type="hidden" name="id" id="cat-id">

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" id="cat-name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="title" id="cat-title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Icon:</label>
                                <input type="file" name="image" class="form-control" onchange="readURL(this);">
                            </div>

                            <div class="form-group">
                                <img id="blah" src="http://placehold.it/300x200" height="50"
                                     class="img-responsive img-thumbnail cat-icon" alt="your image"/>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" id="cat-status" value="active"
                                           checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are You Sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this category information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $('#edit-category').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var title = $(e.relatedTarget).data('title');
            var icon = $(e.relatedTarget).data('icon');
            var status = $(e.relatedTarget).data('status');


            $('#cat-id').val(id);
            $('#cat-name').val(name);
            $('#cat-title').val(title);

            $('img.cat-icon').attr('src', '{{ url('/') }}/' + icon);

            if (status === 'active') {
                $('#cat-status').prop('checked', true);
            }
            else {
                $('#cat-status').prop('checked', false);
            }
        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-category-status')}}/' + id);


        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



    <script>
        $('.delete_form').submit(function (e) {

            e.preventDefault();
            var formData = $(this).serialize();

            console.log(formData);
            var elm = $(this);

            if (confirm('Confirm Delete.')) {
                $.ajax({
                    url: '/delete_news_events',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    error: function (data) {
                        console.log(data.error);
                    },
                    success: function (data) {
                        elm.closest("tr").remove();
                    }
                });
            }
        });
    </script>

@endsection
