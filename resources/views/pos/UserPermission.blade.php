@extends('admin_layouts.default')
@section('content')


    <div class="allforms mar-top-50 client-checkbox">
        <div class="sparkline12-graph">
            <div class="basic-login-form-ad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        @foreach($modules as $module)
                                            <th>{{ $module->name }}</th>
                                        @endforeach
                                        <th>Confirm</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($users as $key => $user)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->getDesignationTitle($user->id) }}</td>

                                            <form action="{{ route('user.permission', ['id' => $user->id]) }}"
                                                  method="post">

                                                {{ csrf_field() }}

                                                <input type="hidden" name="user_id" value="{{$user->id}}">

                                                @foreach($modules as $module)

                                                    <td>
                                                        <div class="form-check">
                                                            <input type="hidden" name="module_id[]"
                                                                   value="{{$module->id}}">

                                                            @php
                                                                $permission = $module->getPermissionInfo($user->id, $module->id);
                                                            @endphp
                                                            <div class="bt-df-checkbox">
                                                                <label>
                                                                    <input type="radio" name="status_{{$module->id}}"
                                                                           class="form-check-input"
                                                                           value="1"
                                                                           @if($permission == 1) checked="checked" @endif
                                                                    > Yes
                                                                </label>

                                                                &nbsp;&nbsp;

                                                                <label>
                                                                    <input type="radio" name="status_{{$module->id}}"
                                                                           class="form-check-input"
                                                                           value="0"
                                                                           @if($permission == 0) checked="checked" @endif>
                                                                    No
                                                                </label>
                                                            </div>


                                                        </div>
                                                    </td>

                                                @endforeach
                                                <td>
                                                    <button class="btn btn-button">Save</button>
                                                </td>
                                            </form>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div><!--end of .table-responsive-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')
    <style>

        .client-checkbox .table > tbody > tr > td, .table-bordered > thead > tr > th {
            vertical-align: middle;
            text-align: center;
        }

        input[type=checkbox], input[type=radio] {
            cursor: pointer;
        }

        .client-checkbox button.btn.btn-button {
            background: #333;
            color: #fff;
            border-radius: 0;
        }

        .client-checkbox button.btn.btn-button:hover, .client-checkbox button.btn.btn-button:focus {
            background: #ff403c;
            color: #fff;
            outline: none;
        }

        .table-bordered > thead > tr > th {
            text-transform: capitalize;
            color: #333;
            font-size: 16px;
            font-weight: bolder;
        }
    </style>

    <script>
        $(document).ready(function () {

            $('form').on('click', function (e) {
                e.preventDefault();

                id = $('input[name="user_id"]').val();

                console.log(id);
            });

        });
    </script>
@endsection