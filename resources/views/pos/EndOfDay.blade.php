@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

@endsection


@section('content')
    @php
        //$date = date('Y-m-d');
        $end_day_check = App\CashStatus::query()->whereDate('created_at', $date)->first();

    @endphp

    <div class="container page-padding-top">

        <div class="user-list-boxarea">
            <div class="row">
                <div class="report-header">
                    <div class="row" style="padding-top: 15px">
                        <form action="" method="get">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="order-table-top ">
                                    <ul class="nav2 text-center">
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#CashDrawer">
                                                <span class="fa fa-balance-scale"></span>Reconcile Cash Drawer
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#Withdraw">
                                                <span class="fa fa-dollar"></span>Withdraw
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#EndDay">
                                                <span class="fa fa-dollar"></span>End Day Cash
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <form action="" method="get">
                                    <div class="prport-tatepiker">
                                        <div class="col-md-8 form-group text-right">
                                            Search Previous Report: <input type="text" class="span2 date_input"
                                                                           name="date"
                                                                           value="@if(isset($d['searched_date'])) {{ $d['searched_date'] }} @endif"
                                                                           required>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="report-search">
                                                <button class="btn btn-button">Search</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>

                        </form>
                    </div>
                </div>

                <hr>

                <div class="col-xs-12 col-sm-12 col-md-12" id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h2>End of Day Report</h2><br>
                        <h4>{{ strtoupper(config('app.name'))}}</h4><br>
                        <h5>Date:
                            @if(isset($date1))
                                {{ $date1->format('Y-m-d') }}
                            @else
{{--                                {{ $date->format('Y-m-d') }}--}}
                                @if(Carbon\Carbon::now()->format('H') >= 6)
                                    <h5>Date: {{ date('Y-m-d') }} </h5>
                                @else
                                    <h5>Date: {{ Carbon\Carbon::yesterday()->format('Y-m-d') }}</h5>
                                @endif
                            @endif</h5>
                    </div>
                    <div class="report-table2 endofday">

                        <table class="table">

                            <thead class="blue-grey lighten-4">
                            <tr>
                                <th class="text-center">Item</th>
                                <th class="text-center">Item Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Total Sales</th>
                                <th class="text-center">Total Discount</th>
                                <th class="text-center">Net Sales</th>
                            </tr>
                            </thead>
                            <!--Table head-->

                            <!--Table body-->
                            <tbody>


                            @if( ( count($report_by_item) > 0 && !isset($end_day_check) ) || isset($show) )
                                @foreach($report_by_item as $report)
                                    <tr class="text-center">
                                        <td>{{ $report['item_name'] }}</td>
                                        <td>{{ $report['price'] }} {{ config('app.currency')}}</td>
                                        <td>{{ $report['sold'] }}</td>
                                        <td>{{ $report['earned'] }} {{ config('app.currency')}}</td>
                                        <td>{{ $report['discount'] }}</td>
                                        <td>{{ $report['net_profit_by_item'] }} {{ config('app.currency')}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <br>
                                        @if(!isset($end_day_check) || isset($show))
                                            <div class="alert alert-danger">No item has not been sold yet!</div>
                                        @else
                                            <div class="alert alert-success">You have ended your day's sell
                                            </div>
                                        @endif
                                    </td>
                                </tr>

                            @endif

                            </tbody>

                            <caption>Today's Sales Report by Item</caption>
                        </table>

                        <hr>

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table">

                                    <thead class="blue-grey lighten-4">
                                    <tr>
                                        <th class="text-center">Start Day Notes</th>
                                        <th class="text-center">End Day Notes</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td class="text-center" style="padding: 10px 0 10px 0">

                                            @if( ( $start_day_notes != NULL && !isset($end_day_check) ) || ( isset($show) && $start_day_notes != NULL ))
                                                @php
                                                    foreach ($start_day_notes as $key => $note)
                                                    {
                                                        echo  App\CurrencyNote::query()->where('value', $note->note_val)->value('name').': '.$note->qty.'<hr>';
                                                    }
                                                @endphp
                                            @else

                                                @if(!isset($end_day_check) || isset($show))
                                                    <div class="alert alert-danger">Start Day Cash Drawer Information
                                                        not found!
                                                    </div>
                                                @else
                                                    <div class="alert alert-success">
                                                        You have ended your days cash information
                                                    </div>
                                                @endif

                                            @endif
                                        </td>
                                        <td class="text-center" style="padding: 10px 0 10px 0">
                                            @if( ( $end_day_notes != NULL && !isset($end_day_check) ) || ( isset($show) && $end_day_notes != NULL ))
                                                @php
                                                    foreach ($end_day_notes as $key => $note)
                                                    {
                                                        echo  App\CurrencyNote::query()->where('value', $note->note_val)->value('name').': '.$note->qty.'<hr>';
                                                    }
                                                @endphp
                                            @else

                                                @if(!isset($end_day_check) || isset($show))
                                                    <div class="alert alert-danger">End Day Cash Drawer Information not
                                                        found!
                                                    </div>
                                                @else
                                                    <div class="alert alert-success">You have ended your days cash information
                                                    </div>
                                                @endif

                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>

                                    <caption>Note Status</caption>
                                </table>
                            </div>

                            <div class="col-md-6">
                                <table class="table">
                                    <thead class="blue-grey lighten-4">
                                    <tr>
                                        <th class="text-center">Issued By</th>
                                        <th class="text-center">Note</th>
                                        <th class="text-center">Amount</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if( ( $withdraws != NULL && !isset($end_day_check) ) || ( isset($show) && $withdraws != NULL ))

                                        @foreach($withdraws as $withdraw)
                                            <tr class="text-center">
                                                <td style="padding: 10px 0 10px 0">{{ $withdraw->getStaffName($withdraw->staff_id) }}</td>
                                                <td style="padding: 10px 0 10px 0">{{ $withdraw->note }}</td>
                                                <td style="padding: 10px 0 10px 0">{{ $withdraw->amount }}</td>
                                            </tr>
                                        @endforeach

                                    @else


                                        @if(!isset($end_day_check) || isset($show))
                                            <tr>
                                                <td colspan="3">No Withdraw data found!</td>
                                            </tr>
                                        @else

                                            <tr>
                                                <td colspan="3">
                                                    <div class="alert alert-success">You have ended your days cash information
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                    </tbody>
                                    <caption>Withdraws</caption>
                                </table>
                            </div>


                            <div class="pull-right"
                                 style="text-align: right; padding: 20px; font-weight: 600; font-size: 13px; margin: 0 auto">

                                <hr>
                                <p>Total Credit Card Sales
                                    Sales: @if( (isset($credit_card) && !isset($end_day_check) ) || isset($show)) {{ number_format($credit_card, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>

                                <p>Total K-Net Sales
                                    Sales: @if( (isset($knet) && !isset($end_day_check) ) || isset($show)) {{ number_format($knet, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>

                                <p>Total Product Gross
                                    Sales: @if( (isset($total_earned) && !isset($end_day_check) ) || isset($show)) {{ number_format($total_earned, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>
                                <p>Total
                                    Discounts: @if( (isset($total_discount) && !isset($end_day_check) ) || isset($show)) {{ number_format($total_discount, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>
                                <p>Total
                                    Quantity: @if( (isset($total_item_sold) && !isset($end_day_check) ) || isset($show) ) {{ $total_item_sold }} @else
                                        0 @endif
                                </p>
                                <hr>
                                <p>Total Net Product
                                    Sales: @if( (isset($net_profit) && !isset($end_day_check) ) || isset($show) )  {{ number_format($net_profit, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>
                                <p>Start Day
                                    Cash: @if( (isset($start_day_cash) && !isset($end_day_check) ) || isset($show) ){{ number_format($start_day_cash,3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>
                                <p>Total
                                    Withdrawn: @if( (isset($total_withdraw) && !isset($end_day_check) ) || isset($show))  {{ number_format($total_withdraw, 3) }} @else
                                        0 @endif
                                    {{ config('app.currency')}}</p>
                                <hr>
                                <p>Current
                                    Cash: @if( (isset($current_cash) && !isset($end_day_check) ) || isset($show)) {{ number_format($current_cash,3) }} @else
                                        0 @endif {{ config('app.currency')}}</p>
                                <p>End
                                    Cash: @if( (isset($end_day_cash) && !isset($end_day_check) ) || isset($show) ) {{ number_format($end_day_cash,3) }} @else
                                        0 @endif {{ config('app.currency')}}</p>

                                <hr>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                       onclick="window.print()">Print This Report</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
        });
    </script>


    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #print, #print * {
                visibility: visible;
            }

            #print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>


    <div id="CashDrawer" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Reconcile Cash Drawer</h2>
                </div>
                <div class="modal-body">

                    <div class="user-inputbox">


                        <form role="form" action="{{ route('calculate.cash') }}" method="post">

                            {{ csrf_field() }}

                            <input type="hidden" name="date" value="{{ date('Y-m-d') }}">

                            <div class="col-md-12" id="note_entry">

                                <div>
                                    <div class="col-md-5">
                                        <div class="form-select-list">
                                            <label for="name">Select Note</label>
                                            <select name="note[]" class="form-control custom-select-value"
                                                    title="">
                                                @foreach($notes as $note)
                                                    <option value="{{ $note->value }}">{{ $note->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="phone">Total Note in Drawer:</label>
                                            <input type="text" name="quantity[]" class="form-control" required title="">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                style="margin-top: 29px;"
                                                onClick="add_note_entry()">
                                            Add
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                style="margin-top: 29px;"
                                                onclick="deleteParentElement(this)">
                                            Remove
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>


                            {{--<div class="col-md-12">--}}
                            {{--<div class="text-center">--}}
                            {{--<label>Total Start Day Cash: <b id="total-start-day-cash">250 {{ config('app.currency')}}</b></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <button class="btn-submit btn-primary" id="store-customer" type="submit">submit</button>
                            </div>

                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



    <div id="Withdraw" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Withdraw</h2>
                </div>
                <div class="modal-body">

                    <div class="user-inputbox">


                        <form role="form" action="{{ route('store.withdraw') }}" method="post">

                            {{ csrf_field() }}

                            <input type="hidden" name="date" value="{{ date('Y-m-d') }}">
                            <input type="hidden" name="withdraw_issued_by" value="{{ auth()->user()->id }}">

                            <div class="form-group">
                                <label for="name">Withdraw Amount:</label>
                                <input type="text" name="withdraw_amount" class="form-control" id="withdraw_amount">
                            </div>

                            <div class="form-group">
                                <label for="name">Withdraw Note:</label>
                                <textarea name="withdraw_note" class="form-control" id="withdraw_note"></textarea>
                            </div>


                            <div class="form-group">
                                <button class="btn-submit btn-primary" id="store-customer" type="submit">submit</button>
                            </div>

                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="EndDay" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">End of Day</h2>
                </div>
                <div class="modal-body">

                    <div class="user-inputbox" style="box-shadow: none;">


                        <form role="form" action="{{ route('store.end.day.cash') }}" method="post">

                            {{ csrf_field() }}
                            <input type="hidden" name="date" value="{{ date('Y-m-d') }}">

                            <div class="col-md-12" id="note_entry1">

                                <div>
                                    <div class="col-md-5">
                                        <div class="form-select-list">
                                            <label for="name">Select Note</label>
                                            <select name="note[]" class="form-control custom-select-value"
                                                    title="">
                                                @foreach($notes as $note)
                                                    <option value="{{ $note->value }}">{{ $note->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="phone">Total Note in Drawer:</label>
                                            <input type="text" name="quantity[]" class="form-control" required title="">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                style="margin-top: 29px;"
                                                onClick="add_note_entry1()">
                                            Add
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                style="margin-top: 29px;"
                                                onclick="deleteParentElement1(this)">
                                            Remove
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-12">

                                    <hr>
                                    <div class="form-group">
                                        <label for="name">Total Sales: <b>{{ $net_profit }} {{ config('app.currency')}}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Start Day Cash: <b>{{ $start_day_cash }} {{ config('app.currency')}}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Total Withdrawn: <b>{{ $total_withdraw }} {{ config('app.currency')}}</b></label>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label for="name">Total Cash: <b>{{ $current_cash }} {{ config('app.currency')}}</b></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="btn-submit btn-primary" id="store-customer" type="submit">
                                            submit
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>


    <script type="text/javascript">


        // CREATING BLANK NOTE ENTRY
        var blank_note_entry = '';
        $(document).ready(function () {
            blank_note_entry = $('#note_entry').html();
        });

        function add_note_entry() {
            $("#note_entry").append(blank_note_entry);
        }

        // REMOVING ENTRY
        function deleteParentElement(n) {
            n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
        }


        var blank_note_entry1 = '';
        $(document).ready(function () {
            blank_note_entry1 = $('#note_entry1').html();
        });

        function add_note_entry1() {
            $("#note_entry1").append(blank_note_entry1);
        }

        // REMOVING ENTRY
        function deleteParentElement1(n1) {
            n1.parentNode.parentNode.parentNode.removeChild(n1.parentNode.parentNode);
        }


    </script>

@endsection