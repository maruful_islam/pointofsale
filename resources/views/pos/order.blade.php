@extends('admin_layouts.default2')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

    <link rel="stylesheet" href="{{ url('libs/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/multi-select/css/multi-select.css') }}">

    <style>
        .select2-searchss .select2-container .select2-selection--single {
            height: 36px;
            padding: 4px 0;
            border-color: #dee7ee;
        }
        span.select2-dropdown.select2-dropdown--above {
            width: 353px !important;
            margin-top: -11px;
        }
        span.select2-dropdown.select2-dropdown--below {
            width: 353px !important;
        }

    </style>
@endsection

@section('content')

    <div class="home-icons" style="display: none">
        <a href="/home"><span class="fa fa-home"></span></a>
    </div>
    <div class="poscat-list-page">
        <div class="poscat-container">
            <div class="poscat-list-inner">
                <div class="row">
                    <aside class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                        <div class="pos-cat-search">
                            <div class="search-area">
                                <div class="search-inpu">

                                    <select class="" id="product_search" name="product_search" style="width: 100%"></select>
                                </div>
                            </div>
                        </div>
                        <div class="pos-cat-list-area">
                            <div class="poscat-menu" id="examples">
                                <div class="poscat-menu-items content">
                                    <ul class="poscat-menu-items" id="pos-cmenu">
                                        <li class="item pos-c-menu filter" data-filter='*'>
                                            <div class="pos-me-img">
                                                <img src='{{  asset("product/categoy/all.jpg") }}' alt="">
                                            </div>
                                            <h6>All</h6>
                                        </li>
                                        @foreach($categories as $category)
                                            <li class="item pos-c-menu filter" data-filter='.category-{{$category->id}}'>
                                                <div class="pos-me-img">
                                                    <img src='{{ asset("$category->icon") }}' alt="">
                                                </div>
                                                <h6>{{$category->name}}</h6>
                                            </li>
                                        @endforeach
                                        <li class="item pos-c-menu filter" id="condiment" data-filter='.condiment-items' style="display: none;">
                                            <div class="pos-me-img">
                                                <img src='{{asset("product/categoy/condiments.jpg")}}' alt="">
                                            </div>
                                            <h6>Condiments</h6>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pos-list-item">
                                <div class="row">
                                    <ul class="allpos-items content">
                                        @foreach($items as $item)
                                            <li class='allpos-item col-xs-12 col-sm-3 col-md-3 category-{{$item->cat_id}}'>
                                                <a href="#" class="add-to-cart" data-name='{{$item->p_name}}' data-id='{{$item->id}}' data-price='{{$item->p_price}}' data-arabic_name='{{$item->arabic_name}}'>
                                                    <div class="allpos-item-content">
                                                        <div class="allpos-item-img">
                                                            <img src='{{asset("$item->p_image")}}' alt="">
                                                        </div>
                                                        <div class="allpos-dec">
                                                            {{$item->p_name}}
                                                            <div class="price"><span> {{number_format($item->p_price, 3)}} {{ config('app.currency')}}</span></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endforeach
                                        @foreach($condiments as $condiment)
                                            <li class='allpos-item col-xs-12 col-sm-3 col-md-3 condiment-items'>
                                                <a href="#" class="add-to-cart" data-name='{{$condiment->name}}' data-id='{{ 'null' }}' data-condiment='{{ $condiment->id }}' data-price='0.000' data-arabic_name='{{$item->arabic_name}}'>
                                                    <div class="allpos-item-content">
                                                        <div class="allpos-item-img">
                                                            {{--                                                            <img src='{{asset("$condiment->image")}}' alt="">--}}
                                                            <img src='https://ilovedummy.com/image/400/200/g' alt="">
                                                        </div>
                                                        <div class="allpos-dec">
                                                            {{$condiment->name}}
                                                            {{--<div class="price"><span> {{number_format($condiment->p_price, 3)}} {{ systemInfo('currency')}}</span></div>--}}
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <aside class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                        <div class="pos-order-table">
                            <div class="order-table-top ">
                                <ul class="nav2">
                                    <li><a href="{{ url('/home') }}" class=""><span class="fa fa-home"></span>Dashboard</a></li>
                                    @foreach($order_types as $key => $order_type)
                                        @if($key == 0)
                                            <li><a href="#" class="clickOrderType active"
                                                   data-order-id='{{"$order_type->id"}}'
                                                   data-order-name='{{"$order_type->name"}}'><span
                                                            class=""></span>{{ $order_type->name }}</a></li>
                                        @else
                                            <li>
                                                <a href="#" class="clickOrderType" data-order-id='{{"$order_type->id"}}'
                                                   data-order-name='{{"$order_type->name"}}'><span
                                                            class=""></span>{{ $order_type->name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                    <li>
                                        {{--<a href="kitchen_list"><span class="fa fa-history"></span>Pre Order</a>--}}
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm"><span class="fa fa-calendar"></span><b><span id="pre-order-label">Pre Order</span></b></button>
                                    </li>

                                    <li>
                                        <a id="customer-popup" href="#" class="user-edits">
                                            <span class="fa fa-user-plus"></span>Customer
                                        </a>
                                    </li>

                                    <!-- <li><a href="#"
                                            id="hold-show-p" ><span
                                                    class="fa fa-history"></span>Hold</a></li> -->
                                    <li><a href="#" onclick="document.getElementById('finalpay').style.display='block'"><span
                                                    class="fa fa-history"></span>Order Status</a>
                                    </li>

                                    <li><a href="kitchen_list">
                                            <span class="fa fa-fire"></span>Kitchen Status</a>
                                    </li>

                                    <li>
                                        <a href="#" onclick="loadCondimentItem()" ><span class="fa fa-th"></span>Condiments</a>
                                        <script>
                                            function loadCondimentItem(){
                                                //$('.poscat-menu-items li').click()
                                                $("#condiment").click();
                                            }
                                        </script>
                                    </li>

                                </ul>
                            </div>
                            <div class="order-table-contetn-table table-responsive">
                                <div class="table-header">
                                    <div class="pro-count pro-countarea">
                                        <ul class="row">
                                            <li class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                                <strong>table:</strong>
                                                <div class="pro-selectarea">
                                                    <select class="header-option tb-select" data-role="sorter"
                                                            id="table_id">
                                                        <option value="0" selected="selected" disabled="">Select
                                                        </option>
                                                        @foreach($tables as $table)
                                                            <option class="get-table-value" value="{{$table->id}}" table-data-id="{{$table->id}}" table-data-name="{{$table->name}}">{{$table->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
                                                <strong>Customer:</strong>
                                                <div class="pro-selectarea select2-searchss">
                                                    <select class="header-option tb-select select2-search" data-role="sorter" id="customer_id">


                                                    </select>
                                                </div>

                                            </li>
                                            <!-- <li>
                                                <strong>Item Code :</strong>
                                                <div class="pos-itemcode">
                                                    <input type="text" class="pos-item-code">
                                                </div>
                                            </li> -->
                                            <li class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                                <strong>Person :</strong>
                                                <div class="pos-itemcode">
                                                    <input type="text" id="person" value="1" class="pos-item-code">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="table-srcoll table-responsive" style="position: relative;height: 377px">
                                    <table class="table pos-product-list-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center" scope="col" width="5.3%">#</th>
                                            <th scope="col" width="50%">Name</th>
                                            <th class="text-center pos-product-list-headQit" scope="col" width="20%">Qt:
                                            </th>
                                            <th class="text-center pos-product-list-headDis" scope="col" width="12%">Dis
                                            </th>
                                            <th class="text-center pos-product-list-headprice" scope="col" width="12.2%">
                                                Price
                                            </th>
                                            <th class="text-center pos-product-list-headprAct" scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody class="pos-pro-list-table-body" id="show-cart">


                                        </tbody>
                                    </table>
                                </div>
                                <div class="pos-total-counts">
                                    <div class="pos-to-left text-left">
                                        <ul>
                                            <li class="total-items">Total item (<span id="count-Cart">0</span>)</li>
                                            <li>Complimentary Status : <input type="checkbox" name="cmp" id="cmp" value="1"></li>
                                            <li>Discount:
                                                <input type="hidden" id="inputDiscount_2" placeholder="">
                                                <input type="number" id="inputDiscount" placeholder="0.00" class="pos-count-discount">
                                                <button class="btn btn-button kdcustom"><span id="show-kd">{{ config('app.currency')}}</span></button>
                                                <button class="btn btn-button" style="background: #e41414;color:  #fff;" onclick="document.getElementById('dispop').style.display='block'">..</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="pos-to-right text-right">
                                        <ul>
                                            <li><strong>Sub Total: </strong> <span id="total-cart">0.00</span> {{ config('app.currency')}}</li>
                                            <li><strong> Item Discount: </strong> <span
                                                        id="total-cart-discount">0.00</span> {{ config('app.currency')}}
                                            </li>
                                            <li><strong> Discount: </strong> <span id="input-discount">0.000</span> {{ config('app.currency')}}
                                            </li>
                                        </ul>
                                        <ul id="show-all-tax">
                                        </ul>
                                        <ul id="show-all-discount">
                                        </ul>

                                        <ul class="grand-toral">
                                            <li>
                                                <strong>Grand Total : </strong> <span id="grand-total">0.00</span> {{ config('app.currency')}}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="grand-toral-links">
                                        <ul>
                                            <li><a href="#" id="clear-cart">Cancel</a></li>
                                            <li><a href="#" id="create-hold-order">Hold</a></li>
                                            <!--  <li onclick="document.getElementById('holdorder').style.display='block'" ><button>Hold</button></li> -->
                                            {{--<li><a href="#" id="bill-print"   >Bill Print</a></li>--}}
                                            <!-- <li><button id="split_order_btn" >Split Order</button></li> -->
                                            <li>
                                                <button id="split-popup">Split Order</button>
                                            </li>
                                            <li>
                                                <button id="create-kitchen-hold">To Kitchen</button>
                                            </li>

                                            <li>
                                                <button id="confirm-popup">Payment</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </aside>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Final Pay order -->
    <div class="Popupmodel" id="finalpay">
        <div class="payment_final animated zoomIn">
            <div class="payment-final-top">
                <div class="close-button"><button class="popupclases"><span class="fa fa-close"></span></button></div>
            </div>
            <div class="payment-final">
                <button class="btn btn-button"  id="open-check">Open <br> Check</button>
                <button class="btn btn-button"  onclick="document.getElementById('finalpayclose').style.display='block'">Closed<br>Check</button>
            </div>
        </div>
    </div>
    <!--End Final Pay order -->
    <!--Final Pay order close -->
    <div class="Popupmodel" id="finalpayclose">
        <div class="payment-container animated zoomIn">
            <div class="hold-areabox finalpay">
                <div class="paytitle text-center">
                    <h1>Closed Check</h1>
                    <div class="close-button">
                        <button class="popupclase"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="hold-area">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                            <div class="hold-list-items" >
                                <div class="hold-l-search">
                                    <input type=search placeholder="Enter Your Hold Keyword....." class="form-control" />
                                    <button type="button" value=""><span class="fa fa-search"></span></button>
                                </div>
                                <div class="hol-scrol" style="height: 520px">
                                    <input type="hidden" id="close-index-number" value="">
                                    <div class="hold-list-item" id="show-all-close-check">





                                    </div>



                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="hold-order-detaisls">
                                <h2 class="text-center">order Details</h2>
                                <div class="hold-order-details-inner">
                                    <div class="order-author-info paidarea">
                                        <div class="close-paid-bg" style="display: none;">
                                            <img src="{{asset('pos/images/paid.png')}}" alt="">
                                        </div>
                                        <ul>
                                            <div class="closeorderls" style="width: 50%">
                                                <li>CHK Number :<span id="show-close-chk-number"></span></li><br>
                                                <li>Date : <span id="show-close-date">xx-xx-xxxx</span></li>
                                            </div>
                                            <div class="closeorderls" style="width: 50%">
                                                <li>Oder Type : <span id="show-close-order-type"> None </span></li><br>
                                                <li>Time : <span id="show-close-time">xx:xx</span></li>
                                            </div>
                                        </ul>
                                    </div>
                                    <table class="table pos-product-list-head">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="text-center">Product name</th>
                                            <th class="text-center pos-product-list-headQit" scope="col">Qi:</th>
                                            <th class="text-center pos-product-list-headDis" scope="col">Dis</th>
                                            <th class="text-center pos-product-list-headprice" scope="col">Unit Price</th>
                                            <th class="text-center pos-product-list-headprAct" scope="col">Sub Price</th>
                                        </tr>
                                        </thead>
                                        <tbody class="pos-pro-list-table-body" id="view-single-close-order">



                                        </tbody>
                                    </table>
                                    <div class="pos-total-counts">
                                        <div class="pos-to-right text-right">
                                            <ul>
                                                <li><strong>Sub Total :</strong> <span id="show-close-subtotal">0.000 </span>{{ config('app.currency')}}</li>
                                                <li><strong>Discount :</strong> <span id="show-close-discount">0.000</span>{{ config('app.currency')}}</li>
                                                <li><strong>Charges :</strong> <span id="show-close-charges">0.000</span>{{ config('app.currency')}}</li>
                                                <li><strong>Tips :</strong> <span id="show-close-tips">0.000</span>{{ config('app.currency')}}</li>

                                            </ul>
                                            <ul class="grand-toral">
                                                <li>
                                                    <span><strong>Grand Total</strong><span id="show-close-total">0.000</span>{{ config('app.currency')}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Final Pay order close -->



    <!-- Start Payment PopUp -->


    <div class="Popupmodel" id="popUp">
        <div class="payment-container animated  zoomIn">
            <div class="payment-conform">
                <div class="paytitle text-center">
                    <h1>Payment</h1>
                    <div class="close-button">
                        <button class="popupclase"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="row">
                    <div class="payment-area">
                        <div class="paybox-area">
                            <div class="col-sm-12 col-md-6">
                                <div class="pos-order-table">
                                    <div class="order-table-contetn-table table-responsive">
                                        <table class="table pos-product-list-head">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="5.8%" scope="col">#</th>
                                                <th scope="col" width="25.9%">Name</th>
                                                <th class="text-center pos-product-list-headQit" width="14%" scope="col">Qty</th>
                                                <th class="text-center pos-product-list-headDis" scope="col" width="11.7%">Each</th>
                                                <th class="text-center pos-product-list-headprice" scope="col" width="13.8%">Total</th>
                                            </tr>
                                            </thead>
                                        </table>
                                        <div class="table-srcoll table-responsive"
                                             style="position: relative;height: 250px">
                                            <table class="table pos-product-list-table">
                                                <tbody class="pos-pro-list-table-body" id="show-cart-hold">


                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pos-total-counts">
                                            <div class="pos-to-left text-left">
                                                <ul>
                                                    <li class="total-items">Total item (<span
                                                                id="count-Cart-hold">0</span>)
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="pos-to-right text-right">
                                                <ul>
                                                    <li><strong>Sub Total: </strong> <span id="total-cart-hold">0.000</span> {{ config('app.currency')}}</li>
                                                    <li><strong>Tips: </strong> <span id="tip-hold">0.000</span> {{ config('app.currency')}}</li>
                                                    <li><strong>Delivery: </strong> <span id="delivery-hold">0.000</span> {{ config('app.currency')}}</li>
                                                    <li><strong> Item Discount: </strong> <span id="total-cart-discount-hold">0.000</span> {{ config('app.currency')}}</li>
                                                    <li><strong> Discount: </strong> <span id="input-discount-hold">0.000</span>{{ config('app.currency')}}</li>
                                                </ul>
                                                <ul id="show-all-tax-hold">
                                                </ul>
                                                <ul id="show-all-discount-hold">
                                                </ul>

                                                <ul class="grand-toral">
                                                    <li>
                                                        <strong>Grand Total: </strong> <span id="grand-total-hold">0.000</span> {{ config('app.currency')}}
                                                        <input type="hidden" value="" id="is_hold">
                                                        <input type="hidden" value="" id="hold_index">
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="pay-box-right">
                                    <div class="input-box">
                                        <ul>
                                            <li><label>Total Price</label> <input type="text" disabled="" class="pay-card-num" id="payment-total"></li>
                                            <li><label>Payment</label> <input type="text" class="pay-card-num" id="input-current-amount"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="paybox-numarea">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="pay-numbox">
                                                <div class="row">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="1"><span>1</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="2"><span>2</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="3"><span>3</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="4"><span>4</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="5"><span>5</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="6"><span>6</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="7"><span>7</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="8"><span>8</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="9"><span>9</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cancel"
                                                                id="remove-cal-char"><span class="fa fa-arrow-left"></span>
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="0"><span>0</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button"
                                                                value="."><span>.</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3">
                                            <div class="pay-actoch">
                                                <button id="new-cal-clear"
                                                        class="btn btn-default btn-block btn-lg paybox-num-button paybox-num-ac">
                                                    <span>AC</span>
                                                </button>
                                                <button class="bg-color btn btn-default btn-block btn-lg paybox-num-button paybox-num-change">
                                                    <span>Change</span><br> <span id="payment-change">0.000 </span> {{ config('app.currency')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-12">
                                            <button class="btn btn-danger btn-block btn-lg" id="split-bill-btn">
                                                <span>Split Bill</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="payment-fild">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <div class="paybox-left" id="paybox" style="display:block">
                                        <input type="hidden" value="" id="show-payment-method-id">
                                        <h2 class="text-left" id="show-payment-method-type">
                                            @foreach($methods as $key => $method)
                                                @if($key == 0)
                                                    <input type="hidden" value="{{$method->id}}" id="show-payment-method-id">
                                                    <span>{{$method->method_name}}</span>
                                                @endif
                                            @endforeach
                                        </h2>
                                        <div class="pay-card-list nav2">
                                            <ul class="cash-card" id="payment-method">
                                                @foreach($methods as $key => $method)
                                                    @if($key == 0)
                                                        <li class="payment-method-type"
                                                            data-method-name='{{$method->method_name}}'
                                                            data-method-id='{{$method->id}}'
                                                            data-method-title='{{$method->title}}'><a
                                                                    class="active"
                                                                    href="#">{{$method->method_name}}</a>
                                                        </li>
                                                    @else
                                                        <li class="payment-method-type"
                                                            data-method-name='{{$method->method_name}}'
                                                            data-method-id='{{$method->id}}'
                                                            data-method-title='{{$method->title}}'><a
                                                                    href="#">{{$method->method_name}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                                <li>
                                                    <button onclick="document.getElementById('tipamount').style.display='block'"
                                                            class="btn btn-button" href="#">tip amount
                                                    </button>
                                                </li>
                                                <li>
                                                    <button onclick="document.getElementById('deliverycharge').style.display='block'"
                                                            class="btn btn-button" href="#">delivery charge
                                                    </button>
                                                </li>

                                            </ul>
                                        </div>
                                        <input class="input-pay" id="payment-method-note" type="text"/>
                                    </div>
                                    <div class="paybox-left" id="split-paybox" style="display:none">
                                        <!--------------------------------- Split Bill One ---------------------------->
                                        <div class="col-md-6">
                                            <input type="hidden" value="" id="show-split-bill-one-payment-method-id">
                                            {{--<h2 class="text-left" id="show-split-bill-one-payment-method-type">--}}
                                            <h2 class="text-left">
                                                @foreach($methods as $key => $method)
                                                    @if($key == 0)
                                                        <input type="hidden" value="{{$method->id}}" id="show-split-bill-one-payment-method-id">
                                                        <span id="show-split-bill-one-payment-method-type">{{$method->method_name}}</span>
                                                        <input type="text" class="" id="split-amount-one">
                                                    @endif
                                                @endforeach
                                            </h2>
                                            <div class="pay-card-list nav2">
                                                <ul class="cash-card" id="payment-one-method">
                                                    @foreach($methods as $key => $method)
                                                        @if($key == 0)
                                                            <li class="payment-one-method-type"
                                                                data-split-bill-one-method-name='{{$method->method_name}}'
                                                                data-split-bill-one-method-id='{{$method->id}}'
                                                                data-split-bill-one-method-title='{{$method->title}}'><a
                                                                        class="active"
                                                                        href="#">{{$method->method_name}}</a>
                                                            </li>
                                                        @else
                                                            <li class="payment-one-method-type"
                                                                data-split-bill-one-method-name='{{$method->method_name}}'
                                                                data-split-bill-one-method-id='{{$method->id}}'
                                                                data-split-bill-one-method-title='{{$method->title}}'><a
                                                                        href="#">{{$method->method_name}}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                    <li>
                                                        <button onclick="document.getElementById('tipamount').style.display='block'"
                                                                class="btn btn-button" href="#">tip amount
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button onclick="document.getElementById('deliverycharge').style.display='block'"
                                                                class="btn btn-button" href="#">delivery charge
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input class="input-pay" id="payment-method-note" type="text"/>
                                        </div>
                                        <!--------------------------------- Split Bill Two ---------------------------->
                                        <div class="col-md-6">
                                            <input type="hidden" value="" id="show-split-bill-two-payment-method-id">
                                            {{--<h2 class="text-left" id="show-split-bill-two-payment-method-type">--}}
                                            <h2 class="text-left">
                                                @foreach($methods as $key => $method)
                                                    @if($key == 0)
                                                        <input type="hidden" value="{{$method->id}}" id="show-payment-method-id">
                                                        <span id="show-split-bill-two-payment-method-type">{{$method->method_name}}</span>
                                                        <input type="text" class="" id="split-amount-two">
                                                    @endif
                                                @endforeach
                                            </h2>
                                            <div class="pay-card-list nav2">
                                                <ul class="cash-card" id="payment-two-method">
                                                    @foreach($methods as $key => $method)
                                                        @if($key == 0)
                                                            <li class="payment-two-method-type"
                                                                data-split-bill-two-method-name='{{$method->method_name}}'
                                                                data-split-bill-two-method-id='{{$method->id}}'
                                                                data-split-bill-two-method-title='{{$method->title}}'><a
                                                                        class="active"
                                                                        href="#">{{$method->method_name}}</a>
                                                            </li>
                                                        @else
                                                            <li class="payment-two-method-type"
                                                                data-split-bill-two-method-name='{{$method->method_name}}'
                                                                data-split-bill-two-method-id='{{$method->id}}'
                                                                data-split-bill-two-method-title='{{$method->title}}'><a
                                                                        href="#">{{$method->method_name}}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                    <li>
                                                        <button onclick="document.getElementById('tipamount').style.display='block'"
                                                                class="btn btn-button" href="#">tip amount
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button onclick="document.getElementById('deliverycharge').style.display='block'"
                                                                class="btn btn-button" href="#">delivery charge
                                                        </button>
                                                    </li>


                                                    <!--
                                                    <li><a class="btn btn-button" href="#">Cash </a></li>
                                                    <li><a class="btn btn-button" href="#">Check</a></li>
                                                    <li><a class="btn btn-button" href="#">Visa</a></li>
                                                    <li><a class="btn btn-button" href="#">Master Card</a></li>
                                                    <li><a class="btn btn-button" href="#">tip amount</a></li>
                                                    <li><a class="btn btn-button" href="#">american express</a></li>
                                                     -->


                                                </ul>
                                            </div>
                                            <input class="input-pay" id="payment-method-note" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pay-footer-button">
                            <button class="btn close-btn btn-button popupclase">close</button>
                            <button class="btn close-btn btn-button" id="confirm-payment" disabled="disabled">Confirm
                                Payment
                            </button>
                        </div>
                    </div>

                </div>
                <!--
                <div class="payment-footer">
                    
                </div> -->
            </div>
        </div>
    </div>



    <!-- tip amount POPUp -->
    <div class="Popupmodel" id="tipamount">
        <div class="dis-container animated bounceIn">
            <div class="dis-inner tip-amount">
                <div class="dis-top">
                    <h2 class="dis-title">Tip Amount</h2>
                    <div class="close-button">
                        <button class="tipclose"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="tip-type">
                    <div class="tip-input">
                        <input type="text" id="tip-amount-input" value="" class="form-control">
                    </div>
                </div>
                <div class="dis-footer">
                    <button class="btn btn-button" id="tip-amount">Ok</button>
                    <button class="btn btn-button tipclose">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End tip amount POPUp -->

    <!-- delivery charge POPUp -->
    <div class="Popupmodel" id="deliverycharge">
        <div class="dis-container animated bounceIn">
            <div class="dis-inner tip-amount">
                <div class="dis-top">
                    <h2 class="dis-title">Delivery Charge</h2>
                    <div class="close-button">
                        <button class="tipclose"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="tip-type">
                    <div class="tip-input">
                        <input type="text" id="delivery-charge-input" value="" class="form-control">
                    </div>
                </div>
                <div class="dis-footer">
                    <button class="btn btn-button" id="delivery-charge">Ok</button>
                    <button class="btn btn-button tipclose">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End delivery charge POPUp -->


    <!-- End Payment PopUp -->

    <!-- HOlD POP UP -->
    <div class="Popupmodel" id="holdpop">
        <div class="payment-container animated zoomIn">
            <div class="hold-areabox">
                <div class="paytitle text-center">
                    <h1 id="hold-show-name">Hold</h1>
                    <div class="close-button">
                        <button class="popupclase"><span class="fa fa-close"></span></button>
                    </div>

                </div>
                <div class="hold-area">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="hold-list-items">
                                <div class="hold-l-search">
                                    <input type=search placeholder="Enter Your Hold Keyword....." class="form-control"/>
                                    <button type="button" value=""><span class="fa fa-search"></span></button>
                                    <input type="hidden" id="action_hold" value="">
                                </div>
                                <div class="hol-scrol" style="height: 600px">
                                    <div class="hold-list-item" id="show-all-hold">

                                        <div class="col-xs-12 colsm-6 col-md-6">
                                            <div class="hold-list-inner">
                                                <div class="item">
                                                    <div class="hold-stock">Take<br>Away</div>
                                                    <div class="hold-item-dec">
                                                        <a href="">Cheesy Black Bean Quesadilla</a>
                                                        <div class="hold-meta">
                                                            <ul>
                                                                <li><a href="" data-toggle="tooltip"
                                                                       title="Delete"><span class="fa fa-trash"></span></a>
                                                                </li>
                                                                <li><a href="" data-toggle="tooltip" title="print"><span
                                                                                class="fa fa-print"></span></a></li>
                                                                <li><a href="" data-toggle="tooltip" title="Pay"><span
                                                                                class="fa fa-fa fa-money"></span>
                                                                        Pay</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="hold-order-detaisls table-srcoll" style="height:800px">
                                <h2 class="text-center">order Details</h2>
                                <div class="hold-order-details-inner table-responsive">
                                    <div class="order-author-info">
                                        <ul>
                                            <li>Author : <span id="holdAuthor"></span></li><br>
                                            <li>Date : <span id="holdDate"></span></li>
                                            <br>
                                            <!-- <li>Code : <span id="holdCode"></span></li> -->
                                            <li>Time : <span id="holdTime"></span></li>
                                        </ul>
                                    </div>
                                    <table class="table pos-product-list-head">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="text-center">Product name</th>
                                            <th class="text-center pos-product-list-headQit" scope="col">Qt:</th>
                                            <th class="text-center pos-product-list-headDis" scope="col">Dis</th>
                                            <th class="text-center pos-product-list-headprice" scope="col">Unit Price
                                            </th>
                                            <th class="text-center pos-product-list-headprAct" scope="col">Sub Price
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="pos-pro-list-table-body" id="show-hold-data">


                                        </tbody>
                                    </table>
                                    <div class="pos-total-counts">
                                        <div class="pos-to-right text-right">
                                            <ul>
                                                <li><strong>Sub Total :</strong> <span id="holdSubTotal"></span> {{ config('app.currency')}}</li>
                                                <li><strong>Discount :</strong> <span id="holdDiscount"></span> {{ config('app.currency')}}</li>
                                                <li><strong>Tax :</strong> <span id="holdTax"></span> {{ config('app.currency')}}</li>
                                            </ul>
                                            <ul class="grand-toral">
                                                <li>
                                                    <span><strong>Grand Total</strong><span id="holdGrandTotal"></span> {{ config('app.currency')}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paynet-footer">
                    <!-- <div class="pay-footer-button">
                        <button class="btn close-btn btn-button popupclase">close</button>
                        <button class="btn close-btn btn-button">Bill Print</button>
                        <button class="btn close-btn btn-button">Payment</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- END HOLD POPUP -->


    <!-- START SPLIT ORDER POPUP -->
    <div class="Popupmodel" id="Splitorder">
        <div class="split-container animated bounceIn">
            <div class="hold-areabox">
                <div class="paytitle text-center">
                    <h1>Split Order</h1>
                </div>
                <div class="hold-area">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5">
                            <div class="chk-nub text-center"><strong> CHK Number : <span id="left-chk"> 105-1</span></strong></div>
                            <input type="hidden" id="left-chk-value" value="">
                            <div class="split-table table-srcoll" style="height: 300px;position: relative;">

                                <table class="table pos-product-list-head" style="min-height: 150px">
                                    <thead>
                                    <tr>
                                        <th scope="col">Product name</th>
                                        <th class="text-center pos-product-list-headQit" scope="col">Qty:</th>
                                        <th class="text-center pos-product-list-headprAct" scope="col">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody class="pos-pro-list-table-body" id="table-split-list-left">


                                    </tbody>
                                </table>
                            </div>
                            <div class="split-check">
                                <div class="pos-to-right text-right">
                                    <ul>
                                        <li><strong> Discount: </strong> <span id="split-order-discount">0.000</span> {{ config('app.currency')}}
                                        </li>
                                    <!-- <li><strong> charges: </strong> <span >0.000</span> {{ config('app.currency')}}</li> -->
                                    </ul>
                                    <ul class="grand-toral">
                                        <li>
                                            <strong> Total : </strong> <span id="split-order-total">0.000</span> {{ config('app.currency')}}
                                        </li>
                                    </ul>
                                    <button class="btn close-btn btn-button" id="payment-split-left">Payment</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2">
                            <div class="split-pas">
                                <div class="split-pas-top">
                                    <a href="#" id="angle-single-right"><span class="fa fa-angle-right"></span></a>
                                    <a href="#" id="angle-double-right"> <span class="fa fa-angle-double-right"></span></a>
                                </div>
                                <div class="split-pas-bottom">
                                    <a href="#" id="angle-single-left"><span class="fa fa-angle-left"></span></a>
                                    <a href="#" id="angle-double-left"><span class="fa fa-angle-double-left"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5">
                            <div class="split-table">
                                <div class="chk-nub text-center"><strong> CHK Number : <span id="right-chk"> 105-2</span></strong></div>
                                <input type="hidden" id="right-chk-value" value="">
                                <div class="split-table table-srcoll" style="height: 300px;position: relative;">
                                    <table class="table pos-product-list-head">
                                        <thead>
                                        <tr>
                                            <th scope="col">Product name</th>
                                            <th class="text-center pos-product-list-headQit" scope="col">Qi:</th>
                                            <th class="text-center pos-product-list-headprAct" scope="col">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody class="pos-pro-list-table-body" id="table-split-list-right">


                                        </tbody>
                                    </table>
                                </div>
                                <div class="split-check">
                                    <div class="pos-to-right text-right">
                                        <ul>
                                            <li><strong> Discount: </strong> <span
                                                        id="split-order-discount-right">0.000</span> {{ config('app.currency')}}
                                            </li>
                                        <!-- <li><strong> charges: </strong> <span >0.000</span> {{ config('app.currency')}}</li> -->
                                        </ul>
                                        <ul class="grand-toral">
                                            <li>
                                                <strong> Total : </strong> <span
                                                        id="split-order-total-right">0.000</span> {{ config('app.currency')}}
                                            </li>
                                        </ul>
                                        <input type="hidden" value="" id="split-array-type">
                                        <button class="btn close-btn btn-button" id="payment-split-right">Payment
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paynet-footer">
                    <div class="pay-footer-button">
                        <button class="btn close-btn btn-button popupclase">close</button>
                        <!-- <button class="btn close-btn btn-button" id="split-confirm-btn">Confirm</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SPLIT ORDER POPUP -->


    <div class="invoice-page" style="display:none ;">
        <div class="invoice-area">
            <div class="inv-header">
                <div class="logoss">
                    <img  src='{{asset("pos/images/ddddd.png")}}' alt="">
                </div>
                <div class="invo-logo text-center">

                    <strong>{{ systemInfo('system_name') }}</strong><br>
                    <strong>{{ systemInfo('local_name') }}</strong>
                    <!--<p>قصر المهراجا</p>-->
                </div>
                <div class="invo-header-content" id="clipPolygon">
                    <div class="invo-header-info">
                        <ul>
                            <?php
                            $phone = config('app.phone');
                            // $email = config('app.email');
                            ?>
                            @if(isset($phone))
                                <li><span id="show-bill-phone"> Phone : {{ config('app.phone') }}</span></li>
                            @endif
                            @if(isset($email))
                            <!--<li><span id="show-bill-email"> Email : {{ config('app.email') }}</span></li>-->
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Invoice Print Start From Here -->
            <div class="invoice-center">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <ul>
                            <li>CHK : <span id="show-bill-order_number"></span></li>
                            <li>Date : <span id="show-bill-current-date"></span></li>
                            <li>Guest : <span id="show-bill-person"></span></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <ul>
                            <li>Time : <span id="show-bill-current-time"></span></li>
                            <li>Order : <span id="show-bill-order-type"></span></li>
                            <li id="table-display">Table : <span id="show-bill-table-number"></span></li>
                        </ul>
                    </div>
                </div>
                <div class="customer-info" style="display: none;">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <ul>
                                <li><strong>Name : </strong><span id="cus-bill-name"></span></li>
                                <li>Area: <span id="cus-bill-area"></span></li>
                                <li>Street no.: <span id="cus-bill-street-no"></span></li>
                                <li>Building: <span id="cus-bill-building"></span></li>
                                <li>Whats-App: <span id="cus-bill-w_number"></span></li>
                            </ul>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <ul>
                                <li><strong>Phone : </strong><span id="cus-bill-phone"></span></li>
                                <li>Street: <span id="cus-bill-street"></span></li>
                                <li>Block: <span id="cus-bill-block"></span></li>
                                <li>Floor: <span id="cus-bill-floor"></span></li>
                                <li>Flat: <span id="cus-bill-flat"></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="voice-tables">
                <div class="voic-table table-responsive">
                    <table class="table" style="margin-bottom: 0;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th width="10%">Qty</th>
                            <th>Rate</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody id="show-bill-print">

                        </tbody>
                    </table>
                </div>

                <div class="pos-total-counts">
                    <div class="pos-to-right ">
                        <ul>

                            <li>
                                <div class="free">
                                    <strong>Discount </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"> <span id="bill-discount">  0.000</span> {{ config('app.currency')}} </span>
                                </div>
                            </li>

                            <li>
                                <div class="free">
                                    <strong>Charges</strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"> <span id="bill-tax">  0.000</span> {{ config('app.currency')}} </span>
                                </div>
                            </li>

                            <li>
                                <div class="free">
                                    <strong>Sub Total </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="bill-sub-total"> 0.000</span> {{ config('app.currency')}}</span>
                                </div>
                            </li>


                        </ul>

                        <ul class="total">
                            {{--<li><strong>Total :</strong> <span id="bill-grand-total">  0.000</span> {{ config('app.currency')}}</li>--}}
                            <li><strong>Total :</strong> <span id="bill-grand-total">  0.000</span> {{ config('app.currency')}}</li>
                        </ul>


                        <ul class='hidden-bill'>
                            <li>
                                <div class="free">
                                    <strong><span id="bill-method-name"></span> </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="bill-payment-type-amount">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                                <div class="free">
                                    <strong><span id="data_split_bill_one_method_name"></span> </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="pay-one">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                                <br>
                                <div class="free">
                                    <strong><span id="data_split_bill_two_method_name"></span> </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="pay-two">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                            </li>
                        </ul>

                        <ul class='hidden-bill' id="bill-show-method-note">

                        </ul>


                        <ul class='hidden-bill'>


                            <li>
                                <div class="free">
                                    <strong>Tips </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="bill-tips-amount">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                            </li>
                            <li>
                                <div class="free">
                                    <strong>Delivery Charge </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="bill-delivery-charge">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                            </li>
                            <ul class="total">
                                <li><strong>Grand Total :</strong> <span id="bill-new-grand-total">  0.000</span> {{ config('app.currency')}}</li>
                            </ul>


                            <li>
                                <div class="free">
                                    <strong>Change Balance </strong>
                                </div>
                                <div class="text-right">
                                    <span class="text-right"><span id="bill-changes-balance">  0.000</span> {{ config('app.currency')}}</span>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <p>Thank you</p>
                    <p>Visit Us Again</p>
                </div>
            </div>
        </div>
    </div>


    <!-- Hold Order -->
    <div class="Popupmodel" id="holdorder">
        <div class="hold-order-container animated  zoomIn">
            <div class="hold-areabox ">
                <div class="paytitle closebtbn">
                    <h1>Pass Code</h1>
                    <div class="close-button">
                        <button id="colose-re-print-popup"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="hold-order-inner">
                    <div class="hol-order-input">
                        <input type="password" class="form-control" id="input-current-amount-hold"/>
                    </div>
                    <div class="hold-order-btn">
                        <button class="btn btn-button cal-button-hold" value="1">1</button>
                        <button class="btn btn-button cal-button-hold" value="2">2</button>
                        <button class="btn btn-button cal-button-hold" value="3">3</button>
                        <button class="btn btn-button cal-button-hold" value="4">4</button>
                        <button class="btn btn-button cal-button-hold" value="5">5</button>
                        <button class="btn btn-button cal-button-hold" value="6">6</button>
                        <button class="btn btn-button cal-button-hold" value="7">7</button>
                        <button class="btn btn-button cal-button-hold" value="8">8</button>
                        <button class="btn btn-button cal-button-hold" value="9">9</button>
                        <button class="btn btn-button cal-button-hold" id="remove-cal-char-hold"><span class="">⌫</span>
                        </button>
                        <button class="btn btn-button cal-button-hold" value="0">0</button>
                        <button class="btn btn-button cal-button-hold " id="hold-ac-btn">AC</button>
                    </div>
                    <input type="hidden" value="" id="close-re-order-type">
                    <div class="hold-submit text-center">
                        <!-- <button class="btn btn-button" id="create-hold-order">Hold order</button> -->
                        <button class="btn btn-button" id="re-print-close-order">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hold Order -->





    <!-- Customer Add Pop Up -->

    <div class="user-form-popup" id="create-customer" style="display: none">
        <div class="user-contaienr" id="userlistpop">
            <div class="us-pop-container animated bounceIn">
                <div class="pop-removed">
                    <button class="" id="customer-modal-close"><span class="fa fa-times"></span></button>
                </div>
                <div class="user-inputbox">
                    <h2 class="user-list-title">Customer Info</h2>

                    <form role="form" class="table-srcoll" id="customerForm" action="{{ route('customer.store') }}">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input title type="text" name="name" class="form-control" id="customer-name">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">E-mail:</label>
                                        <input title type="email" name="email" class="form-control" id="customer-email">
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="phone">Phone:</label>
                                        <input title type="text" name="phone" class="form-control" id="customer-phone">
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="email">Whats-App Number:</label>
                                        <input title type="text" name="w_number" class="form-control" id="w_number">
                                    </div>

                                </div>
                            </div>

                        </div>



                        <h4 style="Padding: 10px;border-top: 1px solid; border-bottom: 1px solid; font-family: 'Bookman Old Style'; text-align: center;">ADDRESS</h4>
                        <hr>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Area:</label>
                                <input title name="area" class="form-control" id="area" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Street Name:</label>
                                        <input title type="text" name="street" class="form-control" id="street">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Street No:</label>
                                        <input title type="text" name="street_no" class="form-control" id="street_no">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Block No:</label>
                                        <input title type="text" name="block" class="form-control" id="block">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Building No:</label>
                                        <input title type="text" name="building" class="form-control" id="building">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Floor No:</label>
                                        <input title type="text" name="floor" class="form-control" id="floor">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Flat No:</label>
                                        <input title type="text" name="flat" class="form-control" id="flat">
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                        {{--<label for="email">Area:</label>--}}
                        {{--<input title name="area" class="form-control" id="area" />--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label class="container">
                                <input type="checkbox" name="is_active" checked="checked" id="customer-status">
                                Status
                            </label>
                        </div>

                        <div class="form-group">
                            <button class="btn-submit btn-primary" id="store-customer" type="submit">submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="Popupmodel" id="tipamount2" style="display: none;z-index: 999999">
        <div class="dis-container animated bounceIn">
            <div class="dis-inner tip-amount">
                <div class="dis-top">
                    <h2 class="dis-title">Tip Amount</h2>
                    <div class="close-button">
                        <button class="tip-amount-cl"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="tip-type">
                    <div class="tip-input">
                        <input type="text" id="tip-amount-input-split" value="" class="form-control">
                    </div>
                </div>
                <div class="dis-footer">
                    <button class="btn btn-button tip-amount-cl" id="tip-amount">Ok</button>
                    <button class="btn btn-button tip-amount-cl">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="Popupmodel" id="deliverycharge2" style="display: none;z-index: 999999">
        <div class="dis-container animated bounceIn">
            <div class="dis-inner tip-amount">
                <div class="dis-top">
                    <h2 class="dis-title">Tip Amount</h2>
                    <div class="close-button">
                        <button class="delivery-charge-cl"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="tip-type">
                    <div class="tip-input">
                        <input type="text" id="delivery-charge-input-split" value="" class="form-control">
                    </div>
                </div>
                <div class="dis-footer">
                    <button class="btn btn-button tip-amount-cl" id="delivery-charge">Ok</button>
                    <button class="btn btn-button tip-amount-cl">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Start split payment popup -->
    <div class="Popupmodel" id="popsplitdor" style="display: none;">
        <div class="payment-container animated  zoomIn">
            <div class="payment-conform">
                <div class="paytitle text-center">
                    <h1>Payment</h1>
                    <div class="close-button">
                        <button class="popupclasep"><span class="fa fa-close"></span></button>
                    </div>
                </div>
                <div class="row">
                    <div class="payment-area">
                        <div class="paybox-area">
                            <div class="col-sm-12 col-md-6">
                                <div style="text-align: center; "><strong> <span id="payment-show-chk-id">CHK Number : 232343-1</span></strong></div>
                                <div class="pos-order-table">
                                    <div class="order-table-contetn-table">
                                        <div class="table-responsive table-srcoll"
                                             style="position: relative;height: 250px">

                                            <table class="table pos-product-list-head  ">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" width="5.8%" scope="col">#</th>
                                                    <th scope="col" width="25.9%">Name</th>
                                                    <th class="text-center pos-product-list-headQit" width="14%"
                                                        scope="col">Qty
                                                    </th>
                                                    <th class="text-center pos-product-list-headDis" scope="col"
                                                        width="11.7%">Each
                                                    </th>
                                                    <th class="text-center pos-product-list-headprice" scope="col"
                                                        width="13.8%">Total
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody class="pos-pro-list-table-body" id="show-order-split-cart">

                                                </tbody>

                                            </table>
                                        </div>


                                        <div class=" table-responsive">
                                            <table class="table pos-product-list-table">
                                                <tbody class="pos-pro-list-table-body" id="show-cart-split">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pos-total-counts">
                                            <div class="pos-to-left text-left">
                                                <!-- <ul>
                                                    <li class="total-items">Total item (<span id="count-Cart-split">0</span>)</li>
                                                </ul> -->
                                            </div>
                                            <div class="pos-to-right text-right">
                                                <ul>
                                                    <li><strong> Discount: </strong> <span id="input-discount-split">0.000</span>
                                                        {{ config('app.currency')}}
                                                    </li>
                                                </ul>
                                                <ul id="show-all-tax-hold"></ul>
                                                <ul id="show-all-discount-hold"></ul>

                                                <ul class="grand-toral">
                                                    <li>
                                                        <strong>Grand Total: </strong> <span id="grand-total-split">0.000</span>
                                                        {{ config('app.currency')}}
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="pay-box-right">
                                    <div class="input-box">
                                        <ul>
                                            <li>
                                                <label>Total Price</label>
                                                <input type="text" disabled="" class="pay-card-num" id="payment-total-split" value="0.000">
                                            </li>
                                            <li>
                                                <label>Payment</label>
                                                <input type="text" class="pay-card-num" id="input-current-amount-split">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="paybox-numarea">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="pay-numbox">
                                                <div class="row">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="1"><span>1</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="2"><span>2</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="3"><span>3</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="4"><span>4</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="5"><span>5</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="6"><span>6</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="7"><span>7</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="8"><span>8</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="9"><span>9</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cancel"
                                                                id="remove-cal-char-split"><span
                                                                    class="fa fa-times"></span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="0"><span>0</span></button>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 paybox-num-buttons">
                                                        <button class="btn btn-default btn-block btn-lg paybox-num-button cal-button-split"
                                                                value="."><span>.</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-3">
                                            <div class="pay-actoch">
                                                <button id="new-cal-clear-split"
                                                        class="btn btn-default btn-block btn-lg paybox-num-button paybox-num-ac">
                                                    <span>AC</span></button>
                                                <button class="bg-color btn btn-default btn-block btn-lg paybox-num-button paybox-num-change">
                                                    <span>Change</span>
                                                    <br> <span id="payment-change-split">0.000 </span> {{ config('app.currency')}}
                                                </button>
                                            </div>
                                        </div>
                                        <div class="payment-fild">
                                            <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                                <div class="paybox-left">
                                                    <input type="hidden" value="" id="show-payment-method-id-split">
                                                    <h2 class="text-left" id="show-payment-method-type-split">
                                                        @foreach($methods as $key => $method)
                                                            @if($key == 0)
                                                                <input type="hidden" value="{{$method->id}}"
                                                                       id="show-payment-method-id-split">
                                                                <span>{{$method->method_name}}</span>
                                                            @endif
                                                        @endforeach
                                                    </h2>
                                                    <div class="pay-card-list nav2">
                                                        <ul class="cash-card" id="payment-method-split">
                                                            @foreach($methods as $key => $method)
                                                                @if($key == 0)
                                                                    <li class="payment-method-type"
                                                                        data-method-name='{{$method->method_name}}'
                                                                        data-method-id='{{$method->id}}'
                                                                        data-method-title='{{$method->title}}'><a
                                                                                class="active"
                                                                                href="#">{{$method->method_name}}</a>
                                                                    </li>
                                                                @else
                                                                    <li class="payment-method-type"
                                                                        data-method-name='{{$method->method_name}}'
                                                                        data-method-id='{{$method->id}}'
                                                                        data-method-title='{{$method->title}}'><a
                                                                                href="#">{{$method->method_name}}</a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                            <li>
                                                                <button onclick="document.getElementById('tipamount2').style.display='block'"
                                                                        class="btn btn-button" href="#">tip amount
                                                                </button>
                                                            </li>
                                                            <li>
                                                                <button onclick="document.getElementById('deliverychange2').style.display='block'"
                                                                        class="btn btn-button" href="#">delivery charge
                                                                </button>
                                                            </li>


                                                            <!--
                                                            <li><a class="btn btn-button" href="#">Cash </a></li>
                                                            <li><a class="btn btn-button" href="#">Check</a></li>
                                                            <li><a class="btn btn-button" href="#">Visa</a></li>
                                                            <li><a class="btn btn-button" href="#">Master Card</a></li>
                                                            <li><a class="btn btn-button" href="#">tip amount</a></li>
                                                            <li><a class="btn btn-button" href="#">american express</a></li>
                                                             -->


                                                        </ul>
                                                    </div>
                                                    <input class="input-pay" id="payment-method-note-split"
                                                           type="text"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paynet-footer">
                    <div class="pay-footer-button">
                        <button class="btn close-btn btn-button popupclasep">close</button>
                        <button class="btn close-btn btn-button" id="confirm-payment-split" disabled="disabled">Confirm
                            Payment
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End split payment popup -->


    <!-- DISCOUNT POPUP -->
    <div class="Popupmodel" id="dispop">
        <div class="dis-container animated bounceIn">
            <div class="dis-inner">
                <h2 class="dis-title text-center">Discount Type</h2>
                <div class="dis-type">
                    <h2 class="dis-showtitle text-center"><span id="dis-type-show">Currency</span> </h2>
                    <div class="dis-select">
                        <label for="tb-select">Select:</label>
                        <select id="tb-select" class="dis-type-select form-control" data-role="sorter">
                            <option selected disabled>Select Discount Type</option>
                            <option value="Currency">Currency</option>
                            <option value="Percent">Percent</option>
                        </select>
                    </div>

                </div>
                <div class="dis-footer">
                    <!-- <button class="btn btn-button popupclase">Ok</button> -->
                    <button class="btn btn-button popupclase">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END DISCOUNT POPUP -->

    <!-- PRE ORDER POPUP -->
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Enter Date</h4>
                </div>
                <div class="modal-body">
                    <p>Enter Order Date</p>
                    <input type="text" id="pre-order" name="order_date" class="date_input form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" id="clear-date" class="btn btn-default">Clear Date</button>
                    <button type="button" id="add-date" class="btn btn-danger" data-dismiss="modal">Add Date</button>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <!-- PRE ORDER POPUP END -->






    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true
        });
    </script>

    <script src="{{ url('libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ url('libs/multi-select/js/jquery.multi-select.js') }}"></script>

    <script src="{{ asset('pos/js/cart.js?ver:1.9') }}"></script>
    <script src="{{ asset('js/split_order.js') }}"></script>
    <script src="{{ asset('js/idle.min.js') }}"></script>


    <script>
        /* Pre Order Start */
        $("#clear-date").click(function(){
            $("#pre-order").val("");
            $("#pre-order-label").text('Pre Order')
        });

        $("#add-date").click(function(){
            var orderDate = $("#pre-order").val();
            if(orderDate !== ""){
                $("#pre-order-label").text(orderDate);
                localStorage.setItem('pre-order-label',orderDate);
            }
        });
        /* Pre Order End */

        $("#customer_id").change(function(){
            var id = $(this).val();
            var csrf = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ url('get-customer-for-bill') }}",
                data: {id:id,_token:csrf},
                type: "post",
            }).done(function(e){
                $(".customer-info").show();
                $("#cus-bill-name").text(e.name);
                $('#cus-bill-phone').text(e.phone);
                $('#cus-bill-w_number').text(e.w_number);
                $('#cus-bill-address').text(e.address);
                $('#cus-bill-area').text(e.area);
                $('#cus-bill-street').text(e.street);
                $('#cus-bill-street-no').text(e.street_no);
                $('#cus-bill-block').text(e.block);
                $('#cus-bill-building').text(e.building);
                $('#cus-bill-flat').text(e.flat);
                $('#cus-bill-floor').text(e.floor);
            })
        });

        var csrf_token = '{{ csrf_token() }}';

        $('#bill-print').click(function (event) {

            var cus_id = localStorage.getItem('customer_id');
            var order_tp = localStorage.getItem('orderTypeName');
            if(order_tp != "Dine In"){
                if(cus_id > 0){
                    customerInfoShow(cus_id);
                }
            }

            //$("#create-hold-order").trigger('click');


            var orderTypeName = localStorage.getItem('orderTypeName');

            if ($('#table_id').val() == null && orderTypeName == "Dine In") {
                alert('Table Is Empty.');
            } else {

                displayCart();
                //$('.hidden-bill').css('display','none');

                var order_type_id = localStorage.getItem('orderTypeId');
                var method_id = localStorage.getItem('data_method_id');
                var split_bill_one_method_id = localStorage.getItem('data_split_bill_one_method_id');
                var split_bill_two_method_id = localStorage.getItem('data_split_bill_two_method_id');
                var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);
//                var get_order_id = localStorage.getItem('order_id')


                var data_method_name = localStorage.getItem('data_method_name');
                var data_split_bill_one_method_name = localStorage.getItem('data_split_bill_one_method_name');
                var data_split_bill_two_method_name = localStorage.getItem('data_split_bill_two_method_name');

                var tableName = localStorage.getItem('tableName');
                var orderTypeName = localStorage.getItem('orderTypeName');

                var person = $('#person').val();
                var payment_change = $('#payment-change').text();

                var table_id = $('#table_id').val();

                var tip_amount = 0.000;
                var delivery_charge = 0.000;
                var user_id = Number($('#curren-user').val());

                var customer_id = $('#customer_id').val();

                var method_note = $('#payment-method-note').val();
                var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
                var dis2 = Number(totalCartDiscount());
                var dis3 = Number($('#inputDiscount_2').val());
                var alldiscount = Number(dis1 + dis2 + dis3).toFixed(3);
                var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);

                var totalcart = Number(totalCart()).toFixed(3);


                var grandTotalValue = (Number(totalcart) + Number(tax1) - Number(alldiscount)).toFixed(3);
                grandTotalValue = isNaN(grandTotalValue) ? 0 : grandTotalValue;

                var input_current_amount = $("#input-current-amount").val();
                var newgrandTotal = (Number(totalcart) + Number(pay_one) + Number(pay_two) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);


                var d = new Date();
                var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                var dt = new Date();
                var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();


                var riportArray = listCart();
                var outputreport = '';


                // setting up csk id and saving it to local storage

                chk_id_generate();
                // var csk_id = localStorage.getItem('order_id');
                var csk_id = localStorage.getItem('hold_id');
                event.preventDefault();

                // if system is in online it will save data into database
                // else it will save order data in local storage
                var x = 1;
                for (var i in riportArray) {

                    //var ser = 1 + Number(i);
                    outputreport += "<tr style='border:transparent'>";
                    outputreport += "<td>";
                    if(riportArray[i].price > 0){
                        outputreport += x++
                    }
                    outputreport += "</td>";
                    outputreport += "<td>" + riportArray[i].name;
                    if(riportArray[i].price > 0){
                        outputreport += "<p>"+ riportArray[i].arabic_name +"</p>";
                    }
                    outputreport += "</td>";
                    outputreport += "<td>";
                    if(riportArray[i].price > 0){
                        outputreport += riportArray[i].count;
                    }
                    outputreport += "</td>";
                    outputreport += "<td>";
                    if(riportArray[i].price > 0){
                        outputreport += Number(riportArray[i].price).toFixed(3);
                    }
                    outputreport += "</td>";
                    outputreport += "<td>";
                    if(riportArray[i].price > 0){
                        outputreport += (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3);
                    }
                    outputreport += "</td>";
                    outputreport += "</tr>"

                }

                $('#show-bill-order_number').html(csk_id);
                var tableName = localStorage.getItem('tableName');
                var orderTypeName = localStorage.getItem('orderTypeName');

                if(orderTypeName != 'Dine In'){
                    $('#table-display').css('display','none');
                }else{
                    $('#table-display').css('display','');
                }

                $('#show-bill-order-type').html(orderTypeName);
                $('#show-bill-person').html(person);
                $('#show-bill-table-number').html(tableName);


                $('#show-bill-table_id').html(table_id);
                $('#show-bill-person').html(person);
                $('#show-bill-current-time').html(currentTime);
                $('#show-bill-current-date').html(currentDate);

                $('#show-bill-print').html(outputreport);

                $('#bill-discount').html(alldiscount);
                $('#bill-tax').html(tax1);
                $('#bill-grand-total').html(grandTotalValue);
                $("#bill-new-grand-total").html(newgrandTotal);
                $('#bill-sub-total').html(totalcart);

                $('#bill-changes-balance').html(payment_change);
                $('#bill-payment-type-amount').html(input_current_amount);
                $('#pay-one').html(pay_one);
                $('#pay-two').html(pay_two);
                $('#bill-method-name').html(data_method_name);
                $('#data_split_bill_one_method_name').html(data_split_bill_one_method_name);
                $('#data_split_bill_two_method_name').html(data_split_bill_two_method_name);

                if(pay_one > 0 || pay_two > 0){
                    $("#bill-method-name").closest('.free').remove();
                    $("#bill-payment-type-amount").closest('.text-right').remove();
                }else{
                    $("#data_split_bill_one_method_name").closest('.free').remove();
                    $("#pay-one").closest('.text-right').remove();
                    $("#data_split_bill_two_method_name").closest('.free').remove();
                    $("#pay-two").closest('.text-right').remove()
                }

                $(".poscat-list-page").css("display", "none");
                $(".header-area").css("display", "none");
                $(".invoice-page").css("display", "block");
                var p_count = 1;
                if(p_count == 1){
                    var clr_interval =  setInterval(function(){
                        p_count++;
                        if(p_count>4){
                            clearInterval(clr_interval);
                            $(".poscat-list-page").css("display", "block");
                            $(".header-area").css("display", "block");
                            $(".invoice-page").css("display", "none");
                            $(".Popupmodel").css("display", "none");
                            getOrderyTypeData(strogOrderData);
                            getOrderIdFn();
                        }else{
                            $(".poscat-list-page").css("display", "none");
                            $(".header-area").css("display", "none");
                            $(".invoice-page").css("display", "block");

                            print();
                        }

                    },100);
                }else{
                    $(".poscat-list-page").css("display", "block");
                    $(".header-area").css("display", "block");
                    $(".invoice-page").css("display", "none");
                    $(".Popupmodel").css("display", "none");

                    getOrderyTypeData(strogOrderData);
                    getOrderIdFn();
                }
            }

            getCloseCheck();

            // Add order in open check while printing
            var is_open_check = localStorage.getItem('is_open_check'); // check if it is from open check
            console.log(is_open_check);
            if(is_open_check === 'null'){
                $("#create-hold-order").trigger('click'); // don't save if from existing open check
            }
            localStorage.setItem('is_open_check',null); // clear open check query status


        });


        $('#confirm-payment').click(function (event) {

            var hold_active = Number($('#action_hold').val());

            if(hold_active != '' ){
                var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
                for(i in allHoldCart){
                    if(allHoldCart[i].csk_id == hold_active){
                        allHoldCart.splice(i,1);
                        localStorage.setItem('all_hold_orders', JSON.stringify(allHoldCart));
                        $('#action_hold').val('');
                        displayHoldCart();
                        break;
                    }
                }
            }


            if ($('#cmp').is(":checked")){
                var cmp = 1;
                $('#cmp').attr('checked', false);
            }else{
                var cmp = 0;
            }

            var cus_id = localStorage.getItem('customer_id');
            var order_tp = localStorage.getItem('orderTypeName');
            if(order_tp != "Dine In"){
                if(cus_id > 0){
                    customerInfoShow(cus_id);
                }
            }

            var is_hold = $('#is_hold').val();
            var is_close =  $('#close-re-order-type').val();
            if(is_close == 'edit'){
                /*close Order Start */

                var close_arr = JSON.parse(localStorage.getItem('close_orders'));
                var closeIndex = $('#close-index-number').val();


                for (i in close_arr) {
                    if (i == closeIndex) {

                        var riportArray = (close_arr[i].cart);
                        var outputreport = '';
                        console.log(close_arr[i]);


                        var data_method_title = localStorage.getItem('data_method_title');
                        var order_type_id = localStorage.getItem('orderTypeId');
                        var method_id = localStorage.getItem('data_method_id');
                        var split_bill_one_method_id = localStorage.getItem('data_split_bill_one_method_id');
                        var split_bill_two_method_id = localStorage.getItem('data_split_bill_two_method_id');
                        var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                        var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);
                        var get_order_id = localStorage.getItem('order_id');
                        var data_method_name = localStorage.getItem('data_method_name');
                        var data_split_bill_one_method_name = localStorage.getItem('data_split_bill_one_method_name');
                        var data_split_bill_two_method_name = localStorage.getItem('data_split_bill_two_method_name');
                        var pre_order = localStorage.getItem('pre-order-label');

                        var person = $('#person').val();
                        var payment_change = $('#payment-change').text();

                        var tip_amount = Number($('#tip-amount-input').val()).toFixed(3);
                        var delivery_charge = Number($('#delivery-charge-input').val()).toFixed(3);
                        delivery_charge = isNaN(delivery_charge)?0:delivery_charge;
                        var user_id = Number($('#curren-user').val());
                        var customer_id = $('#customer_id').val();


                        var table_id = $('#table_id').val();
                        var method_note = $('#payment-method-note').val();

                        var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
                        var dis2 = Number(totalCartDiscount());

                        var dis3 = Number($('#inputDiscount_2').val());
                        var alldiscount = Number(dis1 + dis2 + dis3).toFixed(3);
                        var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);

                        var totalcart = Number(totalCart()).toFixed(3);


                        var grandTotalValue = (Number(totalcart) + Number(tax1) - Number(alldiscount)).toFixed(3);
                        grandTotalValue = isNaN(grandTotalValue) ? 0 : grandTotalValue;

                        var input_current_amount = $("#input-current-amount").val();
                        var newgrandTotal = (Number(totalcart) + Number(pay_one) + Number(pay_two) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);

                        console.log('new grand total '+newgrandTotal);

                        var d = new Date();
                        var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                        var dt = new Date();
                        var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                        var riportArray = listCart();

                        console.log(riportArray);

                        var outputreport = '';


                        event.preventDefault();


                        order_data = {
                            table_id: table_id,
                            order_type_id: order_type_id,
                            person: person,
                            method_note: method_note,
                            method_id: method_id,
                            split_bill_one_method_id: split_bill_one_method_id,
                            split_bill_two_method_id: split_bill_two_method_id,
                            discount: alldiscount,
                            taxs: tax1,
                            total: grandTotalValue,
                            paid: grandTotalValue,
                            sub_total: totalcart,
                            tips: tip_amount,
                            delivery: isNaN(delivery_charge)?0:delivery_charge,
                            pre_order: pre_order,
                            cmp: cmp,
                            user_id: user_id,
                            customer_id: customer_id,

                            _token: '{{ csrf_token() }}'
                        };


                        $.ajax({
                            type: "post",
                            url: "{{ url('/update_resource/') }}/"+close_arr[i].csk_id,
                            dataType: "json",
                            data: order_data,
                            success: function (data) {

                                console.log(data.csk_id);
                                toastr.success('Closed order Successfully Updated.', 'Success Alert', {timeOut: 1000});

                                for (i = 0; i < riportArray.length; i++) {
                                    var csk_id = data.csk_id;
                                    var item_id = riportArray[i].id;
                                    var item_price = riportArray[i].price;
                                    var qty = riportArray[i].count;
                                    var total = item_price * qty;
                                    var discount = riportArray[i].discount;
                                    var created_at = data.created_at;
                                    var updated_at = data.updated_at;

                                    formdata = {
                                        csk_id: csk_id,
                                        item_id: item_id,
                                        item_price: item_price,
                                        qty: qty,
                                        total: total,
                                        discount: discount,
                                        created_at: created_at,
                                        updated_at: updated_at,
                                        _token: '{{ csrf_token() }}'
                                    };

                                    console.log('formdata data');
                                    console.log(formdata);


                                    $.ajax({
                                        dataType: 'json',
                                        type: 'post',
                                        url: '{{route('update.product.order')}}',
                                        data: formdata

                                    }).done(function (data) {
                                        console.log(data);
                                    });

                                }

                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
//var x = 1;
                        for (var i in riportArray) {

//                            var ser = 1 + Number(i);
//                            outputreport += "<tr>";
//                            outputreport += "<td>" + ser + "</td>";
//                            outputreport += "<td>" + riportArray[i].name + "<p>"+ riportArray[i].arabic_name +"</p></td>";
//                            outputreport += "<td>" + riportArray[i].count + "</td>";
//                            outputreport += "<td>" + Number(riportArray[i].price).toFixed(3) + "</td>";
//                            outputreport += "<td>" + (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3) + "</td>";
//                            outputreport += "</tr>"
                            outputreport += "<tr>";
                            outputreport += "<td>";
                            if(riportArray[i].price > 0){
                                outputreport += x++
                            }
                            outputreport += "</td>";
                            outputreport += "<td>" + riportArray[i].name;
                            if(riportArray[i].price > 0){
                                outputreport += "<p>"+ riportArray[i].arabic_name +"</p>";
                            }
                            outputreport += "</td>";
                            outputreport += "<td>";
                            if(riportArray[i].price > 0){
                                outputreport += riportArray[i].count;
                            }
                            outputreport += "</td>";
                            outputreport += "<td>";
                            if(riportArray[i].price > 0){
                                outputreport += Number(riportArray[i].price).toFixed(3);
                            }
                            outputreport += "</td>";
                            outputreport += "<td>";
                            if(riportArray[i].price > 0){
                                outputreport += (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3);
                            }
                            outputreport += "</td>";
                            outputreport += "</tr>"
                        }

                        var tableName = localStorage.getItem('tableName');
                        var orderTypeName = localStorage.getItem('orderTypeName');
                        var order_id = localStorage.getItem('order_id');

                        if(orderTypeName != 'Dine In'){
                            $('#table-display').css('display','none');
                        }else{
                            $('#table-display').css('display','');
                        }


                        $('#show-bill-order-type').html(orderTypeName);

                        $('#show-bill-table-number').html(tableName);


                        $('#show-bill-order_number').html(csk_id);
                        $('#show-bill-table_id').html(table_id);
                        $('#show-bill-person').html(person);
                        $('#show-bill-current-time').html(currentTime);
                        $('#show-bill-current-date').html(currentDate);

                        $('#show-bill-print').html(outputreport);

                        $('#bill-discount').html(alldiscount);
                        $('#bill-tax').html(tax1);
                        $('#bill-grand-total').html(grandTotalValue);
                        $("#bill-new-grand-total").html(newgrandTotal);

                        $('#bill-sub-total').html(totalcart);

                        $('#bill-changes-balance').html(payment_change);
                        $('#bill-payment-type-amount').html(input_current_amount);
                        $('#pay-one').html(pay_one);
                        $('#pay-two').html(pay_two);
                        $('#bill-method-name').html(data_method_name);
                        $('#data_split_bill_one_method_name').html(data_split_bill_one_method_name);
                        $('#data_split_bill_two_method_name').html(data_split_bill_two_method_name);
                        $('#bill-tips-amount').html(tip_amount);
                        $("#bill-delivery-charge").html(isNaN(delivery_charge)?0:delivery_charge);

                        if(pay_one > 0 || pay_two > 0){
                            $("#bill-method-name").closest('.free').remove();
                            $("#bill-payment-type-amount").closest('.text-right').remove();
                        }else{
                            $("#data_split_bill_one_method_name").closest('.free').remove();
                            $("#pay-one").closest('.text-right').remove();
                            $("#data_split_bill_two_method_name").closest('.free').remove();
                            $("#pay-two").closest('.text-right').remove()
                        }


                        if (method_note != '') {
                            var bill_note_output = '';

                            bill_note_output += "<li>";
                            bill_note_output += "<div class='free'>";
                            bill_note_output += "<strong><span id=''>" + data_method_title + "</span> </strong>";
                            bill_note_output += "</div>";
                            bill_note_output += "<div class='text-right'>";
                            bill_note_output += "<span class='text-right'><span id=''>" + method_note + "</span></span>";
                            bill_note_output += "</div>";
                            bill_note_output += "</li>";

                            $('#bill-show-method-note').html(bill_note_output);
                        }


                        $("#popUp").fadeOut(1000);
                        $(".Popupmodel").css("display", "none");


                        $(".poscat-list-page").css("display", "none");
                        $(".header-area").css("display", "none");
                        $(".invoice-page").css("display", "block");
                        print();
                        $(".poscat-list-page").css("display", "block");


                        $(".header-area").css("display", "block");
                        $(".invoice-page").css("display", "none");

                        clearCart();

                        $('#payment-method-note').val('');
                        $('#input-current-amount').val('');
                        $('#tip-amount-input').val('');
                        $('#delivery-charge-input').val('');
                        $('#inputDiscount').val('');
                        $('#input-discount').html('0.000');
                        $('#close-re-order-type').val('');
                        getOrderyTypeData(strogOrderData);

                        getCloseCheck();

                    }
                }

                /*close Order End */
            }else{

                if (is_hold == 1) {

                    var data_method_title = localStorage.getItem('data_method_title');
                    var order_type_id = localStorage.getItem('orderTypeId');
                    var method_id = localStorage.getItem('data_method_id');
                    var split_bill_one_method_id = localStorage.getItem('data_split_bill_one_method_id');
                    var split_bill_two_method_id = localStorage.getItem('data_split_bill_two_method_id');
                    var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                    var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);
                    var get_order_id = localStorage.getItem('order_id');
                    var data_method_name = localStorage.getItem('data_method_name');
                    var data_split_bill_one_method_name = localStorage.getItem('data_split_bill_one_method_name');
                    var data_split_bill_two_method_name = localStorage.getItem('data_split_bill_two_method_name');

                    var payment_change = $('#payment-change').text();

                    var tip_amount = Number($('#tip-amount-input').val()).toFixed(3);
                    var delivery_charge = Number($('#delivery-charge-input').val()).toFixed(3);
                    delivery_charge = isNaN(delivery_charge)?0:delivery_charge;
                    var user_id = Number($('#curren-user').val());

                    var method_note = $('#payment-method-note').val();

                    var d = new Date();
                    var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                    var dt = new Date();
                    var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                    var outputreport = '';

                    chk_id_generate();
                    var csk_id = localStorage.getItem('order_id');

                    var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
                    var holdIndexNumber = Number($('#hold_index').val());

                    console.log(holdIndexNumber);

                    for (i in allHoldCart) {
                        if (i == holdIndexNumber) {

                            var riportArray = (allHoldCart[i].holdCartItem);
                            var outputreport = '';
                            var x = 1;
                            for (var p in riportArray) {
//                                var ser = 1 + Number(p);
//                                outputreport += "<tr>";
//                                outputreport += "<td>" + ser + "</td>";
//                                outputreport += "<td>" + riportArray[p].name + "<p>"+ riportArray[p].arabic_name +"</p></td>";
//                                outputreport += "<td>" + riportArray[p].count + "</td>";
//                                outputreport += "<td>" + Number(riportArray[p].price).toFixed(3) + "</td>";
//                                outputreport += "<td>" + (Number(riportArray[p].price) * Number(riportArray[p].count)).toFixed(3) + "</td>";
//                                outputreport += "</tr>"
                                outputreport += "<tr>";
                                outputreport += "<td>";
                                if(riportArray[p].price > 0){
                                    outputreport += x++
                                }
                                outputreport += "</td>";
                                outputreport += "<td>" + riportArray[p].name;
                                if(riportArray[p].price > 0){
                                    outputreport += "<p>"+ riportArray[i].arabic_name +"</p>";
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[p].price > 0){
                                    outputreport += riportArray[p].count;
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[p].price > 0){
                                    outputreport += Number(riportArray[i].price).toFixed(3);
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[i].price > 0){
                                    outputreport += (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3);
                                }
                                outputreport += "</td>";
                                outputreport += "</tr>"
                            }

                            var person = allHoldCart[i].person;
                            var alldiscount = allHoldCart[i].alldiscount;
                            var tax1 = allHoldCart[i].totalTax;
                            var tableName = allHoldCart[i].table_name;
                            var orderTypeName = allHoldCart[i].holdName;
                            var grandTotalValue = allHoldCart[i].grandTotal;
                            grandTotalValue = isNaN(grandTotalValue) ? 0 : grandTotalValue;
                            var paid = $("#input-current-amount").val();
                            var totalcart = allHoldCart[i].subTotal;
                            var table_id = allHoldCart[i].table_id;
                            var order_type_id = allHoldCart[i].order_type_id;
                            var customer_id = allHoldCart[i].customer_id;
                            var pre_order = $("#pre-order-label").val();
                            // var data_method_name = localStorage.getItem('data_method_name');

                            var order_id = localStorage.getItem('order_id');

                            event.preventDefault();

                            if (checkNetConnection() === true && navigator.onLine) {
                                order_data = {
                                    table_id: table_id,
                                    order_type_id: order_type_id,
                                    person: person,
                                    method_note: method_note,
                                    method_id: method_id,
                                    split_bill_one_method_id: split_bill_one_method_id,
                                    split_bill_two_method_id: split_bill_two_method_id,
                                    discount: alldiscount,
                                    taxs: tax1,
                                    total: grandTotalValue,
                                    paid: paid,
                                    pay_one: pay_one,
                                    pay_two: pay_two,
                                    sub_total: totalcart,

                                    tips: tip_amount,
                                    delivery: isNaN(delivery_charge)?0:delivery_charge,
                                    cmp: cmp,
                                    user_id: user_id,
                                    customer_id: customer_id,

                                    csk_id: csk_id,
                                    _token: '{{ csrf_token() }}'
                                };

                                $.ajax({
                                    type: "post",
                                    url: "{{ url('/save_resource') }}",
                                    dataType: "json",
                                    data: order_data,
                                    success: function (data) {

                                        localStorage.clear();

                                        toastr.success('Order Successfully Created.', 'Success Alert', {timeOut: 1000});

                                        for (i = 0; i < riportArray.length; i++) {
                                            var csk_id = localStorage.getItem('order_id');
                                            var item_id = riportArray[i].id;
                                            var item_price = riportArray[i].price;
                                            var qty = riportArray[i].count;
                                            var total = item_price * qty;
                                            var discount = riportArray[i].discount;

                                            formdata = {
                                                csk_id: csk_id,
                                                item_id: item_id,
                                                item_price: item_price,
                                                qty: qty,
                                                total: total,
                                                discount: discount,
                                                _token: '{{ csrf_token() }}'
                                            };

                                            console.log(formdata);

                                            $.ajax({
                                                dataType: 'json',
                                                type: 'post',
                                                url: '{{route('store.product.order')}}',
                                                data: formdata

                                            }).done(function (data) {
                                                console.log(data);
                                            });

                                        }

                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }

                            else {

                                if (localStorage.getItem('local_orders_with_cart') === null) {
                                    var local_orders_with_cart = [];
                                    localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));
                                }
                                else {
                                    local_orders_with_cart = JSON.parse(localStorage.getItem('local_orders_with_cart'));
                                }

                                console.log('order data is saving in local storage...');
                                console.log(local_orders_with_cart);

                                order_data = {
                                    table_id: table_id,
                                    pre_order: pre_order,
                                    order_type_id: order_type_id,
                                    person: person,
                                    method_note: method_note,
                                    method_id: method_id,
                                    split_bill_one_method_id: split_bill_one_method_id,
                                    split_bill_two_method_id: split_bill_two_method_id,
                                    discount: alldiscount,
                                    taxs: tax1,
                                    total: grandTotalValue,
                                    paid: grandTotalValue,
                                    sub_total: totalcart,
                                    tips: tip_amount,
                                    delivery: isNaN(delivery_charge)?0:delivery_charge,
                                    cmp: cmp,
                                    user_id: user_id,
                                    customer_id: customer_id,
                                    csk_id: csk_id
                                };


                                var carts = riportArray;

                                var HoldCart = [];

                                for (i = 0; i < carts.length; i++) {
                                    var item_id = carts[i].id;
                                    var item_price = carts[i].price;
                                    var qty = carts[i].count;
                                    var total = item_price * qty;
                                    var discount = carts[i].discount;

                                    CartData = {
                                        item_id: item_id,
                                        item_price: item_price,
                                        qty: qty,
                                        total: total,
                                        discount: discount,
                                        csk_id: csk_id
                                    };

                                    HoldCart.push(CartData);
                                }


                                var Hold_Order_Cart = {BackupOrders: order_data, BackupCarts: HoldCart};

                                local_orders_with_cart.push(Hold_Order_Cart);

                                localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));

                                toastr.success('Offline Order Successfully Created.', 'Success Alert', {timeOut: 1000});

//                    console.log(JSON.parse(localStorage.getItem('local_orders_with_cart')));

                            }

                            if(orderTypeName != 'Dine In'){
                                $('#table-display').css('display','none');
                            }else{
                                $('#table-display').css('display','');
                            }


                            $('#show-bill-order-type').html(orderTypeName);
                            $('#show-bill-table-number').html(tableName);


                            $('#show-bill-order_number').html(csk_id);
                            // $('#show-bill-table_id').html(table_id);
                            $('#show-bill-person').html(person);
                            $('#show-bill-current-time').html(currentTime);
                            $('#show-bill-current-date').html(currentDate);

                            $('#show-bill-print').html(outputreport);

                            $('#bill-discount').html(alldiscount);
                            $('#bill-tax').html(tax1);
                            $('#bill-grand-total').html(grandTotalValue);
                            $("#bill-new-grand-total").html(newgrandTotal);
                            $('#bill-sub-total').html(totalcart);

                            $('#bill-changes-balance').html(payment_change);
                            $('#bill-payment-type-amount').html(input_current_amount);
                            $('#pay-one').html(pay_one);
                            $('#pay-two').html(pay_two);
                            $("#bill-new-grand-total").html(newgrandTotal);

                            $('#bill-method-name').html(data_method_name);
                            $('#data_split_bill_one_method_name').html(data_split_bill_one_method_name);
                            $('#data_split_bill_two_method_name').html(data_split_bill_two_method_name);
                            $('#bill-tips-amount').html(tip_amount);
                            $("#bill-delivery-charge").html(isNaN(delivery_charge)?0:delivery_charge);

                            if(pay_one > 0 || pay_two > 0){
                                $("#bill-method-name").closest('.free').remove();
                                $("#bill-payment-type-amount").closest('text-right').remove();
                            }else{
                                $("#data_split_bill_one_method_name").closest('.free').remove();
                                $("#pay-one").closest('.text-right').remove();
                                $("#data_split_bill_two_method_name").closest('.free').remove();
                                $("#pay-two").closest('.text-right').remove()
                            }

                            if (method_note != '') {
                                var bill_note_output = '';

                                bill_note_output += "<li>";
                                bill_note_output += "<div class='free'>";
                                bill_note_output += "<strong><span id=''>" + data_method_title + "</span> </strong>";
                                bill_note_output += "</div>";
                                bill_note_output += "<div class='text-right'>";
                                bill_note_output += "<span class='text-right'><span id=''>" + method_note + "</span></span>";
                                bill_note_output += "</div>";
                                bill_note_output += "</li>";

                                $('#bill-show-method-note').html(bill_note_output);
                            }


                            $("#popUp").fadeOut(1000);
                            $(".Popupmodel").css("display", "none");


                            $(".poscat-list-page").css("display", "none");
                            $(".header-area").css("display", "none");
                            $(".invoice-page").css("display", "block");
                            print();
                            $(".poscat-list-page").css("display", "block");


                            $(".header-area").css("display", "block");
                            $(".invoice-page").css("display", "none");

                            clearCart();

                            $('#payment-method-note').val('');
                            $('#input-current-amount').val('');
                            $('#tip-amount-input').val('');
                            $('#delivery-charge-input').val('');
                            $('#inputDiscount').val('');
                            $('#inputDiscount_2').val('');
                            $('#input-discount').html('0.000');
                            getOrderyTypeData(strogOrderData);

                            for (i in allHoldCart) {
                                if (i == holdIndexNumber) {
                                    allHoldCart.splice(i, 1);
                                    localStorage.setItem('all_hold_orders', JSON.stringify(allHoldCart));
                                    displayHoldCart();
                                    break;
                                }

                            }


                        }
                    }


                    /*end hold....*/


                } else {

                    var orderTypeName = localStorage.getItem('orderTypeName');
                    if (cart.length == 0) {
                        alert('Item Is Empty.');
                    } else {
                        if ($('#table_id').val() == null && orderTypeName == "Dine In") {
                            alert('Table Is Empty.');
                        } else {

                            var data_method_title = localStorage.getItem('data_method_title');
                            var order_type_id = localStorage.getItem('orderTypeId');
                            var method_id = localStorage.getItem('data_method_id');
                            var split_bill_one_method_id = localStorage.getItem('data_split_bill_one_method_id');
                            var split_bill_two_method_id = localStorage.getItem('data_split_bill_two_method_id');
                            var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                            var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);
                            var get_order_id = localStorage.getItem('order_id');
                            var data_method_name = localStorage.getItem('data_method_name');
                            var data_split_bill_one_method_name = localStorage.getItem('data_split_bill_one_method_name');
                            var data_split_bill_two_method_name = localStorage.getItem('data_split_bill_two_method_name');
                            var pre_order = localStorage.getItem('pre-order-label');

                            var input_current_amount = $("#input-current-amount").val();
                            var person = $('#person').val();
                            var payment_change = $('#payment-change').text();

                            var tip_amount = Number($('#tip-amount-input').val()).toFixed(3);
                            var delivery_charge = Number($('#delivery-charge-input').val()).toFixed(3);
                            delivery_charge = isNaN(delivery_charge)?0:delivery_charge;
                            var discount = $("#inputDiscount").val();
                            var user_id = Number($('#curren-user').val());
                            var customer_id = $('#customer_id').val();


                            var table_id = $('#table_id').val();
                            var method_note = $('#payment-method-note').val();

                            var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
                            var dis2 = Number(totalCartDiscount());
                            var dis3 = Number($('#inputDiscount_2').val());
                            var alldiscount = Number(dis1 + dis2 + dis3).toFixed(3);
                            var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);

                            var totalcart = Number(totalCart()).toFixed(3);


                            var grandTotalValue = (Number(totalcart) + Number(tax1) - Number(alldiscount)).toFixed(3);
                            //grandTotalValue = Number(pay_one) > 0 ? 0 : grandTotalValue;

                            var paid = $("#input-current-amount").val();

                            var input_current_amount = $("#input-current-amount").val();
                            var newgrandTotal = (Number(totalcart) + Number(pay_one) + Number(pay_two) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);

                            var d = new Date();
                            var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                            var dt = new Date();
                            var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();


                            var riportArray = listCart();

                            console.log(riportArray);

                            var outputreport = '';


                            chk_id_generate();

                            var csk_id = localStorage.getItem('order_id');

                            event.preventDefault();

                            // setting up csk id and saving it to local storage
                            var timestamp = '' + Date.now() + '';
                            var csk = timestamp.slice(6);


                            if (checkNetConnection() === true && navigator.onLine) {
                                order_data = {
                                    pre_order: pre_order,
                                    table_id: table_id,
                                    order_type_id: order_type_id,
                                    person: person,
                                    method_note: method_note,
                                    method_id: method_id,
                                    split_bill_one_method_id: split_bill_one_method_id,
                                    split_bill_two_method_id: split_bill_two_method_id,
                                    discount: alldiscount,
                                    taxs: tax1,
                                    total: grandTotalValue,
                                    paid: paid,
                                    pay_one: pay_one,
                                    pay_two: pay_two,
                                    sub_total: totalcart,
                                    tips: tip_amount,
                                    delivery: isNaN(delivery_charge)?0:delivery_charge,
                                    cmp: cmp,
                                    user_id: user_id,
                                    customer_id: customer_id,
                                    csk_id: csk_id,
                                    _token: '{{ csrf_token() }}'
                                };

                                $.ajax({
                                    type: "post",
                                    url: "{{ url('/save_resource') }}",
                                    dataType: "json",
                                    data: order_data,
                                    success: function (data) {

                                        localStorage.clear();

                                        toastr.success('Order Successfully Created.', 'Success Alert', {timeOut: 1000});

                                        for (i = 0; i < riportArray.length; i++) {
                                            var csk_id = localStorage.getItem('order_id');
                                            var item_id = isNaN(riportArray[i].id) ? null : riportArray[i].id;
                                            var item_price = riportArray[i].price;
                                            var qty = riportArray[i].count;
                                            var total = item_price * qty;
                                            var discount = riportArray[i].discount;

                                            formdata = {
                                                csk_id: csk_id,
                                                item_id: item_id,
                                                item_price: item_price,
                                                qty: qty,
                                                total: total,
                                                discount: discount,
                                                _token: '{{ csrf_token() }}'
                                            };

                                            console.log(formdata);


                                            $.ajax({
                                                dataType: 'json',
                                                type: 'post',
                                                url: '{{route('store.product.order')}}',
                                                data: formdata

                                            }).done(function (data) {
                                                console.log(data);
                                            });

                                        }

                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                            }
                            else {

                                if (localStorage.getItem('local_orders_with_cart') === null) {
                                    var local_orders_with_cart = [];
                                    localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));
                                }
                                else {
                                    local_orders_with_cart = JSON.parse(localStorage.getItem('local_orders_with_cart'));
                                }

                                console.log('order data is saving in local storage...');
                                console.log(local_orders_with_cart);

                                order_data = {
                                    table_id: table_id,
                                    order_type_id: order_type_id,
                                    person: person,
                                    method_note: method_note,
                                    method_id: method_id,
                                    split_bill_one_method_id: split_bill_one_method_id,
                                    split_bill_two_method_id: split_bill_two_method_id,
                                    discount: alldiscount,
                                    taxs: tax1,
                                    total: grandTotalValue,
                                    paid: grandTotalValue,
                                    sub_total: totalcart,
                                    tips: tip_amount,
                                    delivery: isNaN(delivery_charge)?0:delivery_charge,
                                    cmp: cmp,
                                    user_id: user_id,
                                    customer_id: customer_id,
                                    csk_id: csk_id
                                };

                                var carts = listCart();

                                var HoldCart = [];

                                for (i = 0; i < carts.length; i++) {
                                    var item_id = carts[i].id;
                                    var item_price = carts[i].price;
                                    var qty = carts[i].count;
                                    var total = item_price * qty;
                                    var discount = carts[i].discount;

                                    CartData = {
                                        item_id: item_id,
                                        item_price: item_price,
                                        qty: qty,
                                        total: total,
                                        discount: discount,
                                        csk_id: csk_id
                                    };

                                    HoldCart.push(CartData);
                                }


                                var Hold_Order_Cart = {BackupOrders: order_data, BackupCarts: HoldCart};

                                local_orders_with_cart.push(Hold_Order_Cart);

                                localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));

                                toastr.success('Offline Order Successfully Created.', 'Success Alert', {timeOut: 1000});

//                    console.log(JSON.parse(localStorage.getItem('local_orders_with_cart')));

                            }

                            var x = 1;
                            for (var i in riportArray) {

//                                var ser = 1 + Number(i);
//                                outputreport += "<tr>";
//                                outputreport += "<td>" + ser + "</td>";
//                                outputreport += "<td>" + riportArray[i].name + "<p>"+ riportArray[i].arabic_name +"</p></td>";
//                                outputreport += "<td>" + riportArray[i].count + "</td>";
//                                outputreport += "<td>" + Number(riportArray[i].price).toFixed(3) + "</td>";
//                                outputreport += "<td>" + (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3) + "</td>";
//                                outputreport += "</tr>"
                                outputreport += "<tr>";
                                outputreport += "<td>";
                                if(riportArray[i].price > 0){
                                    outputreport += x++
                                }
                                outputreport += "</td>";
                                outputreport += "<td>" + riportArray[i].name;
                                if(riportArray[i].price > 0){
                                    outputreport += "<p>"+ riportArray[i].arabic_name +"</p>";
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[i].price > 0){
                                    outputreport += riportArray[i].count;
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[i].price > 0){
                                    outputreport += Number(riportArray[i].price).toFixed(3);
                                }
                                outputreport += "</td>";
                                outputreport += "<td>";
                                if(riportArray[i].price > 0){
                                    outputreport += (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3);
                                }
                                outputreport += "</td>";
                                outputreport += "</tr>"
                            }

                            var tableName = localStorage.getItem('tableName');
                            var orderTypeName = localStorage.getItem('orderTypeName');
                            var order_id = localStorage.getItem('order_id');

                            if(orderTypeName != 'Dine In'){
                                $('#table-display').css('display','none');
                            }else{
                                $('#table-display').css('display','');
                            }


                            $('#show-bill-order-type').html(orderTypeName);

                            $('#show-bill-table-number').html(tableName);


                            $('#show-bill-order_number').html(csk_id);
                            $('#show-bill-table_id').html(table_id);
                            $('#show-bill-person').html(person);
                            $('#show-bill-current-time').html(currentTime);
                            $('#show-bill-current-date').html(currentDate);

                            $('#show-bill-print').html(outputreport);

                            $('#bill-discount').html(alldiscount);
                            $('#bill-tax').html(tax1);
                            $('#bill-grand-total').html(grandTotalValue);
                            $("#bill-new-grand-total").html(newgrandTotal);
                            $('#bill-sub-total').html(totalcart);

                            $('#bill-changes-balance').html(payment_change);
                            $('#bill-payment-type-amount').html(input_current_amount);
                            $('#pay-one').html(pay_one);
                            $('#pay-two').html(pay_two);
                            $('#bill-method-name').html(data_method_name);
                            $('#data_split_bill_one_method_name').html(data_split_bill_one_method_name);
                            $('#data_split_bill_two_method_name').html(data_split_bill_two_method_name);
                            $('#bill-tips-amount').html(tip_amount);
                            $('#bill-delivery-charge').html(isNaN(delivery_charge)?0:delivery_charge);

                            if(pay_one > 0 || pay_two > 0){
                                $("#bill-method-name").closest('.free').remove();
                                $("#bill-payment-type-amount").closest('.text-right').remove();
                            }else{
                                $("#data_split_bill_one_method_name").closest('.free').remove();
                                $("#pay-one").closest('.text-right').remove();
                                $("#data_split_bill_two_method_name").closest('.free').remove();
                                $("#pay-two").closest('.text-right').remove()
                            }

                            if (method_note != '') {
                                var bill_note_output = '';

                                bill_note_output += "<li>";
                                bill_note_output += "<div class='free'>";
                                bill_note_output += "<strong><span id=''>" + data_method_title + "</span> </strong>";
                                bill_note_output += "</div>";
                                bill_note_output += "<div class='text-right'>";
                                bill_note_output += "<span class='text-right'><span id=''>" + method_note + "</span></span>";
                                bill_note_output += "</div>";
                                bill_note_output += "</li>";

                                $('#bill-show-method-note').html(bill_note_output);
                            }


                            $("#popUp").fadeOut(1000);
                            $(".Popupmodel").css("display", "none");


                            $(".poscat-list-page").css("display", "none");
                            $(".header-area").css("display", "none");
                            $(".invoice-page").css("display", "block");
                            print();
                            $(".poscat-list-page").css("display", "block");


                            $(".header-area").css("display", "block");
                            $(".invoice-page").css("display", "none");

                            clearCart();

                            $('#payment-method-note').val('');
                            $('#input-current-amount').val('');
                            $('#tip-amount-input').val('');
                            $('#delivery-charge-input').val('');
                            $('#inputDiscount').val('');
                            $('#inputDiscount_2').val('');
                            $('#input-discount').html('0.000');
                            getOrderyTypeData(strogOrderData);


                        }
                    }

                }
                getCloseCheck();
            }

            getOrderIdFn();
            $("#pre-order").val(''); //clear pre-order field
            $("#pre-order-label").text('Pre Order') //reset pre order label

        });


        /* Create new Post */

        $("#store-customer").click(function (e) {

            e.preventDefault();

            var form_action = $("#create-customer").find("form").attr("action");

            var name = $("#customer-name").val();
            var phone = $("#customer-phone").val();
            var email = $("#customer-email").val();
            var street = $("#street").val();
            var street_no = $("#street_no").val();
            var block = $("#block").val();
            var building = $("#building").val();
            var floor = $("#floor").val();
            var flat = $("#flat").val();
            var area = $("#area").val();
            var w_number = $("#w_number").val();
            var is_active = $("#customer-status").val();

            var address = "Area: " +area+ "Street Name: "+street+", Street No: " + street_no +", Block: " + block + ", Building: " + building + ", Floor: " + floor + ", Flat: " + flat;


            $.ajax({
                dataType: 'json',
                type: 'post',
                url: form_action,
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    street: street,
                    street_no: street_no,
                    block: block,
                    building: building,
                    floor: floor,
                    flat: flat,
                    area: area,
                    address: address,
                    w_number: w_number,
                    is_active: is_active,
                    _token: '{{ csrf_token() }}'
                }

            }).done(function (data) {
                $("#customerForm")[0].reset();
                $("#create-customer").fadeOut(500).css('display', 'none');
                toastr.success('Customer Added Successfully.', 'Success Alert', {timeOut: 1000});
                displayCustomer(data.customers);
                getCustomer();

            }).fail(function (error) {
                $("#create-customer").fadeOut(1000).css('display', 'none');
            });

        });


        /*split confirm order*/

        $('#confirm-payment-split').click(function (event) {
            if ($('#cmp').is(":checked")){
                var cmp = 1;

            }else{
                var cmp = 0;
            }

            var cus_id = localStorage.getItem('customer_id');
            var order_tp = localStorage.getItem('orderTypeName');
            if(order_tp != "Dine In"){
                if(cus_id >0){
                    customerInfoShow(cus_id);
                }
            }


            var orderTypeName = localStorage.getItem('orderTypeName');
            if(orderTypeName != 'Dine In'){
                $('#table-display').css('display','none');
            }else{
                $('#table-display').css('display','');
            }

            var phone = "Phone : 22531053/97639030";
            var email = "Email : services@maharajapalacekwt.com";
            $('#show-bill-phone').html(phone);
            $('#show-bill-email').html(email);

            var split_arr_type = $('#split-array-type').val();
            var left_arr = JSON.parse(localStorage.getItem('split_arr'));

            if (split_arr_type == 'left') {
                var left_arr = JSON.parse(localStorage.getItem('split_arr'));
                var riportArray = left_arr;
                var csk_number = $('#left-chk').text();
                var csk_id = Number($('#left-chk-value').val());

            }
            if (split_arr_type == 'right') {
                var riportArray = split_arr_2;

                var csk_number = $('#right-chk').text();
                var csk_id = Number($('#right-chk-value').val());

            }


            if (cart.length == 0) {
                alert('Item Is Empty.');
            } else {
                if ($('#table_id').val() == null && orderTypeName == "Dine In") {
                    alert('Table Is Empty.');
                } else {


                    var data_method_title = localStorage.getItem('data_method_title');
                    var order_type_id = localStorage.getItem('orderTypeId');
                    var method_id = localStorage.getItem('data_method_id');
                    var split_bill_one_method_id = localStorage.getItem('data_split_bill_one_method_id');
                    var split_bill_two_method_id = localStorage.getItem('data_split_bill_two_method_id');
                    var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                    var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);
                    var get_order_id = localStorage.getItem('order_id');
                    var data_method_name = localStorage.getItem('data_method_name');
                    var data_split_bill_one_method_name = localStorage.getItem('data_split_bill_one_method_name');
                    var data_split_bill_two_method_name = localStorage.getItem('data_split_bill_two_method_name');

                    var person = $('#person').val();
                    var payment_change = $('#payment-change-split').text();
                    var tip_amount = Number($('#tip-amount-input-split').val()).toFixed(3);
                    var delivery_charge = Number($('#delivery-charge-input-split').val()).toFixed(3);
                    delivery_charge = isNaN(delivery_charge)?0:delivery_charge;
                    var dis3 = Number($("#input-discount-split").val()).toFixed(3);

                    var table_id = $('#table_id').val();

                    var user_id = Number($('#curren-user').val());
                    var customer_id = $('#customer_id').val();

                    var method_note = $('#payment-method-note-split').val();


                    var d = new Date();
                    var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                    var dt = new Date();
                    var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();


                    var totalDiscount = Number($('#input-discount-split').text());
                    var grandTotalValue = Number($('#grand-total-split').text());
                    grandTotalValue = isNaN(grandTotalValue) ? 0 : grandTotalValue;
                    var input_current_amount = $("#input-current-amount").val();
                    var paid = $("#input-current-amount-split").val();
                    var totalcart = (totalDiscount + grandTotalValue).toFixed(3);
                    var newgrandTotal = (Number(totalcart) + Number(pay_one) + Number(pay_two) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);


                    event.preventDefault();


                    if (checkNetConnection() === true && navigator.onLine) {
                        order_data = {
                            table_id: table_id,
                            order_type_id: order_type_id,
                            person: person,
                            method_note: method_note,
                            method_id: method_id,
                            split_bill_one_method_id: split_bill_one_method_id,
                            split_bill_two_method_id: split_bill_two_method_id,
                            discount: totalDiscount,
                            taxs: 0,
                            total: grandTotalValue,
                            paid: paid,
                            pay_one: pay_one,
                            pay_two: pay_two,
                            sub_total: totalcart,
                            tips: tip_amount,
                            delivery: isNaN(delivery_charge)?0:delivery_charge,
                            cmp: cmp,
                            user_id: user_id,
                            customer_id: customer_id,
                            csk_id: csk_id,
                            _token: '{{ csrf_token() }}'
                        };

                        $.ajax({
                            type: "post",
                            url: "{{ url('/save_resource') }}",
                            dataType: "json",
                            data: order_data,
                            success: function (data) {
                                console.log('dada ');

                                localStorage.clear();

                                toastr.success('Order Successfully Created.', 'Success Alert', {timeOut: 1000});

                                for (i = 0; i < riportArray.length; i++) {
                                    var csk_id = data.csk_id;
                                    var item_id = riportArray[i].id;
                                    var item_price = riportArray[i].price;
                                    var qty = riportArray[i].count;
                                    var total = item_price * qty;
                                    var discount = riportArray[i].discount;

                                    formdata = {
                                        csk_id: csk_id,
                                        item_id: item_id,
                                        item_price: item_price,
                                        qty: qty,
                                        total: total,
                                        discount: discount,
                                        _token: '{{ csrf_token() }}'
                                    };

                                    console.log(formdata);


                                    $.ajax({
                                        dataType: 'json',
                                        type: 'post',
                                        url: '{{route('store.product.order')}}',
                                        data: formdata

                                    }).done(function (data) {
                                        console.log(data);
                                    });

                                }

                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
                    } else {

                        if (localStorage.getItem('local_orders_with_cart') === null) {
                            var local_orders_with_cart = [];
                            localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));
                        }
                        else {
                            local_orders_with_cart = JSON.parse(localStorage.getItem('local_orders_with_cart'));
                        }

                        console.log('order data is saving in local storage...');
                        console.log(local_orders_with_cart);

                        order_data = {
                            table_id: table_id,
                            order_type_id: order_type_id,
                            person: person,
                            method_note: method_note,
                            method_id: method_id,
                            split_bill_one_method_id: split_bill_one_method_id,
                            split_bill_two_method_id: split_bill_two_method_id,
                            discount: totalDiscount,
                            taxs: 0,
                            total: grandTotalValue,
                            paid: grandTotalValue,
                            sub_total: totalcart,
                            tips: tip_amount,
                            delivery: isNaN(delivery_charge)?0:delivery_charge,
                            cmp: cmp,
                            user_id: user_id,
                            customer_id: customer_id,
                            csk_id: csk_id
                        };


                        var HoldCart = [];

                        for (i = 0; i < riportArray.length; i++) {
                            var item_id = riportArray[i].id;
                            var item_price = riportArray[i].price;
                            var qty = riportArray[i].count;
                            var total = item_price * qty;
                            var discount = riportArray[i].discount;

                            CartData = {
                                item_id: item_id,
                                item_price: item_price,
                                qty: qty,
                                total: total,
                                discount: discount,
                                csk_id: csk_id
                            };

                            HoldCart.push(CartData);
                        }


                        var Hold_Order_Cart = {BackupOrders: order_data, BackupCarts: HoldCart};

                        local_orders_with_cart.push(Hold_Order_Cart);

                        localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));

                        toastr.success('Offline Order Successfully Created.', 'Success Alert', {timeOut: 1000});

                        //                    console.log(JSON.parse(localStorage.getItem('local_orders_with_cart')));

                    }


                    var outputreport = '';

                    var x = 1;
                    for (var i in riportArray) {


//                        var ser = 1 + Number(i);
//                        outputreport += "<tr>";
//                        outputreport += "<td>" + ser + "</td>";
//                        outputreport += "<td>" + riportArray[i].name +"<p>"+ riportArray[i].arabic_name +"</p></td>";
//                        outputreport += "<td>" + riportArray[i].count + "</td>";
//                        outputreport += "<td>" + Number(riportArray[i].price).toFixed(3) + "</td>";
//                        outputreport += "<td>" + (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3) + "</td>";
//                        outputreport += "</tr>"
                        outputreport += "<tr>";
                        outputreport += "<td>";
                        if(riportArray[i].price > 0){
                            outputreport += x++
                        }
                        outputreport += "</td>";
                        outputreport += "<td>" + riportArray[i].name;
                        if(riportArray[i].price > 0){
                            outputreport += "<p>"+ riportArray[i].arabic_name +"</p>";
                        }
                        outputreport += "</td>";
                        outputreport += "<td>";
                        if(riportArray[i].price > 0){
                            outputreport += riportArray[i].count;
                        }
                        outputreport += "</td>";
                        outputreport += "<td>";
                        if(riportArray[i].price > 0){
                            outputreport += Number(riportArray[i].price).toFixed(3);
                        }
                        outputreport += "</td>";
                        outputreport += "<td>";
                        if(riportArray[i].price > 0){
                            outputreport += (Number(riportArray[i].price) * Number(riportArray[i].count)).toFixed(3);
                        }
                        outputreport += "</td>";
                        outputreport += "</tr>"
                    }

                    var tableName = localStorage.getItem('tableName');
                    var orderTypeName = localStorage.getItem('orderTypeName');
                    var order_id = localStorage.getItem('order_id');


                    $('#show-bill-order_number').html(csk_number);

                    $('#show-bill-order-type').html(orderTypeName);
                    $('#show-bill-person').html(person);
                    $('#show-bill-table-number').html(tableName);


                    $('#show-bill-table_id').html(table_id);
                    $('#show-bill-person').html(person);
                    $('#show-bill-current-time').html(currentTime);
                    $('#show-bill-current-date').html(currentDate);

                    $('#show-bill-print').html(outputreport);

                    $('#bill-discount').html(totalDiscount);
                    $('#bill-tax').html(0.000);
                    $('#bill-grand-total').html(grandTotalValue);
                    $("#bill-new-grand-total").html(newgrandTotal);
                    $('#bill-sub-total').html(totalcart);

                    $('#bill-changes-balance').html(payment_change);
                    $('#bill-payment-type-amount').html(input_current_amount);
                    $('#pay-one').html(pay_one);
                    $('#pay-two').html(pay_two);
                    $('#bill-method-name').html(data_method_name);
                    $('#data_split_bill_one_method_name').html(data_split_bill_one_method_name);
                    $('#data_split_bill_two_method_name').html(data_split_bill_two_method_name);
                    $('#bill-tips-amount').html(tip_amount);
                    $('#bill-delivery-charge').html(isNaN(delivery_charge)?0:delivery_charge);

                    if(pay_one > 0 || pay_two > 0){
                        $("#bill-method-name").closest('.free').remove();
                        $("#bill-payment-type-amount").closest('.text-right').remove();
                    }else{
                        $("#data_split_bill_one_method_name").closest('.free').remove();
                        $("#pay-one").closest('.text-right').remove();
                        $("#data_split_bill_two_method_name").closest('.free').remove();
                        $("#pay-two").closest('.text-right').remove()
                    }

                    if (method_note != '') {
                        var bill_note_output = '';

                        bill_note_output += "<li>";
                        bill_note_output += "<div class='free'>";
                        bill_note_output += "<strong><span id=''>" + data_method_title + "</span> </strong>";
                        bill_note_output += "</div>";
                        bill_note_output += "<div class='text-right'>";
                        bill_note_output += "<span class='text-right'><span id=''>" + method_note + "</span></span>";
                        bill_note_output += "</div>";
                        bill_note_output += "</li>";

                        $('#bill-show-method-note').html(bill_note_output);
                    }


                    $("#popUp").fadeOut(1000);
                    $(".Popupmodel").css("display", "none");


                    $(".poscat-list-page").css("display", "none");
                    $(".header-area").css("display", "none");
                    $(".invoice-page").css("display", "block");
                    print();
                    $(".poscat-list-page").css("display", "block");


                    $(".header-area").css("display", "block");
                    $(".invoice-page").css("display", "none");


                    $('#payment-method-note-split').val('');
                    $('#input-current-amount-split').val('');
                    $('#tip-amount-input-split').val('');
                    $('#delivery-charge-input-split').val('');
                    // $('#inputDiscount').val('');
                    $('#input-discount-split').val('');


                    $('#payment-change-split').html('0.000');
                    $('#tip-amount-input-split').val('');
                    $('#delivery-charge-input-split').val('');
                    $('#payment-method-note-split').val('');
                    $('#split-array-type').val('');


                    if (split_arr_type == 'left') {
                        left_arr = [];
                        re_displaySplitOrder(left_arr);
                        $('#split-order-discount').html('0.000');
                        $('#split-order-total').html('0.000');

                    }
                    if (split_arr_type == 'right') {
                        split_arr_2 = [];
                        splitOrder_right();
                        $('#split-order-discount-right').html('0.000');
                        $('#split-order-total-right').html('0.000');

                    } else {
                        riportArray = [];
                    }


                    if (split_arr_2.length == 0 && (left_arr.length == 0)) {
                        clearCart();
                        displayCart();
                        $('#cmp').attr('checked', false);
                        $('#inputDiscount').val('');
                        $('#inputDiscount_2').val('');
                        $('#input-discount').html('0.000');
                        $('#grand-total').val('');
                    } else {
                        $('#Splitorder').css('display', 'block');
                    }


                }
            }

            getCloseCheck();
            getOrderIdFn();
        });


        $('#show-all-hold').on('click', '.single-hold-bill-print', function (event) {


            $('#holdpop').css('display', 'none');
            $('#popUp').css('display', 'block');


            var holdIndexNumber = $(this).attr('data-hold-index-number');
            var allHoldCart = JSON.parse(localStorage.getItem('all_hold_orders'));
            //console.log(allHoldCart);
            var output_hold = '';

            for (i in allHoldCart) {
                if (i === holdIndexNumber) {
                    var count_hold_cart = 0;
                    var holdArray = (allHoldCart[i].holdCartItem);
                    //console.log(holdArray);

                    for (var p in holdArray) {
                        count_hold_cart += holdArray[p].count;
                        output_hold += "<tr>";
                        output_hold += "<th scope='row' width='2%' >" + (1) + "</th>";
                        output_hold += "<td width='32%' style='font-size:11px'>" + holdArray[p].name + "</td>";

                        output_hold += "<td width='17.5%' class='text-center' >";
                        output_hold += "<div class='pos-pro-body-qit'>";
                        output_hold += "<input type='text'value='" + holdArray[p].count + "' class='pos-pro-body-qit-input'>";
                        output_hold += "</div>";
                        output_hold += "</td>";

                        output_hold += "<td width='15%' class='text-center'>" + Number((holdArray[p].price)).toFixed(3) + "</td>";
                        output_hold += "<td width='14%' class='text-center'>" + Number((holdArray[p].price * holdArray[p].count) - holdArray[p].discount).toFixed(3) + "</td>";

                        output_hold += "</tr>";
                    }

                    var num = 0.000;
                    $('#show-cart-hold').html(output_hold);

                    $('#total-cart-discount-hold').html(num.toFixed(3));

                    $('#input-discount-hold').html(allHoldCart[i].alldiscount);

                    $('#total-cart-hold').html(allHoldCart[i].subTotal);
                    $('#grand-total-hold').html(allHoldCart[i].grandTotal);

                    $('#payment-total').val(allHoldCart[i].grandTotal);

                    $('#count-Cart-hold').html(count_hold_cart);
                    $('#is_hold').val(1);
                    $('#hold_index').val(holdIndexNumber);
                }

            }




        });


        $('#customer-popup').click(function () {
            $(".user-form-popup").css("display", "block");
        });

        $('#customer-modal-close').click(function () {
            $(".user-form-popup").css("display", "none");
        });




        // <-- THIS IS THE JS SOURCE CODE OF Jquery.Idle --------

        $(document).idle({
            onIdle: function(){
                window.location.reload();
            },
            idle: 5000*12*5
        });


        $(document).ready(function() {
            $('.select2-search').select2();
            $('#confirm-payment').attr('disabled','disabled');
        });


        $('#create-kitchen-hold').click(function(event){

            if ($('#cmp').is(":checked")){
                var cmp = 1;
                $('#cmp').attr('checked', false);
            }else{
                var cmp = 0;
            }



            var d = new Date();
            var currentDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
            var dt = new Date();
            var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var riportArray = listCart();
            var outputreport = '';

            var csk_id = localStorage.getItem('hold_id');

            event.preventDefault();

            // setting up csk id and saving it to local storage
            var timestamp = '' + Date.now() + '';
            var csk = timestamp.slice(6);


            if (checkNetConnection() === true && navigator.onLine) {
                order_data = {
                    cmp: cmp,
                    csk_id: csk_id,
                    is_kitchen: 1,
                    _token: '{{ csrf_token() }}'
                };

                $.ajax({
                    type: "post",
                    url: "{{ url('/save_kitchen') }}",
                    dataType: "json",
                    data: order_data,
                    success: function (data) {
                        console.log(data);
                        toastr.success('Your order has been sent to the kitchen.', 'Success Alert', {timeOut: 1000});

                        for (i = 0; i < riportArray.length; i++) {
                            var csk_id = data.csk_id;
                            var item_id = riportArray[i].id;
                            var qty = riportArray[i].count;

                            formdata = {
                                csk_id: csk_id,
                                item_id: item_id,
                                qty: qty,
                                _token: '{{ csrf_token() }}'
                            };

                            $.ajax({
                                dataType: 'json',
                                type: 'post',
                                url: '{{ route('store.kitchen.product.order') }}',
                                data: formdata

                            }).done(function (data) {
                                console.log(data);
                            });

                        }


                        set_hold_id();

                        //Holde Kitchen Order......


                        var orderTypeName = localStorage.getItem('orderTypeName');

                        if ($('#table_id').val() == null && orderTypeName == "Dine In" ) {
                            alert('Table Is Empty.');
                        }else{
                            if (cart.length == 0) {
                                alert('Cart is Empty.');
                            }else{

                                var holdCode = Number($('#input-current-amount-hold').val());
                                // var order_type_id = Number($('#show-payment-method-id').val());
                                var order_type_id = localStorage.getItem('orderTypeId');

                                var current_user_name = $('#current-user-name').val();
                                var person = $('#person').val();
                                var table_id = $('#table_id').val();
                                var customer_id = $('#customer_id').val();
                                var dis1 = Number(orderTypeTotalDiscount(strogOrderData));
                                var dis2 = Number(totalCartDiscount());
                                var dis3 = Number($('#inputDiscount').val());
                                var alldiscount = Number(dis1+dis2+dis3).toFixed(3);
                                var tax1 = Number(orderTypeTotalTax(strogOrderData)).toFixed(3);
                                var tip_amount = $('#tip-amount-input').val();
                                var delivery_charge = $('#delivery-charge-input').val();
                                delivery_charge = isNaN(delivery_charge)?0:delivery_charge;
                                var pay_one = Number(localStorage.getItem('pay_one')).toFixed(3);
                                var pay_two = Number(localStorage.getItem('pay_two')).toFixed(3);

                                var totalcart = Number(totalCart()).toFixed(3);


                                var grandTotalValue = (Number(totalcart)+Number(tax1)-Number(alldiscount)).toFixed(3);
                                grandTotalValue = isNaN(grandTotalValue) ? 0 : grandTotalValue;
                                var newgrandTotal = (Number(totalcart) + Number(pay_one) + Number(pay_two) + Number(tip_amount) + Number(delivery_charge)).toFixed(3);


                                var d = new Date();
                                var currentDate = d.getDate() + "-" +(d.getMonth()+1) + "-" + d.getFullYear();
                                var dt = new Date();
                                var currentTime = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

                                var holdArray = listCart();

                                inputHold ={
                                    'current_user_name' : current_user_name,
                                    'customer_id' : customer_id,
                                    'holdName' : orderTypeName,
                                    'holdCartItem' : holdArray,
                                    'order_type_id' : order_type_id,
                                    'person' : person,
                                    'table_id' : table_id,
                                    'table_name' : get_table_Name,
                                    'alldiscount' : alldiscount,
                                    'subTotal' : totalcart,
                                    'grandTotal' : grandTotalValue,
                                    'currentDate' : currentDate,
                                    'currentTime' : currentTime,
                                    'totalTax' : tax1,
                                    'holdCode' : holdCode,
                                    'is_hold'     : 0,
                                    'csk_id'     : csk_id
                                };


                                saveHoldData(inputHold);
                                displayHoldCart();
                                cart = [];
                                saveCart();
                                loadOrderTypeData();

                                $('#inputDiscount').val('');
                                $('#input-discount').val('');
                                displayCart();

                                toastr.success('Hold Successfully Added.', 'Success Alert', {timeOut: 1000});

                                $(".Popupmodel").css("display", "none");
                                $('#input-current-amount-hold').val('');
                            }
                        }



                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }


            clearCart();

            $('#payment-method-note').val('');
            $('#input-current-amount').val('');
            $('#tip-amount-input').val('');
            $('#delivery-charge-input').val('');
            $('#inputDiscount').val('');
            $('#inputDiscount_2').val('');
            $('#input-discount').html('0.000');
            getOrderyTypeData(strogOrderData);
            getOrderIdFn();

        });

        getOrderIdFn();

        $('#action_hold').val('');

    </script>

    <script>
        /**
         * Show/hide payment box when click on split bill button
         *
         */
        $("#split-bill-btn").click(function(){
                $("#split-paybox").toggle();
                $("#paybox").toggle();
                $("#confirm-payment").removeAttr('disabled');
                $("#payment-change").text(Number(0.000).toFixed(3));
            }
        );

        $("#split-amount-one").keyup(function(){
            var remaining = Number(grandTotal() - $(this).val()).toFixed(3);
            $("#split-amount-two").val(remaining);
            localStorage.setItem('pay_one',$(this).val());
            localStorage.getItem('pay_one');
            localStorage.setItem('pay_two',Number($('#split-amount-two').val()).toFixed(3));
            localStorage.getItem('pay_two');
        });

        $("#split-amount-two").keyup(function(){
            var remaining = Number(grandTotal() - $(this).val()).toFixed(3);
            $("#split-amount-one").val(remaining);
            localStorage.setItem('pay_one',Number($('#split-amount-one').val()).toFixed(3));
            localStorage.getItem('pay_one');
            localStorage.setItem('pay_two',$(this).val());
            localStorage.getItem('pay_two');
        });

    </script>
@endsection




