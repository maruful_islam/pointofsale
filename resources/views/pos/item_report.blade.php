@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

@endsection


@section('content')
    @php
        $date = date('Y-m-d');
    @endphp

    <div class="container page-padding-top" id="report-desc">

        <div class="user-list-boxarea">
            <div class="row">
                <div class="report-header">
                    <div class="row" style="padding-top: 15px">
                        <form action="" method="get">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-md-offset-1">
                                <form action="" method="get">
                                    <div class="prport-tatepiker">
                                        <div class="col-md-8  form-group">
                                            {{--Search Previous Report: <input type="text" class="span2 date_input" name="date" value="@if(isset($d['searched_date'])) {{ $d['searched_date'] }} @endif" required>--}}
                                            Form Date: <input type="text" class="span2 date_input" name="start_date"
                                                              @if(isset($_GET['start_date'])) value="{{ $_GET['start_date'] }}"
                                                              @endif required>
                                            To Date: <input type="text" class="span2 date_input" name="end_date"
                                                            @if(isset($_GET['end_date'])) value="{{ $_GET['end_date'] }}"
                                                            @endif required>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="report-search">
                                                <button class="btn btn-button">Search</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>

                        </form>
                    </div>
                </div>

                <hr>

                <div class="col-xs-12 col-sm-12 col-md-12" id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h3>REPORT BY ITEM</h3><br>
                        <img src="{{ asset('pos/images/ddddd.png') }}" width="150"/>
                        <h4>{{ strtoupper(systemInfo('system_name'))}}</h4>
                        <h4>{{ systemInfo('local_name') }}</h4><br>
                        @if(isset($start))
                            <h5>Report From: <b>{{ $start->format('l, dS F Y') }}</b>
                                to <b>{{ $end->subHours(6)->format('l, dS F Y') }}</b></h5>
                        @else
                             @if(Carbon\Carbon::now()->format('H') >= 6)
                                <h5>Date: {{ date('l, dS F Y') }} </h5>
                            @else
                                <h5>Date: {{ Carbon\Carbon::yesterday()->format('l, dS F Y') }}</h5>
                            @endif
                        @endif
                    </div>
                    <div class="report-table2 endofday">

                        <table class="table">

                            <thead class="blue-grey lighten-4">
                            <tr>
                                <th class="text-center">Item</th>
                                <th class="text-center">Item Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Total Sales</th>
                                <th class="text-center">Total Discount</th>
                                <th class="text-center">Net Sales</th>
                            </tr>
                            </thead>
                            <!--Table head-->

                            <!--Table body-->
                            <tbody>


                            @if( ( count($report_by_item) > 0 && !isset($end_day_check) ) || isset($show) )
                                @foreach($report_by_item as $report)
                                    <tr class="text-center">
                                        <td>{{ $report['item_name'] }}</td>
                                        <td>{{ $report['price'] }} {{ config('app.currency')}}</td>
                                        <td>{{ $report['sold'] }}</td>
                                        <td>{{ $report['earned'] }} {{ config('app.currency')}}</td>
                                        <td>{{ $report['discount'] }}</td>
                                        <td>{{ $report['net_profit_by_item'] }} {{ config('app.currency')}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <br>
                                        @if(!isset($end_day_check) || isset($show))
                                            <div class="alert alert-danger">No item has not been sold yet!</div>
                                        @else
                                            <div class="alert alert-success">You have ended your day's sell
                                            </div>
                                        @endif
                                    </td>
                                </tr>

                            @endif

                            </tbody>

                            <caption>@if(isset($_GET['date'])) {{ date('d F Y', strtotime($date)) }} @else Today @endif
                                's Sales Report by Item
                            </caption>
                        </table>

                        <hr>


                    </div>

                </div>

                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                       onclick="window.print()">Print This Report</a>
                </div>

                <div class="text-center" style="padding: 20px">
                    <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                       onclick="printReport()">Thermal Print</a>
                </div>

            </div>
        </div>
    </div>



    <div class="container">
        <div id="thermal-print" class="invoice-page" style="display: none;">
            <div class="invoice-area">
                <div class="inv-header">
                    <div class="invo-logo text-center">
                        <strong>Maharaja Palace Restaurant</strong>

                    </div>
                    <div class="invo-header-content" id="clipPolygon">
                        <div class="invo-header-info">
                            <ul>
                                <li><span id="show-bill-phone"> Phone : 22531053/97639030</span></li>
                                <li><span id="show-bill-email"> Email : services@maharajapalacekwt.com</span></li>

                            </ul>
                        </div>
                    </div>
                </div>


                <div class="invoice-center">

                    <hr>

                    <ul style="padding-left: 10px;">
                        <li><b>Date:</b> <span id="show-bill-current-date">{{ date('d M Y') }}</span></li>
                        <li><b>Time:</b> <span id="show-bill-current-time">{{ date('h:i A')  }}</span></li>

                    </ul>

                    <hr>

                </div>
                <div class="voice-tables">

                    <div class="pos-total-counts">
                        <div class="pos-to-right ">
                            <ul>

                                <li>
                                    <div class="free">
                                        <strong>Item Name </strong>
                                    </div>
                                    <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-discount">  <strong>Net Sales</strong></span> </span>
                                    </div>
                                </li>

                                @if( ( count($report_by_item) > 0 && !isset($end_day_check) ) || isset($show) )
                                    @foreach($report_by_item as $report)
                                        <li>
                                            <hr style="margin: 3px!important;">

                                            <div class="free">
                                               {{ substr($report['item_name'],0,20) }} ({{ $report['sold'] }})
                                            </div>

                                            <div class="text-right">
                                        <span class="text-right"> <span
                                                    id="bill-discount">  {{ $report['net_profit_by_item'] }} {{ config('app.currency')}}</span> </span>
                                            </div>

                                        </li>
                                    @endforeach
                                @endif

                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true
        });
    </script>



    <script>
        function printReport() {

            $('#thermal-print').css('display', 'block');
            $('#report-desc').css('display', 'none');
            window.print();
            $('#thermal-print').css('display', 'none');
            $('#report-desc').css('display', 'block');
        }
    </script>
    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #thermal-print, #thermal-print * {
                visibility: visible;
            }

            #thermal-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .invoice-center ul li {
                width: 48%;
            }

            .inv-header {
                border-bottom: none;
            }

        }
    </style>



@endsection