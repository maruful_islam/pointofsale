    
    @extends('admin_layouts.default')
    @section('content')
       <div class="container page-padding-top">

            <div class="user-list-boxarea">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <form action="/res/create-methods" method="post">
                        {{csrf_field()}}
                            <div class="user-inputbox">
                                 <h2 class="user-list-title">Add Payment Method</h2>

									 <!-- Success and error Message Start -->

                                  @if ($errors->any())
                                    <div class="alert alert-danger error-message-show">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                  @endif
                                    @if(Session::has('success'))
                                        <div class="alert alert-success success-message-show"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
                                    @endif

                                <!-- Success and error Message End -->


                                <div class="form-group">
                                  <label for="usr">Method Name:</label>
                                  <input type="text" name="name" class="form-control" >
                                </div>
                               

                                
                                 <div class="form-group">
                                  <label for="pwd">Note:</label>
                                  <input type="text" name="title" class="form-control" >
                                </div>
                               
                                 <div class="form-group">
                                    <label class="container">
                                          <input type="checkbox" name="status" value="active" checked="checke">
                                          Status
                                    </label>
                                </div>
                                <div class="form-group">
                                  <button class="btn-submit btn-primary" type="submit">submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                         <div class="user-list">
                             <div class="user-list-table table-responsive">
                                <h2 class="user-list-title">Method List</h2>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Method Name</th>
                                            <th>Note</th>
                                            <th>Status</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($methods  as $key => $method)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$method->method_name}}</td>
                                             <td>{{$method->title}}</td>
                                            <td>
                                                @if($method->is_active === "active")
                                                    <a data-id="{{ $method->id }}"
                                                       data-status="{{ $method->is_active }}" data-target="#statusUpdate"
                                                       data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                                @else
                                                    <a data-id="{{ $method->id }}"
                                                       data-status="{{ $method->is_active }}" data-target="#statusUpdate"
                                                       data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="user-action">
                                                    <a href="#"
                                                       data-id="{{ $method->id }}"
                                                       data-name="{{ $method->method_name }}"
                                                       data-title="{{ $method->title }}"
                                                       data-status="{{ $method->is_active }}"
                                                       data-target="#edit-method"
                                                       class="user-edits" data-toggle="modal" title="Edit">
                                                        <span class="fa fa-edit"></span>
                                                    </a>
                                                    <a href="/res/delete-method/{{$method->id}}" class="user-removed" data-toggle="tooltip" title="Delete" onclick="return confirm('Are You Sure Delete This Item?');"><span class="fa fa-times"></span></a>

                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                         </div>
                     </div>
                    
                </div>
            </div>
        </div> 

    @endsection

    @section('js')

        <!-- Tax Edit Pop Up -->

        <div id="edit-method" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title user-list-title">Edit Payment Information</h2>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('payment.update') }}" method="post">

                            {{csrf_field()}}

                            <input type="hidden" name="id" id="p-id">

                            <div class="user-inputbox">
                                <h2 class="user-list-title">Update Payment Method</h2>


                                <div class="form-group">
                                    <label for="usr">Method Name:</label>
                                    <input type="text" name="name" id="p-name" class="form-control" >
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Note:</label>
                                    <input type="text" name="title" id="p-title" class="form-control" >
                                </div>

                                <div class="form-group">
                                    <label class="container">
                                        <input type="checkbox" name="status" id="p-status" value="active" checked="checked">
                                        Status
                                    </label>
                                </div>

                                <div class="form-group">
                                    <button class="btn-submit btn-primary" type="submit">submit</button>
                                </div>
                            </div>

                        </form>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <!-- Payment Status Update -->

        <!-- Modal -->
        <div id="statusUpdate" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Are you sure?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you really want to <b id="status-msg"></b> this payment method information? </p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-success" id="statusConfirmation">Yes</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <script>
            $('#edit-method').on('show.bs.modal', function (e) {

                var id = $(e.relatedTarget).data('id');
                var name = $(e.relatedTarget).data('name');
                var title = $(e.relatedTarget).data('title');
                var status = $(e.relatedTarget).data('status');

                console.log(status);

                $('#p-id').val(id);
                $('#p-name').val(name);
                $('#p-title').val(title);

                if (status === 'active') {
                    $('#p-status').prop('checked', true);
                } else {
                    $('#p-status').prop('checked', false);
                }


            });


            $('#statusUpdate').on('show.bs.modal', function (e) {
                var id = $(e.relatedTarget).data('id');
                var status = $(e.relatedTarget).data('status');


                if (status === 'active') {
                    $('#status-msg').text('Deactivate');
                }
                else {
                    $('#status-msg').text('Activate');
                }

                $('a#statusConfirmation').attr('href', '{{ url('/res/update-payment-status')}}/' + id);


            });
        </script>


    @endsection