@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">

                    <form action="/res/create-table" method="post">

                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Table</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control" id="table name">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Title:</label>
                                <input type="text" name="title" class="form-control" id="pwd">
                            </div>

                            <!-- <div class="form-group">
                               <label class="container">
                                     <input type="checkbox" checked="checke">
                                     Status
                               </label>
                                                            </div> -->
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Table List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tables  as $key => $table)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$table->name}}</td>
                                        <td>{{$table->tbl_title}}</td>
                                        <td>
                                            <div class="user-action">

                                                <a href="#"
                                                   data-id = "{{ $table->id }}"
                                                   data-name = "{{ $table->name }}"
                                                   data-title = "{{ $table->tbl_title }}"
                                                   data-target="#edit-table"
                                                   class="user-edits" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/res/delete-table/{{$table->id}}" class="user-removed"
                                                   data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-times"></span></a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Category Edit Pop Up -->

    <div id="edit-table" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Table Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('table.update') }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Update Table</h2>


                            <input type="hidden" name="id" id="t-id">
                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control" id="t-name">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Title:</label>
                                <input type="text" name="title" class="form-control" id="t-title">
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#edit-table').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var title = $(e.relatedTarget).data('title');


            $('#t-id').val(id);
            $('#t-name').val(name);
            $('#t-title').val(title);


        });
    </script>


@endsection