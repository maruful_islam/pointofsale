@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="report-header">
            <div class="row">
                <div class="kcol-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="prport-tatepiker">
                        <form action="" method="get">
                            <table class="table">
                                <thead>
                                <tr>

                                    <th width="90%" class="pull-right">Search By Name/Phone/Email: <input type="text" class="span2"
                                                                                              value="@if(isset($string)) {{ $string }} @endif" name="string"
                                                                                              style="width: 50%;">
                                    </th>
                                    <th>
                                        <div class="report-search pull-left">
                                            <button class="btn btn-button">Search</button>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="user-list">
                        <div class="user-list-table table-responsive">

                            <h2 class="user-list-title">Customer List</h2>

                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Whats-App Number</th>
                                    <th>Activity Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($customers  as $key => $customer)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{ $customer->phone }}</td>
                                        <td>@php echo $customer->address @endphp</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->w_number }}</td>
                                        <td>
                                            @if($customer->is_active === "on")
                                                <a data-id="{{ $customer->id }}"
                                                   data-status="{{ $customer->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $customer->id }}"
                                                   data-status="{{ $customer->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>

                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $customer->id }}" data-name="{{ $customer->name }}"
                                                   data-phone="{{ $customer->phone }}"
                                                   data-email="{{ $customer->email }}"
                                                   data-address="{{ $customer->address }}"
                                                   data-w_number="{{ $customer->w_number }}"
                                                   data-status="{{ $customer->is_active }}"
                                                   data-target="#edit-customer"
                                                   class="btn btn-info" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            @if(count($customers) > 0)

                                <div class="alert alert-info">
                                    Showing {{ $customers->firstItem() }} to {{ $customers->lastItem() }} Customer of
                                    Total {{ $customers->total() }} Customers.
                                </div>

                                <nav aria-label="Page navigation">
                                    @if ($customers->lastPage() > 1)
                                        <ul class="pagination pagination-sm">
                                            <li class="page-item {{ ($customers->currentPage() == 1) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $customers->url(1) }}">Previous</a>
                                            </li>
                                            @for ($i = 1; $i <= $customers->lastPage(); $i++)
                                                <li class="page-item {{ ($customers->currentPage() == $i) ? 'page active' : '' }}">
                                                    <a class="page-link" href="{{ $customers->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item {{ ($customers->currentPage() == $customers->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link"
                                                   href="{{ $customers->url($customers->currentPage()+1) }}">Next</a>
                                            </li>
                                        </ul>
                                    @endif
                                </nav>
                            @else
                                <div class="alert alert-danger">
                                    No Result Found!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

@section('js')



    <!-- Customer Edit Pop Up -->

    <div id="edit-customer" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Customer Information</h2>
                </div>
                <div class="modal-body">

                    <div class="user-inputbox">


                        <form role="form" id="customerForm" action="{{ route('customer.update') }}" method="post">

                            {{ csrf_field() }}

                            <input type="hidden" name="id" id="customer-id">

                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" id="customer-name">
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone:</label>
                                <input type="text" name="phone" class="form-control" id="customer-phone">
                            </div>

                            <div class="form-group">
                                <label for="email">E-mail:</label>
                                <input type="email" name="email" class="form-control" id="customer-email">
                            </div>

                            <div class="form-group">
                                <label for="email">Whats-App Number:</label>
                                <input type="text" name="w_number" class="form-control" id="w_number">
                            </div>

                            <div class="form-group">
                                <label for="email">Address:</label>
                                <textarea name="address" class="form-control" id="customer-address"></textarea>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="is_active" id="customer-status">
                                    Status
                                </label>
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" id="store-customer" type="submit">submit</button>
                            </div>

                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are You Sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this customers information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">

        $('#edit-customer').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var email = $(e.relatedTarget).data('email');
            var phone = $(e.relatedTarget).data('phone');
            var address = $(e.relatedTarget).data('address');
            var w_number = $(e.relatedTarget).data('w_number');
            var status = $(e.relatedTarget).data('status');

            $('#customer-id').val(id);
            $('#customer-name').val(name);
            $('#customer-email').val(email);
            $('#customer-phone').val(phone);
            $('#customer-address').val(address);
            $('#w_number').val(w_number);

            if (status === 'on') {
                $('#customer-status').prop('checked', true);
            } else {
                $('#customer-status').prop('checked', false);
            }

        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            $('#customer-id').val(id);

            if (status === 'on') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-customers-status')}}/' + id);


        });


    </script>


@endsection