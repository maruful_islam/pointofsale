<!doctype html>

<!-- <html class="no-js" lang="en"
    @if(Request::url()== route('order'))
    manifest="{{ url('site.manifest_v1') }}"
      @endif

> -->

<html class="no-js" lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Point Of Sale</title>
    <link href="{{ asset('pos/images/favicon.png') }}" rel="icon"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- bootstrap css -->
    <link href="{{ asset('pos/libraries/bootsrap/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- / bootstrap css -->

    <!-- owl carousel css -->
    <link href="{{ asset('pos/libraries/owlcarousel/owl.carousel.css') }}" rel="stylesheet"/>
    <!-- / owl carousel css -->
    <!-- bootsnav css -->
    <link href="{{ asset('pos/libraries/bootsnav/bootsnav.css') }}" rel="stylesheet"/>
    <!-- / bootsnav css -->
    <!--  icon css -->
    <link href="{{ asset('pos/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <!-- / icon css -->

    <!-- animations css -->
    <link href="{{ asset('pos/libraries/animations/animate.css') }}" rel="stylesheet"/>
    <!-- / animations css -->

    <!-- extra css -->
@yield('css')

<!--  style css -->
    <link href="{{ asset('pos/css/style.css?ver:2.0') }}" rel="stylesheet"/>
    <!-- / style css -->

    <!--  media css -->
    <link href="{{ asset('pos/css/media.css') }}" rel="stylesheet"/>
    <!-- / media css -->

    <!-- / font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:300,300i,400,400i,500,700,900|Roboto:400,700|Roboto:300,300i,400,400i,500,700,900"
          rel="stylesheet">
    <!-- / font css -->

    <!-- fileinput css -->
    <link href="{{ asset('pos/libraries/fileinput/fileinput.min.css') }}" rel="stylesheet"/>
    <!-- / fileinput css -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
            href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">

    <style>
        .alert-danger {
            color: #ecf1f5!important;
            background-color: #ee1c25!important;
            border-color: #ee1c25!important;
        }
        .nav>li>a {

            padding: 10px 5px;
        }
    </style>
</head>

<body>

@php
    
    config(['app.name'=>'JACKPOT','app.currency'=>'KD']);
        
            @endphp
            
@include('admin_layouts.header')

<div class="main">
    @yield('content')
</div>
<!-- @include('admin_layouts.footer') -->
<script src="{{ asset('pos/js/jquery-1.12.4.min.js') }}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--}}
<script src="{{ asset('pos/libraries/bootsrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('pos/libraries/vendor/modernizr-2.8.3.min.js') }}"></script>
<script src="{{ asset('pos/libraries/bootsnav/bootsnav.js') }}"></script>
<script src="{{ asset('pos/libraries/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('pos/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('pos/libraries/animations/wow.min.js') }}"></script>

<!-- extra Js -->

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
            @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";

    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif

    toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'slideUp';
    toastr.options.closeMethod = 'slideUp';
    toastr.options.closeButton = true;
</script>

@yield('js')
<script src="{{ asset('pos/js/main.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('pos/libraries/fileinput/fileinput.min.js') }}"></script>

<script>
    $('.input-group.date').datepicker({format: "dd.mm.yyyy"});
</script>

<script>
    $(document).on('ready', function () {
        $("#input-b6").fileinput({
            showUpload: false,
            maxFileCount: 10,
            mainClass: "input-group-lg"
        });
    });
</script>


<script>

    // checking file access is true of false;

    function checkNetConnection() {
        jQuery.ajaxSetup({async: false});
        re = '';
        r = Math.round(Math.random() * 10000);
        $.get('{{ asset('Demo.jpg') }}', {subins: r}, function (d) {
            re = true;
        }).error(function () {
            re = false;
        });
        return re;
    }


    //checking current internet online/offline message shown or not

    if (localStorage.getItem('online_message_shown') == null) {
        localStorage.setItem('online_message_shown', 1);
        localStorage.setItem('offline_message_shown', 0);
    }


    if (checkNetConnection() === true && navigator.onLine) {
        $('#online-status').css('display', 'inline-block');
        $('#offline-status').css('display', 'none');
    }
    else {
        $('#online-status').css('display', 'none');
        $('#offline-status').css('display', 'inline-block');
    }


    // on interval it is checking systems current internet connection

    setInterval(function (e) {
        if (checkNetConnection() === true && navigator.onLine) {

            $('#online-status').css('display', 'inline-block');
            $('#offline-status').css('display', 'none');

            var msg_status1 = localStorage.getItem('online_message_shown');

            if (msg_status1 == 0) {

                $(document).ready(function () {
                    toastr.success('Your System is in Online Mode!', 'Internet Connected', {timeOut: 10000});
                });

                localStorage.setItem('online_message_shown', 1);
                localStorage.setItem('offline_message_shown', 0);

            }

        }

        else {

            $('#online-status').css('display', 'none');
            $('#offline-status').css('display', 'inline-block');

            var msg_status2 = localStorage.getItem('offline_message_shown');

            if (msg_status2 == 0) {

                $(document).ready(function () {
                    toastr.error('Your System is in Offline Mode!', 'Internet Disconnected', {timeOut: 10000});
                });

                localStorage.setItem('offline_message_shown', 1);
                localStorage.setItem('online_message_shown', 0);
            }


        }
    }, 10000);


    // clicking on synchronize button will sync all local data to database

    $(document).ready(function () {

        $('#synchronize').click(function () {


            if (checkNetConnection() === true && navigator.onLine) {

                var LocalOrders = JSON.parse(localStorage.getItem('local_orders_with_cart'));

                if (LocalOrders != null) {

                    if (LocalOrders.length > 0) {
                        for (i = 0; i < LocalOrders.length; i++) {

                            // getting order data from storage
                            prepare_order_data = LocalOrders[i].BackupOrders;

                            user_id = "{{ auth()->user()->id }}";

                            //data organizing for insertion
                            backup_order = {
                                table_id: prepare_order_data.table_id,
                                order_type_id: prepare_order_data.order_type_id,
                                person: prepare_order_data.person,
                                method_note: prepare_order_data.method_note,
                                method_id: prepare_order_data.method_id,
                                discount: prepare_order_data.discount,
                                taxs: prepare_order_data.taxs,
                                total: prepare_order_data.total,
                                paid: prepare_order_data.paid,
                                sub_total: prepare_order_data.sub_total,
                                tips: prepare_order_data.tip_amount,
                                user_id: user_id,
                                customer_id: prepare_order_data.customer_id,
                                csk_id: prepare_order_data.csk_id,
                                _token: '{{ csrf_token() }}'
                            };


                            $.ajax({
                                type: "post",
                                url: '/save_resource',
                                dataType: "json",
                                data: backup_order,
                                success: function (data) {

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });

                            //getting cart data from storage
                            CartArray = LocalOrders[i].BackupCarts;
                            console.log(CartArray);


                            $.each(CartArray, function (key, value) {

                                var csk_id = value.csk_id;
                                var item_id = value.item_id;
                                var item_price = value.item_price;
                                var qty = value.qty;
                                var total = item_price * qty;
                                var discount = value.discount;

                                backup_cart = {
                                    csk_id: csk_id,
                                    item_id: item_id,
                                    item_price: item_price,
                                    qty: qty,
                                    total: total,
                                    discount: discount,
                                    _token: '{{ csrf_token() }}'
                                };

                                console.log(backup_cart);

                                $.ajax({
                                    dataType: 'json',
                                    type: 'post',
                                    url: '{{route('store.product.order')}}',
                                    data: backup_cart

                                }).done(function (data) {

                                });
                            });

                        }

                        toastr.success('Offline Data Saved Successfully!', 'Success Alert', {timeOut: 10000});

                        var local_orders_with_cart = [];

                        localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));
                    }
                    else {
                        console.log('No Data Found!');
                        toastr.error('Order Cart is Empty!', 'Notice Alert', {timeOut: 10000});
                    }

                }
                else {

                    console.log('No Data Found!');
                    toastr.error('You already took backup!', 'Notice Alert', {timeOut: 10000});
                }
            }
            else {
                toastr.error('You are in offline mode! to take backup make sure your system is in online', 'Notice Alert', {timeOut: 10000});
            }

        })
    })

</script>

</body>

</html>