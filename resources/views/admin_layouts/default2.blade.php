<!doctype html>

<!-- <html class="no-js" lang="en"
    @if(Request::url()== route('order'))
    manifest="{{ url('site.manifest_v1') }}"
      @endif

> -->
 
<html class="no-js" lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Point Of Sale</title>
    <link href="{{ asset('pos/images/favicon.png') }}" rel="icon"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- bootstrap css -->
    <link href="{{ asset('pos/libraries/bootsrap/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- / bootstrap css -->

    <!-- owl carousel css -->
    <link href="{{ asset('pos/libraries/owlcarousel/owl.carousel.css') }}" rel="stylesheet"/>
    <!-- / owl carousel css -->
    <!-- bootsnav css -->
    <link href="{{ asset('pos/libraries/bootsnav/bootsnav.css') }}" rel="stylesheet"/>
    <!-- / bootsnav css -->
    <!--  icon css -->
    <link href="{{ asset('pos/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <!-- / icon css -->

    <!-- animations css -->
    <link href="{{ asset('pos/libraries/animations/animate.css') }}" rel="stylesheet"/>
    <!-- / animations css -->

    <!-- extra css -->
@yield('css')

<!--  style css -->
    <link href="{{ asset('pos/css/style.css?ver:2.0') }}" rel="stylesheet"/>
    <!-- / style css -->

    <!--  media css -->
    <link href="{{ asset('pos/css/media.css') }}" rel="stylesheet"/>
    <!-- / media css -->

    <!-- / font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:300,300i,400,400i,500,700,900|Roboto:400,700|Roboto:300,300i,400,400i,500,700,900"
          rel="stylesheet">
    <!-- / font css -->

    <!-- fileinput css -->
    <link href="{{ asset('pos/libraries/fileinput/fileinput.min.css') }}" rel="stylesheet"/>
    <!-- / fileinput css -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
            href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">

    <style>
        .alert-danger {
            color: #ecf1f5!important;
            background-color: #ee1c25!important;
            border-color: #ee1c25!important;
        }
        .nav>li>a {

            padding: 10px 5px;
        }
    </style>
</head>

<body>

<?php
$currentuserid = Auth::user()->id;
$currentusername = Auth::user()->name;

$order_id = App\Module::query()->where('slug', 'order')->value('id');
$report_id = App\Module::query()->where('slug', 'reports')->value('id');
$user_management_id = App\Module::query()->where('slug', 'user-management')->value('id');
$system_configuration_id = App\Module::query()->where('slug', 'system-configuration')->value('id');
$end_of_day_id = App\Module::query()->where('slug', 'end-of-day')->value('id');

$order_permission = App\Permission::query()->where([ 'user_id' => $currentuserid, 'module_id' => $order_id ])->value('status');
$report_permission = App\Permission::query()->where([ 'user_id' => $currentuserid, 'module_id' => $report_id ])->value('status');
$user_management_permission = App\Permission::query()->where([ 'user_id' => $currentuserid, 'module_id' => $user_management_id ])->value('status');
$system_configuration_permission = App\Permission::query()->where([ 'user_id' => $currentuserid, 'module_id' => $system_configuration_id ])->value('status');
$end_of_day_permission = App\Permission::query()->where([ 'user_id' => $currentuserid, 'module_id' => $end_of_day_id ])->value('status');

?>
<header class="header-area bg-color order-hidden-page">
    <input type="hidden" value="{{$currentuserid}}" id="curren-user">
    <input type="hidden" value="{{$currentusername}}" id="current-user-name">
    <div class="m-container">
        <div class="header-top">
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 mainlogo">
                    <div class="logo1">
                        <a href="{{ route('home') }}">
                            @php $sys_name = config('app.name') @endphp
                            @if(!is_null($sys_name))
                                <img src='{{asset(config('app.logo'))}}' alt="">
                            @else
                                <img src='{{asset("pos/images/logo.png")}}' alt="">
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-xs-3 col-md-1 col-sm-3 mainposlogo">
                    <div class="main-logo">
                        <a href="{{ route('home') }}">
                            @php $sys_name = config('app.name') @endphp
                            @if(!is_null($sys_name))

                                <h4 style="color: #fff; font-weight: bolder">{{ strtoupper($sys_name) }}</h4>

                            @else
                                <img src='{{asset("pos/images/logo.png")}}' alt="">

                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-xs-3 col-md-8 col-sm-3 mainposmenu">
                    <div class="account-info">
                         <nav class="navbar-default-custom" role="navigation">
                             <!-- Start Header Navigation -->
                                 <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-menu">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                <!-- End Header Navigation -->
                                <div class="collapse navbar-collapse" id="top-menu">
                                <ul class="nav navbar-nav">
        
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>User</span>
                                            <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu animated flipInX">
                                            @if($user_management_permission == 1)
                                                <li><a href="{{ route('user.permissions') }}"
                                                       class="@if(Request::url() == route('user.permissions')) active @endif">User
                                                        Permission</a></li>
                                            @endif
        
                                            <li><a href="{{ route('user.settings') }}"><i class="lnr lnr-user"></i> <span>User Settings</span></a>
                                            </li>
        
                                        </ul>
                                    </li>
                                    @if($system_configuration_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Item & Category</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="/res/pro-category">Category</a></li>
                                                <li><a href="/res/pro-item">Item</a></li>
                                                <li><a href="{{ action('CondimentController@create') }}">Condiments</a></li>
                                            </ul>
                                        </li>
                                    @endif
        
                                    @if($system_configuration_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Configurations</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                {{--<li><a href="/res/tables">Table</a>--}}
                                                <li><a href="/res/taxs">Tax</a></li>
                                                <li><a href="/res/discount">Discount</a></li>
                                                <li><a href="{{ action('ManageController@order_type') }}">Order Type</a></li>
                                                <li><a href="{{ action('ManageController@payment_methods') }}">Payment Methods</a></li>
                                                <li><a href="{{ route('currency.settings') }}">Currency Settings</a></li>
                                            </ul>
                                        </li>
        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Inventory Config</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('r.m.category') }}">Raw Materials Category</a></li>
                                                <li><a href="{{ route('r.m.items') }}">Raw Materials Items</a></li>
                                                <li><a href="{{ route('suppliers') }}">Supplier</a></li>
                                            </ul>
                                        </li>
                                    @endif
        
                                    @if($user_management_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Staff & Payroll</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('departments') }}">Departments</a></li>
                                                <li><a href="{{ route('designations') }}">Designations</a></li>
                                                <li><a href="{{ route('add.staff') }}">Add Staff</a></li>
                                                <li><a href="{{ route('staffs') }}">Manage Staff</a></li>
                                                <li><a href="{{ route('access.history') }}">Access History</a></li>
                                            </ul>
                                        </li>
        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Expenses</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('expense.categories') }}">Expense Categories</a></li>
                                                <li><a href="{{ route('expenses') }}">Expenses</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>Account</span>
                                            <i class="fa fa-angle-down"></i></a>
        
                                        <ul class="dropdown-menu animated flipInX">
                                            <li><a href="{{ route('attendance.break') }}">Daily Access</a></li>
        
                                            <li><a href="{{ route('system.settings') }}">System Settings</a></li>
        
                                            <li><a href="{{ route('user.settings') }}"><i class="lnr lnr-user"></i> <span>Account Settings</span></a>
                                            </li>
        
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>
        
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
        
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-1 ringerlogo">
                    <div class="ringer-logo">
                        <img src='{{asset("pos/images/ringer.png")}}' alt="">

                    </div>
                </div>
            </div>
        </div>

                <div class="manearea">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <!-- Start Header Navigation -->
                     <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    <!-- End Header Navigation -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav navbar-center">

                            <li>
                                <a href="{{ route('home') }}"
                                   class="@if(Request::url() == route('home')) active @endif"><span
                                            class="fa fa-home"></span>Dashboard</a>
                            </li>

                            @if($system_configuration_permission == 1)
                                <li>
                                    <a href="{{ route('product.list') }}"
                                       class="@if(Request::url() == route('product.list')) active @endif"><span
                                                class="fa fa-shopping-bag"></span>Menu</a>
                                </li>
                            @endif

                            @if($end_of_day_permission == 1)
                                <li><a href="{{ route('end.of.day') }}"
                                       class="@if(Request::url() == route('end.of.day')) active @endif"><span
                                                class="fa fa-history"></span>End of Day</a>
                                </li>
                            @endif

                            @if($order_permission == 1)
                                <li>
                                    <a href="{{ route('customer.list') }}"
                                       class="@if(Request::url() == route('customer.list')) active @endif">
                                        <span class="fa fa-users"></span>Customers
                                    </a>
                                </li>
                            @endif

                            @if($report_permission == 1)

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('get.report') or
                                                Request::url() == route('purchase.history') or
                                                Request::url() == route('purchase.report')
                                                )
                                            active
@endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-sellsy"></span> Reports <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('get.report') }}"
                                               class="@if(Request::url() == route('get.report')) active @endif">
                                                Sales Reports
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('item.report') }}"
                                               class="@if(Request::url() == route('get.report')) active @endif">
                                                Items Report
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('expense.report') }}"
                                               class="@if(Request::url() == route('expense.report')) active @endif">
                                                Expense Report
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('profit.loss.report') }}"
                                               class="@if(Request::url() == route('profit.loss.report')) active @endif">
                                                Profit/Loss Report
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                            @endif

                            @if($order_permission == 1)
                                <li><a href="{{ action('ManageController@order') }}"
                                       class="@if(Request::url() == url('/res/order')) active @endif"><span
                                                class="fa fa-retweet"></span>Order </a></li>

                                <!-- <li><a href="javascript:void(0)" id="synchronize"><span class="fa fa-download"></span>Sync
                                        Offline Data</a></li> -->

                                <!--<li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('purchase') or
                                                Request::url() == route('purchase.history') or
                                                Request::url() == route('purchase.report')
                                                )
                                            active
@endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-shopping-cart"></span> Purchase <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('purchase') }}"
                                               class="@if(Request::url() == route('purchase')) active @endif">
                                                Add Purchase
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('purchase.history') }}"
                                               class="@if(Request::url() == route('purchase.history')) active @endif">
                                                Purchase History
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('purchase.report') }}"
                                               class="@if(Request::url() == route('purchase.report')) active @endif">
                                                Purchase Report
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('consumption.report') or
                                                Request::url() == route('consumption.history') or
                                                Request::url() == route('consumption')
                                                )
                                            active
@endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span
                                                class="fa fa-calculator"></span> Consumption <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('consumption') }}"
                                               class="@if(Request::url() == route('consumption')) active @endif">
                                                Add Consumption
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('consumption.history') }}"
                                               class="@if(Request::url() == route('consumption.history')) active @endif">
                                                Consumption History
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('consumption.report') }}"
                                               class="@if(Request::url() == route('consumption.report')) active @endif">
                                                Consumption Report
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ route('inventory') }}"
                                       class="@if(Request::url() == route('inventory')) active @endif">
                                        <span class="fa fa-reorder"></span>Inventory
                                    </a>
                                </li>-->

                                <li>
                                    <a href="{{ route('kitchen') }}"
                                       class="@if(Request::url() == route('kitchen')) active @endif">
                                        <span class="fa fa-reorder"></span>Kitchen
                                    </a>
                                </li>

                            @endif

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
                <div class="online-status">
                    <label style="color:#41e1b6; display: none;" id="online-status">Online <span
                                class="fa fa-circle active-c"></span></label>
                    <label style="color:#ff0000c2; display: none;" id="offline-status">Offline <span
                                class="fa fa-circle d-active-c"> </span></label>
                </div>
            </nav>

        </div>
    </div>

</header>

<div class="main">
    @yield('content')
</div>
<!-- @include('admin_layouts.footer') -->
<script src="{{ asset('pos/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('pos/libraries/bootsrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('pos/libraries/vendor/modernizr-2.8.3.min.js') }}"></script>
<script src="{{ asset('pos/libraries/bootsnav/bootsnav.js') }}"></script>
<script src="{{ asset('pos/libraries/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('pos/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('pos/libraries/animations/wow.min.js') }}"></script>

<!-- extra Js -->

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
            @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";

    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif

    toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'slideUp';
    toastr.options.closeMethod = 'slideUp';
    toastr.options.closeButton = true;
</script>

@yield('js')
<script src="{{ asset('pos/js/main.js?ver?1.0') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('pos/libraries/fileinput/fileinput.min.js') }}"></script>

<script>
    $('.input-group.date').datepicker({format: "dd.mm.yyyy"});
</script>

<script>
    $(document).on('ready', function () {
        $("#input-b6").fileinput({
            showUpload: false,
            maxFileCount: 10,
            mainClass: "input-group-lg"
        });
    });
</script>


<script>

    // checking file access is true of false;

    function checkNetConnection() {
        jQuery.ajaxSetup({async: false});
        re = '';
        r = Math.round(Math.random() * 10000);
        $.get('{{ asset('Demo.jpg') }}', {subins: r}, function (d) {
            re = true;
        }).error(function () {
            re = false;
        });
        return re;
    }


    //checking current internet online/offline message shown or not

    if (localStorage.getItem('online_message_shown') == null) {
        localStorage.setItem('online_message_shown', 1);
        localStorage.setItem('offline_message_shown', 0);
    }


    if (checkNetConnection() === true && navigator.onLine) {
        $('#online-status').css('display', 'inline-block');
        $('#offline-status').css('display', 'none');
    }
    else {
        $('#online-status').css('display', 'none');
        $('#offline-status').css('display', 'inline-block');
    }


    // on interval it is checking systems current internet connection

    setInterval(function (e) {
        if (checkNetConnection() === true && navigator.onLine) {

            $('#online-status').css('display', 'inline-block');
            $('#offline-status').css('display', 'none');

            var msg_status1 = localStorage.getItem('online_message_shown');

            if (msg_status1 == 0) {

                $(document).ready(function () {
                    toastr.success('Your System is in Online Mode!', 'Internet Connected', {timeOut: 10000});
                });

                localStorage.setItem('online_message_shown', 1);
                localStorage.setItem('offline_message_shown', 0);

            }

        }

        else {

            $('#online-status').css('display', 'none');
            $('#offline-status').css('display', 'inline-block');

            var msg_status2 = localStorage.getItem('offline_message_shown');

            if (msg_status2 == 0) {

                $(document).ready(function () {
                    toastr.error('Your System is in Offline Mode!', 'Internet Disconnected', {timeOut: 10000});
                });

                localStorage.setItem('offline_message_shown', 1);
                localStorage.setItem('online_message_shown', 0);
            }


        }
    }, 10000);


    // clicking on synchronize button will sync all local data to database

    $(document).ready(function () {

        $('#synchronize').click(function () {


            if (checkNetConnection() === true && navigator.onLine) {

                var LocalOrders = JSON.parse(localStorage.getItem('local_orders_with_cart'));

                if (LocalOrders != null) {

                    if (LocalOrders.length > 0) {
                        for (i = 0; i < LocalOrders.length; i++) {

                            // getting order data from storage
                            prepare_order_data = LocalOrders[i].BackupOrders;

                            user_id = "{{ auth()->user()->id }}";

                            //data organizing for insertion
                            backup_order = {
                                table_id: prepare_order_data.table_id,
                                order_type_id: prepare_order_data.order_type_id,
                                person: prepare_order_data.person,
                                method_note: prepare_order_data.method_note,
                                method_id: prepare_order_data.method_id,
                                discount: prepare_order_data.discount,
                                taxs: prepare_order_data.taxs,
                                total: prepare_order_data.total,
                                paid: prepare_order_data.paid,
                                sub_total: prepare_order_data.sub_total,
                                tips: prepare_order_data.tip_amount,
                                user_id: user_id,
                                customer_id: prepare_order_data.customer_id,
                                csk_id: prepare_order_data.csk_id,
                                _token: '{{ csrf_token() }}'
                            };


                            $.ajax({
                                type: "post",
                                url: '/save_resource',
                                dataType: "json",
                                data: backup_order,
                                success: function (data) {

                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });

                            //getting cart data from storage
                            CartArray = LocalOrders[i].BackupCarts;
                            console.log(CartArray);


                            $.each(CartArray, function (key, value) {

                                var csk_id = value.csk_id;
                                var item_id = value.item_id;
                                var item_price = value.item_price;
                                var qty = value.qty;
                                var total = item_price * qty;
                                var discount = value.discount;

                                backup_cart = {
                                    csk_id: csk_id,
                                    item_id: item_id,
                                    item_price: item_price,
                                    qty: qty,
                                    total: total,
                                    discount: discount,
                                    _token: '{{ csrf_token() }}'
                                };

                                console.log(backup_cart);

                                $.ajax({
                                    dataType: 'json',
                                    type: 'post',
                                    url: '{{route('store.product.order')}}',
                                    data: backup_cart

                                }).done(function (data) {

                                });
                            });

                        }

                        toastr.success('Offline Data Saved Successfully!', 'Success Alert', {timeOut: 10000});

                        var local_orders_with_cart = [];

                        localStorage.setItem('local_orders_with_cart', JSON.stringify(local_orders_with_cart));
                    }
                    else {
                        console.log('No Data Found!');
                        toastr.error('Order Cart is Empty!', 'Notice Alert', {timeOut: 10000});
                    }

                }
                else {

                    console.log('No Data Found!');
                    toastr.error('You already took backup!', 'Notice Alert', {timeOut: 10000});
                }
            }
            else {
                toastr.error('You are in offline mode! to take backup make sure your system is in online', 'Notice Alert', {timeOut: 10000});
            }

        })
    })

</script>

</body>

</html>