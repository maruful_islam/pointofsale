<?php
$currentuserid = Auth::user()->id;
$currentusername = Auth::user()->name;

$order_id = App\Module::query()->where('slug', 'order')->value('id');
$report_id = App\Module::query()->where('slug', 'reports')->value('id');
$user_management_id = App\Module::query()->where('slug', 'user-management')->value('id');
$system_configuration_id = App\Module::query()->where('slug', 'system-configuration')->value('id');
$end_of_day_id = App\Module::query()->where('slug', 'end-of-day')->value('id');

$order_permission = App\Permission::query()->where(['user_id' => $currentuserid, 'module_id' => $order_id])->value('status');
$report_permission = App\Permission::query()->where(['user_id' => $currentuserid, 'module_id' => $report_id])->value('status');
$user_management_permission = App\Permission::query()->where(['user_id' => $currentuserid, 'module_id' => $user_management_id])->value('status');
$system_configuration_permission = App\Permission::query()->where(['user_id' => $currentuserid, 'module_id' => $system_configuration_id])->value('status');
$end_of_day_permission = App\Permission::query()->where(['user_id' => $currentuserid, 'module_id' => $end_of_day_id])->value('status');

?>
<header class="header-area bg-color">
    <input type="hidden" value="{{$currentuserid}}" id="curren-user">
    <input type="hidden" value="{{$currentusername}}" id="current-user-name">
    <div class="m-container">
        <div class="header-top">
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 mainlogo">
                    <div class="logo1">
                        <a href="{{ route('home') }}">
                            @php $sys_name = config('app.name') @endphp
                            @if(!is_null($sys_name))
                                <img src='{{asset(config('app.logo'))}}' alt="">
                            @else
                                <img src='{{asset("pos/images/logo.png")}}' alt="">
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-xs-3 col-md-1 col-sm-3 mainposlogo">
                    <div class="main-logo">
                        <a href="{{ route('home') }}">
                            @php $sys_name = config('app.name') @endphp
                            @if(!is_null($sys_name))

                                <h4 style="color: #fff; font-weight: bolder">{{ strtoupper($sys_name) }}</h4>
                                <!--<h4 style="color: #fff; font-weight: bolder"> JACKPOT </h4>-->

                            @else
                                <img src='{{asset("pos/images/logo.png")}}' alt="">

                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-xs-3 col-md-8 col-sm-3 mainposmenu">
                    <div class="account-info">
                        <nav class="navbar-default-custom" role="navigation">
                            <!-- Start Header Navigation -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target="#top-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <!-- End Header Navigation -->
                            <div class="collapse navbar-collapse" id="top-menu">
                                <ul class="nav navbar-nav">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                           aria-expanded="false"><span>User</span>
                                            <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu animated flipInX">
                                            @if($user_management_permission == 1)
                                                <li><a href="{{ route('user.permissions') }}"
                                                       class="@if(Request::url() == route('user.permissions')) active @endif">User
                                                        Permission</a></li>
                                            @endif

                                            <li><a href="{{ route('user.settings') }}"><i class="lnr lnr-user"></i>
                                                    <span>User Settings</span></a>
                                            </li>

                                        </ul>
                                    </li>
                                    @if($system_configuration_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Item & Category</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ action('ManageController@product_category') }}">Category</a></li>
                                                <li><a href="{{ action('ManageController@pro_item') }}">Item</a></li>
                                                <li><a href="{{ action('CondimentController@create') }}">Condiments</a></li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if($system_configuration_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Configurations</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{url('res/tables')}}">Table</a></li>
                                                <li><a href="{{url('res/taxs')}}">Tax</a></li>
                                                <li><a href="{{url('res/discount')}}">Discount</a></li>
                                                <li><a href="{{ action('ManageController@order_type') }}">Order Type</a></li>
                                                <li><a href="{{ action('ManageController@payment_methods') }}">Payment Methods</a></li>
                                                <li><a href="{{ route('currency.settings') }}">Currency Settings</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Inventory Config</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('r.m.category') }}">Raw Materials Category</a>
                                                </li>
                                                <li><a href="{{ route('r.m.items') }}">Raw Materials Items</a></li>
                                                <li><a href="{{ route('suppliers') }}">Supplier</a></li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if($user_management_permission == 1)
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Staff & Payroll</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('departments') }}">Departments</a></li>
                                                <li><a href="{{ route('designations') }}">Designations</a></li>
                                                <li><a href="{{ route('add.staff') }}">Add Staff</a></li>
                                                <li><a href="{{ route('staffs') }}">Manage Staff</a></li>
                                                <li><a href="{{ route('access.history') }}">Access History</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>Expenses</span>
                                                <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu animated flipInX">
                                                <li><a href="{{ route('expense.categories') }}">Expense Categories</a>
                                                </li>
                                                <li><a href="{{ route('expenses') }}">Expenses</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                           aria-expanded="false"><span>Account</span>
                                            <i class="fa fa-angle-down"></i></a>

                                        <ul class="dropdown-menu animated flipInX">
                                            <li><a href="{{ route('attendance.break') }}">Daily Access</a></li>

                                            <li><a href="{{ route('system.settings') }}">System Settings</a></li>

                                            <li><a href="{{ route('user.settings') }}"><i class="lnr lnr-user"></i>
                                                    <span>Account Settings</span></a>
                                            </li>

                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
    
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-1 ringerlogo">
                    <div class="ringer-logo">
                        <img src='{{asset("pos/images/ringer.png")}}' alt="">

                    </div>
                </div>
            </div>
        </div>

        <div class="manearea">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- End Header Navigation -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav navbar-center">

                            <li>
                                <a href="{{ route('home') }}"
                                   class="@if(Request::url() == route('home')) active @endif"><span
                                            class="fa fa-home"></span>Dashboard</a>
                            </li>

                            @if($system_configuration_permission == 1)
                                <li>
                                    <a href="{{ route('product.list') }}"
                                       class="@if(Request::url() == route('product.list')) active @endif"><span
                                                class="fa fa-shopping-bag"></span>Menu</a>
                                </li>
                            @endif

                            @if($end_of_day_permission == 1)
                                <li><a href="{{ route('end.of.day') }}"
                                       class="@if(Request::url() == route('end.of.day')) active @endif"><span
                                                class="fa fa-history"></span>End of Day</a>
                                </li>
                            @endif

                            @if($order_permission == 1)


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('customer.list') or
                                                Request::url() == route('suppliers')
                                                )
                                            active
@endif" data-toggle="dropdown">
                                        <span class="fa fa-reorder"></span> CRM</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('customer.list') }}"> Customers </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('suppliers') }}">Supplier</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                            @if($report_permission == 1)

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('get.report') or
                                                Request::url() == route('item.report') or
                                                Request::url() == route('expense.report')
                                                )
                                            active
@endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-sellsy"></span> Reports <span class="caret"></span></a>
                                    <ul class="dropdown-menu jackInTheBox animated ">
                                        <li>
                                            <a href="{{ route('get.report') }}"
                                               class="@if(Request::url() == route('get.report')) active @endif">
                                                Sales Reports
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('item.report') }}"
                                               class="@if(Request::url() == route('item.report')) active @endif">
                                                Items Report
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('expense.report') }}"
                                               class="@if(Request::url() == route('expense.report')) active @endif">
                                                Expense Report
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('profit.loss.report') }}"
                                               class="@if(Request::url() == route('profit.loss.report')) active @endif">
                                                Profit/Loss Report
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                            @endif

                            @if($order_permission == 1)
                                <li><a href="{{ action('ManageController@order') }}" class="@if(Request::url() == url('/res/order')) active @endif"><span class="fa fa-retweet"></span>Order </a></li>

                                <!-- <li><a href="javascript:void(0)" id="synchronize"><span class="fa fa-download"></span>Sync
                                        Offline Data</a></li> -->

                               <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(
                                                Request::url() == route('purchase.requisition') or
                                                Request::url() == route('purchase.requisition.list') or
                                                Request::url() == route('purchase') or
                                                Request::url() == route('purchase.history') or
                                                Request::url() == route('purchase.report')
                                                )
                                            active
@endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-shopping-cart"></span> Purchase <span
                                                class="caret"></span></a>
                                    <ul class="dropdown-menu jackInTheBox animated ">
                                        <li>
                                            <a href="{{ route('purchase.requisition') }}"
                                               class="@if(Request::url() == route('purchase.requisition')) active @endif">
                                                Purchase Requisition
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('purchase.requisition.list') }}"
                                               class="@if(Request::url() == route('purchase.requisition.list')) active @endif">
                                                Purchase Requisitions List
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('purchase') }}"
                                               class="@if(Request::url() == route('purchase')) active @endif">
                                                Purchase Order
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('purchase') }}"
                                               class="@if(Request::url() == route('purchase')) active @endif">
                                                Add Purchase
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('purchase.history') }}"
                                               class="@if(Request::url() == route('purchase.history')) active @endif">
                                                Purchase History
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('purchase.report') }}"
                                               class="@if(Request::url() == route('purchase.report')) active @endif">
                                                Purchase Report
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                             <li class="dropdown">
                                    <a href="#" class="dropdown-toggle
                                                @if(

                                                Request::url() == route('consumption.report') or
                                                Request::url() == route('consumption.history') or
                                                Request::url() == route('consumption') or
                                                Request::url() == route('inventory')
                                                )
                                            active
@endif" data-toggle="dropdown"> <span
                                                class="fa fa-shopping-basket"></span> Inventory</a>
                                    <ul class="dropdown-menu jackInTheBox animated">
                                        <li>
                                            <a href="{{ route('consumption') }}"
                                               class="@if(Request::url() == route('consumption')) active @endif">
                                                Stock Out
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('consumption.history') }}"
                                               class="@if(Request::url() == route('consumption.history')) active @endif">
                                                Stock Out History
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('consumption.report') }}"
                                               class="@if(Request::url() == route('consumption.report')) active @endif">
                                                Stock Out Report
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('inventory') }}"
                                               class="@if(Request::url() == route('inventory')) active @endif">
                                                In Stock
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ route('kitchen') }}"
                                       class="@if(Request::url() == route('kitchen')) active @endif">
                                        <span class="fa fa-reorder"></span>Kitchen
                                    </a>
                                </li>

                            @endif

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
                <div class="online-status">
                    <label style="color:#41e1b6; display: none;" id="online-status">Online <span
                                class="fa fa-circle active-c"></span></label>
                    <label style="color:#ff0000c2; display: none;" id="offline-status">Offline <span
                                class="fa fa-circle d-active-c"> </span></label>
                </div>
            </nav>

        </div>
    </div>

</header>