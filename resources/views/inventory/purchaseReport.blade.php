@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

    <style>
        .btn-group-sm > .btn, .btn-sm {
            font-size: 9px;
            border-radius: 100px;
        }

        .btn-primary {
            background-color: #fb787e;
            border-color: #fb787e;
            margin: 3px;
        }
    </style>

@endsection

@section('content')
    <div class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">


                            <div class="kcol-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="prport-tatepiker">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th>Form Date: <input type="text" class="span2 date_input" name="start_date"
                                                                  @if(isset($_GET['start_date'])) value="{{ $_GET['start_date'] }}"
                                                                  @endif required></th>

                                            <th>To Date: <input type="text" class="span2 date_input" name="end_date"
                                                                @if(isset($_GET['end_date'])) value="{{ $_GET['end_date'] }}"
                                                                @endif required></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="report-search text-right">
                                    <br>
                                    <select class="re-monthly" name="sort_type" data-role="sorter" id="sort-report">
                                        <option value="0"
                                                @if(isset($sort_type)) @if($sort_type == 0) selected @endif @endif>Sort By

                                        </option>

                                        <option value="1"
                                                @if(isset($sort_type)) @if($sort_type == 1) selected @endif @endif>
                                            Sort By Category
                                        </option>

                                        <option value="2"
                                                @if(isset($sort_type)) @if($sort_type == 2) selected @endif @endif>Sort
                                            by Item
                                        </option>

                                    </select>

                                    <!-- Category List -->

                                    <select class="re-monthly" name="category_id" data-role="sorter"
                                            style="display: @if(isset($category_display)) {{ $category_display }}@else none @endif"
                                            id="category-list">
                                        <option selected="selected">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                    @if(isset($category_display))
                                                    @if(isset($category_id))
                                                    @if($category_id == $category->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    <!-- Item List -->

                                    <select class="re-monthly" name="item_id" data-role="sorter"
                                            style="display: @if(isset($item_display)) {{ $item_display }}@else none @endif"
                                            id="item-list">
                                        <option selected="selected">Select Item</option>
                                        @foreach($items as $item)
                                            <option value="{{ $item->id }}"
                                                    @if(isset($item_display))
                                                    @if(isset($item_id))
                                                    @if($item_id == $item->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $item->name }}</option>
                                        @endforeach
                                    </select>



                                    <button class="btn btn-button">Search</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h2>Purchase Report</h2><br>
                        <h4>MAHARAJA PALACE RESTAURANT</h4><br>
                        @if(isset($_GET['start_date']))
                            <h5>Report From: <b>{{ date('l, dS F Y', strtotime($_GET['start_date'])) }} </b>
                                to <b>{{ date('l, dS F Y', strtotime($_GET['end_date'])) }}</b></h5>
                        @else
                            <h5>Date: {{ date('l, dS F Y') }} </h5>
                        @endif
                    </div>
                    <div class="report-table2">
                        <!--Table-->

                        @if(count($reports) > 0)
                            <table class="table">

                                <!--Table head-->
                                <thead class="blue-grey lighten-4">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Category Name</th>
                                    <th class="text-center">Item Name</th>
                                    <th class="text-center">Quantity Bought</th>
                                    <th class="text-center">Total Cost</th>
                                    <th class="text-center">Last Bought</th>
                                </tr>
                                </thead>
                                <!--Table head-->

                                <!--Table body-->
                                <tbody>

                                @foreach($reports as $key=>$report)

                                    <tr>
                                        <th scope="row" class="text-center">{{ $key+1 }}</th>

                                        <th scope="row" class="text-center">{{ $report['category_name'] }}</th>

                                        <td class="text-center">
                                            {{ $report['item_name'] }}
                                        </td>


                                        <td class="text-center">
                                            {{ $report['quantity'] }} {{ $report['unit'] }}
                                        </td>

                                        <td class="text-center">
                                            {{ $report['total_price'] }} {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            {{ date('l, dS F Y h:i A', strtotime($report['last_bought'])) }}
                                        </td>
                                    </tr>

                                @endforeach
                                <tr>
                                    <td colspan="4" class="text-right" style="font-weight: bold;">Total Purchase Cost:
                                    </td>
                                    <td colspan="2" style="font-weight: bold;" class="text-center">
                                        &nbsp; @php echo number_format($total_sum, 3). config('app.currency'); @endphp
                                    </td>
                                </tr>
                                </tbody>
                                <!--Table body-->
                            </table>
                        @else
                            <div class="alert alert-danger">
                                No Result Found!
                            </div>
                    @endif

                    <!--Table-->
                    </div>
                </div>

                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color" href="javascript:void(0)"
                       onclick="window.print()">Print This Report</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #print, #print * {
                visibility: visible;
            }

            #print {
                position: absolute;
                left: 0;
                top: 0;
            }

        }
    </style>

    <script>
        $(document).ready(function () {
            $('#sort-report').on('change', function () {
                var sort_type = $(this).val();

                if (sort_type == 1) {
                    $('#category-list').css('display', 'inline-block');
                    $('#item-list').css('display', 'none');
                }
                else if (sort_type == 2) {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'inline-block');
                }
                else {
                    $('#category-list').css('display', 'none');
                    $('#item-list').css('display', 'none');
                }

            });
        });
    </script>

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
        });
    </script>

    <script>

    </script>

@endsection