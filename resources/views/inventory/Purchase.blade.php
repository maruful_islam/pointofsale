@extends('admin_layouts.default')

@section('css')
    <style>
        button.btn-submit {
            border: none;
            background: #EE1C25;
            padding: 7px 25px;
            border-radius: 16px;
            font-size: 14px;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form id="purchase_form" enctype="multipart/form-data">

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Purchase Info</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->

                            <div class="form-group">
                                <label for="usr">Requisition ID:</label>
                                {{ Form::select('requisition_id',$repository->requisitions(),null,['class'=>'form-control','id'=>'requisition_id','placeholder'=>'Select Requisition ID']) }}
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Select Category:</label>--}}
                                {{--<select title name="" class="form-control" id="category">--}}
                                    {{--<option value="0">Select Category</option>--}}
                                    {{--@foreach($categories as $category)--}}
                                        {{--<option value="{{ $category->id }}">{{ $category->name }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Select Item:</label>--}}
                                {{--<select title name="" class="form-control" id="item">--}}
                                    {{--<option value="0">Select Item</option>--}}

                                {{--</select>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-9">--}}
                                        {{--<label for="usr">Quantity</label>--}}
                                        {{--<input title type="text" name="quantity" id="quantity" class="form-control"--}}
                                               {{--required>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-3">--}}
                                        {{--<label for="">Unit</label><br>--}}
                                        {{--<b style="font-weight: bold; font-size: 18px" id="unit-type">KG</b>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Price:</label>--}}
                                {{--<input title type="text" name="name" id="price" required class="form-control">--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="usr">Select Supplier:</label>--}}
                                {{--<select title name="" class="form-control" id="supplier">--}}
                                    {{--<option value="0">Select Supplier</option>--}}
                                    {{--@foreach($suppliers as $supplier)--}}
                                        {{--<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<button class="btn-submit btn-primary" id="add-to-cart" type="submit">Add To Cart--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Purchased Cart's Items</h2>

                            {{--<form id="purchased-cart" >--}}
                            {{ Form::open(['action'=>'InventoryController@StorePurchasedInfo','method'=>'post','id'=>'purchased-cart']) }}
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Supplier</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td colspan="8">
                                        <div class="alert alert-danger">
                                            No Item Added Yet!
                                        </div>
                                    </td>

                                </tr>

                                @include('inventory.requisition_list_item')

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="8" style="text-align: right">

                                        {{ Form::hidden('total_cost',null,['class'=>'total']) }}
                                        Total Cost: <b id="total" class="total">0</b>

                                        <hr style="margin: 5px 0;">

                                        Date: @php echo date('l, dS F Y h:i A', strtotime('+5 Hours')) @endphp

                                        <hr style="margin: 5px 0;">

                                        Added By: @php echo auth()->user()->name @endphp


                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="8" style="text-align: center">
                                        <button class="btn-submit btn btn-danger" type="submit">
                                            Save Purchase Info
                                        </button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            {{--</form>--}}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')

    <!-- Items Edit Pop Up -->

    <div id="edit-cart" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Items Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('category.update') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">

                            <input type="hidden" name="id" id="cat-id">

                            <div class="form-group">
                                <label for="usr">Select Category:</label>
                                <select title name="" class="form-control" id="category">
                                    <option value="0">Select Category</option>
                                    <option value="">Category 1</option>
                                    <option value="">Category 2</option>
                                    <option value="">Category 3</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="usr">Select Item:</label>
                                <select title name="" class="form-control" id="item">
                                    <option value="0">Select Item</option>
                                    <option value="">Item 1</option>
                                    <option value="">Item 2</option>
                                    <option value="">Item 3</option>
                                </select>
                            </div>

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="usr">Quantity</label>
                                        <input title type="text" name="quantity" id="quantity" class="form-control">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="">Unit</label>
                                        <b style="font-weight: bold; font-size: 18px" id="unit-type">KG</b>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="usr">Price:</label>
                                <input title type="text" name="name" id="price" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Select Supplier:</label>
                                <select title name="" class="form-control" id="supplier">
                                    <option value="">Supplier 1</option>
                                    <option value="">Supplier 2</option>
                                    <option value="">Supplier 3</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">Update Cart</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <script>

        $(document).ready(function () {

            $('#category').on('change', function (e) {
                category_id = this.value;

                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: '{{ url('/get-item-by-category/') }}' + '/' + category_id,
                    success: function (data) {

                        $('#item').html('').append('<option value="0">Select Item</option>');

                        $.each(data, function (key, value) {
                            $('#item').append('<option value="' + value.id + '">' + value.name + '</option>')
                        });

                    }
                })
            });

            $('#item').on('change', function (e) {
                item_id = this.value;

                $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: '{{ url('/get-item-unit-by-id/') }}' + '/' + item_id,
                    success: function (data) {

                        $('#unit-type').html(data);

                    }
                })
            });

            CartInfo = JSON.parse(localStorage.getItem('purchased_cart'));

            if (CartInfo == null) {
                carts = [];
            }
            else {
                carts = CartInfo;
            }

            // ADD TO CART BUTTON TRIGGER

            $('#add-to-cart').on('click', function (e) {
                e.preventDefault();

                category_id = $('#category').val();
                category_name = $('#category :selected').text();

                item_id = $('#item').val();
                item_name = $('#item :selected').text();

                qty = $('#quantity').val();
                measurement = $('#unit-type').text();

                price = $('#price').val();

                supplier_id = $('#supplier').val();

                if (supplier_id == 0) {
                    supplier_name = 'N/A';
                }
                else {
                    supplier_name = $('#supplier :selected').text();
                }


                if (category_id === 0) {
                    alert('Please Select Category');
                }
                else if (item_id === 0) {
                    alert('Please Select Item');
                }
                else {


                    // CHECKING IF THIS ITEM EXISTS OR NOT

                    updateCartItem = 0;

                    if (carts !== null) {
                        for (i = 0; carts.length > i; i++) {

                            if (parseInt(carts[i].item_id) === parseInt(item_id)) {

                                updateCartItem = carts[i];

                                carts.splice(i, 1);

                            }
                        }
                    }

                    // UPDATING CART IF HAS EXIST ITEM ON CART

                    if (updateCartItem !== 0) {

                        updateQty = parseInt(updateCartItem.qty) + parseInt(qty);
                        updatePrice = parseInt(updateCartItem.price) + parseInt(price);

                        cart = {
                            category_id: category_id,
                            category_name: category_name,
                            item_id: item_id,
                            item_name: item_name,
                            qty: updateQty,
                            measurement: measurement,
                            price: updatePrice,
                            supplier_id: supplier_id,
                            supplier_name: supplier_name
                        };


                    }
                    else {

                        cart = {
                            category_id: category_id,
                            category_name: category_name,
                            item_id: item_id,
                            item_name: item_name,
                            qty: qty,
                            measurement: measurement,
                            price: price,
                            supplier_id: supplier_id,
                            supplier_name: supplier_name
                        };

                    }

                    carts.push(cart);

                    localStorage.setItem('purchased_cart', JSON.stringify(carts));

                    $('#purchase_form')[0].reset();

                    data = JSON.parse(localStorage.getItem('purchased_cart'));

                    manageRow(data);
                    totalPrice(data);

                    $('#item').text('<option value="0" selected>Select Item</option>')

                }


            });

            data = JSON.parse(localStorage.getItem('purchased_cart'));

            manageRow(data);

            totalPrice(data);

            function totalPrice(data) {
                total_price = 0;

                $.each(data, function (key, value) {
                    total_price += parseInt(value.price);
                });

                $('#total').text(total_price + ' {{ config('app.currency')}}');

                return total_price;
            }


            /* Add new Item table row */
            function manageRow(data) {
                var rows = '';

                $.each(data, function (key, value) {

                    rows = rows + '<tr>';
                    rows = rows + '<td>' + (key + 1) + '</td>';
                    rows = rows + '<td>' + value.category_name + '</td>';
                    rows = rows + '<td>' + value.item_name + '</td>';
                    rows = rows + '<td>' + value.qty + '</td>';
                    rows = rows + '<td>' + value.measurement + '</td>';
                    rows = rows + '<td>' + value.price + '</td>';
                    rows = rows + '<td>' + value.supplier_name + '</td>';

                    rows = rows + '<td data-id="' + key + '" data-cat-name="' + value.category_name + '" data-item-name="' + value.item_name + '" >';

                    rows = rows + '<button class="btn btn-danger btn-sm  item-delete"  data-item_id="' + value.item_id + '" >' +
                        '<i class="fa fa-trash" aria-hidden="true"></i>' +
                        '</button>';
                    rows = rows + '</td>';
                    rows = rows + '</tr>';

                });

                if (rows === '') {
                    rows = '<tr><td colspan="8"><div class="alert alert-danger">No Item Added Yet!</div></td></tr>';
                }

                $("tbody").html(rows);
            }

            $('#save-purchase').on('click', function (e) {
                e.preventDefault();

                data = JSON.parse(localStorage.getItem('purchased_cart'));

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    data: {data: data},
                    url: '{{ route('store.purchase') }}',
                    success: function (data) {
                        console.log('successful');

                        toastr.success('Purchase Information Saved Successfully.', 'Success Alert', {timeOut: 5000});
                        toastr.options.showMethod = 'slideDown';
                        toastr.options.hideMethod = 'slideUp';
                        toastr.options.closeMethod = 'slideUp';
                        toastr.options.closeButton = true;

                        localStorage.removeItem('purchased_cart');


                        rows = '<tr><td colspan="8"><div class="alert alert-success">All Item Info Added to stock!</div></td></tr>';


                        $("tbody").html(rows);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                })

            });


            $('body').on('click', '.item-delete', function (e) {
                e.preventDefault();
                id = $(this).data('item_id');

                carts = CartInfo;
                updateCartItem = 0;

                if (carts !== null)
                {
                    for (i = 0; carts.length > i; i++) {

                        console.log('in loop');
                        if (parseInt(carts[i].item_id) === parseInt(id) ){

                            console.log('item found');
                            carts.splice(i, 1);

                        }
                    }
                }

                console.log(carts);

                if(carts.length > 0)
                {
                    localStorage.setItem('purchased_cart', JSON.stringify(carts));
                }
                else {
                    localStorage.removeItem('purchased_cart');
                }


                data = JSON.parse(localStorage.getItem('purchased_cart'));

                manageRow(data);
                totalPrice(data);

            });

            $("#requisition_id").change(function(){
                var requisition = $(this).val();
                var csrf = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ url('requisition-call') }}",
                    data: {requisition:requisition,_token:csrf},
                    type: "POST"
                }).done(function(e){
                    $("#purchased-cart table tbody").html(e);
                })
            })


        });


    </script>

    <script>

    </script>



@endsection
