@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Total Item Used in Previous Days</h2>
                            <form id="consumption-form" action="{{ route('stock.update') }}" method="post">
                                {{ csrf_field() }}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Item</th>
                                        <th>Quantity Used</th>
                                        <th>Last Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($histories as $key=>$history)
                                        <tr>
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td>
                                                {{ $history->getRMCName($history->items->rmc_id) }}
                                            </td>
                                            <td>
                                                {{ $history->items->name }}
                                            </td>
                                            <td>
                                                {{ $history->quantity }} {{ $history->items->measurement }}
                                            </td>

                                            <td>
                                                {{ date('l, dS F Y h:i A', strtotime($history->updated_at) + 3600*5) }}
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>

                                </table>

                                @if(count($histories) > 0)

                                    <div class="alert alert-info">
                                        Showing {{ $histories->firstItem() }} to {{ $histories->lastItem() }} Consumption of
                                        Total {{ $histories->total() }} Consumptions.
                                    </div>

                                    <nav aria-label="Page navigation">
                                        @if ($histories->lastPage() > 1)
                                            <ul class="pagination pagination-sm">
                                                <li class="page-item {{ ($histories->currentPage() == 1) ? ' disabled' : '' }}">
                                                    <a class="page-link" href="{{ $histories->url(1) }}">Previous</a>
                                                </li>
                                                @for ($i = 1; $i <= $histories->lastPage(); $i++)
                                                    <li class="page-item {{ ($histories->currentPage() == $i) ? 'page active' : '' }}">
                                                        <a class="page-link" href="{{ $histories->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @endfor
                                                <li class="page-item {{ ($histories->currentPage() == $histories->lastPage()) ? ' disabled' : '' }}">
                                                    <a class="page-link"
                                                       href="{{ $histories->url($histories->currentPage()+1) }}">Next</a>
                                                </li>
                                            </ul>
                                        @endif
                                    </nav>
                                @else
                                    <div class="alert alert-danger">
                                        No Result Found!
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

    </script>

@endsection
