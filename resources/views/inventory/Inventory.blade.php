@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="report-header">
                        <div class="row">
                            <div class="kcol-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="prport-tatepiker">
                                    <form action="" method="get">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Search By Item Name:
                                                    <input style="width: 80%" type="text" class="span2" name="search"
                                                           id="search-inventory">
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Current Inventory Items</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Item</th>
                                    <th>Quantity Left</th>
                                    <th>Last Updated</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($stocks as $key=>$stock)
                                    <tr>
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>
                                            @if(isset($stock->items->rmc_id))

                                            {{ $stock->getRMCName($stock->items->rmc_id) }}

                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($stock->items->name))

                                            {{ $stock->items->name }}

                                            @endif
                                        </td>
                                        <td class="@if($stock->quantity <= $stock->items->warning_level) alert alert-danger @endif">
                                            {{ $stock->quantity }}  {{ $stock->items->measurement }}
                                        </td>
                                        <td>
                                            {{ date('l, dS F Y h:i A', strtotime($stock->updated_at) + 3600*5) }}
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function () {

            var current_output = $('tbody').html();

            /* Search Inventory */
            $("#search-inventory").keyup(function () {

                var str = $("#search-inventory").val();

                if (str.length > 1) {
                    $.get("{{ url('inventory/search?string=') }}" + str, function (data) {

                        if (data !== false) {

                            if (data.warning_level >= data.qty) {
                                warn = 'alert alert-danger';
                            }
                            else {
                                warn = '';
                            }


                            $('tbody').html('<tr>' +
                                '<td>' + data.key + '</td>' +
                                '<td>' + data.category + '</td>' +
                                '<td>' + data.item + '</td>' +
                                '<td class=" ' + warn + ' "> ' + data.qty + '</td>' +
                                '<td>' + data.time + '</td>' +
                                '</tr>')
                        }
                        else {

                            $('tbody').html('<tr><td colspan="5"><div class="alert dark alert-danger">Nothing Found!</div></td></tr>')
                        }

                    });
                }
                else {
                    $('tbody').html(current_output);
                }
            });
        })
    </script>

@endsection
