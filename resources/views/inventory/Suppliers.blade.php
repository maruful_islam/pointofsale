@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="{{ route('create.supplier') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Supplier</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Email:</label>
                                <input type="text" name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Phone:</label>
                                <input type="text" name="phone" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Address:</label>
                                <input type="text" name="address" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="is_active" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Supplier List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($suppliers  as $key => $supplier)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$supplier->name}}</td>
                                        <td>{{$supplier->email}}</td>
                                        <td>{{$supplier->phone}}</td>
                                        <td>{{$supplier->address}}</td>
                                        <td>
                                            @if($supplier->is_active === "active")
                                                <a data-id="{{ $supplier->id }}"
                                                   data-status="{{ $supplier->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $supplier->id }}"
                                                   data-status="{{ $supplier->is_active }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>

                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $supplier->id }}" data-name="{{ $supplier->name }}"
                                                   data-email="{{ $supplier->email }}"
                                                   data-phone="{{ $supplier->phone }}"
                                                   data-address="{{ $supplier->address }}"
                                                   data-status="{{ $supplier->is_active }}"
                                                   data-target="#edit-supplier"
                                                   class="user-edits"
                                                   data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/delete-raw-materials-supplier/{{$supplier->id}}"
                                                   class="user-removed" data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                            class="fa fa-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach




                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Items Edit Pop Up -->

    <div id="edit-supplier" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Suppliers Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('update.supplier') }}" method="post" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="user-inputbox">

                            <input type="hidden" name="id" id="s-id">

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" id="s-name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Email:</label>
                                <input type="text" name="email" id="s-email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Phone:</label>
                                <input type="text" name="phone" id="s-phone" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Address:</label>
                                <input type="text" name="address" id="s-address" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="is_active" id="s-status" value="active"
                                           checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are You Sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this supplier information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $('#edit-supplier').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var email = $(e.relatedTarget).data('email');
            var phone = $(e.relatedTarget).data('phone');
            var address = $(e.relatedTarget).data('address');
            var status = $(e.relatedTarget).data('status');


            $('#s-id').val(id);
            $('#s-name').val(name);
            $('#s-email').val(email);
            $('#s-phone').val(phone);
            $('#s-address').val(address);

            if (status === 'active') {
                $('#s-status').prop('checked', true);
            }
            else {
                $('#s-status').prop('checked', false);
            }
        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/update-supplier')}}/' + id);


        });


    </script>



@endsection
