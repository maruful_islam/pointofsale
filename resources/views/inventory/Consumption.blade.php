@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Current Inventory Items</h2>
                            <form id="consumption-form" action="{{ route('stock.update') }}" method="post">
                                {{ csrf_field() }}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Item</th>
                                        <th>Current Quantity</th>
                                        <th>Quantity Used</th>
                                        <th>Last Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($stocks as $key=>$stock)
                                        <tr>
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td>
                                                {{ $stock->getRMCName($stock->items->rmc_id) }}
                                            </td>
                                            <td>
                                                {{ $stock->items->name }}
                                                <input type="hidden" name="rmi_id[]" value="{{ $stock->rmi_id }}">
                                            </td>
                                            <td class="@if($stock->quantity < $stock->items->warning_level) alert alert-danger @endif">
                                                {{ $stock->quantity }} {{ $stock->items->measurement }}
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="qty_used[]"
                                                       id="qty-used" value="0" step="0.1">
                                            </td>
                                            <td>
                                                {{ date('l, dS F Y h:i A', strtotime($stock->updated_at) + 3600*5) }}
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>

                                    <tfoot>

                                    <tr>
                                        <td colspan="8" style="text-align: center">
                                            <button class="btn-submit btn btn-danger" id="update-stock" type="submit">
                                                Update Stock Info
                                            </button>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

    </script>

@endsection
