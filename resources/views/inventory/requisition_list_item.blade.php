@php $x = 1 @endphp

@foreach($carts as $cart)
    <tr class="item-row-{{ $x }}">
        <td>{{ $x++ }}</td>
        <td>
            {{ Form::hidden('rmc_id[]',$cart->rmc_id) }}
            {{ $cart->category->name }}
        </td>
        <td>
            {{ Form::hidden('rmi_id[]',$cart->id) }}
            {{ $cart->name }}
        </td>
        <td>
            {{ Form::text('quantity[]',$cart->pivot->qty,['class'=>'form-control quantity','id'=>'quantity-'.$x]) }}
        </td>
        <td></td>
        <td>
            {{ Form::text('price[]',null,['class'=>'form-control price','id'=>'price-'.$x]) }}
            {{ Form::hidden('amount[]',null,['class'=>'amount','id'=>'amount-'.$x]) }}
        </td>
        <td>{{ Form::select('supplier_id[]',$repository->suppliers(),null,['class'=>'form-control']) }}</td>
        <td>
            <button type="button" class="btn btn-danger btn-sm btn-icon icon-left remove" style="margin-top: 3px;"><i class="fa fa-trash"></i></button>
        </td>
    </tr>

    <script>
        $(".remove").click(function () {
            var div = $(this).closest('tr').remove();
        });

        $(document).on('keyup','input[id^="price"]',function(){
            for(var i=0;i<10;i++) {
                var quantity = $("#quantity-"+i).val();
                var price = $("#price-"+i).val();
                var amount = quantity * price;
                $("#amount-"+i).val(amount);
            }

            var total = 0;
            $('.amount').each(function() {
                total += Number($(this).val());
            });
            $("#total").text(total);
            $(".total").val(total);

        })

    </script>

@endforeach

