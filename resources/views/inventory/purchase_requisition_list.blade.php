@extends('admin_layouts.default')
@section('content')

    <div id="app">
        <div class="container page-padding-top">
            <div id="report-desc" class="user-list-boxarea">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="user-list">
                            <div class="user-list-table table-responsive">
                                <h2 class="user-list-title">Purchase Requisitions</h2>

                                {{ csrf_field() }}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#Requision ID</th>
                                        <th width="15%">Remarks</th>
                                        <th>Cart Information</th>
                                        <th>User</th>
                                        <th>Status</th>
                                        <th>Print</th>
                                        <th width="10%">Time</th>
                                        <th width="15%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($requisitions as $key=>$requisition)
                                        <tr>
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td>
                                                {{ $requisition->requisition_id }}
                                            </td>
                                            <td>
                                                {{ $requisition->remarks }}
                                            </td>
                                            <td>
                                                @php $carts = json_decode($requisition->carts, true); @endphp

                                                @foreach($carts as $cart)
                                                    <b>
                                                        Category:</b> {{ App\RMCategory::where('id', $cart['rmc_id'])->value('name') }}
                                                    <br>
                                                    <b>
                                                        Item:</b> {{ App\RMItem::where('id', $cart['rmi_id'])->value('name') }}
                                                    <br>
                                                    <b>
                                                        Qty:</b> {{ $cart['qty'] }} {{ App\RMItem::where('id', $cart['rmi_id'])->value('measurement') }}
                                                    <hr style="margin-top: 5px; margin-bottom: 5px;">
                                                @endforeach
                                            </td>
                                            <td>
                                                {{ $requisition->user->name }}
                                            </td>
                                            <td>
                                                @if($requisition->status == 1)
                                                    <a href="/change-status-purchase-requisition/{{$requisition->id}}/{{ $requisition->status }}"
                                                       onclick="return confirm('Are sure about to Change this requisition status to Pending?')"
                                                       class="btn btn-success btn-sm">Approved</a>
                                                @else

                                                    <a href="/change-status-purchase-requisition/{{$requisition->id}}/{{ $requisition->status }}"
                                                       onclick="return confirm('Are sure about to Change this requisition status to Approved?')"
                                                       class="btn btn-danger btn-sm">Pending</a>
                                                @endif
                                            </td>
                                            <td>
                                                {{--<a class="btn btn-info pos-small-btn" href="javascript:void(0)"--}}
                                                {{--onclick="printReport({{$requisition->carts}})">Thermal Print</a>--}}
                                                <a class="btn btn-info pos-small-btn" href="javascript:void(0)"
                                                   onclick="printReport({{$requisition->carts}})">Thermal
                                                    Print</a>
                                            </td>

                                            <td>
                                                {{ date('l, d.m.Y h:i A', strtotime($requisition->created_at)) }}
                                            </td>

                                            <td>
                                                <div class="user-action">
                                                    <a href="#"
                                                       title="Edit">
                                                        <span class="fa fa-edit"></span>
                                                    </a>
                                                    <a href="/delete-purchase-requisition/{{$requisition->id}}"
                                                       class="user-removed" data-toggle="tooltip" title="Delete"
                                                       onclick="return confirm('Are You Sure Delete This Requisition?');"><span
                                                                class="fa fa-trash"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>

                                </table>

                                @if(count($requisitions) > 0)

                                    <div class="alert alert-info">
                                        Showing {{ $requisitions->firstItem() }} to {{ $requisitions->lastItem() }}
                                        Requisition of
                                        Total {{ $requisitions->total() }} Requisitions.
                                    </div>

                                    <nav aria-label="Page navigation">
                                        @if ($requisitions->lastPage() > 1)
                                            <ul class="pagination pagination-sm">
                                                <li class="page-item {{ ($requisitions->currentPage() == 1) ? ' disabled' : '' }}">
                                                    <a class="page-link" href="{{ $requisitions->url(1) }}">Previous</a>
                                                </li>
                                                @for ($i = 1; $i <= $requisitions->lastPage(); $i++)
                                                    <li class="page-item {{ ($requisitions->currentPage() == $i) ? 'page active' : '' }}">
                                                        <a class="page-link"
                                                           href="{{ $requisitions->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @endfor
                                                <li class="page-item {{ ($requisitions->currentPage() == $requisitions->lastPage()) ? ' disabled' : '' }}">
                                                    <a class="page-link"
                                                       href="{{ $requisitions->url($requisitions->currentPage()+1) }}">Next</a>
                                                </li>
                                            </ul>
                                        @endif
                                    </nav>
                                @else
                                    <div class="alert alert-danger">
                                        No Result Found!
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="container">

            <div id="thermal-print" class="invoice-page" style="display: none">
                <div class="invoice-area">
                    <div class="inv-header">
                        <div class="invo-logo text-center">
                            <strong>{{ strtoupper(config('app.name'))}}</strong>

                        </div>
                        <div class="invo-header-content" id="clipPolygon">
                            <div class="invo-header-info">
                                <ul>
                                    <li><span id="show-bill-phone"> Phone : {{ config('app.phone')}}</span></li>
                                    <li><span id="show-bill-email"> Email : {{ config('app.email')}}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="invoice-center">

                        <hr>

                        <ul style="padding-left: 10px;">
                            <li><b>Date:</b> <span id="show-bill-current-date">{{ date('d M Y') }}</span></li>
                            <li><b>Time:</b> <span id="show-bill-current-time">{{ date('h:i A')  }}</span></li>

                        </ul>

                        <hr>

                    </div>
                    <div class="line"></div>
                    <div class="voice-tables">
                        <div class="voic-table table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th width="10%">Qty</th>
                                </tr>
                                </thead>
                                <tbody id="show-bill-print">


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')

    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #thermal-print, #thermal-print * {
                visibility: visible;
            }

            #thermal-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .invoice-center ul li {
                width: 48%;
            }

            .inv-header {
                border-bottom: none;
            }

        }
    </style>


    <script>
        function printReport(data) {
            carts = [];

            for (i = 0; i < data.length; i++) {
                const item_id = data[i].rmi_id;

                $.ajax({
                    type: "get",
                    url: '/get-item-info-by-id' + '/' + item_id,
                    dataType: "json",
                    async: false,
                    success: function (success) {
                        var cart = {
                            name: success.name,
                            qty: data[i].qty,
                            unit: success.measurement
                        };
                        carts.push(cart);

                    },
                    error: function (error) {
                        console.log('Error:', error);
                    }
                });


            }

            var rows = '';

            $.each(carts, function (key, value) {

                rows = rows + '<tr>';
                rows = rows + '<td style="text-align: left">' + (key + 1) + '</td>';
                rows = rows + '<td style="text-align: left">' + value.name + '</td>';
                rows = rows + '<td style="text-align: center">' + value.qty + ' ' + value.unit + '</td>';
                rows = rows + '</tr>';

            });

            $("tbody#show-bill-print").html(rows);

            var PhoneText = 'Phone : {{ config('app.phone')}}';
            var EmailText = 'Email : {{ config('app.email')}}';
            $('#show-bill-phone').html(PhoneText);
            $('#show-bill-email').html(EmailText);


            $('#thermal-print').css('display', 'block');
            $('#report-desc').css('display', 'none');
            window.print();
            $('#thermal-print').css('display', 'none');
            $('#report-desc').css('display', 'block');


        }
    </script>
@endsection
