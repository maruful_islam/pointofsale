@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Purchase History</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Purchase Description</th>
                                    <th>Total Cost</th>
                                    <th>Added By</th>
                                    <th>Last Updated</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($histories as $key=>$history)
                                    <tr>
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>
                                            @foreach($history->purchased_item as $key=>$item)
                                                <b>#</b>{{ $key+1 }}:
                                                <b>Category:</b> {{ $history->getRMCName($item['rmc_id']) }},
                                                <b>Item: </b> {{ $history->getRMIName($item['rmi_id']) }},
                                                <b>Quantity: </b> {{ $item['quantity'].' '.$item['unit'] }},
                                                <b>Price:</b>  {{ $item['price'].' '.config('app.currency') }},
                                                <b>Supplier: </b> {{ $history->getSupplierName($item['supplier_id']) }}

                                                <hr>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{ $history->total_cost }} {{ config('app.currency')}}
                                        </td>
                                        <td>
                                            {{ $history->user->name }}
                                        </td>
                                        <td>
                                            {{ date('l, dS F Y h:i A', strtotime($history->updated_at)) }}
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                            @if(count($histories) > 0)

                                <div class="alert alert-info">
                                    Showing {{ $histories->firstItem() }} to {{ $histories->lastItem() }} Purchase of
                                    Total {{ $histories->total() }} Purchases.
                                </div>

                                <nav aria-label="Page navigation">
                                    @if ($histories->lastPage() > 1)
                                        <ul class="pagination pagination-sm">
                                            <li class="page-item {{ ($histories->currentPage() == 1) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $histories->url(1) }}">Previous</a>
                                            </li>
                                            @for ($i = 1; $i <= $histories->lastPage(); $i++)
                                                <li class="page-item {{ ($histories->currentPage() == $i) ? 'page active' : '' }}">
                                                    <a class="page-link" href="{{ $histories->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item {{ ($histories->currentPage() == $histories->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link"
                                                   href="{{ $histories->url($histories->currentPage()+1) }}">Next</a>
                                            </li>
                                        </ul>
                                    @endif
                                </nav>
                            @else
                                <div class="alert alert-danger">
                                    No Result Found!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Items Edit Pop Up -->

    <div id="edit-category" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Items Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('category.update') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="user-inputbox">

                            <input type="hidden" name="id" id="cat-id">

                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" id="cat-name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Note:</label>
                                <input type="text" name="title" id="cat-title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Icon:</label>
                                <input type="file" name="image" class="form-control" onchange="readURL(this);">
                            </div>

                            <div class="form-group">
                                <img id="blah" src="http://placehold.it/300x200" height="50"
                                     class="img-responsive img-thumbnail cat-icon" alt="your image"/>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" id="cat-status" value="active"
                                           checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are You Sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this category information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $('#edit-category').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var title = $(e.relatedTarget).data('title');
            var icon = $(e.relatedTarget).data('icon');
            var status = $(e.relatedTarget).data('status');


            $('#cat-id').val(id);
            $('#cat-name').val(name);
            $('#cat-title').val(title);

            $('img.cat-icon').attr('src', '{{ url('/') }}/' + icon);

            if (status === 'active') {
                $('#cat-status').prop('checked', true);
            }
            else {
                $('#cat-status').prop('checked', false);
            }
        });

        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-category-status')}}/' + id);


        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



    <script>
        $('.delete_form').submit(function (e) {

            e.preventDefault();
            var formData = $(this).serialize();

            console.log(formData);
            var elm = $(this);

            if (confirm('Confirm Delete.')) {
                $.ajax({
                    url: '/delete_news_events',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    error: function (data) {
                        console.log(data.error);
                    },
                    success: function (data) {
                        elm.closest("tr").remove();
                    }
                });
            }
        });
    </script>

@endsection
