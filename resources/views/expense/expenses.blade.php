@extends('admin_layouts.default')
@section('css')
    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="{{ route('save.expense') }}" method="post">
                        {{csrf_field()}}
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Expense</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->

                            <div class="form-group">
                                <label for="usr">Item Category:</label>
                                <select title class="form-control" name="category_id">
                                    <option disabled="" selected="">Select Category</option>
                                    @foreach($expense_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="usr">Title:</label>
                                <input title type="text" name="title" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="pwd">Amount:</label>
                                <input title type="number" step="0.001" name="amount" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Remark:</label>
                                <textarea title name="remarks" class="form-control"></textarea>
                            </div>

                            <div class="form-group prport-tatepiker">
                                <label for="usr">Date:</label>
                                <input title type="text" name="date" class="form-control date_input">
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Expense List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Remark</th>
                                    <th>Date</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($expenses  as $key => $expense)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            @if(isset($expense->expense_category->name))
                                                {{$expense->expense_category->name}}
                                            @endif
                                        </td>
                                        <td>{{$expense->title}}</td>
                                        <td>{{$expense->amount}} {{ config('app.currency')}}</td>
                                        <td>{{$expense->remarks}}</td>
                                        <td>{{$expense->date}}</td>
                                        <td>
                                            <div class="user-action">
                                                <a href="#"
                                                   data-id="{{ $expense->id }}"
                                                   data-category="{{ $expense->category_id }}"
                                                   data-title="{{ $expense->title }}"
                                                   data-amount="{{ $expense->amount }}"
                                                   data-remarks="{{ $expense->remarks }}"
                                                   data-date="{{ $expense->date }}"
                                                   data-target="#edit-discount"
                                                   class="user-edits" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span></a>
                                                <a href="/delete-expense/{{$expense->id}}"
                                                   class="user-removed"
                                                   data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Expense?');"><span
                                                            class="fa fa-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>
                            </table>

                            {{ $expenses->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true,
        });
    </script>

    <!-- Tax Edit Pop Up -->

    <div id="edit-discount" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Expense Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('update.expense') }}" method="post">
                        {{csrf_field()}}

                        <input title type="hidden" name="id" id="d-id">
                        <div class="user-inputbox">
                            <h2 class="user-list-title">Update Expense</h2>

                            <div class="form-group">
                                <label for="usr">Item Category:</label>
                                <select title class="form-control" name="category_id" id="category_id">
                                    <option disabled="" selected="">Select Category</option>
                                    @foreach($expense_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="usr">Title:</label>
                                <input title type="text" name="title" id="title" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="pwd">Amount:</label>
                                <input title type="number" name="amount" id="amount" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Remark:</label>
                                <textarea title name="remarks" id="remarks" class="form-control"></textarea>
                            </div>

                            <div class="form-group prport-tatepiker">
                                <label for="usr">Date:</label>
                                <input title type="text" name="date" id="date" class="form-control date_input">
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#edit-discount').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var category_id = $(e.relatedTarget).data('category');
            var title = $(e.relatedTarget).data('title');
            var amount = $(e.relatedTarget).data('amount');
            var remarks = $(e.relatedTarget).data('remarks');
            var date = $(e.relatedTarget).data('date');

            $('#d-id').val(id);
            $('select#category_id').val(category_id);
            $('#title').val(title);
            $('#amount').val(amount);
            $('#remarks').val(remarks);
            $('#date').val(date);


        });

    </script>


@endsection