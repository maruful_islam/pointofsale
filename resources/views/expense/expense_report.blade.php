@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

    <style>
        .btn-group-sm > .btn, .btn-sm {
            font-size: 9px;
            border-radius: 100px;
        }

        .btn-primary {
            background-color: #fb787e;
            border-color: #fb787e;
            margin: 3px;
        }

        .invoice-page .invo-header-info {
            width: 100%;
        }
    </style>

@endsection

@section('content')
    <div id="report-desc" class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">


                            <div class="kcol-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="prport-tatepiker">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th>Form Date: <input type="text" class="span2 date_input" name="start_date"
                                                                  @if(isset($_GET['start_date'])) value="{{ $_GET['start_date'] }}"
                                                                  @endif required></th>

                                            <th>To Date: <input type="text" class="span2 date_input" name="end_date"
                                                                @if(isset($_GET['end_date'])) value="{{ $_GET['end_date'] }}"
                                                                @endif required></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="report-search text-right">
                                    <br>
                                    <select class="re-monthly" name="sort_type" data-role="sorter" id="sort-sales">
                                        <option value="0"
                                                @if(isset($sort_type)) @if($sort_type == 0) selected @endif @endif>Sort
                                            By
                                        </option>

                                        <option value="1"
                                                @if(isset($sort_type)) @if($sort_type == 1) selected @endif @endif>Sort
                                            by Category
                                        </option>

                                        <option value="2"
                                                @if(isset($sort_type)) @if($sort_type == 2) selected @endif @endif>Sort
                                            by Purchase
                                        </option>

                                    </select>

                                    <!-- Category List -->

                                    <select class="re-monthly" name="category_id" data-role="sorter"
                                            style="display: @if(isset($category_display)) {{ $category_display }}@else none @endif"
                                            id="category-list">
                                        <option selected="selected">All Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                    @if(isset($category_display))
                                                    @if(isset($category_id))
                                                    @if($category_id == $category->id)
                                                    selected
                                                    @endif
                                                    @endif
                                                    @endif
                                            >{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-button">Search</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h2>Expense Report</h2><br>
                        <img src="{{ asset('pos/images/ddddd.png') }}" width="150"/>
                        <h4>{{ strtoupper(systemInfo('system_name'))}}</h4>
                        <h4>{{ systemInfo('local_name') }}</h4><br>
                        @if(isset($_GET['start_date']))
                            <h5>Report From: <b>{{ date('l, dS F Y', strtotime($_GET['start_date'])) }} </b>
                                to <b>{{ date('l, dS F Y', strtotime($_GET['end_date'])) }}</b></h5>
                        @else
                            <h5>Date: {{ \Carbon\Carbon::now()->format('l, dS F Y') }} </h5>
                        @endif
                    </div>
                    <div class="report-table2">
                        <!--Table-->

                        <table class="table">

                            <!--Table head-->
                            <thead class="blue-grey lighten-4">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center" width="10%">Category</th>
                                <th class="text-center" width="35%">Title</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Remark</th>
                                <th class="text-center">Date</th>
                            </tr>
                            </thead>
                            <!--Table head-->

                            <!--Table body-->
                            <tbody>

                            @if(!isset($category_id))
                                <tr>
                                    <th scope="row" class="text-center">1</th>

                                    <th scope="row" class="text-center"> Purchase</th>

                                    @if(isset($_GET['start_date']))
                                        <td class="text-center">
                                            Total Purchase Cost From {{ date('d.M.Y', strtotime($_GET['start_date'])) }} to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                                        </td>
                                    @else
                                        <td class="text-center">
                                            Total Purchase Cost Till {{ date('d.M.Y') }}
                                        </td>
                                    @endif

                                    <td class="text-center">
                                        {{ $purchase_cost }}  {{ config('app.currency')}}
                                    </td>

                                    <td class="text-center">
                                        -
                                    </td>

                                    <td class="text-center">
                                        {{ date('d.M.Y') }}
                                    </td>

                                </tr>
                            @endif

                            @if(isset($reports))

                                @foreach($reports as $key=>$report)

                                    <tr>
                                        @if(!isset($category_id))
                                            <th scope="row" class="text-center">{{ $key+2 }}</th>
                                        @else
                                            <th scope="row" class="text-center">{{ $key+1 }}</th>
                                        @endif
                                        <th scope="row" class="text-center">{{ $report->expense_category->name }}</th>

                                        <td class="text-center">
                                            {{ $report->title }}
                                        </td>

                                        <td class="text-center">
                                            {{ $report->amount }}  {{ config('app.currency')}}
                                        </td>

                                        <td class="text-center">
                                            {{ $report->remarks }}
                                        </td>

                                        <td class="text-center">
                                            {{ date('d.M.Y', strtotime($report->date)) }}
                                        </td>

                                    </tr>

                                @endforeach
                            @endif


                            <tr>
                                <td colspan="6">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right" style="font-weight: bold;">Total Expense:
                                </td>
                                <td colspan="3" style="font-weight: bold;" class="text-center">
                                    @php echo number_format($total_expense, 3). ' '.config('app.currency'); @endphp
                                </td>
                            </tr>

                            </tbody>
                            <!--Table body-->
                        </table>
                        @if(isset($reports))
                            @if(count($reports) == 0 && $purchase_cost == 0)
                                <div class="alert alert-danger">
                                    No Expense Information Found!
                                </div>
                            @endif
                            @if(count($reports) > 0)

                                <div class="alert alert-info">
                                    Total {{ count($reports) }} Expense Information Found.
                                </div>
                        @endif
                    @endif
                    <!--Table-->
                    </div>
                </div>

                @php $current_url = Request::getPathInfo() . (Request::getQueryString() ? ('?' . Request::getQueryString()) : '');
                    $query_string = substr($current_url,15);
                @endphp
                <div class="text-center">
                    <a class="btn btn-primary pos-small-btn bg-color"
                       href="{{ url('/pdf-expense-report'.$query_string) }}">Save
                        As PDF</a>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('js')

    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #thermal-print, #thermal-print * {
                visibility: visible;
            }

            #thermal-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .invoice-center ul li {
                width: 48%;
            }

            .inv-header {
                border-bottom: none;
            }

        }
    </style>

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#sort-sales').on('change', function () {
                var sort_type = $(this).val();

                if (sort_type == 1) {
                    $('#category-list').css('display', 'inline-block');
                    $('#purchase-list').css('display', 'none');


                }
                else if (sort_type == 2) {
                    $('#category-list').css('display', 'none');
                    $('#purchase-list').css('display', 'inline-block');

                }

                else {
                    $('#category-list').css('display', 'none');
                    $('#purchase-list').css('display', 'none');

                }

            });
        });
    </script>

@endsection