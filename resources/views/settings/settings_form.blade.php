@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <form action="{{ url()->current() }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">System Settings</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                        @endif

                        <!-- Success and error Message End -->


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input title type="text" name="name"
                                       value="@if(isset($system_name)) {{ $system_name }} @endif" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="usr">Email:</label>
                                <input title type="text" name="email"
                                       value="@if(isset($system_name)) {{ $system_email }} @endif" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="pwd">Phone:</label>
                                <input title type="text" name="phone" value="@if(isset($phone)) {{ $phone }} @endif"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Address:</label>
                                <input title type="text" name="address"
                                       value="@if(isset($address)) {{ $address }} @endif" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Currency:</label>
                                <input title type="text" name="currency"
                                       value="@if(isset($currency)) {{ $currency }} @endif" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Timezone:</label>
                                <select title name="timezone" class="form-control selectpicker" data-live-search="true">
                                    <option value="Pacific/Midway"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Midway") selected @endif @endif>
                                        (GMT-11:00) Midway Island, Samoa
                                    </option>

                                    <option value="America/Adak"
                                            @if(isset($timezone)) @if($timezone == "America/Adak") selected @endif @endif
                                    >(GMT-10:00) Hawaii-Aleutian
                                    </option>

                                    <option value="Etc/GMT+10"
                                            @if(isset($timezone)) @if($timezone == "Etc/GMT+10") selected @endif @endif
                                    >(GMT-10:00) Hawaii
                                    </option>

                                    <option value="Pacific/Marquesas"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Marquesas") selected @endif @endif
                                    >(GMT-09:30) Marquesas Islands
                                    </option>

                                    <option value="Pacific/Gambier"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Gambier") selected @endif @endif
                                    >(GMT-09:00) Gambier Islands
                                    </option>

                                    <option value="America/Anchorage"
                                            @if(isset($timezone)) @if($timezone == "America/Anchorage") selected @endif @endif
                                    >(GMT-09:00) Alaska
                                    </option>

                                    <option value="America/Ensenada"
                                            @if(isset($timezone)) @if($timezone == "America/Ensenada") selected @endif @endif
                                    >(GMT-08:00) Tijuana, Baja California
                                    </option>

                                    <option value="Etc/GMT+8"
                                            @if(isset($timezone)) @if($timezone == "Etc/GMT+8") selected @endif @endif
                                    >(GMT-08:00) Pitcairn Islands
                                    </option>

                                    <option value="America/Los_Angeles"
                                            @if(isset($timezone)) @if($timezone == "America/Los_Angeles") selected @endif @endif
                                    >(GMT-08:00) Pacific Time (US & Canada)
                                    </option>

                                    <option value="America/Denver"
                                            @if(isset($timezone)) @if($timezone == "America/Denver") selected @endif @endif
                                    >(GMT-07:00) Mountain Time (US & Canada)
                                    </option>

                                    <option value="America/Chihuahua"
                                            @if(isset($timezone)) @if($timezone == "America/Chihuahua") selected @endif @endif
                                    >(GMT-07:00) Chihuahua, La Paz, Mazatlan
                                    </option>

                                    <option value="America/Dawson_Creek"
                                            @if(isset($timezone)) @if($timezone == "America/Dawson_Creek") selected @endif @endif
                                    >(GMT-07:00) Arizona
                                    </option>

                                    <option value="America/Belize"
                                            @if(isset($timezone)) @if($timezone == "America/Belize") selected @endif @endif
                                    >(GMT-06:00) Saskatchewan, Central America
                                    </option>

                                    <option value="America/Cancun"
                                            @if(isset($timezone)) @if($timezone == "America/Cancun") selected @endif @endif
                                    >(GMT-06:00) Guadalajara, Mexico City, Monterrey
                                    </option>

                                    <option value="Chile/EasterIsland"
                                            @if(isset($timezone)) @if($timezone == "Chile/EasterIsland") selected @endif @endif
                                    >(GMT-06:00) Easter Island
                                    </option>

                                    <option value="America/Chicago"
                                            @if(isset($timezone)) @if($timezone == "America/Chicago") selected @endif @endif
                                    >(GMT-06:00) Central Time (US & Canada)
                                    </option>

                                    <option value="America/New_York"
                                            @if(isset($timezone)) @if($timezone == "America/New_York") selected @endif @endif
                                    >(GMT-05:00) Eastern Time (US & Canada)
                                    </option>

                                    <option value="America/Havana"
                                            @if(isset($timezone)) @if($timezone == "America/Havana") selected @endif @endif
                                    >(GMT-05:00) Cuba
                                    </option>

                                    <option value="America/Bogota"
                                            @if(isset($timezone)) @if($timezone == "America/Bogota") selected @endif @endif
                                    >(GMT-05:00) Bogota, Lima, Quito, Rio Branco
                                    </option>

                                    <option value="America/Caracas"
                                            @if(isset($timezone)) @if($timezone == "America/Caracas") selected @endif @endif
                                    >(GMT-04:30) Caracas
                                    </option>

                                    <option value="America/Santiago"
                                            @if(isset($timezone)) @if($timezone == "America/Santiago") selected @endif @endif
                                    >(GMT-04:00) Santiago
                                    </option>

                                    <option value="America/La_Paz"
                                            @if(isset($timezone)) @if($timezone == "America/La_Paz") selected @endif @endif
                                    >(GMT-04:00) La Paz
                                    </option>

                                    <option value="Atlantic/Stanley"
                                            @if(isset($timezone)) @if($timezone == "Atlantic/Stanley") selected @endif @endif
                                    >(GMT-04:00) Faukland Islands
                                    </option>

                                    <option value="America/Campo_Grande"
                                            @if(isset($timezone)) @if($timezone == "America/Campo_Grande") selected @endif @endif
                                    >(GMT-04:00) Brazil
                                    </option>

                                    <option value="America/Goose_Bay"
                                            @if(isset($timezone)) @if($timezone == "America/Goose_Bay") selected @endif @endif
                                    >(GMT-04:00) Atlantic Time (Goose Bay)
                                    </option>

                                    <option value="America/Glace_Bay"
                                            @if(isset($timezone)) @if($timezone == "America/Glace_Bay") selected @endif @endif
                                    >(GMT-04:00) Atlantic Time (Canada)
                                    </option>

                                    <option value="America/St_Johns"
                                            @if(isset($timezone)) @if($timezone == "America/St_Johns") selected @endif @endif
                                    >(GMT-03:30) Newfoundland
                                    </option>

                                    <option value="America/Araguaina"
                                            @if(isset($timezone)) @if($timezone == "America/Araguaina") selected @endif @endif
                                    >(GMT-03:00) UTC-3
                                    </option>

                                    <option value="America/Montevideo"
                                            @if(isset($timezone)) @if($timezone == "America/Montevideo") selected @endif @endif
                                    >(GMT-03:00) Montevideo
                                    </option>

                                    <option value="America/Miquelon"
                                            @if(isset($timezone)) @if($timezone == "America/Miquelon") selected @endif @endif
                                    >(GMT-03:00) Miquelon, St. Pierre
                                    </option>

                                    <option value="America/Godthab"
                                            @if(isset($timezone)) @if($timezone == "America/Godthab") selected @endif @endif
                                    >(GMT-03:00) Greenland
                                    </option>

                                    <option value="America/Argentina/Buenos_Aires"
                                            @if(isset($timezone)) @if($timezone == "America/Argentina/Buenos_Aires") selected @endif @endif
                                    >(GMT-03:00) Buenos Aires
                                    </option>

                                    <option value="America/Sao_Paulo"
                                            @if(isset($timezone)) @if($timezone == "America/Sao_Paulo") selected @endif @endif
                                    >(GMT-03:00) Brasilia
                                    </option>

                                    <option value="America/Noronha"
                                            @if(isset($timezone)) @if($timezone == "America/Noronha") selected @endif @endif
                                    >(GMT-02:00) Mid-Atlantic
                                    </option>


                                    <option value="Atlantic/Cape_Verde"
                                            @if(isset($timezone)) @if($timezone == "Atlantic/Cape_Verde") selected @endif @endif
                                    >(GMT-01:00) Cape Verde Is.
                                    </option>

                                    <option value="Atlantic/Azores"
                                            @if(isset($timezone)) @if($timezone == "Atlantic/Azores") selected @endif @endif
                                    >(GMT-01:00) Azores
                                    </option>

                                    <option value="Europe/Belfast"
                                            @if(isset($timezone)) @if($timezone == "Europe/Belfast") selected @endif @endif
                                    >(GMT) Greenwich Mean Time : Belfast
                                    </option>

                                    <option value="Europe/Dublin"
                                            @if(isset($timezone)) @if($timezone == "Europe/Dublin") selected @endif @endif
                                    >(GMT) Greenwich Mean Time : Dublin
                                    </option>

                                    <option value="Europe/Lisbon"
                                            @if(isset($timezone)) @if($timezone == "Europe/Lisbon") selected @endif @endif
                                    >(GMT) Greenwich Mean Time : Lisbon
                                    </option>

                                    <option value="Europe/London"
                                            @if(isset($timezone)) @if($timezone == "Europe/London") selected @endif @endif
                                    >(GMT) Greenwich Mean Time : London
                                    </option>

                                    <option value="Africa/Abidjan"
                                            @if(isset($timezone)) @if($timezone == "Africa/Abidjan") selected @endif @endif
                                    >(GMT) Monrovia, Reykjavik
                                    </option>

                                    <option value="Europe/Amsterdam"
                                            @if(isset($timezone)) @if($timezone == "Europe/Amsterdam") selected @endif @endif
                                    >(GMT+01:00) Amsterdam, Berlin, Bern, Rome,
                                        Stockholm, Vienna
                                    </option>

                                    <option value="Europe/Belgrade"
                                            @if(isset($timezone)) @if($timezone == "Europe/Belgrade") selected @endif @endif
                                    >(GMT+01:00) Belgrade, Bratislava, Budapest,
                                        Ljubljana, Prague
                                    </option>

                                    <option value="Europe/Brussels"
                                            @if(isset($timezone)) @if($timezone == "Europe/Brussels") selected @endif @endif
                                    >(GMT+01:00) Brussels, Copenhagen, Madrid, Paris
                                    </option>

                                    <option value="Africa/Algiers"
                                            @if(isset($timezone)) @if($timezone == "Africa/Algiers") selected @endif @endif
                                    >(GMT+01:00) West Central Africa
                                    </option>

                                    <option value="Africa/Windhoek"
                                            @if(isset($timezone)) @if($timezone == "Africa/Windhoek") selected @endif @endif
                                    >(GMT+01:00) Windhoek
                                    </option>

                                    <option value="Asia/Beirut"
                                            @if(isset($timezone)) @if($timezone == "Asia/Beirut") selected @endif @endif
                                    >(GMT+02:00) Beirut
                                    </option>

                                    <option value="Africa/Cairo"
                                            @if(isset($timezone)) @if($timezone == "Africa/Cairo") selected @endif @endif
                                    >(GMT+02:00) Cairo
                                    </option>

                                    <option value="Asia/Gaza"
                                            @if(isset($timezone)) @if($timezone == "Asia/Gaza") selected @endif @endif
                                    >(GMT+02:00) Gaza
                                    </option>

                                    <option value="Africa/Blantyre"
                                            @if(isset($timezone)) @if($timezone == "Africa/Blantyre") selected @endif @endif
                                    >(GMT+02:00) Harare, Pretoria
                                    </option>

                                    <option value="Asia/Jerusalem"
                                            @if(isset($timezone)) @if($timezone == "Asia/Jerusalem") selected @endif @endif
                                    >(GMT+02:00) Jerusalem
                                    </option>

                                    <option value="Europe/Minsk"
                                            @if(isset($timezone)) @if($timezone == "Europe/Minsk") selected @endif @endif
                                    >(GMT+02:00) Minsk
                                    </option>

                                    <option value="Asia/Damascus"
                                            @if(isset($timezone)) @if($timezone == "Asia/Damascus") selected @endif @endif
                                    >(GMT+02:00) Syria
                                    </option>

                                    <option value="Europe/Moscow"
                                            @if(isset($timezone)) @if($timezone == "Europe/Moscow") selected @endif @endif
                                    >(GMT+03:00) Moscow, St. Petersburg, Volgograd
                                    </option>

                                    <option value="Africa/Addis_Ababa"
                                            @if(isset($timezone)) @if($timezone == "Africa/Addis_Ababa") selected @endif @endif
                                    >(GMT+03:00) Nairobi
                                    </option>

                                    <option value="Asia/Kuwait"
                                            @if(isset($timezone)) @if($timezone == "Asia/Kuwait") selected @endif @endif
                                    >(GMT+03:00) Kuwait
                                    </option>

                                    <option value="Asia/Tehran"
                                            @if(isset($timezone)) @if($timezone == "Asia/Tehran") selected @endif @endif
                                    >(GMT+03:30) Tehran
                                    </option>

                                    <option value="Asia/Dubai"
                                            @if(isset($timezone)) @if($timezone == "Asia/Dubai") selected @endif @endif
                                    >(GMT+04:00) Abu Dhabi, Muscat
                                    </option>

                                    <option value="Asia/Yerevan"
                                            @if(isset($timezone)) @if($timezone == "Asia/Yerevan") selected @endif @endif
                                    >(GMT+04:00) Yerevan
                                    </option>

                                    <option value="Asia/Kabul"
                                            @if(isset($timezone)) @if($timezone == "Asia/Kabul") selected @endif @endif
                                    >(GMT+04:30) Kabul
                                    </option>

                                    <option value="Asia/Yekaterinburg"
                                            @if(isset($timezone)) @if($timezone == "Asia/Yekaterinburg") selected @endif @endif
                                    >(GMT+05:00) Ekaterinburg
                                    </option>

                                    <option value="Asia/Tashkent"
                                            @if(isset($timezone)) @if($timezone == "Asia/Tashkent") selected @endif @endif
                                    >(GMT+05:00) Tashkent
                                    </option>

                                    <option value="Asia/Kolkata"
                                            @if(isset($timezone)) @if($timezone == "Asia/Kolkata") selected @endif @endif
                                    >(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi
                                    </option>

                                    <option value="Asia/Katmandu"
                                            @if(isset($timezone)) @if($timezone == "Asia/Katmandu") selected @endif @endif
                                    >(GMT+05:45) Kathmandu
                                    </option>

                                    <option value="Asia/Dhaka"
                                            @if(isset($timezone)) @if($timezone == "Asia/Dhaka") selected @endif @endif
                                    >(GMT+06:00) Astana, Dhaka
                                    </option>

                                    <option value="Asia/Novosibirsk"
                                            @if(isset($timezone)) @if($timezone == "Asia/Novosibirsk") selected @endif @endif
                                    >(GMT+06:00) Novosibirsk
                                    </option>

                                    <option value="Asia/Rangoon"
                                            @if(isset($timezone)) @if($timezone == "Asia/Rangoon") selected @endif @endif
                                    >(GMT+06:30) Yangon (Rangoon)
                                    </option>

                                    <option value="Asia/Bangkok"
                                            @if(isset($timezone)) @if($timezone == "Asia/Bangkok") selected @endif @endif
                                    >(GMT+07:00) Bangkok, Hanoi, Jakarta
                                    </option>

                                    <option value="Asia/Krasnoyarsk"
                                            @if(isset($timezone)) @if($timezone == "Asia/Krasnoyarsk") selected @endif @endif
                                    >(GMT+07:00) Krasnoyarsk
                                    </option>

                                    <option value="Asia/Hong_Kong"
                                            @if(isset($timezone)) @if($timezone == "Asia/Hong_Kong") selected @endif @endif
                                    >(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi
                                    </option>

                                    <option value="Asia/Irkutsk"
                                            @if(isset($timezone)) @if($timezone == "Asia/Irkutsk") selected @endif @endif
                                    >(GMT+08:00) Irkutsk, Ulaan Bataar
                                    </option>

                                    <option value="Australia/Perth"
                                            @if(isset($timezone)) @if($timezone == "Australia/Perth") selected @endif @endif
                                    >(GMT+08:00) Perth
                                    </option>

                                    <option value="Australia/Eucla"
                                            @if(isset($timezone)) @if($timezone == "Australia/Eucla") selected @endif @endif
                                    >(GMT+08:45) Eucla
                                    </option>

                                    <option value="Asia/Tokyo"
                                            @if(isset($timezone)) @if($timezone == "Asia/Tokyo") selected @endif @endif
                                    >(GMT+09:00) Osaka, Sapporo, Tokyo
                                    </option>

                                    <option value="Asia/Seoul"
                                            @if(isset($timezone)) @if($timezone == "Asia/Seoul") selected @endif @endif
                                    >(GMT+09:00) Seoul
                                    </option>

                                    <option value="Asia/Yakutsk"
                                            @if(isset($timezone)) @if($timezone == "Asia/Yakutsk") selected @endif @endif
                                    >(GMT+09:00) Yakutsk
                                    </option>

                                    <option value="Australia/Adelaide"
                                            @if(isset($timezone)) @if($timezone == "Australia/Adelaide") selected @endif @endif
                                    >(GMT+09:30) Adelaide
                                    </option>

                                    <option value="Australia/Darwin"
                                            @if(isset($timezone)) @if($timezone == "Australia/Darwin") selected @endif @endif
                                    >(GMT+09:30) Darwin
                                    </option>

                                    <option value="Australia/Brisbane"
                                            @if(isset($timezone)) @if($timezone == "Australia/Brisbane") selected @endif @endif
                                    >(GMT+10:00) Brisbane
                                    </option>

                                    <option value="Australia/Brisbane"
                                            @if(isset($timezone)) @if($timezone == "Australia/Brisbane") selected @endif @endif
                                    >(GMT+10:00) Hobart
                                    </option>

                                    <option value="Asia/Vladivostok"
                                            @if(isset($timezone)) @if($timezone == "Asia/Vladivostok") selected @endif @endif
                                    >(GMT+10:00) Vladivostok
                                    </option>

                                    <option value="Australia/Lord_Howe"
                                            @if(isset($timezone)) @if($timezone == "Australia/Lord_Howe") selected @endif @endif
                                    >(GMT+10:30) Lord Howe Island
                                    </option>

                                    <option value="Etc/GMT-11"
                                            @if(isset($timezone)) @if($timezone == "Etc/GMT-11") selected @endif @endif
                                    >(GMT+11:00) Solomon Is., New Caledonia
                                    </option>

                                    <option value="Asia/Magadan"
                                            @if(isset($timezone)) @if($timezone == "Asia/Magadan") selected @endif @endif
                                    >(GMT+11:00) Magadan
                                    </option>

                                    <option value="Pacific/Norfolk"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Norfolk") selected @endif @endif
                                    >(GMT+11:30) Norfolk Island
                                    </option>

                                    <option value="Asia/Anadyr"
                                            @if(isset($timezone)) @if($timezone == "Asia/Anadyr") selected @endif @endif
                                    >(GMT+12:00) Anadyr, Kamchatka
                                    </option>

                                    <option value="Pacific/Auckland"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Auckland") selected @endif @endif
                                    >(GMT+12:00) Auckland, Wellington
                                    </option>

                                    <option value="Etc/GMT-12"
                                            @if(isset($timezone)) @if($timezone == "Etc/GMT-12") selected @endif @endif
                                    >(GMT+12:00) Fiji, Kamchatka, Marshall Is.
                                    </option>

                                    <option value="Pacific/Chatham"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Chatham") selected @endif @endif
                                    >(GMT+12:45) Chatham Islands
                                    </option>

                                    <option value="Pacific/Tongatapu"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Tongatapu") selected @endif @endif
                                    >(GMT+13:00) Nuku'alofa
                                    </option>

                                    <option value="Pacific/Kiritimati"
                                            @if(isset($timezone)) @if($timezone == "Pacific/Kiritimati") selected @endif @endif
                                    >(GMT+14:00) Kiritimati
                                    </option>
                                </select>
                            </div>


                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-6">
                    <form action="{{ route('save.logo') }}" method="post" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">System Logo</h2>

                            <!-- Success and error Message Start -->

                            @if ($errors->any())
                                <div class="alert alert-danger error-message-show">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <div class="alert alert-success success-message-show"><span
                                            class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em>
                                </div>
                            @endif

                        <!-- Success and error Message End -->


                            <div class="form-group">
                                <label for="usr">Logo:</label>
                                <input title id="input-b32" name="logo" type="file" class="file form-control"
                                       data-show-upload="false" data-show-caption="true"
                                       data-msg-placeholder="Select Logo for upload..."
                                >
                            </div>

                            @if(isset($logo))
                                <div class="form-group">
                                    <img src="{{ asset($logo) }}" alt="">
                                </div>
                            @endif

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
@endsection