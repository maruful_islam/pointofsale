@extends('admin_layouts.default')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/report.css') }}">

    <style type="text/css">
    	.btn-primary {
    background-color: #fb787e;
    border-color: #fb787e;
    margin: 3px;
        padding: 2px 15px;
}
    </style>

@endsection


@section('content')
    @php
        $date = date('Y-m-d');
    @endphp

    <div class="container page-padding-top" id="report-desc">

        <div class="user-list-boxarea">
            <div class="row">

            <div class="report-header">
                    <div class="row">
                        <form action="" method="get">


                            <div class="kcol-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="prport-tatepiker">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Form Date: <input type="text" class="span2 date_input" name="start_date" required=""></th>
                                            <th>To Date: <input type="text" class="span2 date_input" name="end_date" required=""></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="report-search text-right">
                                    <br>

                                    <button class="btn btn-button">Search</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
               

                <div class="col-xs-12 col-sm-12 col-md-12" id="print">
                    <div class="text-center" style="padding-top: 20px">
                        <h2>Kitchen Status</h2><br>
                        <h4>{{ strtoupper(config('app.name'))}}</h4><br>
                        <!-- @if(isset($date1))
                            <h5>Report From: <b>{{ date('l, dS F Y', strtotime($date1)) }} </b>
                                to <b>{{ date('l, dS F Y', strtotime($date2)) }}</b></h5>
                        @else
                            <h5>Date: {{ date('l, dS F Y') }} </h5>
                        @endif -->
                    </div>
                    <div class="report-table2 endofday">

                        <table class="table">

                            <thead class="blue-grey lighten-4">
                            <tr>
                                <th class="text-center" width="5%">#</th>
                                <th class="text-center" width="10%">Order No</th>
                                <th class="text-center" width="50%">Item</th>
                                <th class="text-center" width="10%">Status</th>
                            </tr>
                            </thead>
                            <!--Table head-->

                            <!--Table body-->
                            <tbody>
                            @foreach($close_order as $key => $product)
								<tr>
									<th style="text-align: center;">{{ $key+1 }}</th>
									<th style="text-align: center;">{{ $product['csk_id'] }}</th>
									<th style="text-align: left;">
									@foreach($product['cart'] as $item)
									<button class="btn btn-primary btn-sm">
										{{ $item['name'] }} ({{$item['count']}})
                                    </button>
                                    @endforeach
                                                    </th>

									<th style="text-align: center;"> 
									@if($product['is_kitchen'] == 1)
									<button class="btn btn-xs btn-danger">Pending</button>
									@elseif($product['is_kitchen'] == 2)
									<button class="btn btn-xs btn-info">Processing</button>
									@elseif($product['is_kitchen'] == 3)
									<button class="btn btn-xs btn-success">Finish</button>
									@endif
									 </th>
									
								</tr>
							@endforeach

                           

                            </tbody>
                            
                        </table>

                        <hr>
                        @if(count($close_order) <= 0)
                        <div class="alert alert-danger">
                                No Result Found!
                            </div>
                        @endif


                    </div>

                </div>


            </div>
        </div>
    </div>





@endsection

@section('js')

    <script src="{{ url('libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

        $('.date_input').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            todayHighlight: true
        });
    </script>



    <script>
        function printReport() {

            $('#thermal-print').css('display', 'block');
            $('#report-desc').css('display', 'none');
            window.print();
            $('#thermal-print').css('display', 'none');
            $('#report-desc').css('display', 'block');
        }
    </script>
    <style>

        @media print {
            body * {
                visibility: hidden;
            }

            #thermal-print, #thermal-print * {
                visibility: visible;
            }

            #thermal-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .invoice-center ul li {
                width: 48%;
            }

            .inv-header {
                border-bottom: none;
            }

        }
    </style>



@endsection