@extends('admin_layouts.default2')

@section('css')

    <link rel="stylesheet" href="{{ url('libs/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/multi-select/css/multi-select.css') }}">

    <style>
        .select2-searchss .select2-container .select2-selection--single {
            height: 36px;
            padding: 4px 0;
            border-color: #dee7ee;
        }
        span.select2-dropdown.select2-dropdown--above {
            width: 353px !important;
            margin-top: -11px;
        }
        span.select2-dropdown.select2-dropdown--below {
            width: 353px !important;
        }
        .single-kitchen {
            margin-bottom: 22px;
            position: relative;
        }
        .btn-kitchen {
            color: #ee1c25;
            background-color: #231f20;
            border-color: #ee1c25;
        }
        .btn-kitchen:hover {
            color: #ee1c25;
        }
        .overlay {
            position: absolute;
            content: "";
            width: 93%;
            height: 100%;
            background: #17171794;
            top: 0;
        }

        .fa_icon i {
            position: absolute;
            top: 47%;
            right: 17px;
            left: 0;
            margin: 0 auto;
            width: 0px;
            height: 0px;
            /* background: red; */
            font-size: 43px;
            /* padding: 20px; */
            color: #5cb85c9c;
        }
        .closeorderls li span {
            float: right;
            padding-right: 39px;
            /* font-weight: bold; */
        }
        .per-kitchen{
            box-shadow: 0px 0px 5px 1px #e6e1e1;
            margin: 2px 5px;
            border-radius: 5px;
            padding: 0 5px;
        }
        .per-kitchen button.btn {
            padding: 6px 14px;
            margin: 5px 3px;
        }
        .per-kitchen button.btn {
            padding: 6px 14px;
            margin: 5px 3px;
            color: #fff;
            border: none;
            text-transform: capitalize;
        }
        .kitchen-container .order-author-info ul li {
            line-height: 26px;
        }
        .kitchen-container .hold-order-detaisls .table>tbody>tr>td {
            padding: 6px 10px;
        }
        .kitchen-container .hold-order-detaisls {
            background: #fff;
            padding: 12px 25px;
            padding-top: 0;
            padding-bottom: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="home-icons" style="display: none">
        <a href="/home"><span class="fa fa-home"></span></a>
    </div>
    <div class="poscat-list-page">
        <div class="poscat-container">

        </div>
    </div>

@endsection

@section('js')


    <!--Final Pay order close -->
    <div class="kitchen-container" id="">
        <div class="payment-container ">
            <div class="hold-areabox">

                <div class="hold-area">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <div class="hold-list-items" >
                                <div class="hold-l-search">
                                    <input type=search placeholder="Enter Your Hold Keyword....." class="form-control" />
                                    <button type="button" value=""><span class="fa fa-search"></span></button>
                                </div>
                                <div class="hol-scrol" style="height: 520px">
                                    <input type="hidden" id="close-index-number" value="">
                                    <div class="kitchen-list-item" id="show-all-kitchen-check" >


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="hold-order-detaisls">
                                <h2 class="text-center">order Details</h2>
                                <div class="hold-order-details-inner">
                                    <div class="order-author-info paidarea">
                                        <div class="close-paid-bg" style="display: none;">
                                            <img src="{{asset('pos/images/paid.png')}}" alt="">
                                        </div>
                                        <ul>
                                            <div class="closeorderls" style="width: 100%">
                                                <li>Order No :<span id="show-kitchen-chk-number"></span></li><br>
                                                <li>Date : <span id="show-kitchen-date">xx-xx-xxxx</span></li><br>
                                                <li>Time : <span id="show-kitchen-time">xx:xx</span></li>
                                            </div>
                                        </ul>
                                    </div>
                                    <table class="table pos-product-list-head">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="text-center">#</th>
                                            <th scope="col" class="text-center">Product name</th>
                                            <th class="text-center pos-product-list-headQit" scope="col">Qty:</th>
                                        </tr>
                                        </thead>
                                        <tbody class="pos-pro-list-table-body" id="view-single-kitchen-order">


                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Final Pay order close -->


    <div class="invoice-page " id="inv-page" style="display:  none;">
        <div class="invoice-area">
            <div class="hold-order-detaisls">
                <h2 class="text-center">order Details</h2>
                <div class="hold-order-details-inner">
                    <div class="order-author-info paidarea">
                        <div class="close-paid-bg" style="display: none;">
                            <img src="http://localhost:8000/pos/images/paid.png" alt="">
                        </div>
                        <ul>
                            <div class="closeorderls" style="width: 100%">
                                <li>Order No :<span id="inv-kitchen-chk-number"></span></li><br>
                                <li>Date : <span id="inv-kitchen-date">xx-xx-xxxx</span></li><br>
                                <li>Time : <span id="inv-kitchen-time">xx:xx</span></li>
                            </div>
                        </ul>
                    </div>
                    <table class="table pos-product-list-head">
                        <thead>
                        <tr>
                            <th scope="col" class="text-center">#</th>
                            <th scope="col" class="text-center">Product name</th>
                            <th class="text-center pos-product-list-headQit" scope="col">Qty:</th>
                        </tr>
                        </thead>
                        <tbody class="pos-pro-list-table-body" id="inv-view-single-kitchen-order">

                        <tr>
                            <td>1</td>
                            <td>FISH PAKODA</td>
                            <td>2</td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>






    <script src="{{ url('libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ url('libs/multi-select/js/jquery.multi-select.js') }}"></script>


    <script src="{{ asset('pos/js/cart.js') }}"></script>
    <script src="{{ asset('js/split_order.js') }}"></script>
    <script src="{{ asset('js/idle.min.js') }}"></script>

    <script type="text/javascript">
        /*window.onload = function() {
         var hour = 30;
         var sec = 60;
         setInterval(function() {
         document.getElementById("timer").innerHTML = hour + " : " + sec;
         sec--;
         if (sec == 00) {
         hour--;
         sec = 60;
         if (hour == 0) {
         hour = 2;
         }
         }
         }, 1000);
         setInterval(function() {
         document.getElementById("timer2").innerHTML = hour + " : " + sec;
         sec--;
         if (sec == 00) {
         hour--;
         sec = 60;
         if (hour == 0) {
         hour = 2;
         }
         }
         }, 1000);
         }*/


        /******* Kitchen check *********/
        myFunction();
        page_reload();

        function myFunction() {
            setInterval(function(){

                getKitchenCheck();

            }, 5000);
        }

        function page_reload() {
            setInterval(function(){
                location.reload();
            }, 40000);
        }

        function getKitchenCheck(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",

                url: "{{ url('/get_kitchen_check') }}",
                success: function (data) {
                    localStorage.setItem('kitchen_orders', JSON.stringify(data.close_order));
                    kitchen_orders  = JSON.parse(localStorage.getItem('kitchen_orders'));

                    displayKitchenCheck(kitchen_orders);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function displayKitchenCheck(closes_check){
            var kitchen_output = '';
            console.log(closes_check);
            for(i in closes_check){

                var startDate = new Date(closes_check[i].created_at);
                // Do your operations
                var endDate   = new Date();
                //console.log('---------------');
                //console.log('startDate');
                //console.log(startDate);
                //console.log('endDate');
                //console.log(endDate);
                ///console.log('---------------');
                var diff = ((startDate.getTime()+(30 * 60 * 1000)) - endDate.getTime());
                var msec = diff;
                var hh = Math.floor(msec / 1000 / 60 / 60);
                msec -= hh * 1000 * 60 * 60;
                var mm = Math.floor(msec / 1000 / 60);
                msec -= mm * 1000 * 60;
                var ss = Math.floor(msec / 1000);
                msec -= ss * 1000;

                if (hh<0 || mm<0) {
                    mm = 0,
                        ss = 0
                }


                //console.log("h = "+hh+ " M = "+mm +"s = "+ss);
                //console.log('-----');

                if (closes_check[i].is_kitchen != "3") {
                    kitchen_output += "<div class='col-xs-12 col-sm-12 col-md-6 col-lg-4 single-kitchen' data-index='"+i+"' >",
                        kitchen_output += "<div class='per-kitchen' data-timer-value='"+mm+"' data-timer-sec='"+ss+"' >",
                        kitchen_output += "<table class='table table-striped'>",
                        kitchen_output += "<tbody>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th>Order No: </th>",
                        kitchen_output += "<td>"+closes_check[i].csk_id+"</td>",
                        kitchen_output += "</tr>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th>Time: </th>",
                        kitchen_output += "<td>"+closes_check[i].time+"</td>",
                        kitchen_output += "</tr>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th>Date: </th>",
                        kitchen_output += "<td>"+closes_check[i].date+"</td>",
                        kitchen_output += "</tr>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th>Status: </th>"
                    if (closes_check[i].is_kitchen == "1") {
                        kitchen_output += "<td><button type='button' class='btn btn-xs btn-danger'>Pending</button></td>"
                    }else if(closes_check[i].is_kitchen == "2"){
                        kitchen_output += "<td><button  type='button' class='btn btn-xs btn-info'>Processing</button></td>"
                    }else if(closes_check[i].is_kitchen == "3"){
                        kitchen_output += "<td><button  type='button' class='btn btn-xs btn-success'>Finish</button></td>"
                    }
                    kitchen_output += "</tr>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th colspan='2'><button class='btn btn-kitchen class_timer' id='timer"+closes_check[i].id+"' style='width: 100%;font-weight: bold; font-size: 18px;'>00:00</button></th>",
                        kitchen_output += "</tr>",
                        kitchen_output += "<tr>",
                        kitchen_output += "<th colspan='2'>",
                        kitchen_output += "<button class='btn btn-xs btn-success finish' data-finish-id='"+closes_check[i].id+"'>Finish</button>",
                        kitchen_output += "<button class='btn btn-xs btn-info processing' data-processing-id='"+closes_check[i].id+"' >Processing</button>",
                        kitchen_output += "<button class='btn btn-xs btn-warning print-order' data-index='"+i+"'>Print</button>",
                        kitchen_output += "</th>",
                        kitchen_output += "</tr>",
                        kitchen_output += "</tbody>",
                        kitchen_output += "</table>",
                        kitchen_output += "</div>",
                            /*if (closes_check[i].is_kitchen == "3") {
                             kitchen_output += "<div class='overlay' ></div>"
                             kitchen_output += "<div class='fa_icon'><i class='fa fa-check'></i></div>"
                             }else{
                             kitchen_output += "<div class='overlay' style='display:none'></div>"
                             kitchen_output += "<div class='fa_icon'><i class='fa fa-check' style='display:none'></i></div>"
                             }*/

                        kitchen_output += " </div>"
                }


            }
            $('#show-all-kitchen-check').html(kitchen_output);

            // console.log($('.per-kitchen'));

            $('.per-kitchen').each(function (index,item) {
                // console.log(index);
                var hour = Number($(this).attr('data-timer-value'));
                var sec = Number($(this).attr('data-timer-sec'));
                if (!(hour == 0 && sec==0)) {
                    setInterval(function() {
                        document.getElementsByClassName('class_timer')[index].innerHTML = hour + " : " + sec;
                        sec--;
                        if (sec == 00) {
                            hour--;
                            sec = 60;
                            if (hour == 0) {
                                hour = 2;
                            }
                        }
                    }, 1000);
                }

            });

        }

        getKitchenCheck();

        $('#show-all-kitchen-check').on('click','.finish',function(){
            var id = $(this).attr('data-finish-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: "{{ url('/kitchen-finish') }}/"+id,
                success: function (data) {
                    $('#input-current-amount-hold').val('');
                    toastr.success('Closed order Successfully Deleted.', 'Success Alert', {timeOut: 3000});
                    getKitchenCheck();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('#show-all-kitchen-check').on('click','.processing',function(){
            var id = $(this).attr('data-processing-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: "{{ url('/kitchen-processing') }}/"+id,
                success: function (data) {
                    $('#input-current-amount-hold').val('');
                    toastr.success('Closed order Successfully Deleted.', 'Success Alert', {timeOut: 3000});
                    getKitchenCheck();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('#show-all-kitchen-check').on('click','.single-kitchen',function(){
            var kitchen_output = '';
            var kitchen_orders  = JSON.parse(localStorage.getItem('kitchen_orders'));
            var index = Number($(this).attr('data-index'));

            for(var i in kitchen_orders){
                if (i==index) {
                    $('#show-kitchen-chk-number').html(kitchen_orders[i].csk_id);
                    $('#show-kitchen-date').html(kitchen_orders[i].date);
                    $('#show-kitchen-time').html(kitchen_orders[i].time);
                    var arr = (kitchen_orders[i].cart);
                    for(var p=0 in arr){
                        kitchen_output+="<tr>"
                        kitchen_output+="<td>"+(Number(p)+1)+"</td>";
                        kitchen_output+="<td>"+arr[p].name+"</td>";
                        kitchen_output+="<td>"+arr[p].count+"</td>";
                        kitchen_output+="</tr>"
                    }

                }

            }

            $('#view-single-kitchen-order').html(kitchen_output);


        });

        $('#show-all-kitchen-check').on('click','.print-order',function(){
            var kitchen_output = '';
            var kitchen_orders  = JSON.parse(localStorage.getItem('kitchen_orders'));
            var index = Number($(this).attr('data-index'));

            for(var i in kitchen_orders){
                if (i==index) {
                    $('#inv-kitchen-chk-number').html(kitchen_orders[i].csk_id);
                    $('#inv-kitchen-date').html(kitchen_orders[i].date);
                    $('#inv-kitchen-time').html(kitchen_orders[i].time);
                    var arr = (kitchen_orders[i].cart);
                    for(var p=0 in arr){
                        kitchen_output+="<tr>"
                        kitchen_output+="<td>"+(Number(p)+1)+"</td>"
                        kitchen_output+="<td>"+arr[p].name+"</td>"
                        kitchen_output+="<td>"+arr[p].count+"</td>"
                        kitchen_output+="</tr>"
                    }

                }

            }
            $('#inv-view-single-kitchen-order').html(kitchen_output);


            $(".poscat-list-page").css("display", "none");
            $(".header-area").css("display", "none");
            $(".payment-container ").css("display", "none");
            $("#inv-page").css("display", "");
            print();
            $(".poscat-list-page").css("display", "");
            $(".header-area").css("display", "");
            $(".payment-container ").css("display", "");
            $("#inv-page").css("display", "none");

        });


    </script>

@endsection




