@extends('admin_layouts.default')
@section('content')

    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <!-- Success and error Message Start -->

                    @if ($errors->any())
                        <div class="alert alert-danger error-message-show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif
                <!-- Success and error Message End -->

                    <form action="{{ route('store.staff') }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Add Staff Profile</h2>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="usr">Name:</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Email:</label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Phone:</label>
                                    <input type="text" name="phone" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Father's Name:</label>
                                    <input type="text" name="f_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Mother's Name:</label>
                                    <input type="text" name="m_name" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Department:</label>
                                    <select class="form-control custom-select-value selectpicker"
                                            data-live-search="true" name="department_id">
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Designation:</label>
                                    <select class="form-control custom-select-value selectpicker"
                                            data-live-search="true" name="designation_id">
                                        @foreach($designations as $designation)
                                            <option value="{{ $designation->id }}">{{ $designation->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                            </div>


                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Job type:</label>
                                    <select class="form-control custom-select-value" name="job_type">
                                        <option value="Full Time">Full Time</option>
                                        <option value="Part Time">Part Time</option>
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Country:</label>
                                    <select class="form-control custom-select-value selectpicker" name="country_id"
                                            data-live-search="true">

                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">City:</label>
                                    <input type="text" name="city" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Address:</label>
                                    <input type="text" name="address" class="form-control">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Blood Group:</label>
                                    <select class="form-control custom-select-value selectpicker"
                                            data-live-search="true" name="blood_group">
                                        <option>Select Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <div class="form-select-list">
                                    <label for="pwd">Select Gender:</label>
                                    <select class="form-control custom-select-value" name="gender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">Employee ID:</label>
                                    <input type="text" name="employee_id" class="form-control">
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Select Date of Birth:</label>
                                    <div class="input-group date" data-date-format="dd.mm.yyyy">
                                        <input type="text" name="d_o_b" class="form-control date_input"
                                               placeholder="DD.MM.YYYY">
                                        <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="staff_code"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="pwd">Formal Image:</label>
                                    <input id="input-b3" name="image" type="file" class="file form-control"
                                           data-show-upload="false" data-show-caption="true"
                                           data-msg-placeholder="Select Image for upload..."/>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="pwd">Upload Identity Card (e.g. National ID/Driving License) Scan
                                        Copy:</label>
                                    <input id="input-b31" name="id_card" type="file" class="file form-control"
                                           data-show-upload="false" data-show-caption="true"
                                           data-msg-placeholder="Select  ID Card Scanned Image for upload..."/>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="pwd">Upload CV:</label>
                                    <input id="input-b32" name="cv" type="file" class="file form-control"
                                           data-show-upload="false" data-show-caption="true"
                                           data-msg-placeholder="Select CV File (PDF, Word) for upload..."/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Passcode for Staff:</label>
                                    <input type="password" name="password" class="form-control"
                                           placeholder="Enter Passcode for this staff">

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Select Join Date:</label>
                                    <div class="input-group date" data-date-format="dd.mm.yyyy">
                                        <input type="text" name="join_date" class="form-control"
                                               placeholder="DD.MM.YYYY">
                                        <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="pwd">Login Password for Staff:</label>
                                    <input type="password" name="login_password" class="form-control"
                                           placeholder="Enter Login Password for this staff">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <style>
        #staff_code {
            font-weight: bold;
            color: red;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('input[name="employee_id"]').on('keyup', function () {
                var emp_id = $(this).val();

                $.ajax({
                    url: '{{ url('/res/get-emp-id/') }}' + '/' + emp_id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        if (data == null) {
                            $('#staff_code').html('<p class="well"><b style="color: #1ab394;"> Available! </b></p>');
                            $("button").removeAttr('disabled');
                        }
                        else if (data.length != 0) {

                            $('#staff_code').html('<p class="well">This ID has been assigned to <b style="color: #0c3c50;">' + data + '</b>, please input new ID!</p>');
                            $("button").attr('disabled', 'disabled');

                        }
                    }
                });


            });
        });
    </script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


@endsection