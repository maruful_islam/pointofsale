@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <form action="{{ route('store.department') }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">

                            <h2 class="user-list-title">Add Department</h2>


                            <div class="form-group">
                                <label for="usr">Department Name:</label>
                                <input type="text" name="name" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="pwd">Department Work Description:</label>
                                <textarea name="description" class="form-control" title="enter description"></textarea>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Department List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($departments  as $key => $department)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $department->name }}</td>
                                    <td>{{ $department->description }}</td>
                                    <td>
                                        @if($department->status === "active")
                                            <a data-id="{{ $department->id }}"
                                               data-status="{{ $department->status }}" data-target="#statusUpdate"
                                               data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                        @else
                                            <a data-id="{{ $department->id }}"
                                               data-status="{{ $department->status }}" data-target="#statusUpdate"
                                               data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="user-action">

                                            <a href="#"
                                               data-id="{{ $department->id }}"
                                               data-name="{{ $department->name }}"
                                               data-description="{{ $department->description }}"
                                               data-status="{{ $department->status }}"
                                               data-target="#edit-department"
                                               class="user-edits" data-toggle="modal" title="Edit">
                                                <span class="fa fa-edit"></span>
                                            </a>

                                            <a href="{{ route('delete.department', ['id' => $department->id]) }}" class="user-removed"
                                               data-toggle="tooltip" title="Delete"
                                               onclick="return confirm('Are You Sure Delete This Item?');"><span
                                                        class="fa fa-trash-o"></span></a>
                                        </div>
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Tax Edit Pop Up -->

    <div id="edit-department" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title user-list-title">Edit Department Information</h2>
                </div>
                <div class="modal-body">

                    <form action="{{ route('update.department') }}" method="post">

                        {{csrf_field()}}

                        <input type="hidden" id='id' name="id">

                        <div class="user-inputbox">

                            <h2 class="user-list-title">Update Department</h2>


                            <div class="form-group">
                                <label for="usr">Department Name:</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="pwd">Department Work Description:</label>
                                <textarea name="description" id="desc" class="form-control" title="enter description"></textarea>
                            </div>

                            <div class="form-group">
                                <label class="container">
                                    <input type="checkbox" name="status" id="status" value="active" checked="checked">
                                    Status
                                </label>
                            </div>

                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">submit</button>
                            </div>
                        </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Payment Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this department information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#edit-department').on('show.bs.modal', function (e) {

            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var description = $(e.relatedTarget).data('description');
            var status = $(e.relatedTarget).data('status');


            $('#id').val(id);
            $('#name').val(name);
            $('#desc').val(description);

            if (status === 'active') {
                $('#status').prop('checked', true);
            } else {
                $('#status').prop('checked', false);
            }


        });


        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');


            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-department-status')}}/' + id);


        });
    </script>

@endsection