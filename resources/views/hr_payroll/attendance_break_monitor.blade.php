@extends('admin_layouts.default')
@section('content')
    @php
        $id = auth()->user()->id;
        $date = date('d.m.Y');
        $in_time_check = DB::table('attendances')->where(['emp_id'=> $id, 'date' => $date])->value('entry_time');
        $out_time_check = DB::table('attendances')->where(['emp_id'=> $id, 'date' => $date])->value('leave_time');

        $break_check = DB::table('break_logs')->where(['emp_id'=>$id,'date'=>$date])->orderBy('id','desc')->limit(1)->first();


    @endphp


    <div class="employ-section" style="width:25%;margin:0 auto">
        @if(session('message'))
            <div class="alert alert-danger">
                {{ session('message') }}

                @if($out_time_check)
                    <a class="btn btn-primary" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                @endif
            </div>
        @endif
        <div class="employ-inner">
            <h2>Employee Time Clock</h2>


            <div class="employ-box row">
                <div class="employ-left col-md-8">
                    <input type="password" name="password" id="password" class="form-control"/>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="buttons">
                                <div class="big-btns">
                                    <button class="btn btn-button" id="remove-cal-char">Backspace</button>
                                </div>
                                <div class="btn-smalls">
                                    <button class="btn btn-button small-btn calc" value="1">1</button>
                                    <button class="btn btn-button small-btn calc" value="2">2</button>
                                    <button class="btn btn-button small-btn calc" value="3">3</button>
                                    <button class="btn btn-button small-btn calc" value="4">4</button>
                                    <button class="btn btn-button small-btn calc" value="5">5</button>
                                    <button class="btn btn-button small-btn calc" value="6">6</button>
                                    <button class="btn btn-button small-btn calc" value="7">7</button>
                                    <button class="btn btn-button small-btn calc" value="8">8</button>
                                    <button class="btn btn-button small-btn calc" value="9">9</button>
                                    <button class="btn btn-button small-btn cancel-btn">C</button>
                                    <button class="btn btn-button small-btn calc">0</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="in-btn">
                                <button class="btn btn-button big-btn
                                        @if($in_time_check) record-success @endif" @if($in_time_check) disabled
                                        @endif id="in-btn">In
                                </button>
                                <button class="btn btn-button big-btn @if($out_time_check) record-success @endif"
                                        @if($out_time_check) disabled @endif id="out-btn">Out
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="employ-right col-md-4">
                    <div class="timer-section">
                        <div class="clock-asection">
                            <div id="clock">
                                <p class="unit" id="hours"></p><span>:</span>
                                <p class="unit" id="minutes"></p><span>:</span>
                                <p class="unit" id="seconds"></p>
                                <p class="unit" id="ampm"></p>
                            </div>
                            <p id="date"></p>
                        </div>
                    </div>
                    <div class="employ-right-btn">
                        <button class="btn btn-button color-1" id="start-break">Start Break
                        </button>
                        <button class="btn btn-button color-1" id="end-break">End Break
                        </button>
                        <button class="btn btn-button color-2">Correction</button>
                        <button class="btn btn-button color-3" id="exit">Exit</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('pos/libraries/datepicker/bootstrap-datepicker.js') }}"></script>

    <script>


        $(document).ready(function () {

            //Receives input from number button and displays on password input field

            $('.calc').on('click', function () {

                var button_value = $(this).val();
                var latestInput = $('#password').val();

                var currentValue = latestInput + button_value;

                $('#password').val(currentValue);

                console.log(latestInput);
            });


            $('#remove-cal-char').click(function () {

                var latestInputs = $('#password').val();
                var currentValues = latestInputs.slice(0, -1);
                $('#password').val(currentValues);

            });

            $('.cancel-btn').click(function () {
                $('#password').val('');

            });

            //--- ---//
            $('#exit').on('click', function () {
                window.location.href = '{{ url('/home') }}';

            });

            // Receives In Button Click and send request to server

            $('#in-btn').on('click', function () {
                var password = $('#password').val();
                var email = '{{ auth()->user()->email }}';
                var date = '{{ date('d.m.Y') }}';
                var entry_time = '{{ date('Y-m-d h:i:s') }}';
                var status = 'Working';
                var action = '{{ route('emp.in.time') }}';

                formdata = {
                    password: password,
                    email: email,
                    date: date,
                    entry_time: entry_time,
                    current_status: status
                };

                var request = $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: action,
                    data: formdata

                });

                request.done(function (data) {

                    $('#in-btn').addClass('record-success').prop('disabled', true);
                    $('#start-break').removeClass('record-success').prop('disabled', false);
                    $('#password').val('');
                    toastr.success('Your In Time Recorded Successfully, Start Right Way!', 'Success Alert', {timeOut: 5000});

                    var delay = 2000;
                    setTimeout(function () {
                        window.location = '{{ url('/home') }}';
                    }, delay);

                });

                request.fail(function (data) {
                    toastr.error('Your password did not matched', 'Error Alert', {timeOut: 5000});
                });
            });


            // Receives Out Button Click and send request to server

            $('#out-btn').on('click', function () {
                var password = $('#password').val();
                var email = '{{ auth()->user()->email }}';
                var date = '{{ date('d.m.Y') }}';
                var leave_time = '{{ date('Y-m-d h:i:s') }}';
                var status = 'Day Work Finished';
                var action = '{{ route('emp.out.time') }}';

                formdata = {
                    password: password,
                    email: email,
                    date: date,
                    leave_time: leave_time,
                    current_status: status
                };

                var request = $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: action,
                    data: formdata

                });

                request.done(function (data) {

                    $('#out-btn').addClass('record-success').prop('disabled', true);
                    $('#start-break').prop('disabled', true);
                    $('#end-break').prop('disabled', true);
                    $('#password').val('');
                    toastr.success('Your Out Time Recorded Successfully, Please Logout Now!', 'Success Alert', {timeOut: 5000});

                    var delay = 1500;
                    setTimeout(function () {
                        window.location = '{{ url('/logout') }}';
                    }, delay);
                });

                request.fail(function (e) {
                    toastr.error('Your password did not matched', 'Error Alert', {timeOut: 5000});
                });

            });


            // Receives Start Break Button Click

            $('#start-break').on('click', function () {
                var password = $('#password').val();
                var email = '{{ auth()->user()->email }}';
                var date = '{{ date('d.m.Y') }}';
                var start_break = '{{ date('Y-m-d h:i:s') }}';
                var status = 'On Break';
                var action = '{{ route('emp.start.break') }}';

                formdata = {
                    password: password,
                    email: email,
                    date: date,
                    start_break: start_break,
                    current_status: status
                };

                var request = $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: action,
                    data: formdata

                });

                request.done(function (data) {

                    $('#start-break').addClass('break-success').prop('disabled', true);
                    $('#end-break').removeClass('break-success').prop('disabled', false);
                    $('#password').val('');
                    toastr.success('Your break has been started!', 'Success Alert', {timeOut: 5000});

                });

                request.fail(function (data) {
                    toastr.error('Your password did not matched', 'Error Alert', {timeOut: 5000});
                });
            });


            $('#end-break').on('click', function () {
                var password = $('#password').val();
                var email = '{{ auth()->user()->email }}';
                var date = '{{ date('d.m.Y') }}';
                var end_break = '{{ date('Y-m-d h:i:s') }}';
                var status = 'Working';
                var action = '{{ route('emp.end.break') }}';

                formdata = {
                    password: password,
                    email: email,
                    date: date,
                    end_break: end_break,
                    current_status: status
                };

                var request = $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: action,
                    data: formdata

                });

                request.done(function (data) {

                    $('#end-break').addClass('break-success').prop('disabled', true);
                    $('#start-break').removeClass('break-success').prop('disabled', false);
                    $('#password').val('');

                    toastr.success('Welcome Back!', 'Success Alert', {timeOut: 5000});

                    var delay = 1500;
                    setTimeout(function () {
                        window.location = '{{ url('/home') }}';
                    }, delay);

                });

                request.fail(function (data) {
                    toastr.error('Your password did not matched', 'Error Alert', {timeOut: 5000});
                });
            });

        });
    </script>

@endsection