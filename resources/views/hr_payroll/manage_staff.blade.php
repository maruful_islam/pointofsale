@extends('admin_layouts.default')
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="user-list">
                        <div class="user-list-table table-responsive">
                            <h2 class="user-list-title">Staff List</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>phone</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Profile</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($staffs  as $key => $staff)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            <img src='{{asset($staff->image)}}' width="50px"
                                                 class="img-rounded"> &nbsp;{{ $staff->name }}
                                        </td>
                                        <td>{{ $staff->phone }}</td>
                                        <td>{{ $staff->getDesignationTitle($staff->designation_id) }}</td>
                                        <td>{{ $staff->getDepartmentName($staff->department_id) }}</td>
                                        <td>{{ $staff->getCountryName($staff->country_id) }}</td>
                                        <td>{{ $staff->city }}</td>
                                        <td><a href="{{ route('staff.profile',['id'=>$staff->id]) }}" class="btn btn-primary">View Profile</a></td>
                                        <td>
                                            @if($staff->status === "active")
                                                <a data-id="{{ $staff->id }}"
                                                   data-status="{{ $staff->status }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-color ">Active</a>
                                            @else
                                                <a data-id="{{ $staff->id }}"
                                                   data-status="{{ $staff->status }}" data-target="#statusUpdate"
                                                   data-toggle="modal" class="btn btn-primary pos-small-btn bg-red ">Inactive</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="user-action">
                                                <a href="{{ route('edit.staff', ['id'=>$staff->id]) }}"
                                                   class="user-edits" data-toggle="modal" title="Edit">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                                <a href="/res/delete-staff/{{$staff->id}}" class="user-removed"
                                                   data-toggle="tooltip" title="Delete"
                                                   onclick="return confirm('Are You Sure Delete This Staff Information?');"><span
                                                            class="fa fa-trash"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            @if(count($staffs) > 0)

                                <div class="alert alert-info">
                                    Showing {{ $staffs->firstItem() }} to {{ $staffs->lastItem() }} Staff of
                                    Total {{ $staffs->total() }} Staffs.
                                </div>

                                <nav aria-label="Page navigation">
                                    @if ($staffs->lastPage() > 1)
                                        <ul class="pagination pagination-sm">
                                            <li class="page-item {{ ($staffs->currentPage() == 1) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $staffs->url(1) }}">Previous</a>
                                            </li>
                                            @for ($i = 1; $i <= $staffs->lastPage(); $i++)
                                                <li class="page-item {{ ($staffs->currentPage() == $i) ? 'page active' : '' }}">
                                                    <a class="page-link" href="{{ $staffs->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="page-item {{ ($staffs->currentPage() == $staffs->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link"
                                                   href="{{ $staffs->url($staffs->currentPage()+1) }}">Next</a>
                                            </li>
                                        </ul>
                                    @endif
                                </nav>
                            @else
                                <div class="alert alert-danger">
                                    No Result Found!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <!-- Customer Edit Pop Up -->


    <!-- Customer Status Update -->

    <!-- Modal -->
    <div id="statusUpdate" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Do you really want to <b id="status-msg"></b> this staff's information? </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" id="statusConfirmation">Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <script>
        $('#statusUpdate').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var status = $(e.relatedTarget).data('status');

            if (status === 'active') {
                $('#status-msg').text('Deactivate');
            }
            else {
                $('#status-msg').text('Activate');
            }

            $('a#statusConfirmation').attr('href', '{{ url('/res/update-staff-status')}}/' + id);


        });

    </script>

@endsection