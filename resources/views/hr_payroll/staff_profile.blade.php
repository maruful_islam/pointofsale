@extends('admin_layouts.default')
@section('content')

<div class="user-info-section">
    <div class="container">
        <div class="u-info-inner">
            <div class="user-info-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-9 col-lg-9">
                        <div class="user-b-info">
                            <div class="u-info-title">
                                <h2>Staff Basic Information</h2>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="u-b-info-left">
                                        <ul>
                                            <li> <strong>Name :</strong><span>{{ $info->name }}</span></li>
                                            <li><strong>Email :</strong><span>{{ $info->email }}</span></li>
                                            <li><strong>Phone :</strong><span>{{ $info->phone }}</span></li>
                                            <li><strong>Blood Group :</strong><span>{{ $info->blood_group }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="u-b-info-right">
                                        <ul>
                                            <li><strong>Gender :</strong><span>{{ $info->gender }}</span></li>
                                            <li><strong>Date of Birth :</strong><span>{{ $info->d_o_b }}</span></li>
                                            <li><strong>Employee ID  :</strong><span>{{ $info->employee_id }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="user-info-img">
                            <img src="{{ asset($info->image) }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-info-medil">
                <div class="u-info-title">
                    <h2>Parent  & Address</h2>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="user-b-info">
                            <div class="u-b-info-left">
                                <ul>
                                    <li> <strong>Father's Name :</strong><span>{{ $info->f_name }}</span></li>
                                    <li><strong>Mother's Name :</strong><span>{{ $info->m_name }}</span></li>
                                    <li><strong>Department :</strong><span>{{ $info->getDepartmentName($info->department_id) }}</span></li>
                                    <li><strong>Designations :</strong><span>{{ $info->getDesignationTitle($info->designation_id)  }}</span></li>
                                    <li> <strong>Job Type :</strong><span>{{ $info->job_type }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="user-b-info">
                            <div class="user-b-info">
                                <div class="u-b-info-left">
                                    <ul>
                                        <li><strong>Country :</strong><span>{{ $info->getCountryName($info->country_id) }}</span></li>
                                        <li><strong>City :</strong><span>{{ $info->city }}</span></li>
                                        <li><strong>Address :</strong><span>{{ $info->address }}</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="user-info-downloads text-center">
                    <a class="btn btn-button" target="_blank" href="{{ asset($info->cv) }}">CV</a>
                    <a class="btn btn-button" data-target="#id-card" data-toggle="modal">ID Card</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')

    <!-- Modal -->
    <div id="id-card" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ID Card</h4>
                </div>
                <div class="modal-body">
                    <img src="{{ asset($info->id_card) }}" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection