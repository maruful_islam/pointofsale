@extends('admin_layouts.default')

@section('css')
    <style>
        label#password-error {
            width: 100%;
            display: block;
        }

        .form-group.new-pass .error.valid {
            position: absolute;
            top: 39px;
            right: -4px;
            float: right;
            text-align: right;
        }

        label#password-error {
            width: 100%;
            display: block;
        }

        .form-group.new-pass {
            position: relative;
        }

        label.error {
            padding-left: 23px !important;
        }

        label#password_confirmation-error, #current_password-error {
            display: block;
            width: 100%;
            margin-bottom: 20px;
        }
    </style>
@endsection
@section('content')
    <div class="container page-padding-top">
        <div class="user-list-boxarea">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <form action="{{ route('update.password') }}" method="post" class="validatedForm">

                        {{csrf_field()}}

                        <div class="user-inputbox">

                            <h2 class="user-list-title">Update Password</h2>


                            <div class="form-group new-pass {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="usr">Current Password:</label>
                                <input type="password" name="current_password" id="current_password"
                                       class="form-control">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group new-pass">
                                <label class="user-s-paa" for="usr">New Password:</label>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>

                            <div class="form-group form-group new-pass {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="user-s-pss" for="usr">Confirm Password:</label>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                       class="form-control">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <button class="btn-submit btn-primary" id="password-submit" type="submit">Update</button>
                            </div>

                        </div>

                    </form>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <form action="{{ route('update.profile') }}" method="post">

                        {{csrf_field()}}

                        <div class="user-inputbox">
                            <h2 class="user-list-title">Update Profile</h2>


                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" name="name" value="{{ $info->name }}" class="form-control">

                            </div>


                            <div class="form-group">
                                <label for="pwd">Phone:</label>
                                <input type="text" name="phone" value="{{ $info->phone }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Blood Group:</label>
                                <select class="form-control custom-select-value selectpicker"
                                        data-live-search="true" name="blood_group">
                                    <option>Select Blood Group</option>
                                    <option value="A+" @if($info->blood_group == 'A+') selected @endif>A+</option>
                                    <option value="A-" @if($info->blood_group == 'A-') selected @endif>A-</option>
                                    <option value="B+" @if($info->blood_group == 'B+') selected @endif>B+</option>
                                    <option value="B-" @if($info->blood_group == 'B-') selected @endif>B-</option>
                                    <option value="AB+" @if($info->blood_group == 'AB+') selected @endif>AB+</option>
                                    <option value="AB-" @if($info->blood_group == 'AB-') selected @endif>AB-</option>
                                    <option value="O+" @if($info->blood_group == 'O+') selected @endif>O+</option>
                                    <option value="O-" @if($info->blood_group == 'O-') selected @endif>O-</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="pwd">Father's Name:</label>
                                <input type="text" name="f_name" value="{{ $info->f_name }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Mother's Name:</label>
                                <input type="text" name="m_name" value="{{ $info->m_name }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Select Date of Birth:</label>
                                <div class="input-group date col-md-12" data-date-format="dd.mm.yyyy">
                                    <input type="text" name="d_o_b" class="form-control" value="{{ $info->d_o_b }}"
                                           placeholder="DD.MM.YYYY">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="pwd">City:</label>
                                <input type="text" name="city" value="{{ $info->city }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="pwd">Address:</label>
                                <textarea name="address" class="form-control">{{ $info->address }}</textarea>
                            </div>


                            <div class="form-group">
                                <button class="btn-submit btn-primary" type="submit">Save</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>



    <script>
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $(".validatedForm").validate({
            rules: {
                current_password: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password'
                }
            },
            messages: {
                'current_password': {
                    required: 'Please enter your current password'
                },
                'password': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                },
                'password_confirmation': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above'
                }
            },

            submitHandler: function (form) {
                if ($(form).valid())
                    form.submit();

                return false; // prevent normal form posting
            }
        });

    </script>
@endsection