@extends('admin_layouts.default')
@section('content')

    <div class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">
                            <div class="kcol-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="prport-tatepiker table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Date: <input type="text" name="start_date" class="span2"
                                                             value="@if(isset($date)){{ $date }}@endif" id="dpd1"
                                                             data-date-format="dd.mm.yyyy"></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="report-search text-right">

                                    <select class="re-monthly" name="emp_id" data-role="sorter">
                                        <option selected="selected">Select Employee</option>
                                        @foreach($staffs as $staff)
                                            <option value="{{ $staff->id }}"

                                                    @if($emp_id == $staff->id)
                                                    selected
                                                    @endif
                                            >{{ $staff->name }}</option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-button" type="submit">Search</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="report-table2 table-responsive">
                @if(count($break_logs) > 0)
                    <!--Table-->
                        <table class="table">

                            <!--Table head-->
                            <thead class="blue-grey lighten-4">

                            <tr style="background: #fff">
                                <th colspan="4" class="text-center" style="border: #fff;">

                                    <div class="alert alert-info">
                                        <h4>Staff Name: {{ $name }}</h4>
                                    </div>
                                    <div class="alert alert-warning">
                                        <h5>Total Break Taken: {{ count($break_logs) }}</h5>
                                    </div>
                                    <div class="alert alert-danger">
                                        <h5>Total Break Time: {{ $total_break }} Minutes</h5>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th class="text-center">Break Started</th>
                                <th class="text-center">Break Ended</th>
                                <th class="text-center">Total Time</th>
                            </tr>
                            </thead>
                            <!--Table head-->

                            <!--Table body-->
                            <tbody>
                            @foreach($break_logs as $key=>$break_log)
                                <tr>

                                    <th scope="row" class="text-center">{{ $key+1 }}</th>

                                    <td class="text-center">{{ date('h:i A', strtotime($break_log->start_break)) }}</td>

                                    <td class="text-center">{{ date('h:i A', strtotime($break_log->end_break)) }}</td>

                                    <td class="text-center">
                                        @php
                                            $to = strtotime($break_log->end_break);
                                            $from = strtotime($break_log->start_break);

                                            echo round(abs($to - $from) / 60,2). " minutes";
                                        @endphp

                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                            <!--Table body-->
                        </table>

                    @else
                        <div class="alert alert-success">
                            No Break Taken!
                        </div>
                @endif
                <!--Table-->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});
    </script>


@endsection