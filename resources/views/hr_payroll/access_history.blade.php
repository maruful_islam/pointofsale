@extends('admin_layouts.default')
@section('content')

    <div class="container">
        <div class="report-page2">
            <div class="container">
                <div class="report-header">
                    <div class="row">
                        <form action="" method="get">
                            <div class="kcol-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="prport-tatepiker table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Form Date: <input required="" type="text" name="start_date" class="span2"
                                                                  value="@if(isset($date1)){{ $date1 }}@endif" id="dpd1"
                                                                  data-date-format="dd.mm.yyyy"></th>
                                            <th>To Date: <input required="" type="text" name="end_date" class="span2"
                                                                value="@if(isset($date2)){{ $date2 }}@endif" id="dpd2"
                                                                data-date-format="dd.mm.yyyy"></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="kcol-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="report-search text-right">
                                    <select class="re-monthly" name="staff_type" data-role="sorter" id="staff-filter">
                                        <option selected="selected" value="1">All Staff</option>
                                        <option value="2" @if(isset($staff_type)) selected @endif>Attendance by Staff
                                        </option>
                                    </select>

                                    <select class="re-monthly" name="emp_id" data-role="sorter"
                                            style="display: @if(isset($display)) {{ $display }}@else none @endif"
                                            id="staff-list">
                                        <option selected="selected">Select Employee</option>
                                        @foreach($staffs as $staff)
                                            <option value="{{ $staff->id }}"
                                                    @if(isset($display))
                                                    @if($emp_id == $staff->id)
                                                    selected
                                                    @endif
                                                    @endif
                                            >{{ $staff->name }}</option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-button" type="submit">Search</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="report-table2 table-responsive">
                    <!--Table-->
                    <table class="table">

                        <!--Table head-->
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th>#</th>
                            <th class="text-center" width="10%">Name</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Entry Time</th>
                            <th class="text-center">Leave Time</th>
                            <th class="text-center">Break Log</th>
                            <th class="text-center">Total Break Time</th>
                            <th class="text-center">Current Status</th>
                        </tr>
                        </thead>
                        <!--Table head-->

                        <!--Table body-->
                        <tbody>
                        @foreach($histories as $key=>$history)
                            <tr>

                                <th scope="row" class="text-center">{{ $key+1 }}</th>

                                <td class="text-center">{{ $history->getStaffName($history->emp_id) }}</td>

                                <td class="text-center">{{ date('l, dS F Y', strtotime($history->date)) }}</td>

                                <td class="text-center">
                                    @if($history->entry_time == NULL)
                                        <div class="label label-info">
                                            Have Not Checked out!
                                        </div>
                                    @else
                                        {{ date('h:i A', strtotime($history->entry_time)) }}
                                    @endif

                                </td>

                                <td class="text-center">
                                    @if($history->leave_time == NULL)
                                        <div class="label label-info">
                                            Have Not Checked out!
                                        </div>
                                    @else
                                        {{ date('h:i A', strtotime($history->leave_time)) }}
                                    @endif
                                </td>

                                <td class="text-center">
                                    <a class="btn btn-info btn-sm" href="{{ route('break.log',['date'=>$history->date, 'emp_id'=>$history->emp_id]) }}">View Break Log</a>
                                </td>

                                <td class="text-center">
                                    @php
                                    $break = new \App\BreakLog();
                                    echo $break->GetTotalBreakTime($history->date, $history->emp_id).' Minutes';
                                    @endphp
                                </td>

                                <td class="text-center">

                                    @if($history->current_status === "Working")
                                        <a class="btn btn-primary pos-small-btn bg-color ">{{ $history->current_status }}</a>
                                    @elseif($history->current_status === "Day Work Finished")
                                        <a class="btn btn-primary pos-small-btn bg-red ">{{ $history->current_status }}</a>
                                    @else
                                        <a class="btn btn-primary pos-small-btn ">{{ $history->current_status }}</a>
                                    @endif

                                </td>


                            </tr>
                        @endforeach

                        </tbody>
                        <!--Table body-->
                    </table>

                    @if(count($histories) > 0)

                        <div class="alert alert-info">
                            Showing {{ $histories->firstItem() }} to {{ $histories->lastItem() }} Staff of
                            Total {{ $histories->total() }} Staffs.
                        </div>

                        <nav aria-label="Page navigation">
                            @if ($histories->lastPage() > 1)
                                <ul class="pagination pagination-sm">
                                    <li class="page-item {{ ($histories->currentPage() == 1) ? ' disabled' : '' }}">
                                        <a class="page-link" href="{{ $histories->url(1) }}">Previous</a>
                                    </li>
                                    @for ($i = 1; $i <= $histories->lastPage(); $i++)
                                        <li class="page-item {{ ($histories->currentPage() == $i) ? 'page active' : '' }}">
                                            <a class="page-link" href="{{ $histories->url($i) }}">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    <li class="page-item {{ ($histories->currentPage() == $histories->lastPage()) ? ' disabled' : '' }}">
                                        <a class="page-link"
                                           href="{{ $histories->url($histories->currentPage()+1) }}">Next</a>
                                    </li>
                                </ul>
                            @endif
                        </nav>
                    @else
                        <div class="alert alert-danger">
                            No Result Found!
                        </div>
                @endif
                <!--Table-->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.input-group.date').datepicker({format: "dd.mm.yyyy"});
    </script>


    <script>
        $(document).ready(function () {
            $('#staff-filter').on('change', function () {
                var staff_type = $(this).val();

                if (staff_type == 1) {
                    $('#staff-list').css('display', 'none');
                }
                else {
                    $('#staff-list').css('display', 'inline-block');
                }

            });
        });
    </script>

@endsection