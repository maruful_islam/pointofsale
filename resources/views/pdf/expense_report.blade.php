<!DOCTYPE html>
<html lang="en">
<head>
    <title>POS Report</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
        }

        .container-table100 {
            width: 100%;
            min-height: 100vh;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .wrap-table100 {
            width: 100%;
        }

        table {
            border-spacing: 1px;
            border-collapse: collapse;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }

        table * {
            position: relative;
        }

        table td, table th {
            padding-left: 8px;
        }

        table thead tr {
            height: 60px;
            background: #36304a;
        }

        table tbody tr {
            height: 50px;
        }

        table tbody tr:last-child {
            border: 0;
        }

        /*table td, table th {*/
        /*text-align: left;*/
        /*}*/

        /*table td.l, table th.l {*/
        /*text-align: right;*/
        /*}*/

        /*table td.c, table th.c {*/
        /*text-align: center;*/
        /*}*/

        /*table td.r, table th.r {*/
        /*text-align: center;*/
        /*}*/

        .table100-head th {
            font-family: OpenSans-Regular;
            font-size: 18px;
            color: #fff;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:nth-child(even) {
            background-color: #f5f5f5;
        }

        tbody tr {
            font-family: OpenSans-Regular;
            font-size: 15px;
            color: #808080;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:hover {
            color: #555555;
            background-color: #f5f5f5;
            cursor: pointer;
        }

        .column0 {
            width: 10px;
        }

        .column1 {
            width: 80px;
        }

        .column2 {
            width: 200px;
        }

        .column3 {
            width: 70px;
        }

        .column4 {
            width: 240px;
            text-align: center;
        }

        .column5 {
            width: 140px;
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="limiter">
    <div class="text-center" style="padding-top: 20px; padding-bottom: 20px">
        <h2 style="text-align: center">Expense Report</h2><br>
        <h4 style="text-align: center">{{ strtoupper(config('app.name'))}}</h4><br>
        @if(isset($date1))
            <h5 style="text-align: center">Report From: <b>{{ date('l, dS F Y', strtotime($date1)) }} </b>
                to <b>{{ date('l, dS F Y', strtotime($date2)) }}</b></h5>
        @else
            <h5 style="text-align: center">Date: {{ date('l, dS F Y') }} </h5>
        @endif
    </div>


    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100">

                <table>
                    <thead>

                    <tr class="table100-head">
                        <th class="column0">#</th>
                        <th class="column1">Category</th>
                        <th class="column2">Title</th>
                        <th class="column3">Amount</th>
                        <th class="column4">Remark</th>
                        <th class="column5">Date</th>
                    </tr>

                    </thead>

                    <tbody>
                    @if(!isset($category_id))
                        <tr>
                            <th scope="row" class="column0">1</th>

                            <th scope="row" class="column1"> Purchase</th>

                            @if(isset($_GET['start_date']))
                                <td class="column2">
                                    Total Purchase Cost From {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                                    to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                                </td>
                            @else
                                <td class="column2">
                                    Total Purchase Cost Till {{ date('d.M.Y') }}
                                </td>
                            @endif

                            <td class="column3">
                                {{ $purchase_cost }}  {{ config('app.currency')}}
                            </td>

                            <td class="column4">
                                -
                            </td>

                            <td class="column5">
                                {{ date('d.M.Y') }}
                            </td>

                        </tr>
                    @endif

                    @if(isset($reports))

                        @foreach($reports as $key=>$report)

                            <tr>
                                @if(!isset($category_id))
                                    <th scope="row" class="column0">{{ $key+2 }}</th>
                                @else
                                    <th scope="row" class="column0">{{ $key+1 }}</th>
                                @endif
                                <th scope="row" class="column1">{{ $report->expense_category->name }}</th>

                                <td class="column2">
                                    {{ $report->title }}
                                </td>

                                <td class="column3">
                                    {{ $report->amount }}  {{ config('app.currency')}}
                                </td>

                                <td class="column4">
                                    {{ $report->remarks }}
                                </td>

                                <td class="column5">
                                    {{ date('d.M.Y', strtotime($report->date)) }}
                                </td>

                            </tr>

                        @endforeach
                    @endif


                    <tr>
                        <td colspan="6">

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right" style="font-weight: bold;">Total Expense:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            @php echo number_format($total_expense, 3). ' '.config('app.currency'); @endphp
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


</body>
</html>