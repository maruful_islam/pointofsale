<!DOCTYPE html>
<html lang="en">
<head>
    <title>POS Report</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
        }

        .container-table100 {
            width: 100%;
            min-height: 100vh;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .wrap-table100 {
            width: 100%;
        }

        table {
            border-spacing: 1px;
            border-collapse: collapse;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }

        table * {
            position: relative;
        }

        table td, table th {
            padding-left: 8px;
        }

        table thead tr {
            height: 60px;
            background: #36304a;
        }

        table tbody tr {
            height: 50px;
        }

        table tbody tr:last-child {
            border: 0;
        }

        /*table td, table th {*/
        /*text-align: left;*/
        /*}*/

        /*table td.l, table th.l {*/
        /*text-align: right;*/
        /*}*/

        /*table td.c, table th.c {*/
        /*text-align: center;*/
        /*}*/

        /*table td.r, table th.r {*/
        /*text-align: center;*/
        /*}*/

        .table100-head th {
            font-family: OpenSans-Regular;
            font-size: 18px;
            color: #fff;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:nth-child(even) {
            background-color: #f5f5f5;
        }

        tbody tr {
            font-family: OpenSans-Regular;
            font-size: 15px;
            color: #808080;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:hover {
            color: #555555;
            background-color: #f5f5f5;
            cursor: pointer;
        }

        .column0 {
            width: 10px;
        }

        .column1 {
            width: 50px;
        }

        .column2 {
            width: 80px;
        }

        .column3 {
            width: 70px;
        }

        .column4 {
            width: 40px;
            text-align: center;
        }

        .column5 {
            width: 40px;
            text-align: center;
        }

        .column6 {
            width: 40px;
            text-align: center;
        }

        .column7 {
            width: 30px;
            text-align: center;
        }

        .column8 {
            width: 60px;
            text-align: center;
        }

        .column9 {
            width: 70px;
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="limiter">
    <div class="text-center" style="padding-top: 20px; padding-bottom: 20px">
        <h2 style="text-align: center">Orders Report</h2><br>
        <h4 style="text-align: center">{{ strtoupper(config('app.name'))}}</h4><br>
        @if(isset($date1))
            <h5 style="text-align: center">Report From: <b>{{ date('l, dS F Y', strtotime($date1)) }} </b>
                to <b>{{ date('l, dS F Y', strtotime($date2)) }}</b></h5>
        @else
            <h5 style="text-align: center">Date: {{ date('l, dS F Y') }} </h5>
        @endif
    </div>


    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100">

                <table>
                    <thead>

                    <tr class="table100-head">
                        <th class="column0">#</th>
                        <th class="column1">CHK</th>
                        <th class="column2">Order Type</th>
                        <th class="text-center">Order Items</th>
                        <th class="text-center">Table Name</th>
                        <th class="column3">Payment Method</th>
                        <th class="column4">Total</th>
                        <th class="column5">Tips</th>
                        <th class="column5">Delivery</th>
                        <th class="column6">Discount</th>
                        <th class="column7">CMP Discount</th>
                        <th class="column8">Grand Total</th>
                        <th class="column8">Paid</th>
                        <th class="column9">Time</th>
                    </tr>

                    </thead>
                    
                    <tbody>

                    @foreach($reports as $key=>$report)
                        <tr>
                            <td class="column0">{{ $key+1 }}</td>
                            <td class="column1">{{ $report->csk_id }}</td>
                            <td class="column2">{{ $report->GetOrderType($report->order_type_id) }}</td>
                            <td>
                                @php
                                    $products = $report->GetOrderItems($report->csk_id);

                                @endphp

                                @foreach ($products as $product)
                                    @php
                                        echo $product->p_name.'('.$product->qty.'),';
                                    @endphp
                                @endforeach
                            </td>

                            <td class="text-center">
                                @php $name = $report->GetTableName($report->table_id);
                                            $table = ($name == NULL) ? 'N/A' : $name;
                                            echo $table;
                                @endphp
                            </td>
                            <td class="column3">
                                @php
                                    if ($report->method_id){
                                        echo $report->GetMethodName($report->method_id);
                                    }else{
                                        echo 'No Info Found!';
                                    }
                                @endphp
                            </td>
                            <td class="column4">
                                {{ $report->total }} {{ config('app.currency')}}
                            </td>
                            <td class="column5">
                                @php $tips = ($report->tips == NULL) ? 0 : $report->tips; echo $tips; @endphp
                                {{ config('app.currency')}}
                            </td>
                            <td class="column5">
                                {{ $report->delivery or '0.000' }}
                                {{ config('app.currency')}}
                            </td>
                            <td class="column6">
                                @php $discount = ($report->discount == NULL) ? 0 : $report->discount; echo $discount; @endphp
                                {{ config('app.currency')}}
                            </td>
                            <td class="column7">
                                @if($report->cmp == 1)
                                    <div class="label label-warning">Yes</div>
                                @else
                                    <div class="label label-info">No</div>
                                @endif
                            </td>
                            <td class="column8">
                                {{ $report->total + $report->tips + $report->delivery }} {{ config('app.currency')}}
                            </td>
                            <td class="column8">
                                {{ $report->paid }} {{ config('app.currency')}}
                            </td>

                            <td class="column9">
                                {{ date('d.M.Y h:i A', strtotime($report->updated_at)) }}
                            </td>

                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="11"></td>
                        <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Restaurant Sales</u></b></td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Cash Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                           {{ number_format(($reports->where('method_id',1)->sum('sub_total') + $reports->where('method_id',1)->sum('tips') + $reports->where('method_id',1)->sum('delivery')) - ($reports->where('method_id',1)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Credit Card Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->whereIn('method_id',[3,4,5])->sum('sub_total') + $reports->whereIn('method_id', [3,4,5])->sum('tips') + $reports->whereIn('method_id', [3,4,5])->sum('delivery'))-($reports->whereIn('method_id',[3,4,5])->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">K-Net Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',6)->sum('sub_total') + $reports->where('method_id',6)->sum('tips') + $reports->where('method_id',6)->sum('delivery')) - ($reports->where('method_id',6)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Ledger Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{-- number_format($sheeel_total, 3).' KD'--}} 0.000 KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11"></td>
                        <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Makan Sales</u></b></td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Makan Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('order_type_id',5)->sum('sub_total') + $reports->where('order_type_id',5)->sum('tips') + $reports->where('order_type_id',5)->sum('delivery')) - ($reports->where('order_type_id',5)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Cash Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                           {{ number_format(($reports->where('method_id',12)->sum('sub_total') + $reports->where('method_id',12)->sum('tips') + $reports->where('method_id',12)->sum('delivery')) - ($reports->where('method_id',12)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Knet Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',9)->sum('sub_total') + $reports->where('method_id',9)->sum('tips') + $reports->where('method_id',9)->sum('delivery')) - ($reports->where('method_id',9)->sum('discount')),3) }} KD
                        </td>
                    </tr>
                    

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Makan-Credit Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',13)->sum('sub_total') + $reports->where('method_id',13)->sum('tips') + $reports->where('method_id',13)->sum('delivery')) - ($reports->where('method_id',13)->sum('discount')),3) }} KD
                        </td>
                    </tr>
                    
                    <!-- Deliveroo Start -->
                                 <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Deliveroo Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                         {{ number_format(($reports->where('order_type_id',7)->sum('sub_total') + $reports->where('order_type_id',7)->sum('tips') + $reports->where('order_type_id',7)->sum('delivery')) - ($reports->where('order_type_id',7)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style ="font-weight: bold;">Deliveroo-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',15)->sum('sub_total') + $reports->where('method_id',15)->sum('tips') + $reports->where('method_id',15)->sum('delivery')) - ($reports->where('method_id',15)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',16)->sum('sub_total') + $reports->where('method_id',16)->sum('tips') + $reports->where('method_id',16)->sum('delivery')) - ($reports->where('method_id',16)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Deliveroo-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',17)->sum('sub_total') + $reports->where('method_id',17)->sum('tips') + $reports->where('method_id',17)->sum('delivery')) - ($reports->where('method_id',17)->sum('discount')),3) }} KD
                                    </td>
                                </tr>
                                <!-- Deliveroo End -->
                                
                                <!-- Carriage Start -->
                                 <tr>
                                    <td colspan="11"></td>
                                    <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Carriage Sales</u></b></td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                         {{ number_format(($reports->where('order_type_id',8)->sum('sub_total') + $reports->where('order_type_id',8)->sum('tips') + $reports->where('order_type_id',8)->sum('delivery')) - ($reports->where('order_type_id',8)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style ="font-weight: bold;">Carriage-Cash Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',18)->sum('sub_total') + $reports->where('method_id',18)->sum('tips') + $reports->where('method_id',18)->sum('delivery')) - ($reports->where('method_id',18)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage-Knet Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',19)->sum('sub_total') + $reports->where('method_id',19)->sum('tips') + $reports->where('method_id',19)->sum('delivery')) - ($reports->where('method_id',19)->sum('discount')),3) }} KD
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="11" class="text-right" style="font-weight: bold;">Carriage-Credit Sales:
                                    </td>
                                    <td colspan="3" style="font-weight: bold;" class="text-center">
                                        {{ number_format(($reports->where('method_id',20)->sum('sub_total') + $reports->where('method_id',20)->sum('tips') + $reports->where('method_id',20)->sum('delivery')) - ($reports->where('method_id',20)->sum('discount')),3) }} KD
                                    </td>
                                </tr>
                                <!-- Carriage End -->
                                

                    <tr>
                        <td colspan="11"></td>
                        <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Talabat Sales</u></b></td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Talabat Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('order_type_id',6)->sum('sub_total') + $reports->where('order_type_id',6)->sum('tips') + $reports->where('order_type_id',6)->sum('delivery')) - ($reports->where('order_type_id',6)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style ="font-weight: bold;">Talabat-Cash Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',10)->sum('sub_total') + $reports->where('method_id',10)->sum('tips') + $reports->where('method_id',10)->sum('delivery')) - ($reports->where('method_id',10)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Talabat-Knet Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',8)->sum('sub_total') + $reports->where('method_id',8)->sum('tips') + $reports->where('method_id',8)->sum('delivery')) - ($reports->where('method_id',8)->sum('discount')),3) }} KD
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Talabat-Credit Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->where('method_id',13)->sum('sub_total') + $reports->where('method_id',13)->sum('tips') + $reports->where('method_id',13)->sum('delivery')) - ($reports->where('method_id',13)->sum('discount')),3) }} KD
                        </td>
                    </tr>

                    <tr>
                        <td colspan="11"></td>
                        <td colspan="3" style="text-align: center"> <b style="color: #ff403c"><u>Total Sales Information</u></b></td>
                    </tr>

                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Gross Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->sum('sub_total') + $reports->sum('tips') + $reports->sum('delivery')),3) }} KD
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Total Complementary
                            Discount:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            @php echo number_format($reports->where('cmp', 1)->sum('discount'), 3).' KD'; @endphp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Total Discount:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            @php echo number_format($reports->sum('discount'), 3).' KD'; @endphp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Total Tips:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            @php echo number_format($reports->sum('tips'), 3).' KD'; @endphp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Total Delivery:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            @php echo number_format($reports->sum('delivery'), 3).' KD'; @endphp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11" class="text-right" style="font-weight: bold;">Net Sales:
                        </td>
                        <td colspan="3" style="font-weight: bold;" class="text-center">
                            {{ number_format(($reports->sum('total') + $reports->sum('tips') + $reports->sum('delivery')),3) }} KD
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


</body>
</html>