<!DOCTYPE html>
<html lang="en">
<head>
    <title>POS Report</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
        }

        .container-table100 {
            width: 100%;
            min-height: 100vh;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .container-table100 {
            width: 100%;
            min-height: 100px;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .container-table200 {
            width: 100%;
            min-height: 100px;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .container-table300 {
            width: 100%;
            min-height: 100px;
            background: #c850c0;
            background: -webkit-linear-gradient(45deg, #4158d0, #c850c0);
            background: -o-linear-gradient(45deg, #4158d0, #c850c0);
            background: -moz-linear-gradient(45deg, #4158d0, #c850c0);
            background: linear-gradient(45deg, #4158d0, #c850c0);

            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            /*padding: 33px 30px;*/
        }

        .wrap-table100 {
            width: 100%;
        }

        table {
            border-spacing: 1px;
            border-collapse: collapse;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }

        table * {
            position: relative;
        }

        table td, table th {
            padding-left: 8px;
        }

        table thead tr {
            height: 60px;
            background: #36304a;
        }

        table tbody tr {
            height: 50px;
        }

        table tbody tr:last-child {
            border: 0;
        }

        /*table td, table th {*/
        /*text-align: left;*/
        /*}*/

        /*table td.l, table th.l {*/
        /*text-align: right;*/
        /*}*/

        /*table td.c, table th.c {*/
        /*text-align: center;*/
        /*}*/

        /*table td.r, table th.r {*/
        /*text-align: center;*/
        /*}*/

        .table100-head th {
            font-family: OpenSans-Regular;
            font-size: 18px;
            color: #fff;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:nth-child(even) {
            background-color: #f5f5f5;
        }

        tbody tr {
            font-family: OpenSans-Regular;
            font-size: 15px;
            color: #808080;
            line-height: 1.2;
            font-weight: unset;
        }

        tbody tr:hover {
            color: #555555;
            background-color: #f5f5f5;
            cursor: pointer;
        }

        .column0 {
            width: 10px;
        }

        .column1 {
            width: 80px;
        }

        .column2 {
            width: 200px;
        }

        .column3 {
            width: 70px;
        }

        .column4 {
            width: 240px;
            text-align: center;
        }

        .column5 {
            width: 140px;
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="limiter">
    <div class="text-center" style="padding-top: 20px; padding-bottom: 20px">
        <h2 style="text-align: center">Profit Loss Report</h2><br>
        <h4 style="text-align: center">{{ strtoupper(config('app.name'))}}</h4><br>
        @if(isset($date1))
            <h5 style="text-align: center">Report From: <b>{{ date('l, dS F Y', strtotime($date1)) }} </b>
                to <b>{{ date('l, dS F Y', strtotime($date2)) }}</b></h5>
        @else
            <h5 style="text-align: center">Date: {{ date('l, dS F Y') }} </h5>
        @endif
    </div>


    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100">

                <h3 style="text-align: center; margin-bottom: 20px; margin-top:20px;">Income Report</h3>

                <table>
                    <thead>

                    <tr class="table100-head">
                        <th class="column3">Total Order</th>
                        <th class="column4">Total Payment Received</th>
                        <th class="column5">Date Range</th>
                    </tr>

                    </thead>

                    <tbody>

                    <tr>

                        <td class="column5">
                            {{ $total_order }}
                        </td>

                        <td class="column4">
                            {{ $total_income }}  {{ config('app.currency')}}
                        </td>

                        <td class="column5">
                            {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                            to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="container-table200">
        <div class="wrap-table100">
            <div class="table100">

                <h3 style="text-align: center; margin-bottom: 20px; margin-top:20px;">Expense Report</h3>

                <table>
                    <thead>

                    <tr class="table100-head">
                        <th class="column3">Expense Type</th>
                        <th class="column4">Total Payment</th>
                        <th class="column5">Date Range</th>
                    </tr>

                    </thead>

                    <tbody>


                    <tr>
                        <th class="column5">Purchase Expense</th>

                        <td class="column4">
                            {{ $purchase_expenses }}  {{ config('app.currency')}}
                        </td>

                        <td class="column5">
                            {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                            to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                        </td>

                    </tr>

                    <tr>
                        <th class="column5">Other Expense</th>

                        <td class="column4">
                            {{ $other_expenses }}  {{ config('app.currency')}}
                        </td>

                        <td class="column5">
                            {{ date('d.M.Y', strtotime($_GET['start_date'])) }}
                            to {{ date('d.M.Y', strtotime($_GET['end_date'])) }}
                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="container-table300">
        <div class="wrap-table100">
            <div class="table100">

                <h3 style="text-align: center; margin-bottom: 20px; margin-top:20px;">Final Statement</h3>

                <table>
                    <thead >
                    <tr>
                        <th style="background: #36304a; color: #FFFFFF" class="column4">Total Income</th>
                        <th class="column4"
                            style="background: #fff">{{ $total_income }}  {{ config('app.currency')}}</th>

                    </tr>

                    <tr>
                        <th style="background: #36304a; color: #FFFFFF" class="column4">Total Expense</th>
                        <th class="column4"
                            style="background: #fff">{{ $total_expense }}  {{ config('app.currency')}}</th>

                    </tr>

                    @php $calculation = $total_income - $total_expense; @endphp
                    <tr>
                        @if($calculation > 0)
                            <th style="background: #36304a; color: #FFFFFF" class="column4">Total Profit</th>
                        @else
                            <th style="background: #36304a; color: #FFFFFF" class="column4">Total Loss</th>
                        @endif
                        <th class="column4"
                            style="background: #fff">{{ $calculation }}  {{ config('app.currency')}}</th>

                    </tr>

                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>


</body>
</html>